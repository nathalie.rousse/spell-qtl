#ifndef _XML_CPP_BASE_H_
#define _XML_CPP_BASE_H_

#include <iostream>

#include <expat.h>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <typeinfo>
#include <algorithm>

#define BUFF_SIZE 1024


namespace XML {
struct Editor_Base;
struct Element_Base;
struct Constraint_Base;

template <typename BoundStruc> struct Element;
template <typename X> struct Element;

template <typename X> X convert(std::string data) {
    X ret;
    std::istringstream iss(data);
    iss >> ret;
    return ret;
}

template <> std::string convert<std::string>(std::string data) {
    return data;
}

struct Operation_Base {
    virtual void operator() (Editor_Base*ed, std::string& data) = 0;
};

template <typename V>
struct _del_ptr {
    void operator () (std::pair<const std::string, V*>& x) { delete x.second; }
};

template <typename V>
struct Pointer_Map : public std::map<std::string, V*> {
    using typename std::map<std::string, V*>::value_type;
    using std::map<std::string, V*>::begin;
    using std::map<std::string, V*>::end;
    using std::map<std::string, V*>::insert;
    using std::map<std::string, V*>::find;
    virtual ~Pointer_Map()
    {
        std::for_each(begin(), end(), _del_ptr<V>());
    }
};



typedef Pointer_Map<Element_Base> Element_Map;
typedef Pointer_Map<Operation_Base> Operation_Map;
typedef std::map<std::string, bool> Bool_Map;

typedef Operation_Map::value_type Op;

typedef std::vector<Editor_Base*> Editor_Stack;


struct Operation_Element_Base {
    Element_Base* sub_elt;
    Operation_Element_Base(Element_Base* se) : sub_elt(se) {}
    virtual ~Operation_Element_Base() {}
    virtual void enter(Editor_Stack& es) = 0;
    virtual void leave(Editor_Stack& es) = 0;
};

typedef Pointer_Map<Operation_Element_Base> OperationElement_Map;
typedef OperationElement_Map::value_type OpElt;
typedef std::map<std::string, bool> Constraint_Map;
typedef Constraint_Map::value_type Constraint;

struct _and {
    bool _;
    _and() : _(true) {}
    void operator() (Bool_Map::value_type& v) { _ &= v.second; }
};

struct Element_Base {
    std::string name;
    Operation_Map attributes;
    OperationElement_Map elements;
    Operation_Base* data;
    Constraint_Map attribute_constraints;
    Constraint_Map contents_constraints;
    Element_Base(const char* _name)
        : name(_name)
        , attributes()
        , elements()
        , data(0)
        , attribute_constraints()
        , contents_constraints()
    {}
    virtual ~Element_Base()
    {
        if (data) { delete data; }
    }
};

}

#endif

