#include <iostream>
#include <functional>
#include <algorithm>

namespace predicate {

    namespace detail {
        struct dummy_struc_type {};

        template <typename A, typename B>
        struct must_be_equal;

        template <typename A>
        struct must_be_equal<A, A> { typedef A type; };

        template <typename A>
        struct must_be_equal<A, dummy_struc_type> { typedef A type; };

        template <typename A>
        struct must_be_equal<dummy_struc_type, A> { typedef A type; };

        template <typename P1, typename P2>
        struct match_struc_type
            : public must_be_equal<typename P1::struc_type, typename P2::struc_type>
        {};

        template <typename P1, typename P2>
        struct match_value_type
            : public must_be_equal<typename P1::value_type, typename P2::value_type>
        {};
    }

    namespace impl {
        template <typename P1, typename P2>
        struct and_predicate {
            P1 p1;
            P2 p2;
            typedef typename detail::match_struc_type<P1, P2>::type struc_type;
            typedef bool value_type;
            and_predicate(P1 _1, P2 _2) : p1(_1), p2(_2) {}
            bool operator() (const struc_type& x)
            {
                return p1(x) && p2(x);
            }
        };

        template <typename P1, typename P2>
        struct or_predicate {
            P1 p1;
            P2 p2;
            typedef typename detail::match_struc_type<P1, P2>::type struc_type;
            typedef bool value_type;
            or_predicate(P1 _1, P2 _2) : p1(_1), p2(_2) {}
            bool operator() (const struc_type& x)
            {
                return p1(x) || p2(x);
            }
        };

        template <typename P1>
        struct not_predicate {
            P1 p1;
            typedef typename P1::struc_type struc_type;
            typedef bool value_type;
            not_predicate(P1 p) : p1(p) {}
            bool operator() (const struc_type& x) {
                return !p1(x);
            }
        };


        template <typename P1>
        struct negate_predicate {
            P1 p1;
            typedef typename P1::struc_type struc_type;
            typedef bool value_type;
            negate_predicate(P1 p) : p1(p) {}
            bool operator() (const struc_type& x) {
                return -p1(x);
            }
        };


        template <typename P1, typename P2, template <typename> class Operation>
        struct binop_predicate {
            typedef typename detail::match_struc_type<P1, P2>::type struc_type;
            typedef typename detail::match_value_type<P1, P2>::type value_type;
            P1 p1;
            P2 p2;
            Operation<value_type> op;
            binop_predicate(P1 _1, P2 _2) : p1(_1), p2(_2), op() {}
            bool operator() (const struc_type& x)
            {
                return op(p1(x), p2(x));
            }
        };
    }


    template <typename TStruc, typename TValue>
    struct member {
        typedef TStruc struc_type;
        typedef TValue value_type;
        typedef TValue TStruc::* member_type;
        member_type m_member;

        member(member_type m)
            : m_member(m)
        {}

        value_type get(const struc_type& s) const {
            return s .* m_member;
        }
    };

    template <typename TValue>
    struct constant {
        typedef detail::dummy_struc_type struc_type;
        typedef TValue value_type;
        TValue data;
        constant(const TValue& d) : data(d) {}
        template <typename X>
        TValue get(const X& x)
        {
            (void)x;
            return data;
        }

        operator TValue() const { return data; }
    };


    template <typename G>
    struct Gettable : public G {
        typedef typename G::struc_type struc_type;
        typedef typename G::value_type value_type;

        template <typename A>
        Gettable(A a) : G(a) {}

        template <typename X>
        typename G::value_type operator() (const X& x)
        {
            return get(x);
        }
    };


    template <typename P>
    struct Predicate : public P {
        typedef typename P::struc_type struc_type;
        typedef typename P::value_type value_type;

        typedef Predicate<Gettable<constant<value_type> > > constant_type;

        template <typename A, typename B>
        Predicate(A a, B b): P(a, b) {}

        template <typename A>
        Predicate(A a): P(a) {}

        template <typename P1>
            struct Predicate<impl::and_predicate<Predicate<P>, Predicate<P1> > >
            operator && (Predicate<P1> p1)
            {
                return Predicate<impl::and_predicate<Predicate<P>, Predicate<P1> > >
                       (*this, p1);
            }

        template <typename P1>
            struct Predicate<impl::or_predicate<Predicate<P>, Predicate<P1> > >
            operator || (Predicate<P1> p1)
            {
                return Predicate<impl::or_predicate<Predicate<P>, Predicate<P1> > >
                       (*this, p1);
            }

        struct Predicate<impl::not_predicate<Predicate<P> > >
            operator ! ()
            {
                return Predicate<impl::not_predicate<Predicate<P> > >
                       (*this);
            }

        struct Predicate<impl::negate_predicate<Predicate<P> > >
            operator - ()
            {
                return Predicate<impl::negate_predicate<Predicate<P> > >
                       (*this);
            }

#define DECLARE_OP(_operation, _operator) \
        template <typename P1> \
            Predicate<impl::binop_predicate<Predicate<P>, Predicate<P1>, _operation> > \
            operator _operator (Predicate<P1> p1) \
            { \
                return Predicate<impl::binop_predicate<Predicate<P>, Predicate<P1>, \
                       _operation> > \
                       (*this, p1); \
            } \
        Predicate<impl::binop_predicate<Predicate<P>, constant_type, _operation> > \
            operator _operator (const value_type p1) \
            { \
                return Predicate<impl::binop_predicate<Predicate<P>, constant_type, \
                       _operation> > \
                       (*this, constant_type(p1)); \
            }

        DECLARE_OP(std::less, < )
        DECLARE_OP(std::greater, > )
        DECLARE_OP(std::less_equal, <= )
        DECLARE_OP(std::greater_equal, >= )
        DECLARE_OP(std::equal_to, == )
        DECLARE_OP(std::not_equal_to, != )
        DECLARE_OP(std::multiplies, * )
        DECLARE_OP(std::divides, / )
        DECLARE_OP(std::plus, + )
        DECLARE_OP(std::minus, - )
        DECLARE_OP(std::modulus, % )

#undef DECLARE_OP

        template <typename Callable>
        struct call_predicate {
            typedef typename Callable::value_type value_type;
            typedef detail::dummy_struc_type struc_type;
            Callable cb;
            P p;
            call_predicate(const Callable& c, const P& p_) : cb(c), p(p_) {}
            call_predicate(const call_predicate<Callable>& c) : cb(c.cb), p(c.p) {}
            template <typename X>
            value_type operator () (const X& x) {
                return cb(p(x));
            }
        };

        template <typename TStruc, typename Coll>
        struct in_callback {
            typedef bool value_type;
            typedef TStruc struc_type;
            const Coll* coll;
            P p;
            in_callback(const Coll* c, P& p_)
                : coll(c), p(p_)
            {}
            in_callback(const in_callback<TStruc, Coll>& c)
                : coll(c.coll), p(c.p)
            {}

            bool operator () (const struc_type& x)
            {
                typename P::value_type v = p(x);
                return std::find(coll->begin(), coll->end(), v) != coll->end();
            }
        };

        template <typename Callable>
            Predicate<call_predicate<Callable> >
            eval(Callable c)
            {
                return Predicate<call_predicate<Callable> >
                       (c, *this);
            }

        template <typename Coll>
            Predicate<in_callback<struc_type, Coll> >
            in(const Coll& c)
            {
                return Predicate<in_callback<struc_type, Coll> >
                       (&c, *this);
            }

    };


    template <typename S, typename F>
    Predicate<Gettable<member<S, F> > >
    m(F S::* x)
    {
        return Predicate<Gettable<member<S, F> > >(x);
    }


    template <typename Coll, typename Expr>
    Coll filter(const Coll& src, Expr pred)
    {
        Coll ret;
        typename Coll::const_iterator i, j = src.end();
        for(i = src.begin(); i != j; ++i) {
            if (pred(*i)) {
                ret.push_back(*i);
            }
        }
        return ret;
    }
}

