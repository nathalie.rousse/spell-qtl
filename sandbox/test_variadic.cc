#include <iostream>
#include <exception>
#include <complex>
using std::complex;

#if 1

template <typename... Args>
struct toto;

template <typename Arg1, typename... Args>
struct toto<Arg1, Args...> {
    Arg1 a;
    toto<Args...> inner;
    toto(Arg1 x, Args... y)
        : a(x), inner(y...)
    {}
};

template <typename Arg1>
struct toto<Arg1> {
    Arg1 a;
    toto(Arg1 x) : a(x) {}
};

template <typename OSTREAM, typename... Args>
OSTREAM& operator << (OSTREAM& os, toto<Args...> x)
{
    return os << "got " << x.a << ' ' << x.inner;
}

template <typename OSTREAM, typename Arg>
OSTREAM& operator << (OSTREAM& os, toto<Arg> x)
{
    return os << "got " << x.a;
}

/*template <typename OSTREAM>*/
/*OSTREAM& operator << (OSTREAM& os, toto<> x)*/
/*{*/
    /*return os << "got " << x.a << ' ' << x.inner;*/
/*}*/


template <typename... Args>
toto<Args...> make_toto(Args... args)
{
    return toto<Args...> { args... };
}



constexpr complex<double> operator "" i(long double d) { return {0, (double) d}; }
constexpr complex<double> operator "" i(unsigned long long d) { return {0, (double) d}; }


int main(int argc, char** argv)
{
    auto x = make_toto("a", 42, 123.45);
    auto z = 1.+2i;
    std::cout << x << std::endl;
    std::cout << z << std::endl;
    return 0;
}

#else

/*template <typename... X> void printf(const char* s, X...);*/

/*template <>*/
void _printf(const char *s)
{
    while (*s) {
        if (*s == '%') {
            if (*(s + 1) == '%') {
                ++s;
            }
            else {
                /*throw std::runtime_error("invalid format string: missing arguments");*/
            }
        }
        std::cout << *s++;
    }
}
 
template<typename T, typename... Args>
void _printf(const char *s, T value, Args... args)
{
    while (*s) {
        if (*s == '%') {
            if (*(s + 1) == '%') {
                ++s;
            }
            else {
                std::cout << value;
                _printf(s + 1, args...); // call even when *s == 0 to detect extra arguments
                return;
            }
        }
        std::cout << *s++;
    }
    /*throw std::logic_error("extra arguments provided to printf");*/
}


int main(int argc, char** argv)
{
    _printf("toto % pouet %\n", 42, "coin");
    return 0;
}

#endif

