#include "include/xml.h"

struct test {
    int a;
    double b;
    std::string hop;
    test() : a(0), b(0), hop() {}
};

struct manip {
    void operator () (struct test* x)
    {
        x->a *= 10;
    }
};

#if 0
DTD<test> dtd =
    Element<int>("a").charData(),
    Element<double>("b").attr("value"),
    Element<manip>("op"),
    Element<test>("test")
        .contains(&test::a, "a")
        .contains(&test::b, "b")
        .contains("op");

DTD<test> dtd = Element<int>("a").charData(),
                Element<double>("b").attr("value")
                Element<manip>("op"),
                Element<test>("test")
                    .contains(&test::a, "a")
                    .contains(&test::b, "b")
                    .contains("op")
                    .constraints((E("a"), E("b")) & opt(E("op"))
                ;

DTD<test> dtd = Element<int>("a").charData(),
                Element<double>("b").attr("value"),
                Element<manip>("op"),
                Element<test>("test")
                    | (E(&test::a, "a"), E(&test::b, "b")) & E("op")
                :
#endif

int main(int argc, char** argv) {
    XML::Element<test> etest("test");
    etest.attr(&test::hop, "hop");
    XML::Element<int> ea("a");
    ea.charData();
    XML::Element<double> eb("b");
    eb.attr("value");
    XML::Element<manip> eop("op");
    etest.contains(&test::a, &ea);
    etest.contains(&test::b, &eb);
    etest.contains(&eop);
    {
        std::istringstream iss("<test hop=\"pouet\"><a>42</a><b value=\"123.45\"/></test>");
        test t;
        etest.read(&t, iss);

        std::cout << "a=" << t.a << std::endl;
        std::cout << "b=" << t.b << std::endl;
    }

    {
        std::istringstream iss("<test><a>42</a><b value=\"123.45\"/><op/></test>");
        test* t = etest.read(iss);

        std::cout << "a=" << t->a << std::endl;
        std::cout << "b=" << t->b << std::endl;
        delete t;
    }

    return 0;
}

