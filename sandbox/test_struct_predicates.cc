#include "struct_predicates.h"
#include <vector>
#include <string>
#include <typeinfo>

/*template <class X>*/
/*struct Searchable {*/
    /*template <typename T>*/
    /*int operator ~ (T X::& f) {*/
        /*return 1;*/
    /*}*/
/*};*/

struct SomeTag {
    int toto;
    int titi;
    std::string pouet;
};

struct OtherTag {
    int coin;
};

using predicate::filter;
using predicate::m;

int main(int argc, char** argv)
{
    (void)argc;
    (void)argv;
    /*bool x = m(&SomeTag::toto) < 5;*/
    /*comp_predicate<SomeTag, int, std::less> x = m(&SomeTag::toto) < 5;*/
    std::vector<int> coll;
    std::vector<SomeTag> test, f;
    coll.push_back(20);
    coll.push_back(42);
    coll.push_back(160);
    SomeTag a; a.toto = 23; a.pouet = ""; a.titi = 23;
    SomeTag b; b.toto = 42; b.pouet = "dilbert"; b.titi = 23;
    SomeTag c; c.toto = 42; c.pouet = "wally"; c.titi = 42;
    test.push_back(a);
    test.push_back(b);
    test.push_back(c);

    f = filter(test, m(&SomeTag::toto) <= 30 && m(&SomeTag::toto) > 10);
    std::cout << f.size() << std::endl;
    f = filter(test, m(&SomeTag::toto) == m(&SomeTag::titi));
    std::cout << f.size() << std::endl;
    f = filter(test, m(&SomeTag::toto) % m(&SomeTag::titi) == 0);
    std::cout << f.size() << std::endl;
    f = filter(test, m(&SomeTag::toto) % m(&SomeTag::titi) == 0 && m(&SomeTag::toto) == 42);
    std::cout << f.size() << std::endl;
    f = filter(test, m(&SomeTag::toto).in(coll));
    std::cout << f.size() << std::endl;
    f = filter(test, m(&SomeTag::toto).in(coll));
    std::cout << f.size() << std::endl;
    return 1;
}

