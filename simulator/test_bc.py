from simu import *
import sys
import scipy
from itertools import izip

noise_range = [2, 4, 8, 12, 16, 24, 32]
n_runs = [100, 104, 290, 715, 1334, 2223, 5000]
dirname = "results_BC"


try:
    os.mkdir(dirname)
except:
    pass

os.chdir(dirname)

d = Design()


def gen_trait(noise_gain):
    global d
    d += """
map,100|100,150|150
line,A,a
line,B,b
cross,F1,A,B,1,None
cross,BC,F1,A,500,aa:A ba:H
qtls,1
    """.split('\n')

    d += "trait,trait1,BC,1,aa:1.7321 ba:0,0,%f,0,1" % noise_gain


table = open('results_BC_tmp.txt', 'w')

print >> table, " run chrom pos rss thres ftest r2",
#print >> table, "ftest_at_qtl",
print >> table, "M0rss qtl_pos noise tmean tvar tss"

i = 0
lineno = 0


def clean_list(string):
    return [x for x in string.split(' ') if x]


for noise, n_run in izip(noise_range, n_runs):
    #noise = noise
    for run in xrange(n_run):
    #for run in xrange(1):
        gen_trait(noise)
        s = SpellQtl("test", d, parallel='auto')
        so = s.output
        i += 1
        if not so:
            print "isofoirate de foirure foirique !"
            continue
        trait = d.traits["trait1"]
        mean = scipy.mean(trait.values)
        var = scipy.var(trait.values)
        tss = var * len(trait.values)
        output_lineno = lineno
        for l in so:
            output_lineno += 1
            print >> table, output_lineno, i, l.strip(), noise, mean, var, tss
        table.flush()
        tfm_lineno = lineno
        for tstr, fstr, mstr, flstr in izip(s.testpos, s.tpftac,
                                            s.maha, s.ftlod):
            tfm_lineno += 1
            t = clean_list(tstr)
            f = clean_list(fstr)
            m = clean_list(mstr)
            ft = clean_list(flstr)
            tpftac = open('testpos_ftac_maha.' + str(tfm_lineno) + '.txt', 'w')
            print >> tpftac, 'pos f.pval maha f.lod'
            print >> tpftac, '\n'.join(' '.join(col)
                                       for col in izip(t, f, m, ft))
        lineno = output_lineno
