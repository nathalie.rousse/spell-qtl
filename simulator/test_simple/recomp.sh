#!/bin/bash
set -e




(cd ../../src/pedigree && make -j)
(cd ../../src/bayes/ && make -j)
(cd ../../src/ && make -j)

##
../../src/pedigree/spell-pedigree -p test.ped 
##
../../src/bayes/spell-marker -n test -p test.ped.ped-data -mos format-ABHCD.xml -mos format-ABHCD-2.xml -mos format-ABHCD-3.xml -m F3:ABHCD test_F3.gen -m F3_2:ABHCD2 test_F3_2.gen -m F3_3:ABHCD3 test_F3_3.gen
##
../../src/spell-qtl -P auto -wd tmp -n test-qtl -gm test.map \
    -p test.popdata F3 test_F3.phen \
    -p test.popdata F3_2 test_F3_2.phen \
    -p test.popdata F3_3 test_F3_3.phen \
    connected \
    cofactor-exclusion-window 20 skeleton auto 20 QTL-detection CIM- --clean


##
grep 'QTL\|single_trait' test.info
