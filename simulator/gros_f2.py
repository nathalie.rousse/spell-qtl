from simu import *
import sys
import scipy

noise_range = xrange(5, 6)
n_runs = 1


d = Design()


def gen_trait(noise_gain):
    global d
    d += """
map,10000,200
line,A,a
line,B,b
cross,F1,A,B,1,None
self,F2,F1,500,aa:A ba:H ab:H bb:B
qtls,1
    """.split('\n')

    d += "trait,trait1,F2,1,aa:2 ba:1 ab:1 bb:0,0,%f,0,1" % noise_gain

table = open('results_F2_tmp.txt.experimental', 'w')

print >> table, " run chrom pos rss thres ftest r2 ftest_at_qtl M0rss",
print >> table, "qtl_pos noise tmean tvar tss"

tpftac = open('tpftac_BC.txt.experimental', 'w')

i = 0
lineno = 0

for noise in noise_range:
    #noise = noise
    for run in xrange(n_runs):
    #for run in xrange(1):
        gen_trait(noise)
        s = SpellQtl("bigpop", d, parallel=9)
        so = s.output
        i += 1
        if not so:
            print "isofoirate de foirure foirique !"
            continue
        trait = d.traits["trait1"]
        mean = scipy.mean(trait.values)
        var = scipy.var(trait.values)
        tss = var * len(trait.values)
        for l in so:
            lineno += 1
            print >> table, lineno, i, l.strip(), noise, mean, var, tss
        table.flush()
        print >> tpftac, '\n'.join(s.tpftac)
