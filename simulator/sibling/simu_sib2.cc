#include <iostream>
#include <fstream>
#include <sstream>
/*#include <random>*/
/*#include <bitset>*/
#include <array>
#include <cstring>
#include <cmath>
#include <sys/time.h>

#if 0
unsigned int seed()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000000 + tv.tv_usec;
}

/*typedef std::minstd_rand random_t;*/

/*bool toss_coin_slow(double t)*/
/*{*/
    /*static std::uniform_real_distribution<> dis(0, 1);*/
    /*static random_t r(seed());*/
    /*return dis(r) < t;*/
/*}*/


/* The state must be seeded so that it is not everywhere zero. */
uint64_t s[2] = {seed64(), seed64()};
 
inline
uint64_t xorshift128plus(void) {
	uint64_t x = s[0];
	uint64_t const y = s[1];
	s[0] = y;
	x ^= x << 23; // a
	x ^= x >> 17; // b
	x ^= y ^ (y >> 26); // c
	s[1] = x;
	return x + y;
}


bool toss_coin_less_slow(double t)
{
    uint64_t ut = (uint64_t) -1;
    ut *= t;
    return xorshift128plus() < ut;
}


inline
bool toss_coin(double t)
{
    static uint64_t rnd = seed64();
    uint64_t ut = (uint64_t) -1;
    ut *= t;
    rnd = 2862933555777941757ULL * rnd + 3037000493ULL;  /* http://nuclear.llnl.gov/CNP/rng/rngman/node4.html */
    return rnd <= ut;
}
#endif

inline
uint64_t seed64()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t ret = tv.tv_sec;
    ret <<= 32;
    ret += tv.tv_usec;
    return ret;
}


inline
bool toss_coin_fast(uint64_t ut)
{
    static uint64_t rnd = seed64();
    rnd = 2862933555777941757ULL * rnd + 3037000493ULL;  /* http://nuclear.llnl.gov/CNP/rng/rngman/node4.html */
    return rnd < ut;
}



#ifndef N_MARK
#error "You MUST define N_MARK at compile-time using -DN_MARK=<number of markers between M1 and M2>"
#endif


#ifndef N_GEN
#error "You MUST define N_GEN at compile-time using -DN_GEN=<number of INTERMEDIARY generations (n-1 for a Fn result)>"
#endif





/*typedef std::array<bool, (N_MARK + 3) * 2> chromosome_t;*/
/*typedef std::array<bool, N_MARK + 3> gamete_t;*/
/*typedef bool chromosome_t[(N_MARK + 3) * 2];*/
/*typedef bool gamete_t[N_MARK + 3];*/
typedef bool chromosome_t[(N_MARK + 3) * 2];
typedef bool gamete_t[N_MARK + 3];


inline
size_t get_mark(const chromosome_t& chr, size_t mark)
{
    size_t base = mark << 1;
    size_t ret = (size_t) chr[base] + (chr[base + 1] << 1);
    /*std::cout << "mark_value=" << ret << std::endl;*/
    return ret;
}


static char mrk_obs[3] = {'A', 'H', 'B'};


inline
size_t get_obs_i(const chromosome_t& chr, size_t mark)
{
    /*static size_t mrk_obs_i[4] = {0, 1, 1, 2};*/
    /*return mrk_obs_i[get_mark(chr, mark)];*/
    size_t base = mark << 1;
    return (size_t) chr[base] + chr[base + 1];
}


inline
char get_obs(const chromosome_t& chr, size_t mark)
{
    return mrk_obs[get_obs_i(chr, mark)];
}


/*double gen_map[N_MARK + 2];*/
uint64_t gen_map[N_MARK + 2];

#define UINT64_HALF ((((uint64_t) -1) >> 1) + 1ULL)


void init_gen_map(double dist_M2, double dist_M3)
{
    double delta = dist_M2 / (N_MARK + 1);
    for (size_t i = 0; i <= N_MARK; ++i) {
        gen_map[i] = UINT64_MAX * delta;
    }
    gen_map[N_MARK + 1] = UINT64_MAX * dist_M3;
}


template <int I>
struct unroll_gen_gamete {
    void operator () (gamete_t& ret)
    {
        ret[I] = ret[I - 1] ^ toss_coin_fast(gen_map[I - 1]);
        unroll_gen_gamete<I + 1>()(ret);
    }
};

template <>
struct unroll_gen_gamete<0> {
    void operator () (gamete_t& ret)
    {
        ret[0] = toss_coin_fast(UINT64_HALF);
        unroll_gen_gamete<1>()(ret);
    }
};

template <>
struct unroll_gen_gamete<N_MARK + 3> {
    void operator () (gamete_t& ret) {}
};


inline
void gen_gamete(gamete_t& ret)
{
#if 0
    bool strand = toss_coin_fast(UINT64_HALF);  /* .5 */
    ret[0] = strand;
    for (size_t i = 0; i < (N_MARK + 2); ++i) {
        strand ^= toss_coin_fast(gen_map[i]);
        ret[i + 1] = strand;
    }
#else
    unroll_gen_gamete<0>()(ret);
#endif
    /*std::cout << "GAMETE " << ret << std::endl;*/
}


#if 0
gamete_t gen_haplo(const chromosome_t& chr, const gamete_t& gam)
{
    gamete_t ret;
    size_t base = 0;
    for (size_t i = 0; i < N_MARK + 3; ++i, base += 2) {
        ret[i] = chr[base + gam[i]];
    }
    /*std::cout << "HAPLO " << gam << " X " << chr << " => " << ret << std::endl;*/
    return ret;
}
#elif 0
void gen_haplo(const chromosome_t& chr, const gamete_t& gam, gamete_t& ret)
{
    for (size_t i = 0; i < N_MARK + 3; ++i) {
        ret[i] = chr[(i << 1) + gam[i]];
    }
    /*std::cout << "HAPLO " << gam << " X " << chr << " => " << ret << std::endl;*/
}
#else

template <int I>
struct unroll_gen_haplo {
    void operator () (const chromosome_t& chr, const gamete_t& gam, gamete_t& ret)
    {
        ret[I] = chr[(I << 1) + gam[I]];
        unroll_gen_haplo<I + 1>()(chr, gam, ret);
    }
};

template <>
struct unroll_gen_haplo<0> {
    void operator () (const chromosome_t& chr, const gamete_t& gam, gamete_t& ret)
    {
        ret[0] = chr[gam[0]];
        unroll_gen_haplo<1>()(chr, gam, ret);
    }
};

template <>
struct unroll_gen_haplo<N_MARK + 3> {
    void operator () (const chromosome_t& chr, const gamete_t& gam, gamete_t& ret) {}
};

void gen_haplo(const chromosome_t& chr, const gamete_t& gam, gamete_t& ret)
{
    unroll_gen_haplo<0>()(chr, gam, ret);
}

#endif


#if 0
void do_cross(const gamete_t& h1, const gamete_t& h2, chromosome_t& ret)
{
    size_t base = 0;
    for (size_t i = 0; i < N_MARK + 3; ++i, base += 2) {
        ret[base] = h1[i];
        ret[base + 1] = h2[i];
    }
}

void do_cross_2(const chromosome_t& c1, const gamete_t& g1, const chromosome_t& c2, const gamete_t& g2, chromosome_t& ret)
{
    size_t base = 0;
    for (size_t i = 0; i < N_MARK + 3; ++i, base += 2) {
        ret[base] = c1[base + g1[i]];
        ret[base + 1] = c2[base + g2[i]];
    }
}

#elif 0

template <int I>
struct unroll_do_cross {
    void operator () (const chromosome_t& c1, const gamete_t& g1, const chromosome_t& c2, const gamete_t& g2, chromosome_t& ret)
    {
        ret[I << 1] = c1[(I << 1) + g1[I]];
        ret[(I << 1) + 1] = c2[(I << 1) + g2[I]];
        unroll_do_cross<I + 1>()(c1, g1, c2, g2, ret);
    }
};

template <>
struct unroll_do_cross<0> {
    void operator () (const chromosome_t& c1, const gamete_t& g1, const chromosome_t& c2, const gamete_t& g2, chromosome_t& ret)
    {
        ret[0] = c1[g1[0]];
        ret[1] = c2[g2[0]];
        unroll_do_cross<1>()(c1, g1, c2, g2, ret);
    }
};

template <>
struct unroll_do_cross<N_MARK + 3> {
    void operator () (const chromosome_t& c1, const gamete_t& g1, const chromosome_t& c2, const gamete_t& g2, chromosome_t& ret) {}
};

inline
void do_cross_2(const chromosome_t& c1, const gamete_t& g1, const chromosome_t& c2, const gamete_t& g2, chromosome_t& ret)
{
    unroll_do_cross<0>()(c1, g1, c2, g2, ret);
}

#else

template <int I>
struct unroll_do_cross {
    void operator () (const chromosome_t& c1, register bool& strand1, const chromosome_t& c2, register bool& strand2, chromosome_t& ret)
    {
        strand1 ^= toss_coin_fast(gen_map[I - 1]);
        strand2 ^= toss_coin_fast(gen_map[I - 1]);
        ret[I << 1] = c1[(I << 1) + strand1];
        ret[(I << 1) + 1] = c2[(I << 1) + strand2];
        unroll_do_cross<I + 1>()(c1, strand1, c2, strand2, ret);
    }
};

template <>
struct unroll_do_cross<0> {
    void operator () (const chromosome_t& c1, const chromosome_t& c2, chromosome_t& ret)
    {
        register bool strand1 = toss_coin_fast(UINT64_HALF);
        register bool strand2 = toss_coin_fast(UINT64_HALF);
        ret[0] = c1[strand1];
        ret[1] = c2[strand2];
        unroll_do_cross<1>()(c1, strand1, c2, strand2, ret);
    }
};

template <>
struct unroll_do_cross<N_MARK + 3> {
    void operator () (const chromosome_t& c1, register bool& strand1, const chromosome_t& c2, register bool& strand2, chromosome_t& ret) {}
};

inline
void do_cross_2(const chromosome_t& c1, const chromosome_t& c2, chromosome_t& ret)
{
    unroll_do_cross<0>()(c1, c2, ret);
}

#endif

inline
void crossing(const chromosome_t& c1, const chromosome_t& c2, chromosome_t& ret)
{
    static gamete_t g1, g2, h1, h2;
    /*gen_gamete(g1);*/
    /*gen_gamete(g2);*/
    /*gen_haplo(c1, g1, h1);*/
    /*gen_haplo(c2, g2, h2);*/
    /*do_cross(h1, h2, ret);*/
    /*do_cross_2(c1, g1, c2, g2, ret);*/
    do_cross_2(c1, c2, ret);
    /*return do_cross(gen_haplo(c1, g1), gen_haplo(c2, g2));*/
}

chromosome_t A;
chromosome_t B;

chromosome_t F1;

chromosome_t parents[2][2];

template <int n_gen>
void deep_sibling(const chromosome_t& p1, const chromosome_t& p2, chromosome_t& ret)
{
    memcpy(parents[0][0], p1, sizeof parents[0][0]);
    memcpy(parents[0][1], p2, sizeof parents[0][1]);
    /*parents[0][0] = p1;*/
    /*parents[0][1] = p2;*/
    size_t i = 0;
    size_t this_gen;
    if (n_gen > 0) {
        for (i = 1; i < n_gen; ++i) {
            this_gen = i & 1;
            size_t parent_gen = 1 - this_gen;
            /*parents[this_gen][0] = crossing(parents[parent_gen][0], parents[parent_gen][1]);*/
            /*parents[this_gen][1] = crossing(parents[parent_gen][0], parents[parent_gen][1]);*/
             crossing(parents[parent_gen][0], parents[parent_gen][1], parents[this_gen][0]);
             crossing(parents[parent_gen][0], parents[parent_gen][1], parents[this_gen][1]);
        }
    }
    /*std::cout << "i=" << i << std::endl;*/
    crossing(parents[this_gen][0], parents[this_gen][1], ret);
}

          /* M1 M2 M3   M1....M3  {A H B} */
uint64_t tables[3][3][3][N_MARK + 3][3];

inline
void add_stats(const chromosome_t& chr)
{
    size_t m1 = get_obs_i(chr, 0);
    size_t m2 = get_obs_i(chr, N_MARK + 1);
    size_t m3 = get_obs_i(chr, N_MARK + 2);
    auto& table = tables[m1][m2][m3];
    for (size_t i = 0; i < N_MARK + 3; ++i) {
        table[i][get_obs_i(chr, i)] += 1;
    }
}


/*std::ostream& operator << (std::ostream& os, const chromosome_t& chr)*/
/*{*/
    /*std::bitset<2 * (N_MARK + 3)> tmp = chr;*/
    /*os << tmp << std::endl;*/
    /*for (size_t i = 0; i < N_MARK + 3; ++i) {*/
        /*os << get_obs(chr, i);*/
    /*}*/
    /*return os;*/
/*}*/
void dump_chromosome(const chromosome_t& chr)
{
    for (size_t i = 0; i < 2 * (N_MARK + 3); ++i) {
        std::cout << chr[i];
    }
    std::cout << std::endl;
    for (size_t i = 0; i < N_MARK + 3; ++i) {
        std::cout << ' ' << get_obs(chr, i);
    }
    std::cout << std::endl;
}


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

struct file_stat {
    bool exists;
    int err;
    bool is_file;
    bool is_dir;
    bool writable;
    bool readable;
    file_stat(const std::string& path)
    {
        struct stat st;
        if (stat(path.c_str(), &st)) {
            exists = false;
            err = errno;
        } else {
            exists = true;
            err = 0;
        }
        if (exists) {
            is_file = !!S_ISREG(st.st_mode);
            is_dir = !!S_ISDIR(st.st_mode);
            writable = !!(st.st_mode & (S_IWUSR | S_IWGRP | S_IWOTH));
            readable = !!(st.st_mode & (S_IRUSR | S_IRGRP | S_IROTH));
        } else {
            is_file = is_dir = writable = readable = false;
        }
    }
};


static inline
bool check_file(const std::string& path, bool req_directory, bool req_writable, bool display=true)
{
    file_stat fs(path);
    if (fs.err != 0) {
        if (display) {
             std::cerr << "Path " << path << " is invalid: " << strerror(errno) << std::endl;
        }
        return false;
    }

    if (req_directory && !fs.is_dir) {
        if (display) {
             std::cerr << path << " is not a directory" << std::endl;
        }
        return false;
    } else if (!fs.is_file) {
        if (display) {
             std::cerr << path << " is not a regular file or a symbolic link to a regular file" << std::endl;
        }
        return false;
    }

    if (!fs.readable) {
        if (display) {
              std::cerr << path << " is not readable" << std::endl;
        }
        return false;
    }
    if (req_writable && !fs.writable) {
        if (display) {
              std::cerr << path << " is not writable" << std::endl;
        }
        return false;
    }
    return true;
}

static inline
bool ensure_directory_exists(const std::string& path)
{
    return check_file(path, true, true, false)
        || mkdir(path.c_str(), 0770) != -1;
}

static inline std::string __fetch_string(const std::ostream& os)
{
    return dynamic_cast<const std::stringstream*>(&os)->str();
}

#define MKSTR(_str_, _expr_) std::string _str_; { std::stringstream tmp; tmp << _expr_; _str_ = tmp.str(); }


std::string JOB_NAME;
int DIST_N;
int M2_DIST_I;
int M3_DIST_I;
int N_REP;

std::string prepare_directories()
{
    /*std::string base = MKSTR("counts_" << DIST_N);*/
    MKSTR(gen, "gen_" << N_GEN);
    ensure_directory_exists(gen);
    MKSTR(base, gen << '/' << "counts_" << DIST_N);
    ensure_directory_exists(base);
    MKSTR(d1, base << '/' << M2_DIST_I);
    ensure_directory_exists(d1);
    MKSTR(d2, d1 << '/' << M3_DIST_I);
    ensure_directory_exists(d2);
    for (size_t m1 = 0; m1 < 3; ++m1) {
        for (size_t m2 = 0; m2 < 3; ++m2) {
            for (size_t m3 = 0; m3 < 3; ++m3) {
                MKSTR(d3, d2 << '/' << mrk_obs[m1] << mrk_obs[m2] << mrk_obs[m3]);
                ensure_directory_exists(d3);
            }
        }
    }
    return d2;
}


void write_stats()
{
    std::string base = prepare_directories();
    for (size_t m1 = 0; m1 < 3; ++m1) {
        for (size_t m2 = 0; m2 < 3; ++m2) {
            for (size_t m3 = 0; m3 < 3; ++m3) {
                auto& table = tables[m1][m2][m3];
                MKSTR(path, base << '/' << mrk_obs[m1] << mrk_obs[m2] << mrk_obs[m3] << '/' << JOB_NAME << ".txt");
                std::ofstream os(path);
                os << "marker\tA\tH\tB" << std::endl;
                os << "M1\t" << table[0][0] << '\t' << table[0][1] << '\t' << table[0][2] << std::endl;
                for (size_t i = 1; i <= N_MARK; ++i) {
                    os << "M1." << i << '\t' << table[i][0] << '\t' << table[i][1] << '\t' << table[i][2] << std::endl;
                }
                os << "M2\t" << table[N_MARK + 1][0] << '\t' << table[N_MARK + 1][1] << '\t' << table[N_MARK + 1][2] << std::endl;
                os << "M3\t" << table[N_MARK + 2][0] << '\t' << table[N_MARK + 2][1] << '\t' << table[N_MARK + 2][2] << std::endl;
            }
        }
    }
}


int main(int argc, char** argv)
{
    if (argc == 1) {
        return 0;
    }

    for (size_t i = 0; i < (N_MARK + 3) * 2; ++i) {
        A[i] = 0;
        B[i] = 1;
    }
    crossing(A, B, F1);

    memset(tables, 0, sizeof tables);

    JOB_NAME = argv[1];
    DIST_N = atoi(argv[2]);
    M2_DIST_I = atoi(argv[3]);
    M3_DIST_I = atoi(argv[4]);
    N_REP = atoi(argv[5]);
    /*std::cout << "DIST_N=" << DIST_N << std::endl;*/
    /*std::cout << "M2_DIST_I=" << M2_DIST_I << std::endl;*/
    /*std::cout << "M3_DIST_I=" << M3_DIST_I << std::endl;*/
    /*std::cout << "DIST_N=" << DIST_N << std::endl;*/
    /* prepare map */
    double r2 = .5 * M2_DIST_I / DIST_N;
    double r3 = .5 * M3_DIST_I / DIST_N;
    /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/
    /*if (r2 == .5) { r2 = .4999; }*/
    /*if (r3 == .5) { r3 = .4999; }*/
    /*double d2 = -log(1. - 2. * r2) * .5;*/
    /*double d3 = -log(1. - 2. * r3) * .5;*/
    init_gen_map(r2, r3);
    /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/

    /*std::cout << "MAP:";*/
    /*for (double d: gen_map) {*/
        /*std::cout << ' ' << d;*/
    /*for (uint64_t d: gen_map) {*/
        /*std::cout << ' ' << (d / (double) UINT64_MAX);*/
    /*}*/
    /*std::cout << std::endl;*/
    /*dump_chromosome(A);*/
    /*dump_chromosome(B);*/
    /*dump_chromosome(F1);*/

    /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/
    for (int i = 0; i < N_REP; ++i) {
        chromosome_t Fx;
        deep_sibling<N_GEN>(F1, F1, Fx);
        /*dump_chromosome(Fx);*/
        add_stats(Fx);
    }
    /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/
    write_stats();
    /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/
    /*std::cout << "sizeof(bool)=" << sizeof(bool) << std::endl;*/
}

