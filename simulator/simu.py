#!/usr/bin/env python
from datetime import datetime
import os
import random
import math
from itertools import izip, chain, imap, combinations, product
import sys
from os.path import isfile


def toss_coin(threshold):
    return random.uniform(0, 1) < threshold


def haldane(dist):
    return (1. - math.exp(-.02 * dist)) * .5


#genotype_to_observation = {
#        'aa': 'A',
#        'ab': 'H',
#        'ba': 'H',
#        'bb': 'B'
#}


class Chromosome(object):

    def __init__(self, genmap):
        self.map = genmap
        self.init("", [0], [0], [""])

    def init(self, name, positions, intervals, marker_names):
        self.name = name
        self.marker_names = marker_names
        self.Minterval = zip(self.marker_names, intervals)
        self.M = zip(self.marker_names, positions)
        self.map_file_hack = ([(len(positions), marker_names[0])]
                              + zip(intervals[1:], self.marker_names[1:]))
        return self

    @staticmethod
    def generate(genmap, name, mark_prefix, n_mark, length, constraint=[0]):
        self = Chromosome(genmap)
        if constraint[0] != 0.:
            constraint = [0.] + constraint
        if constraint[-1] == length:
            constraint = constraint[:-1]
        positions = sorted(constraint
                           + [round(random.uniform(0, length), 2)
                              for x in xrange(n_mark - len(constraint) - 1)]
                           + [length])
        intervals = [0] + [round(positions[i] - positions[i - 1], 2)
                           for i in xrange(1, n_mark)]
        marker_names = ["%s%i" % (mark_prefix, i + 1)
                        for i in xrange(n_mark)]
        return self.init(name, positions, intervals, marker_names)

    def __str__(self):
        size = (len(self)
                - len(set.intersection(self.map.qtls, self.M[1:-1])))
        map_items = [(size, self.map_file_hack[0][1])]
        offset = 0
        #print self.map.qtls
        for i in xrange(1, len(self.map_file_hack) - 1):
            offset += self.map_file_hack[i][0]
            #print self.M[i], self.map.qtls,
            #print self.M[i] not in self.map.qtls
            if self.M[i] not in self.map.qtls:
                map_items.append((offset, self.map_file_hack[i][1]))
                offset = 0
        map_items.append((offset + self.map_file_hack[-1][0],
                          self.map_file_hack[-1][1]))

        #self.map_file_hack[0] = (size, self.map_file_hack[0][1])
        #print "chromosome has", size, "non-qtl markers"
        return ' '.join(chain(['*' + self.name],
                              [' '.join(imap(str, x))
                               for x in map_items]))
#                               if x == self.map_file_hack[0]
#                               or x == self.map_file_hack[-1]
#                               or x[1] not in self.map.qtl_names]))

    __repr__ = __str__

    def save(self, f):
        print >> f, str(self)
        return self

    @staticmethod
    def load(genmap, line):
        items = line.strip()[1:].split(' ')
        self = Chromosome(genmap)
        self.marker_names = items[2::2]
        intervals = [round(float(x), 2) for x in items[3::2]]
        positions = reduce(lambda p, i: p + [p[-1] + i], intervals, [0])
        return self.init(items[0], positions, [0] + intervals, items[2::2])

    def __eq__(self, c):
        return (self.name == c.name and self.map_file_hack == c.map_file_hack)

    def __gen_gamete(self, strand=None):
        if strand is None:
            idx = 1 if toss_coin(.5) else 0
        else:
            idx = strand
        #print "idx=%i" % idx,
        for (m, interval) in self.Minterval:
            prob = haldane(interval)
            if toss_coin(prob):
                idx = 1 - idx
            yield idx

    def gen_gamete(self, strand=None):
        return list(self.__gen_gamete(strand))

    def __len__(self):
        return len(self.marker_names)

    def __getitem__(self, index):
        return self.M[index]

    def index(self, mrk):
        return self.marker_names.index(mrk)


class Map(object):

    def __init__(self):
        self.chromosomes = []
        self.qtls = set()
        self.qtl_names = set()

    @staticmethod
    def generate(n_mark_list, length_list, constraint_list=None):
        self = Map()
        if constraint_list is None:
            constraint_list = [[0]] * len(length_list)
        self.chromosomes = [Chromosome.generate(self,
                                                'ch%i' % (i + 1),
                                                'M_%i_' % (i + 1),
                                                n_mark_list[i],
                                                length_list[i],
                                                constraint_list[i])
                            for i in xrange(len(n_mark_list))]
        return self

    def pick_qtl(self):
        if len(self.qtls) == len(self):
            return None
        qtl = None
        while qtl is None:
            qtl = random.choice(self)
            if qtl in self.qtls:
                qtl = None
        self.qtls.add(qtl)
        self.qtl_names.add(qtl[0])
        return qtl

    def __str__(self):
        return '\n'.join(imap(str, self.chromosomes))

    __repr__ = __str__

    def save(self, filename):
        print >> open(filename, 'w'), self
        print >> open(filename + '.qtl', 'w'), self.qtls
        return self

    @staticmethod
    def load(filename):
        self = Map()
        self.chromosomes = map(lambda x: Chromosome.load(self, x),
                               open(filename).xreadlines())
        return self

    def __eq__(self, m):
        return self.chromosomes == m.chromosomes

    def __iter__(self):
        return (m for c in self.chromosomes for m in c.marker_names)

    def gen_gamete(self, strand=None):
        return (idx for c in self.chromosomes for idx in c.gen_gamete(strand))

    def __len__(self):
        return reduce(lambda a, b: a + len(b), self.chromosomes, 0)

    def __getitem__(self, index):
        for c in self.chromosomes:
            if index < len(c):
                return c[index]
            index -= len(c)
        return None

    def index(self, mrk):
        base = 0
        for c in self.chromosomes:
            try:
                return base + c.index(mrk)
            except:
                base += len(c)
        raise ValueError(mrk + " not in map")

    def chromosome_of(self, mrk):
        for c in self.chromosomes:
            try:
                if c.index(mrk) != -1:
                    return c
            except ValueError:
                pass
        raise ValueError(mrk + " not in map")


class Individual(object):

    def __init__(self, i, pop=None, mi=-1, fi=-1):
        self.father_idx = fi
        self.mother_idx = mi
        self.idx = i
        self.pop = pop
        #print "New", self

    @property
    def observations(self):
        return (self.pop.observations[i][self.idx]
                for i in xrange(len(self.pop.map)))

    def meiosis(self, strand=None):
        return (g[idx] for (g, idx) in izip(self.observations,
                                            self.pop.map.gen_gamete(strand)))

    def apply(self, genotype):
        #print "APPLY", self.idx, genotype
        #print self.pop.observations
        for mvec, geno in izip(self.pop.observations, genotype):
            mvec[self.idx] = ''.join(geno)
        return self

    @staticmethod
    def ancestor(allele, pop, pedigree):
        a = allele + allele
        ind_num = 1 + len(pedigree)
        pedigree.append((pop.name, ind_num, 0, 0))
        pop.ped_id = [ind_num]
        genotype = (''.join(random.sample(allele, 1) * 2) for m in pop.map)
        return Individual(0, pop).apply(genotype)

    @staticmethod
    def crossing(i, pop, mi, fi, pedigree):
        hf = pop.father_pop.individuals[fi].meiosis()
        hm = pop.mother_pop.individuals[mi].meiosis()
        genotype = izip(hm, hf)
        ind_num = 1 + len(pedigree)
        pedigree.append((pop.name, ind_num,
                         pop.mother_pop.ped_id[mi],
                         pop.father_pop.ped_id[fi]))
        pop.ped_id[i] = ind_num
        return Individual(i, pop, mi, fi).apply(genotype)

    @staticmethod
    def selfing(i, pop, pi, pedigree):
        hf = pop.father_pop.individuals[pi].meiosis()
        hm = pop.mother_pop.individuals[pi].meiosis()
        genotype = izip(hm, hf)
        ind_num = 1 + len(pedigree)
        pedigree.append((pop.name, ind_num,
                         pop.mother_pop.ped_id[pi],
                         pop.father_pop.ped_id[pi]))
        pop.ped_id[i] = ind_num
        return Individual(i, pop, pi, pi).apply(genotype)

    @staticmethod
    def ril(i, pop, pi, geno_constraint=[]):
        #print "RIL"
        ok = False
        counter = 0
        print "",
        while not ok:
            parent = pop.father_pop.individuals[pi]
            #counter += 1
            #if counter % 10 == 0:
            #    print '\r', i, counter,
            #    sys.stdout.flush()
            for z in xrange(10):
                #print parent
                #print pop.observations
                hf = parent.meiosis(0)
                hm = parent.meiosis(0)
                genotype = zip(hm, hf)
                #print genotype
                hz = any(g[0] != g[1] for g in genotype)
                if not hz:
                    break
                parent = Individual(i, pop, i, i).apply(genotype)
            genotype = [g if g[0] == g[1] else ('', '') for g in genotype]
            #print genotype
            ok = not geno_constraint
            for gc in geno_constraint:
                ok = all(''.join(genotype[m]) == g for (m, g) in gc)
                if ok:
                    break
            if not ok:
                #print i, "constraint not satisfied"
                #print "|", ' '.join(''.join(genotype[m])
                #                    for (m, g) in gc),
                pass
        return Individual(i, pop, pi, pi).apply(genotype)

    def __str__(self):
        from_ = ''
        if self.pop.mother_pop is not None:
            m = '%s#%i,' % (self.pop.mother_pop.name, self.mother_idx)
            from_ = ' from '
        else:
            m = ''
        if self.pop.father_pop is not None:
            f = '%s#%i' % (self.pop.father_pop.name, self.father_idx)
            from_ = ' from '
        else:
            f = ''
        return "<ind #%i%s%s%s>" % (self.idx, from_, m, f)

    __repr__ = __str__


class Population(object):

    def __init__(self, name, genmap, n_ind, mother_pop=None, father_pop=None):
        self.name = name
        self.n_ind = n_ind
        self.map = genmap
        self.father_pop = father_pop
        self.mother_pop = mother_pop
        self.observations = [[None] * n_ind for m in genmap]
        self.individuals = []
        self.geno_to_obs = {}
        self.ped_id = range(n_ind)

    @staticmethod
    def generate_line(name, genmap, allele, pedigree, geno_to_obs=None):
        self = Population(name, genmap, 1)
        self.individuals = [Individual.ancestor(allele, self, pedigree)]
        self.geno_to_obs = geno_to_obs or (dict((a+a, a) for a in allele)
                                           if len(allele) > 1 else None)
        return self

    @staticmethod
    def generate_crossing(name, mother_pop, father_pop, n_ind, geno2obs,
                          pedigree):
        #assert (mother_pop.map == father_pop.map,
        #        "Parent populations MUST share the same genetic map")
        self = Population(name, mother_pop.map, n_ind, mother_pop, father_pop)
        mi = lambda: random.choice(xrange(self.mother_pop.n_ind))
        fi = lambda: random.choice(xrange(self.father_pop.n_ind))
        self.individuals = [Individual.crossing(i, self, mi(), fi(), pedigree)
                            for i in xrange(n_ind)]
        self.geno_to_obs = geno2obs
        return self

    @staticmethod
    def generate_sibling(name, mother_pop, n_ind, geno2obs):
        self = Population(name, mother_pop.map, n_ind, mother_pop, mother_pop)
        mi = [random.choice(xrange(self.mother_pop.n_ind))
              for i in xrange(n_ind)]

        def _fi(m):
            if self.mother_pop.n_ind == 1:
                return m
            ret = m
            print "m =", m,
            while ret == m:
                ret = random.choice(xrange(self.mother_pop.n_ind))
                print ret,
            print
            return ret

        fi = [_fi(mi[i]) for i in xrange(n_ind)]
        #mi = lambda: random.choice(xrange(self.mother_pop.n_ind))
        #fi = lambda: random.choice(xrange(self.father_pop.n_ind))
        self.individuals = [Individual.crossing(i, self, mi[i], fi[i])
                            for i in xrange(n_ind)]
        self.geno_to_obs = geno2obs
        return self

    @staticmethod
    def generate_selfing(name, mother_pop, n_ind, geno2obs, pedigree):
        self = Population(name, mother_pop.map, n_ind, mother_pop, mother_pop)
        mi = lambda: random.choice(xrange(self.mother_pop.n_ind))
        self.individuals = [Individual.selfing(i, self, mi(), pedigree)
                            for i in xrange(n_ind)]
        self.geno_to_obs = geno2obs
        return self

    @staticmethod
    def generate_ril(name, mother_pop, n_ind, geno2obs, geno_constraint=[]):
        self = Population(name, mother_pop.map, n_ind, mother_pop, mother_pop)
        mi = lambda: random.choice(xrange(self.mother_pop.n_ind))
        self.individuals = [Individual.ril(i, self, mi(), geno_constraint)
                            for i in xrange(n_ind)]
        self.geno_to_obs = geno2obs
        return self

    def to_str(self, missing_prob):
        if self.geno_to_obs is None:
            return "<Population %s, %i individuals, not observed>" % (
                self.name, len(self.individuals)
            )
        self.geno_to_obs[''] = '-'
        array = [
                    ['data type', self.name],
                    [str(len(self.individuals)), str(len(self.map)), '0 0']
                ] + [
                    ['*' + m, ''.join('-' if toss_coin(missing_prob) else self.geno_to_obs[x]
                                      for x in self.observations[i])]
                    for (i, m) in enumerate(self.map)]
        return '\n'.join(' '.join(l) for l in array)

    def __str__(self):
        return self.to_str(0)

    __repr__ = __str__


class TraitDef(object):

    def __init__(self, name, n_qtl, genmap,
                 parent_effects, dominance, epistasis,
                 noise_gain, noise_mu, noise_sigma):
        self.name = name
        self.qtls = random.sample(genmap, n_qtl)
        self.qtl_idx = tuple(pop.map.index(q) for (q, p) in self.qtls)

    def get_obs(self, pop, ind):
        return [ind.observations[q] for q in self.qtl_idx]

    def effect(self, obs):
        additive_effect = 0
        for q, o in izip(self.parent_effects, obs):
            for a in o:
                additive_effect += q[a]
            additive_effect += self.dominance[''.join(sorted(o))]


class Trait(object):

    def __init__(self, name):
        self.noise_gain = 0
        self.noise_mu = 0
        self.noise_sigma = 0
        self.pop = None
        self.qtls = None
        self.qtl_effect = None
        self.qtl_epistasis = None
        self.values = []
        self.name = name

    def init(self, pop, trait_qtls, qtl_effect, qtl_epistasis,
             noise_gain, noise_mu, noise_sigma):
        self.pop = pop
        self.qtls = trait_qtls
        self.qtl_effect = qtl_effect
        self.qtl_epistasis = {}
        #for k, v in qtl_epistasis.iteritems():
        #    split = tuple(ord(c) - ord('0') for c in k)
        #    print "k", k, "split", split
        #    self.qtl_epistasis[split] = v
        self.qtl_epistasis = qtl_epistasis or {}
        self.noise_gain = noise_gain
        self.noise_mu = noise_mu
        self.noise_sigma = noise_sigma
        #print "qtl_effect", qtl_effect
        #print "qtl_epistasis", self.qtl_epistasis
        qtls = [pop.map.index(q) for (q, p) in trait_qtls]
        qtl_obs = lambda obs: [obs[q] for q in qtls]

        def effect(ind):
            obs = qtl_obs(list(ind.observations))
            additive_effect = 0
            for q, o in izip(qtl_effect, obs):
                for a in o:
                    additive_effect += q[a]
            #epistatis_sign = 1
            #count = 0
            #breaks = 0
            #for qq, coef in self.qtl_epistasis.iteritems():
                #order = [(0, 1) for q in qq]
                #print "qq", qq, "coef", coef
                #for o in product(*order):
                    #print "o", o
                    #count += 1
            additive_effect += self.qtl_epistasis.get(''.join(obs), 0)

            return additive_effect

        #effect = lambda obs: [q[o] for (q, o) in izip(qtl_effect, obs)]
        #epistasis_effect = lambda eq, epis: eq and epis or -epis
        #epistasis = lambda obs: [epistasis_effect(o1 == o2, qe)
        #                         for ((o1, o2), qe)
        #                         in izip(combinations(obs, 2), qtl_epistasis)]
        noise = lambda: noise_gain * random.gauss(noise_mu, noise_sigma)
        #trait = lambda obs: sum(effect(obs)) + sum(epistasis(obs)) + noise()
        trait = lambda ind: effect(ind) + noise()
        self.values = map(trait, pop.individuals)
        return self

    def __str__(self):
        return ' '.join(chain(['*' + self.name], imap(str, self.values)))

    __repr__ = __str__


def _list(t, sep="|"):

    def list_impl(x):
        if x == 'None':
            return None
        return map(t, x.split(sep))

    return list_impl


def _dict(tk, tv):

    def dict_impl(x):
        if x == 'None':
            return None
        return dict(imap(lambda (k, v): (tk(k.strip()), tv(v.strip())),
                         (kv.split(':')
                          for kv in x.split(' '))))

    return dict_impl


class Design(object):
    arg_types = {
        'map': (_list(int), _list(float), _list(_list(float, " "))),
        'line': (str, str),
        'lineobs': (str, str, _dict(str, str)),
        'cross': (str, str, str, int, _dict(str, str)),
        'self': (str, str, int, _dict(str, str)),
        'ril': (str, str, int, _dict(str, str)),
        'sib': (str, str, int, _dict(str, str)),
        'trait': (str, _list(str), int, _list(_dict(str, float)),
                  _dict(str, float), float, float, float),
        'qtls': (int,),
    }

    def __init__(self, something=None):
        self.pops = {}
        self.map = None
        self.traits = {}
        if something is not None:
            self.__iadd__(something)
        self.pedigree = []
        self.trait_qtl_assign = {}
        self.missing_gen_prob = 0
        self.missing_trait_prob = 0

    def gen_map(self, n_mark_list, length_list, constraint_list):
        self.map = Map.generate(n_mark_list, length_list, constraint_list)
        return self

    def load_map(self, filename):
        self.map = Map.load(filename)
        return self

    def gen_line(self, name, allele):
        self.pops[name] = Population.generate_line(name, self.map, allele,
                                                   self.pedigree)
        return self

    def gen_lineobs(self, name, allele, geno_to_obs):
        self.pops[name] = Population.generate_line(name, self.map, allele,
                                                   self.pedigree, geno_to_obs)
        return self

    def gen_cross(self, name, p1, p2, n_ind, geno2obs):
        self.pops[name] = Population.generate_crossing(name,
                                                       self.pops[p1],
                                                       self.pops[p2],
                                                       n_ind,
                                                       geno2obs,
                                                       self.pedigree)
        return self

    def gen_self(self, name, p1, n_ind, geno2obs):
        self.pops[name] = Population.generate_selfing(name,
                                                      self.pops[p1],
                                                      n_ind,
                                                      geno2obs,
                                                      self.pedigree)
        return self

    def gen_ril(self, name, p1, n_ind, geno2obs, geno_constraint=[]):
        self.pops[name] = Population.generate_ril(name,
                                                  self.pops[p1],
                                                  n_ind,
                                                  geno2obs,
                                                  geno_constraint)
        return self

    def gen_sib(self, name, p1, n_ind, geno2obs):
        self.pops[name] = Population.generate_sibling(name,
                                                      self.pops[p1],
                                                      n_ind,
                                                      geno2obs)
        return self

    def load_pop(self, filename):
        p = Population.load(filename)
        self.pops[p.name] = p
        return self

    def gen_qtls(self, n_qtl):
        for i in xrange(n_qtl):
            self.map.pick_qtl()
        return self

    def gen_trait(self, name, pops, n_qtl, qtl_effect, qtl_epistasis,
                  noise_gain, noise_mu, noise_sigma):
        if name in self.trait_qtl_assign:
            trait_qtls = self.trait_qtl_assign[name]
        else:
            trait_qtls = random.sample(self.map.qtls, n_qtl)
            self.trait_qtl_assign = trait_qtls
        for pop in pops:
            self.traits[pop + '_' + name] = (
                Trait(name).init(self.pops[pop], trait_qtls, qtl_effect,
                                 qtl_epistasis, noise_gain, noise_mu,
                                 noise_sigma))
        return self

    def __iadd__(self, something):
        if type(something) in (list, tuple):
            map(self.__iadd__, something)
        elif type(something) is file:
            map(self.__iadd__, something.xreadlines())
        elif type(something) is str:
            something = something.strip()
            if len(something) == 0 or something.startswith('#'):
                return self
            toks = map(str.strip, something.split(','))
            #print toks
            if len(toks) == 2 and isfile(toks[1]):
                method = 'load_' + toks[0]
                action = "loaded"
                args = toks[1:]
            else:
                method = 'gen_' + toks[0]
                action = "generated"

                def process_arg(args):
                    #print args
                    return args[0](args[1])

                args = map(process_arg,
                           izip(Design.arg_types[toks[0]], toks[1:]))
            try:
                self.__getattribute__(method)(*args)
            except:
                import traceback
                traceback.print_exc()
                raise Exception(toks[0] + " can't be " + action)
        return self

    @property
    def observed_generations(self):
        return filter(lambda pop: pop.geno_to_obs is not None,
                      self.pops.itervalues())

    def traits_by_generation(self, gen):
        if type(gen) is Population:
            name = gen.name
        else:
            name = gen
        return filter(lambda t: t.pop.name == name, ('-' if toss_coin(self.missing_trait_prob) else t
                                                     for t in self.traits.itervalues()))

    def write(self, prefix):
        f = open(prefix + '.map', 'w')
        print >> f, self.map
        f.flush()
        f.close()
        for g in self.observed_generations:
            f = open(prefix + '_' + g.name + '.gen', 'w')
            print >> f, g.to_str(self.missing_gen_prob)
            f.flush()
            f.close()
            tl = self.traits_by_generation(g.name)
            if len(tl) > 0:
                phenofile = prefix + '_' + g.name + '.phen'
                f = open(phenofile, 'w')
                print >> f, '\n'.join(imap(str, tl))
                f.flush()
                f.close()
        f = open(prefix + '.info', 'w')
        print >> f, datetime.now()
        for p in self.pops.itervalues():
            print >> f, "POP", p.name, len(p.individuals)
        print >> f, "QTL", ' '.join(str(p) for (q, p) in self.map.qtls)
        for t in self.traits.itervalues():
            print >> f, "trait", t.name, "qtl", t.qtls, "effect", t.qtl_effect,
            print >> f, "epistasis", t.qtl_epistasis,
            print >> f, "noise", t.noise_gain, t.noise_mu, t.noise_sigma
        f.flush()
        f.close()
        pedfile = open(prefix + ".ped", 'w')
        #pedfile.write("generation;individual;mother;father\n")
        print >> pedfile, "generation;individual;mother;father"
        for p in self.pedigree:
            print >> pedfile, ';'.join(str(e) for e in p)
        #pedfile.write('\n'.join(';'.join(t) for t in self.pedigree))
        #print self.pedigree.keys()
        #print sorted(self.pedigree.keys())
        #for k in self.pedigree.iterkeys():
        #    g, m, f = self.pedigree[k]
        #    s = ';'.join((g, k, m, f)) + '\n'
        #    print s
        #    pedfile.write(s)
        #print >> pedfile, "generation individual mother father"
        #for p in self.pops.itervalues():
        #    if p.mother_pop is None and p.father_pop is None:
        #        continue
        #    for i in p.individuals:
        #        print >> pedfile, p.name, i.idx, i.mother_idx, i.father_idx
        pedfile.flush()
        pedfile.close()
        return self


class SpellQtl(object):
    root = "/media/Stock/devel/MCQTL/v6/"

    path = root + "src/spell-qtl"

    def __init__(self, name, design, **kw):
        self.tpftac = []
        self.testpos = []
        self.maha = []
        self.ftlod = []
        design.write(name)
        og = design.observed_generations
        tg = filter(lambda g: len(design.traits_by_generation(g.name)) > 0, og)
        if len(tg) == 0:
            print "No qtl generation. Aborting."
            return
        qtl_gen = max(tg).name
        gen_file = name + '_' + qtl_gen + '.gen'
        phen_file = name + '_' + qtl_gen + '.phen'
        map_file = name + '.map'
        parallel = 'parallel' in kw and str(kw['parallel']) or 'auto'
        step = 'step' in kw and str(kw['step']) or '1'
        #print "using qtl generation", qtl_gen
        qtl_pos = min(design.map.qtls)[1]
        qtl_chrom = design.map.chromosome_of(min(design.map.qtls)[0])
        cmd = ' '.join([
            #'gdb-7.6', '--args',
            SpellQtl.path,
            '-n', name,
            '-P', parallel,  # '1',
            '-wd', name + '_tmp_' + str(datetime.now().strftime('%s%f')),
            '-bds', SpellQtl.root + 'data/design-%s.xml' % qtl_gen,
            '-gm', map_file,
            '-mos', SpellQtl.root + 'data/format-ABHCD.xml',
            '-p', 'toto', qtl_gen, phen_file,
            '-m', qtl_gen, gen_file,
            'step', step,
            'estimate-at', qtl_chrom.name + ':' + str(qtl_pos)
        ])
        #'|tee'])
        print cmd
        #os.system(cmd)
        self.output = os.popen(cmd).read()
        #print "INFO<<", open('test.info').readlines()[-1], ">>"
        #print "OUTPUT<<", self.output, ">>"
        if 'FOIRURE' in self.output:
            dirn = "foirure_" + str(datetime.now().strftime('%s%f'))
            os.mkdir(dirn)
            design.write(dirn + '/' + name)
            self.output = []
            print >> open(dirn + '/cmdline', 'w'), cmd
        else:
            tmp = self.output.strip().split('\n')
            qtl = min(design.map.qtls)
            qc = int(qtl[0].split('_')[1]) - 1
            qp = '%.2f' % qtl[1]
            self.output = [
                self.split_ftac(l.strip())
                + (i == qc and (' ' + qp) or ' NA')
                for i, l in enumerate(tmp)
            ]
        #self.output = self.output.strip() + ' %f' % min(design.map.qtls)[1]

    def split_ftac(self, l):
        tmp, ftlod = l.split("FTestLOD")
        tmp, maha = tmp.split("Mahalanobis")
        before, tmp = tmp.split("TESTPOS<<")
        testpos, tmp = tmp.split(">>FTAC<<")
        ftac, after = tmp.split(">>")
        self.testpos.append(testpos)
        self.tpftac.append(ftac)
        self.maha.append(maha)
        self.ftlod.append(ftlod)
        return before + after


if __name__ == '__main__':
    d = Design()
    try:
        d.missing_gen_prob, d.missing_trait_prob = [float(x) for x in sys.argv[sys.argv.index("-m") + 1].split(':')]
    except:
        pass
    try:
        input_file = sys.argv[sys.argv.index("-i") + 1]
        #print "reading from", input_file
        d += open(input_file)
    except Exception, e:
        import traceback
        traceback.print_exc(e)
        #print "reading from stdin"
        d += sys.stdin
    try:
        output_prefix = sys.argv[sys.argv.index("-o") + 1]
        #print "writing data to prefix ", output_prefix
        d.write(output_prefix)
    except:
        pass
    try:
        sys.argv.index("-r")
        #print "running spell-qtl"
        try:
            s = SpellQtl("test", d)
        except:
            import traceback
            traceback.print_exc()
    except:
        pass

#
