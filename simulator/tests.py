from simu import *
import sys
import scipy

d = Design()


def gen_trait(noise_gain):
    global d
    d += """
map,100|100,150|150
line,A,a
line,B,b
cross,F1,A,B,1,None
self,F2,F1,100,None
qtls,1
    """.split('\n')

    d += "self,F3,F2,500,aa:A ab:H ba:H bb:B"
    d += "trait,trait1,F3,1,aa:2 ab:1 ba:1 bb:0,0,%f,0,1" % noise_gain

table = open('results.txt', 'w')

print >> table, " run chrom pos rss thres ftest r2 ftest_at_qtl M0rss",
print >> table, "qtl_pos noise tmean tvar tss"

i = 0
lineno = 0

noise_gain = []
n_runs = []

for noise in xrange(2, 21):
    #noise = noise
    for run in xrange(150):
    #for run in xrange(1):
        gen_trait(noise)
        s = SpellQtl("test", d).output
        i += 1
        trait = d.traits["trait1"]
        mean = scipy.mean(trait.values)
        var = scipy.var(trait.values)
        tss = var * len(trait.values)
        for l in s:
            lineno += 1
            print >> table, lineno, i, l.strip(), noise, mean, var, tss
        table.flush()
