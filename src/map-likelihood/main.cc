#include "io/error.h"
#include "data/geno_matrix.h"
#include "io/output.h"
#include "map-likelihood/cli.h"

#ifndef SPELL_MAP_MAIN
#  define SPELL_MAP_MAIN main
#endif

int SPELL_MAP_MAIN(int argc, const char** argv)
{
    gamete_LV_database gamete_LV;
    auto settings = mapqa_settings_t::from_args(argc, argv);
    
    if (!settings) {
        print_usage_pedigree();
        return -1;
    }
    {
        ifile gam(SPELL_STRING(settings->work_directory << "/" << settings->name << ".cache/gamete.data"));
        rw_base() (gam, gamete_LV);
    }
    MSG_DEBUG("Have gamete_LV");
    MSG_DEBUG(gamete_LV.data);
    MSG_DEBUG("Marker order: " << settings->group.raw.marker_name);
    MSG_DEBUG("Distances: " << settings->group.raw.marker_locus);
    
    const auto& locvec = settings->group.raw.marker_locus;
    const auto& nvec = settings->group.raw.marker_name;
//     double lh = gamete_LV.map_likelihood(nvec, locvec);
//     MSG_DEBUG("Computed likelihood (log): " << lh);
//     MSG_DEBUG("Computed likelihood (log10): " << (lh / log(10.)));
    
    
    auto map = gamete_LV.EM(settings->group.raw.marker_name);
    MSG_DEBUG("n_iter " << map.n_iterations);
    MSG_DEBUG("distances: " << map.distances);

    return 0;
}

#include "io/output_impl.h"
