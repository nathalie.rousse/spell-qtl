/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "eigen.h"
#include "input.h"
//#include "settings.h"
#include <set>
#include <map>
#include <algorithm>
#include "io/banner.h"
//#include "labelled_matrix.h"
//#include "cache2.h"
#include "model.h"
#include "computations/base.h"
#include "computations/basic_data.h"
#include "computations/probabilities.h"

#include <regex>

#include <cerrno>  /* for errno */
#include <cstring> /* for strerror */

extern "C" {
#include <ftw.h>
}

#define OUT(__x__) do { msg_handler_t::cout() << __x__; } while (0)

/*#include "computations.h"*/

/*namespace read_data {*/
/*settings_t* read_settings(std::istream&) {*/
#if 0
    if (!is.good()) {
        throw input_exception(is, "couldn't read from file");
    }
    return check_consistency(settings_dtd.parse(is));
#endif
    /*return NULL;*/
/*}*/
/*}*/


static const char* prg_name = NULL;


template <typename T>
T to(const std::string& x)
{
    std::stringstream s(x);
    T ret;
    s >> ret;
    return ret;
}


#define VCCI std::vector<const char*>::iterator
#define CALLBACK_ARGS settings_t*& target, VCCI& ai, VCCI& aj
#define SAFE_IGNORE_CALLBACK_ARGS do { (void)target; (void)ai; (void)aj; } while(0)

typedef std::function<void(CALLBACK_ARGS)> argument_parser_callback_type;


extern int MAGIC8_ORDER;
extern int RIL_ORDER;

locus_probabilities_type
locus_probabilities(const context_key& ck, const locus_key& lk,
                    int ind,
                    const std::vector<double>& loci);

struct default_value_display {
    std::string str;

    default_value_display()
        : str()
    {}

    default_value_display(bool req)
        : str()
    {
        std::stringstream ss;
        ss << WHITE << (req ? "MANDATORY" : "Optional") << NORMAL;
        str = ss.str();
    }

    default_value_display(const char* s)
        : str()
    {
        std::stringstream ss;
        ss << "Default: " << WHITE << s << NORMAL;
        str = ss.str();
    }

    default_value_display(default_value_display&& dvd)
        : str(std::forward<std::string>(dvd.str))
    {}

    default_value_display(const default_value_display& dvd)
        : str(dvd.str)
    {}

    template <typename C, typename X>
    default_value_display(X C::* x)
        : str()
    {
        C s;
        std::stringstream ss;
        ss << "Default: " << WHITE << model_print::to_string(s.*x) << NORMAL;
        str = ss.str();
    }

    template <typename CX, typename X, typename CY, typename Y>
    default_value_display(X CX::* x, Y CY::* y)
        : str()
    {
        CX cx;
        CY cy;
        std::stringstream ss;
        ss << "Default: " << WHITE << model_print::to_string(cx.*x) << NORMAL << ", " << WHITE << model_print::to_string(cy.*y) << NORMAL;
        str = ss.str();
    }

    template <typename K>
    default_value_display(const K* var)
        : str()
    {
        std::stringstream ss;
        ss << "Default: " << WHITE << model_print::to_string(*var) << NORMAL;
        str = ss.str();
    }
};



struct argument {
    std::vector<std::string> names;
    std::vector<std::string> parameters;
    std::string description;
    bool hidden;
    default_value_display dvd;
    argument_parser_callback_type callback;

    argument(std::vector<std::string>&& n,
             std::vector<std::string>&& p,
             std::string&& d,
             bool h,
             default_value_display&& dv,
             argument_parser_callback_type c)
        : names(std::forward<std::vector<std::string>>(n))
        , parameters(std::forward<std::vector<std::string>>(p))
        , description(std::forward<std::string>(d))
        , hidden(h)
        , dvd(std::forward<default_value_display>(dv))
        , callback(std::forward<argument_parser_callback_type>(c))
    {}

    void install(std::map<std::string, argument_parser_callback_type>& amap,
                 std::map<std::string, int>& pmap) const
    {
        for (auto& n: names) {
            amap[n] = callback;
            pmap[n] = parameters.size();
        }
    }
};


settings_t* ensure(settings_t*& target)
{
    if (target == NULL) {
        target = new settings_t();
    }
    return target;
}

void destroy(settings_t*& target)
{
    if (target != NULL) {
        delete target;
        target = NULL;
    }
}


struct argument_section {
    std::string title;
    std::string description;
    bool hidden;
    std::vector<argument> arguments;
};

typedef std::vector<argument_section> argument_section_list_t;

std::string last_pop_name = "";

#if 0
pop_data_type& last_pop(settings_t* target)
{
    if (last_pop_name == "") {
        MSG_ERROR("No population has been defined yet.", "Use '-p pop_name qtl_gen traits_file' before any other population-related argument");
    }
    return target->populations[last_pop_name];
}
#endif

void
read_locus_list(std::string& s, settings_t* target)
{
    std::map<const chromosome*, std::vector<double>>& ll = target->estimation_loci;
//     std::string token;
    auto i = s.cbegin();
    auto j = s.cend();
    size_t locus_end, colon;
    while (i != j) {
        locus_end = s.find_first_of(',');
        if (locus_end == std::string::npos) {
            locus_end = j - i;
        }
        colon = s.find_first_of(':');
        if (colon == std::string::npos) {
            colon = j - i;
        }
        if (locus_end <= colon) {
            MSG_ERROR("Invalid locus around \"" << std::string(i, i + locus_end) << "\": Locus format is chromosome_name:position", "Specify loci in the correct format");
        }
        std::string name(i, i + colon);
        const chromosome* chr = target->find_chromosome(name);
        if (!chr) {
            MSG_ERROR("Chromosome \"" << name << "\" doesn't exist.", "Use correct chromosome names");
        } else {
            double locus = to<double>(std::string(i + colon + 1, i + locus_end));
            ll[chr].push_back(locus);
        }
        i += locus_end;
        i += (i != j);  /* skip comma if not at end */
    }
    for (auto& kv: ll) {
        std::sort(kv.second.begin(), kv.second.end());
    }
}


void
read_threshold_values(const std::string& s, settings_t* target)
{
    std::map<std::string, double>& ql = target->qtl_thresholds;
    std::string token;
    auto i = s.cbegin();
    auto j = s.cend();
    size_t locus_end, colon;
    while (i != j) {
        locus_end = s.find_first_of(',');
        if (locus_end == std::string::npos) {
            locus_end = j - i;
        }
        colon = s.find_first_of('=');
        if (colon == std::string::npos) {
            colon = j - i;
        }
        if (locus_end <= colon) {
            MSG_ERROR("Invalid single_trait threshold around \"" << std::string(i, i + locus_end) << "\": Trait threshold format is trait_name=value", "Specify single_trait thresholds in the correct format");
        }
        std::string name(i, i + colon);
        ql[name] = to<double>(std::string(i + colon + 1, i + locus_end));
        i += locus_end;
        i += (i != j);  /* skip comma if not at end */
    }
}


std::vector<std::string>
cut(const std::string& s, int width)
{
    std::vector<std::string> ret;
    auto i = s.begin();
    auto z = s.end();
    while (i != z) {
        auto j = z;
        if ((z - i) >= width) {
            j = i + width;
#if 0
        } else {
            ret.push_back(std::string(i, z));
            return ret;
            /*j = z;*/
#endif
        }
        auto last = j;
        auto k = i;
        for (; k != j; ++k) {
            if (*k == ' ') {
                last = k;
            } else if (*k == '\n') {
                last = k;
                break;
            }
        }
        if (k == z) {
            last = z;
        }
        ret.push_back(std::string(i, last));
        /*OUT(ret.back() << std::endl);*/
        i = last + (last != z);
    }
    return ret;
}

settings_t* read_settings(const std::string& filename, ifile& is);
//design_type* read_design(const std::string& filename, std::istream& is);
/*format_specification_t* read_format(const std::string& filename, std::istream& is);*/
void read_format(std::map<std::string, marker_observation_spec>& settings, const std::string& filename, std::istream& is);

void print_usage_impl(bool show_hidden, const argument_section_list_t& aslt)
{
    int max_cols = msg_handler_t::termcols();
    int opt_width = 0;
    int doc_width = 0;
    if (max_cols <= 0) {
        max_cols = 80;
    }
    settings_t tmp;
    OUT("Usage: " << WHITE << prg_name << " [arguments...]" << NORMAL << std::endl);
    OUT("Arguments:" << std::endl);
    for (auto& kv: aslt) {
        if (kv.hidden && !show_hidden) {
            continue;
        }
        for (auto& a: kv.arguments) {
            if (a.hidden && !show_hidden) {
                continue;
            }
            auto ni = a.names.begin(), nj = a.names.end();
            int width = 0;
            if (ni != nj) {
                width = ni->size();
                for (++ni; ni != nj; ++ni) {
                    width += 1 + ni->size();
                }
            }
            for (auto& par: a.parameters) {
                width += 3 + par.size();
            }
            if (width > opt_width) {
                opt_width = width;
            }
        }
    }
    opt_width += 2;
    doc_width = max_cols - opt_width;
    for (auto& kv: aslt) {
        if (kv.hidden && !show_hidden) {
            continue;
        }
        OUT(std::endl << CYAN <<  "* " << kv.title << NORMAL << std::endl);
        if (kv.description.size() > 0) {
            for (const auto& line: cut(kv.description, max_cols - 2)) {
                OUT("  " << line << std::endl);
            }
        }
        for (auto& a: kv.arguments) {
            if (a.hidden && !show_hidden) {
                continue;
            }
            auto ni = a.names.begin(), nj = a.names.end();
            std::stringstream s;
            s << WHITE << (*ni);
            ++ni;
            for (; ni != nj; ++ni) { s << NORMAL << ',' << WHITE << (*ni); }
            s << NORMAL;
            for (auto& par: a.parameters) { s << " <" << YELLOW << par << NORMAL << '>'; }
            /* find max length under 30 without cutting words */
            std::vector<std::string> lines = cut(a.description, doc_width);
            auto i = lines.begin(), j = lines.end();
            OUT(std::setw(opt_width) <<  "" << (*i));
            OUT('\r' << s.str() << std::endl);
            for (++i; i != j; ++i) {
                OUT(std::setw(opt_width) << "" << (*i) << std::endl);
            }
            if (a.dvd.str.size()) {
                lines = cut(a.dvd.str, doc_width);
                i = lines.begin();
                j = lines.end();
                OUT(std::setw(opt_width) <<  "" << (*i) << std::endl);
                for (++i; i != j; ++i) {
                    OUT(std::setw(opt_width) << "" << (*i) << std::endl);
                }
            }
        }
    }
}

void print_usage(bool);

#define is_hex(_c) (((_c) >= 'a' && (_c) <= 'f') || ((_c) >= '0' && (_c) <= '9') || ((_c) >= 'A' && (_c) <= 'F'))

extern "C" {
int clear_cache(const char *fpath, const struct stat *, int typeflag, struct FTW*)
{
    /* FIXME check for some magic bytes or something before deleting! */
    static std::regex is_cache_file("^.*/[a-f0-9]{1,16}$");
    /*std::string f(fpath);*/
    if (typeflag == FTW_F) {  /* ignore all but regular files */
        /*MSG_DEBUG("on " << fpath);*/
        if (std::regex_match(fpath, fpath + strlen(fpath), is_cache_file)) {
            /*remove(fpath);*/
            if (remove(fpath) != 0) {
                MSG_WARNING("Couldn't remove file \"" << fpath << "\": " << strerror(errno));
            /*} else {*/
                /*MSG_DEBUG("Deleted cache file <" << fpath << '>');*/
            }
        }
    } else if (typeflag == FTW_D) {
        size_t x = strlen(fpath) - 3;
        if (fpath[x] == '/' && is_hex(fpath[x + 1]) && is_hex(fpath[x + 2])) {
            if (remove(fpath) != 0) {
                MSG_WARNING("Couldn't remove directory \"" << fpath << "\": " << strerror(errno));
            }
        }
    }
    return 0;
}
}

static
argument_section_list_t
arguments = {
    {"Advanced (NOT INTENDED FOR USERS!)", "", true, {
#if 0
        {{"-Dd", "--dump-design"},
            {},
            "[DEBUG] Dump breeding design and exit",
            true,
            {},
            [](CALLBACK_ARGS)
            {
                OUT((*ensure(target)->design) << std::endl);
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
        {{"-Dg", "--dump-generation"},
            {"gen"},
            "[DEBUG] Dump one generation and exit",
            true,
            {},
            [](CALLBACK_ARGS)
            {
                std::string name = *++ai;
                auto it = ensure(target)->design->generation.find(name);
                if (it == ensure(target)->design->generation.end()) {
                    MSG_ERROR("Generation " << name << " not found.", "");
                    exit(0);
                }
                const generation_rs* gen = it->second;
                OUT((*gen) << std::endl);
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
#endif
        {{"-Dpop", "--dump-POP"},
            {},
            "[DEBUG] Dump parental origin probabilities and exit",
            true,
            {},
            [](CALLBACK_ARGS)
            {
                ensure(target);
                active_settings = target;

                /* TODO: FIXME */
#if 0
                msg_handler_t::check(true);
                active_settings->finalize();
                active_settings->set_title("Checking the validity of the configuration");
                if (!active_settings->sanity_check()) {
                    exit(-1);
                }
                msg_handler_t::check(true);
                locus_key lk;
                Eigen::IOFormat fmt(StreamPrecision, DontAlignCols, "\t", "\n", "", "", "", "");
                for (const chromosome& c: active_settings->map) {
                    auto loci = compute_steps(c.condensed.marker_locus, target->step);
                    for (const auto& kv: active_settings->populations) {
                        const auto& pop = kv.second;
                        context_key ck(new context_key_struc(&pop, &c, loci));
                        auto vck = as_value(ck);
                        /*collection<multi_generation_observations>*/
                            /*vmgo = make_collection<Disk>(population_marker_obs,*/
                                                         /*vck, range<int>(0, ck->pop->size(), 1));*/
                        collection<locus_probabilities_type>
                            alp = make_collection<Disk>(locus_probabilities,
                                    vck, as_value(lk), range<int>(0, ck->pop->size(), 1), as_value(ck->loci));
                        OUT("CHROMOSOME " << c.name << " POPULATION " << kv.first << std::endl);
                        int i = 0;
                        for (const auto& pm: alp) {
                            OUT('#' << (++i) << std::endl << pm->transpose() << std::endl);
                        }
                    }
                }
#endif
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
#if 0
        {{"-Dstfopom", "--dump-stfopom"},
            {"gen", "order"},
            "[DEBUG] Dump ST(fo)POM",
            true,
            {},
            [](CALLBACK_ARGS)
            {
                std::string name = *++ai;
                auto it = ensure(target)->design->generation.find(name);
                if (it == ensure(target)->design->generation.end()) {
                    MSG_ERROR("Generation " << name << " not found.", "");
                    exit(0);
                }
                int order = to<int>(*++ai);
                locus_key lk(new locus_key_struc());
                population pop;
                pop.qtl_generation_name = name;
                chromosome chr;
                active_settings = target;  /* because context_key_struc needs it :( */
                context_key ck(new context_key_struc(&pop, &chr, std::vector<double>()));
                int o = order + 1;
                int unit = o % 10;

                while (order > 0) {
                    lk = lk + (double) (order--);
                }

                auto sd = get_stpom_data(ck, lk);
                auto stfopom = compute_state_to_parental_origin(ck, lk);

                OUT("State to " << o << (unit == 1 ? "st" : unit == 2 ? "nd" : unit == 3 ? "rd" : "th") << " order parental origin matrix" << std::endl);
                OUT("Per-haplotype matrices" << std::endl << sd << std::endl);
                OUT("Full matrix" << std::endl << stfopom << std::endl);

                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
#endif
        {{"--verbose-help"},
            {},
            "Display COMPLETE usage and exit",
            true,
            {},
            [](CALLBACK_ARGS)
            {
                print_usage(true);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
    }},
    {"Miscellaneous", "", false, {
        {{"-c", "--counters"},
            {},
            "Display counters and timers when exiting",
            true,
            {"not displayed"},
            [](CALLBACK_ARGS)
            {
                chrono::display() = true;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-v", "--version"},
            {},
            "Display version and exit",
            false,
            {},
            [](CALLBACK_ARGS)
            {
                OUT(BANNER << std::endl << std::endl);
                exit(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-h", "--help"},
            {},
            "Display usage and exit",
            false,
            {},
            [](CALLBACK_ARGS)
            {
                print_usage(false);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-n", "--name"},
            {"name"},
            "User-friendly name for this configuration",
            false,
            {true},
            [](CALLBACK_ARGS)
            {
                ensure(target)->name = *++ai;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-N", "--notes"},
            {"text"},
            "Optional free text",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                ensure(target)->notes = *++ai;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-wd", "--work-directory"},
            {"path"},
            "Path to directory for cache files and outputs",
            false,
            {&settings_t::work_directory},
            [](CALLBACK_ARGS) {
                ensure(target)->work_directory = *++ai;
                if (!check_file(ensure(target)->work_directory, true, true, false)) {
                    ensure_directories_exist(ensure(target)->work_directory);
                    check_file(ensure(target)->work_directory, true, true, true);
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-P", "--parallel"},
            {"n_cores"},
            "Setup parallel computations (number of cores to use or 'auto')",
            false,
            {&settings_t::parallel},
            [](CALLBACK_ARGS) {
                /*ensure(target)->parallel = to<int>(*++ai);*/
                ensure(target)->set_parallel(*++ai);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

//        {{"-i", "--input-XML"},
//            {"path"},
//            "Read configuration from this XML file",
//            false,
//            {},
//            [](CALLBACK_ARGS) {
//                std::string filename = *++ai;
//                if (target) {
//                    MSG_WARNING("Reading configuration from XML overrides previous command-line arguments");
//                    delete target;
//                }
//                check_file(filename, false, false);
//                MSG_INFO("Processing " << filename);
//                ifile ifs(filename);
//                target = read_settings(filename, ifs);
//                if (!target) {
//                    MSG_ERROR("Couldn't load XML file " << filename, "");
//                }
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},
//
//        {{"-o", "--output-XML"},
//            {"path"},
//            "Output configuration to this XML file",
//            false,
//            {},
//            [](CALLBACK_ARGS) {
//                const char* path = *++ai;
//                if (std::string("-") == path) {
//                    OUT((*ensure(target)));
//                } else {
//                    ofile o(path);
//                    o << (*ensure(target));
//                }
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},

        {{"--clean"},
            {},
            "Clears all cached files",
            false,
            {},
            [](CALLBACK_ARGS) {
                const std::string& wd = ensure(target)->work_directory;
                if (wd == "") {
                    return;
                }
                MSG_DEBUG("Clearing all files in work directory " << wd);
                std::string cache_path = SPELL_STRING(wd << '/'<< ensure(target)->name << ".cache");
                nftw(cache_path.c_str(), clear_cache, 10, FTW_DEPTH);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-a", "--ansi"},
            {},
            "Use ANSI escape sequences to display colors and realtime progress information at the top of the terminal",
            false,
            {"enabled if output on a terminal"},
            [](CALLBACK_ARGS) {
                msg_handler_t::set_color(1);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
        {{"-na", "--no-ansi"},
            {},
            "Don't use ANSI escape sequences, don't display colors or realtime progress information",
            false,
            {"see -a"},
            [](CALLBACK_ARGS) {
                msg_handler_t::set_color(0);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-rj", "--join-radius"},
            {"distance"},
            "Specify the maximum distance from a selected locus to compute joint probabilities. TODO: fix this doc.",
            false,
            {&settings_t::Rjoin},
            [](CALLBACK_ARGS)
            {
                ensure(target)->Rjoin = to<double>(*++ai);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-rs", "--skip-radius"},
            {"distance"},
            "Specify the maximum distance from a selected locus to skip tests. TODO: fix this doc.",
            false,
            {&settings_t::Rskip},
            [](CALLBACK_ARGS)
            {
                ensure(target)->Rskip = to<double>(*++ai);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},


#if 0
        {{"-ro", "--RIL-order"},
            {"order"},
            "Specify the precision for RIL populations (order of the Taylor series).",
            false,
            {&RIL_ORDER},
            [](CALLBACK_ARGS)
            {
                int order = to<int>(*++ai);
                if (order <= 1 || order > 25) {
                    MSG_ERROR("Invalid value for order: " << order, "The order parameter must be an integer between 2 and 25, both included.");
                } else {
                    RIL_ORDER = order;
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-m8o", "--MAGIC-8-order"},
            {"order"},
            "Specify the precision for MAGIC-8 populations (order of the Taylor series).",
            false,
            {&MAGIC8_ORDER},
            [](CALLBACK_ARGS)
            {
                int order = to<int>(*++ai);
                if (order <= 1 || order > 25) {
                    MSG_ERROR("Invalid value for order: " << order, "The order parameter must be an integer between 2 and 25, both included.");
                } else {
                    MAGIC8_ORDER = order;
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
#endif
    }},

//    {"Input files", "The following are used to specify the various input files. They are ALL compulsory.", false, {
//        {{"-gm", "--genetic-map"},
//            {"path"},
//            "Path to the genetic map file",
//            false,
//            {true},
//            [](CALLBACK_ARGS)
//            {
//                ensure(target)->map_filename = *++ai;
//                ifile ifs(ensure(target)->map_filename);
//                ensure(target)->map = read_data::read_map(ifs);
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},
//
//#if 0
//        {{"-bds", "--breeding-design"},
//            {"path"},
//            "Path to the breeding design specification file",
//            false,
//            {true},
//            [](CALLBACK_ARGS)
//            {
//                std::string filename = *++ai;
//                check_file(filename, false, false);
//                /*ensure(target)->design_filename = *++ai;*/
//                ifile ifs(filename);
//                ensure(target)->design = read_design(filename, ifs);
//                if (ensure(target)->design) {
//                    ensure(target)->design->filename = filename;
//                } else {
//                    MSG_ERROR("Couldn't read breeding design file", "");
//                }
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},
//
//        {{"-ot", "--observation-type"},
//            {"type"},
//            "Specify how to interpret the observation data.\nIf type is 'line'" /* TODO */,
//            false,
//            {&settings_t::observation_type},
//            [](CALLBACK_ARGS)
//            {
//                /* TODO */
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},
//
//        {{"-mos", "--marker-observations-spec"},
//            {"path"},
//            "Path to the marker observation specification file",
//            false,
//            {true},
//            [](CALLBACK_ARGS)
//            {
//                std::string filename = *++ai;
//                check_file(filename, false, false);
//                /*ensure(target)->marker_observation_specs_filename = *++ai;*/
//                ifile ifs(filename);
//                /*ensure(target)->marker_observation_specs = read_format(filename, ifs);*/
//                read_format(ensure(target)->marker_observation_specs, filename, ifs);
//                ensure(target)->marker_observation_specs_filenames.push_back(filename);
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},
//#endif
//    }},

    {"Input datasets", "The following specify the datasets you want processed.\nA dataset specification starts with argument -p, followed by one or more arguments -m.\nArguments -l, -e, and -z are non-requisite and may appear anywhere after -p.", false, {
        {{"-gm", "--genetic-map"},
            {"path"},
            "Path to the genetic map file",
            false,
            {true},
            [](CALLBACK_ARGS)
            {
                ensure(target)->map_filename = *++ai;
                ifile ifs(ensure(target)->map_filename);
                ensure(target)->map = read_data::read_map(ifs);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-p", "--population"},
         {"QTL generation name", "data file path"},
         "Specify a new population (dataset) to work on.",
         false,
         {true},
         [](CALLBACK_ARGS)
         {
             std::string qtl_gen_name = *++ai;
             std::string observed_traits_filename = *++ai;
             ensure(target)->datasets.emplace_back(qtl_gen_name, observed_traits_filename);
             SAFE_IGNORE_CALLBACK_ARGS;
         }},

//        {{"-c", "--covariable"},
//         {"QTL generation name", "data file path"},
//         "Specify a new covariable or set of covariables to use as static column(s) in the model. The data file format is the same as the traits file format.",
//         false,
//         {true},
//         [](CALLBACK_ARGS)
//         {
//             std::string qtl_gen_name = *++ai;
//             std::string observed_traits_filename = *++ai;
//             ensure(target)->covariables.emplace_back(qtl_gen_name, observed_traits_filename);
//             SAFE_IGNORE_CALLBACK_ARGS;
//         }},
#if 0
        {{"-m", "--observed-markers"},
            {"gen:format", "path"},
            "Path to the marker observations file of generation 'gen' with given format for the last declared population",
            false,
            {true},
            [](CALLBACK_ARGS)
            {
                population& p = last_pop(ensure(target));
                std::string gen_and_format = *++ai;
                std::string filename(*++ai);
                size_t colon = gen_and_format.find_first_of(':');
                if (colon == std::string::npos) {
                    MSG_ERROR("No format was specified for the observations of generation " << gen_and_format << " in population " << p.name, "Specify observations format name along with the generation");
                    return;
                }
                std::string gen_name(gen_and_format.cbegin(), gen_and_format.cbegin() + colon);
                std::string format_name(gen_and_format.cbegin() + 1 + colon, gen_and_format.cend());
                if (!ensure(target)->design->generation[gen_name]) {
                    MSG_ERROR("Generation '" << gen_name << "' isn't defined.", "Specify a generation name that is defined in the breeding design XML file");
                }
                if (0) {
                    const auto& mos = ensure(target)->marker_observation_specs;
                    if (mos.find(format_name) == mos.end()) {
                        MSG_ERROR("Marker observation format '" << format_name << "' isn't defined.", "");
                    }
                }
                size_t colon1 = filename.find_first_of(':');
                int start = -1;
                int end = -1;
                if (colon1 != std::string::npos) {
                    size_t colon2 = filename.find_first_of(':', colon1 + 1);
                    if (colon2 != std::string::npos) {
                        std::stringstream s1(filename.substr(colon1 + 1, colon2 - colon1 - 1));
                        std::stringstream s2(filename.substr(colon2 + 1));
                        s1 >> start;
                        s2 >> end;
                    } else {
                        std::stringstream s1(filename.substr(colon1 + 1));
                        s1 >> start;
                        end = start;
                    }
                    filename.resize(colon1);
                }
                if (check_file(filename, false, false)) {
                    ifile ifs(filename);
                    p.observed_mark[gen_name] = {filename, format_name, 0,
                        read_data::read_marker(ifs, start, end), {}};
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-z", "--noise"},
            {"value"},
            "Noise value for the marker observations in [0,0.5]",
            false,
            {&population::noise},
            [](CALLBACK_ARGS)
            {
                last_pop(ensure(target)).noise = to<double>(*++ai);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"-e", "--pedigree"},
            {"path"},
            "Path to the pedigree file for the last declared population",
            false,
            {false},
            [](CALLBACK_ARGS)
            {
                std::string filename(*++ai);
                ifile ifs(filename);
                last_pop(ensure(target)).pedigree
                    = read_pedigree(ensure(target)->design, filename, ifs);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
#endif
//        {{"-l", "--ld"},
//            {"generation_name", "path"},
//            "Path to the LD file for the last declared population",
//            false,
//            {false},
//            [](CALLBACK_ARGS)
//            {
//                std::string qtl_gen(*++ai);
//                std::string filename(*++ai);
//                ifile ifs(filename);
//                read_ld(ensure(target), qtl_gen, filename, ifs);
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},
    }},

    {"Model options", "The following configures the construction of the linear model.", false, {
        {{"connected"},
            {},
            "Select connected mode",
            false,
            {"disconnected"},
            [](CALLBACK_ARGS)
            {
                ensure(target)->connected = true;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

//        {{"max-order"},
//            {"order"},
//            "Maximum block order allowed in the model",
//            false,
//            {1},
//            [](CALLBACK_ARGS)
//            {
//                ensure(target)->max_order = to<size_t>(*++ai);
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},

//        {{"epistasis"},
//            {},
//            "Detect epistasis",
//            false,
//            {"don't detect epistasis"},
//            [](CALLBACK_ARGS)
//            {
//                ensure(target)->epistasis = true;
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},

//        {{"pleiotropy"},
//            {"tolerance"},
//            "Detect pleiotropic QTLs",
//            false,
//            {"don't detect pleiotropic QTLs"},
//            [](CALLBACK_ARGS) {
//                ensure(target)->pleiotropy = true;
//                ensure(target)->pleiotropy_tolerance = to<double>(*++ai);
//                SAFE_IGNORE_CALLBACK_ARGS;
//            }},
    }},

    {"Working set options", "The following configures the analysis domain.", false, {
        {{"lg"},
         {"linkage group names"},
         "Specify the list of linkage groupe to study",
         false,
         {false},
         [](CALLBACK_ARGS)
         {
             std::istringstream iss(*++ai);
             std::string name;
             std::vector<std::string>& lg = ensure(target)->with_lg;
             while (std::getline(iss, name, ',')) {
                 lg.push_back(name);
             }
         }},
        {{"covar"},
         {"covariable names"},
         "Specify the list of covariables to put in the model",
         false,
         {false},
         [](CALLBACK_ARGS)
         {
             std::istringstream iss(*++ai);
             std::string name;
             std::vector<std::string>& covars = ensure(target)->with_covariables;
             while (std::getline(iss, name, ',')) {
                 covars.push_back(name);
             }
         }},
        {{"traits"},
         {"trait names"},
         "Specify the list of traits to analyse",
         false,
         {false},
         [](CALLBACK_ARGS)
         {
             std::istringstream iss(*++ai);
             std::string name;
             std::vector<std::string>& with_traits = ensure(target)->with_traits;
             while (std::getline(iss, name, ',')) {
                 with_traits.push_back(name);
             }
         }},
        {{"pleiotropy"},
         {"pleiotropic trait name", "tolerance", "trait names"},
         "Specify a pleiotropic trait. This trait will be added to the list of traits to analyze. TODO: explain tolerance.",
         false,
         {false},
         [](CALLBACK_ARGS)
         {
             ensure(target)->pleiotropic_traits_descr.emplace_back();
             pleiotropy_descr& pleio = ensure(target)->pleiotropic_traits_descr.back();
             pleio.name = *++ai;
             pleio.tolerance = to<double>(*++ai);
             std::istringstream iss(*++ai);
             std::string name;
             while (std::getline(iss, name, ',')) {
                 pleio.traits.push_back(name);
             }
         }},
    }},

    {"Processing options", "The following configures the QTL analysis.\nThe standard pipeline is:\n - skeleton creation\n - cofactors detection\n - QTLs detection\n - effects estimation", false, {
        {{"output-nppop"},
         {},
         "Output the n-point POP for the given datasets and exit",
         false,
         {false},
         [](CALLBACK_ARGS)
         {
//             auto& list = ensure(target)->npoint_gen;
//             std::istringstream iss(*ai++);
//             std::string gen;
//             while (std::getline(iss, gen, ',')) {
//                 list.push_back(gen);
//             }
             ensure(target)->output_npoint = true;
             SAFE_IGNORE_CALLBACK_ARGS;
         }},
        {{"qtl-threshold-permutations"},
            {"value"},
            "Set the number of permutations to compute the QTL threshold value in automatic mode",
            false,
            {&settings_t::n_permutations},
            [](CALLBACK_ARGS)
            {
                ensure(target)->n_permutations = to<int>(*++ai);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"qtl-threshold-quantile"},
            {"value"},
            "Set the quantile value in range [0:1] to select the QTL threshold value in automatic mode",
            false,
            {&settings_t::qtl_threshold_quantile},
            [](CALLBACK_ARGS)
            {
                ensure(target)->qtl_threshold_quantile = to<double>(*++ai);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"qtl-threshold-value"},
            {"single_trait=value,..."},
            "Set the QTL threshold value manually for some traits",
            false,
            {"automatic"},
            [](CALLBACK_ARGS)
            {
                read_threshold_values(*++ai, ensure(target));
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"cofactor-threshold"},
            {"single_trait=value,..."},
            "Set the cofactor threshold value manually for some traits",
            false,
            {"automatic; value of QTL threshold * .9"},
            [](CALLBACK_ARGS)
            {
                OUT("TODO" << std::endl);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"cofactor-exclusion-window"},
            {"distance"},
            "Set the half-size (in cM) of the exclusion window around cofactors. No detection will be performed inside this window.",
            false,
            {&settings_t::cofactor_exclusion_window_size},
            [](CALLBACK_ARGS)
            {
                ensure(target)->cofactor_exclusion_window_size = to<double>(*++ai);
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"step"},
         {"value"},
         "Step size in cM",
         false,
         {&settings_t::step},
         [](CALLBACK_ARGS)
         {
             ensure(target)->step = to<double>(*++ai);
             SAFE_IGNORE_CALLBACK_ARGS;
         }},

        {{"lod-support"},
         {"inner value", "outer value"},
         "LOD support value",
         false,
         {&settings_t::lod_support_inner, &settings_t::lod_support_outer},
         [](CALLBACK_ARGS)
         {
             ensure(target)->lod_support_inner = to<double>(*++ai);
             ensure(target)->lod_support_outer = to<double>(*++ai);
             SAFE_IGNORE_CALLBACK_ARGS;
         }},

#if 0
        {{"estimate-at"},
            {"locus_list"},
            "Request model estimation at given loci (comma-delimited list of chromosome:position)",
            false,
            [](CALLBACK_ARGS)
            {
                std::string locus_list(*++ai);
                read_locus_list(locus_list, ensure(target));
                SAFE_IGNORE_CALLBACK_ARGS;
            }}
#endif
        {{"skeleton"},
            {"mode", "marker,... OR distance"},
            "Setup the cofactor detection skeleton. Mode can be either 'manual', 'auto' or 'none'.\nIf 'manual', specify a comma-separated marker list.\nIf 'auto', specify the minimum interval between markers in cM.",
            false,
            {&settings_t::skeleton_mode, &settings_t::skeleton_interval},
            [](CALLBACK_ARGS)
            {
                std::string mode(*++ai);
                std::string list_or_dist(*++ai);
                ensure(target)->skeleton_mode = mode;
                if (mode == "manual") {
                    std::istringstream iss(list_or_dist);
                    std::string marker;
                    std::vector<std::string>& marker_list = ensure(target)->skeleton_markers;
                    while (std::getline(iss, marker, ',')) {
                        marker_list.push_back(marker);
                    }
                } else if (mode == "auto") {
                    ensure(target)->skeleton_interval = to<double>(list_or_dist);
                } else if (mode != "none") {
                    MSG_ERROR("Skeleton generation mode MUST be one of 'manual', 'auto', or 'none'.", "Specify a valid skeleton generation mode");
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"cofactor-detection"},
            {"algorithm"},
            "Specify the cofactor detection algorithm. Available algorithms are 'forward', 'backward', 'none', and 'all'.",
            false,
            {&settings_t::cofactor_algorithm},
            [](CALLBACK_ARGS)
            {
                static std::vector<std::string> allowed = {"forward", "backward", "none", "all"};
                std::string algo = *++ai;
                if (std::find(allowed.begin(), allowed.end(), algo) == allowed.end()) {
                    MSG_ERROR("Invalid cofactor detection algorithm '" << algo << "'", "");
                } else {
                    ensure(target)->cofactor_algorithm = algo;
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"initial-selection"},
            {"selection"},
            "Specify the initial selection of QTLs for the detection algorithm.\nThe selection should be a comma-separated list of CHROMOSOME:POSITION values.\nSetting an initial selection overrides and cancels skeleton generation and cofactor detection.",
            false,
            {&settings_t::initial_selection},
            [](CALLBACK_ARGS)
            {
                ensure(target)->initial_selection = *++ai;
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"QTL-detection"},
            {"algorithm"},
            "Specify the QTL detection algorithm. Available algorithms are 'none', 'CIM', 'CIM-', 'iQTLm', and 'iQTLm-GW'.",
            false,
            {&settings_t::qtl_algorithm},
            [](CALLBACK_ARGS)
            {
                static std::vector<std::string> allowed = {"CIM", "CIM-", "iQTLm", "iQTLm-GW", "none"};
                std::string algo = *++ai;
                if (!algo.compare(0, 3, "py:")) {
                    ensure(target)->qtl_algorithm = algo;
                } else {
                    if (std::find(allowed.begin(), allowed.end(), algo) == allowed.end()) {
                        MSG_ERROR("Invalid QTL detection algorithm '" << algo << "'", "");
                    } else {
                        ensure(target)->qtl_algorithm = algo;
                    }
                }
                SAFE_IGNORE_CALLBACK_ARGS;
            }},
    }},

#if 0
    {"Actions", "The following tell the software what to do. All the specified actions will be processed in the given order.", false, {
        {{"output-thresholds"},
            {},
            "Output the QTL detection thresholds",
            false,
            [](CALLBACK_ARGS)
            {
                /*ensure(target)->add_action(action::OutputThresholds);*/
                OUT("TODO");
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"output-model"},
            {},
            "Output the final model",
            false,
            [](CALLBACK_ARGS)
            {
                /*ensure(target)->add_action(action::OutputThresholds);*/
                OUT("TODO");
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"manual-skeleton"},
            {"marker_list"},
            "Initialize the skeleton manually",
            false,
            [](CALLBACK_ARGS)
            {
                OUT("TODO");
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"auto-skeleton"},
            {"min_distance"},
            "Initialize the skeleton automatically",
            false,
            [](CALLBACK_ARGS)
            {
                OUT("TODO");
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"cofactor-detection"},
            {"algorithm"},
            "Specify the cofactor detection algorithm (forward or backward or skeleton)",
            false,
            [](CALLBACK_ARGS)
            {
                OUT("TODO");
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

        {{"qtl-detection"},
            {"algorithm"},
            "Specify the QTL detection algorithm (CIM- or iQTLm or iQTLm++)",
            false,
            [](CALLBACK_ARGS)
            {
                OUT("TODO");
                SAFE_IGNORE_CALLBACK_ARGS;
            }},

#if 0
        {{"DET", "--detect"},
            {"method", "window"},
            "Detect QTLs; method must be either 'iQTLm' or 'CIM-'",
            false,
            [](CALLBACK_ARGS)
            {
                using dm = settings_t::detection_method_t;
                std::string d(*++ai);
                if (d == "iQTLm") {
                    ensure(target)->detection_method = dm::iQTLm;
                } else if (d == "CIM-") {
                    ensure(target)->detection_method = dm::CIM;
                } else {
                    MSG_ERROR("Unknown detection method " << d << "; must be either iQTLm or CIM-", "Change the detection method to either iQTLm or CIM-");
                    ensure(target)->detection_method = dm::Undef;
                }
                ensure(target)->detection_window = to<double>(*++ai);
                SAFE_IGNORE_CALLBACK_ARGS;
            }}
#endif
    }},
#endif
};


void print_usage(bool show_hidden) { print_usage_impl(show_hidden, arguments); }


struct argument_parser {
    std::map<std::string, argument_parser_callback_type> amap;
    std::map<std::string, int> pmap;
    argument_parser(const std::vector<argument>& args)
        : amap()
    {
        for (auto& a: args) { a.install(amap, pmap); }
    }
    argument_parser(const argument_section_list_t& sections)
        : amap()
    {
        for (auto& kv: sections) {
            for (auto& a: kv.arguments) { a.install(amap, pmap); }
        }
    }

    void fail_usage()
    {
        print_usage(false);
        exit(-1);
    }

    bool operator () (CALLBACK_ARGS)
    {
        if (amap.find(*ai) == amap.end()) {
            MSG_ERROR("Unknown argument " << (*ai), "");
            fail_usage();
        }
        bool parameters_ok = (aj - ai) > pmap[*ai]; /* il reste au moins n_param mots dans la CLI */
        if (parameters_ok) {
            auto ak = ai;
            ++ak;
            for (int k = pmap[*ai]; parameters_ok && k > 0; --k, ++ak) {
                parameters_ok = (pmap.find(*ak) == pmap.end());
            }
            if (parameters_ok) {
                parameters_ok = (ak == aj || pmap.find(*ak) != pmap.end()); /* il faut s'assurer que le mot suivant est une option valide, ou qu'on a fini */
                if (!parameters_ok && ak != aj) {
                    MSG_ERROR("Unknown argument " << (*ak), "");
                    return false;
                }
            }
        }
        if (!parameters_ok) {
            MSG_ERROR("Expected exactly " << pmap[*ai] << " parameter" << (pmap[*ai] > 1 ? "s" : "") << " after argument " << (*ai), "");
            fail_usage();
            return false;
        }
        amap[*ai](target, ai, aj);
        return true;
    }
} arg_map(arguments);


settings_t* settings_t::from_args(int argc, const char** argv)
{
    std::vector<const char*> args(argv + 1, argv + argc);
    std::vector<const char*>::iterator ai = args.begin();
    std::vector<const char*>::iterator aj = args.end();
    prg_name = argv[0];

    settings_t* ret = NULL;
    
    while (ai != aj && arg_map(ret, ai, aj)) {
        ++ai;
    }

    return ret;
}



struct settings_constraint_t {
    std::string message;
    std::string workaround;
    std::function<bool(const settings_t*)> predicate;

    static std::vector<settings_constraint_t>& list()
    {
        static std::vector<settings_constraint_t> _;
        return _;
    }

    static void check(const settings_t* s)
    {
        /*msg_handler_t::reset();*/
        std::pair<bool, std::set<std::string>> ret;
        /*MSG_DEBUG("constraints: " << list().size());*/
        chrono::start("Consistency check");
        for (auto& c: list()) {
            bool result = c.predicate(s);
            if (!result) {
                MSG_ERROR(c.message, c.workaround);
            }
        }
        chrono::stop("Consistency check");
        msg_handler_t::check(true);
    }

    settings_constraint_t(
            const std::string& m,
            const std::string& w,
            std::function<bool(const settings_t*)> p)
        : message(m), workaround(w), predicate(p)
    {
        list().push_back(*this);
    }
};


/*range<int>*/
/*individual_range(const population* pop);*/

/*multi_generation_observations*/
/*population_marker_obs(const context_key& ck, int ind);*/


#define PREDICATE [](const settings_t* s) -> bool

settings_constraint_t
    all_marker_names_are_unique(
            "At least one marker name appears at least twice in the genetic map",
            "Clean up the genetic map",
            PREDICATE
            {
                const auto& m = s->map;
                bool ret = true;
                std::set<std::string> names;
                for (const auto& chrom: m) {
                    for (const auto& mark: chrom.raw.marker_name) {
                        if (names.find(mark) != names.end()) {
                            MSG_ERROR("Marker name " << mark << " appears more than one in the genetic map", "");
                            ret = false;
                        }
                        names.insert(mark);
                    }
                }
                return ret;
            }),
    at_least_one_pop(
            "There are no datasets to process",
            "Specify at least one population",
            PREDICATE { return s->populations.size() > 0; }),
#if 0
    all_pops_have_different_names(
            "There is a duplicate population name",
            "Specify a unique name for each population",
            PREDICATE
            {
                std::set<std::string> uniq_names;
                for (auto& kv: s->populations) {
                    uniq_names.insert(kv.first);
                }
                return uniq_names.size() == s->populations.size();
            }),
	all_pops_have_at_least_one_observed_generation(
			"A population has no marker observations",
			"Specify marker observations for at least one generation in each population",
			PREDICATE
			{
				for (auto& kv: s->populations) {
					if (kv.second.LV.data.size() == 0) {
						return false;
					}
				}
				return true;
			}),
	all_pops_have_at_least_one_observed_trait(
			"A population has no trait observations",
			"Specify observations for at least one trait in each population",
			PREDICATE
			{
				for (auto& kv: s->populations) {
					if (kv.second.observed_traits.size() == 0) {
						return false;
					}
				}
				return true;
			}),
    all_pops_have_an_existing_qtl_generation(
            "The QTL generation doesn't exist for a population",
            "Check the QTL generation is actually defined in the breeding design XML file",
            PREDICATE
            {
				bool ok = true;
				for (auto& kv: s->populations) {
                    if (kv.second.families.find(kv.second.qtl_generation_name) == kv.second.families.end()) {
                        MSG_ERROR("Generation " << kv.second.qtl_generation_name << " doesn't exist", "");
						ok = false;
                    }
				}
				return ok;
            }),
    all_marker_obs_have_an_existing_generation(
            "The generation doesn't exist for a marker observation dataset",
            "Check the generation name is actually defined in the breeding design XML file",
            PREDICATE
            {
				bool ok = true;
				for (auto& kv: s->populations) {
                    for (auto& mo: kv.second.marker_observations) {
                        if (s->design->generation[mo.generation_name] == NULL) {
                            MSG_ERROR("Generation " << kv.second.qtl_generation_name << " doesn't exist", "");
                            ok = false;
                        }
                    }
				}
				return ok;
            }),
	all_traits_have_same_size_in_each_pop(
			"There must be the same number of observations for each trait in a population",
			"Check your data, here may have been a problem when the file was generated",
			PREDICATE
			{
				bool ok = true;
				for (auto& kv: s->populations) {
					const pop_data_type& pop = kv.second;
					auto beg = pop.observed_traits.begin();
					auto end = pop.observed_traits.end();
					if (beg != end) {
						size_t sz = beg->values.size();
						while (beg != end && beg->values.size() == sz) {
							++beg;
						}
						ok &= beg == end;
					}
				}
				return ok;
			}),
    all_marker_obs_have_a_valid_format(
            "The format specified for a marker observation must be valid",
            "Specify a valid format for each marker observation file",
            PREDICATE
            {
                bool ok = true;
                for (const auto& pop: s->populations) {
                    for (const auto& om: pop.second.observed_mark) {
                        auto it = s->marker_observation_specs.find(om.second.format_name);
                        if (it == s->marker_observation_specs.end()) {
                            ok = false;
                            MSG_ERROR("Format " << om.second.format_name << " is not defined", "");
                        /*} else {*/
                            /*ok &= it->second.size() != 0;*/
                        }
                    }
                }
                return ok;
            }),
	trait_and_marker_observation_must_be_coherent(
			"The number of traits and marker observations differ in a population",
			"If the traits and markers are observed on the same generation, make sure there is the same number of observations in each data file",
			PREDICATE
			{
				for (auto& kv: s->populations) {
					const population& pop = kv.second;
					auto it = pop.observed_mark.find(pop.qtl_generation_name);
					if (it != pop.observed_mark.end()) {
						if (it->second.observations.n_obs != pop.observed_traits.front().values.size()) {
                            MSG_ERROR("In population " << pop.name
                                      << ", have " << pop.observed_traits.front().values.size()
                                      << " observations per trait but "
                                      << it->second.observations.n_obs << " individuals", "");
							return false;
						}
					}
				}
				return true;
			}),
    breeding_design_defined(
            "The breeding design hasn't been defined",
            "Specify a path to a breeding design file",
            PREDICATE { return s->design != NULL; }),
#endif
    map_defined(
            "There are no chromosomes to process",
            "Specify a path to a map file",
            PREDICATE { return s->map.size() > 0; }),
    chromosome_selection_is_valid(
            "The chromosome selection is invalid",
            "Double-check the names of the chromosomes and the path to the map file",
            PREDICATE
            {
                bool ok = true;
                for (auto& c: s->chromosome_selection) {
                    auto i = std::find_if(s->map.begin(), s->map.end(),
                                          [&](const chromosome& k) { return k.name == c; });
                    if (i == s->map.end()) {
                        ok = false;
                        MSG_ERROR("Selected chromosome " << c << " doesn't exist", "Check the chromosome name " << c << " is correctly spelled and exists in the map");
                    }
                }
                return ok;
            }),
#if 0
    format_spec_defined(
            "No format specification file was provided",
            "Specify a format specification file",
            PREDICATE { return s->marker_observation_specs.size() != 0; }),
            /*PREDICATE { return s->marker_observation_specs != NULL; }),*/

    marker_observations_are_coherent(
            "Some marker observations are incoherent, the parental origin probabilities cannot be computed.",
            "Ensure the coherence of the observed haplotypes between all the generations in each population, remove the incoherent individuals from your data file, or specify a non-zero noise level",
            PREDICATE
            {
                bool ok = true;
#if 0
                /*MSG_DEBUG("populations: " << s->populations.size());*/
                /*MSG_DEBUG("chromosomes: " << s->map.size());*/
                for (const auto& pkv: s->populations) {
                    const population& pop = pkv.second;
                    /*MSG_DEBUG(pop);*/
                    if (pop.observed_mark.size() == 0) {
                        continue;
                    }
                    for (const chromosome& chr: s->map) {
                        /*MSG_DEBUG(chr);*/
                        /*for (const auto& kv: pop.observed_mark) {*/
                            auto gen = s->design->generation[pop.qtl_generation_name];
                            if (!gen) {
                                continue;
                            }
                            auto obs_qtl_gen_it = pop.observed_mark.find(gen->name);
                            size_t n_ind;
                            if (obs_qtl_gen_it == pop.observed_mark.end()) {
                                n_ind = pop.pedigree.find(gen)->second.size();
                            } else {
                                n_ind = obs_qtl_gen_it->second.observations.n_obs;
                            }
                            context_key ck(new context_key_struc(&pop, &chr));
                            collection<multi_generation_observations>
                                mgos = make_collection<Disk|Mem>(population_marker_obs, value<context_key>{ck}, range<int>(0, n_ind, 1));
                            /*MSG_DEBUG("mgos: " << mgos.size());*/
                            for (size_t i = 0; i < mgos.size(); ++i) {
                                /*MSG_DEBUG(mgos[i]->LV);*/
                                /*MSG_DEBUG("");*/
                                /*MSG_DEBUG(mgos[i]->LV.colwise().any());*/
                                if (!mgos[i]->LV.colwise().any().all()) {
                                    const auto& lv = mgos[i]->LV;
                                    auto hi = chr.begin();
                                    for (int k = 0; k < lv.outerSize(); ++k) {
                                        if (!lv.col(k).any()) {
                                            std::stringstream s;
                                            std::pair<int, int> mrk = *hi;
                                            s << chr.raw.marker_name[mrk.first];
                                            for (++mrk.first; mrk.first != mrk.second; ++mrk.first) {
                                                s << ", " << chr.raw.marker_name[mrk.first];
                                            }
                                            if (pop.noise == 0.) {
                                                MSG_ERROR("Incoherent marker score in invidivual #" << (i + 1) << " in population " << pop.name << " regarding markers " << s.str(), "");
                                            } else {
                                                MSG_WARNING("Incoherent marker score in invidivual #" << (i + 1) << " in population " << pop.name << " regarding markers " << s.str() << ", but a non-zero noise level was specified");
                                            }
#if 0
                                        } else {
                                            std::stringstream s;
                                            std::pair<int, int> mrk = *hi;
                                            MSG_DEBUG(mrk.first << "," << mrk.second);
                                            s << chr.raw.marker_name[mrk.first];
                                            for (++mrk.first; mrk.first != mrk.second; ++mrk.first) {
                                                MSG_DEBUG(mrk.first);
                                                s << ", " << chr.raw.marker_name[mrk.first];
                                            }
                                            MSG_DEBUG("haplotype " << s.str());
#endif
                                        }
                                        ++hi;
                                    }
                                    if (pop.noise == 0.) {
                                        ok = false;
                                    }
                                }
                            }
                        /*}*/
                    }
                }
#endif
                return ok;
            }),
#endif
//    pop_dont_exist(
//        "1-point Parental Origin Probabilities were not output for at least one required generation",
//        "Remove -o from the commandline of spell-marker, or add all required generations to the list after -o",
//        PREDICATE {
//            for (const auto& ds: s->datasets) {
//                if (s->pop_data->LV.data[ds.first].size() == 0) {
//                    return false;
//                }
//            }
//            return true;
//        }
//    ),
    name_not_null(
            "This configuration has no name",
            "Define a non-empty name for the configuration",
            PREDICATE { return s->name != ""; });







bool settings_t::sanity_check() const
{
    ensure_directories_exist(work_directory);
    settings_constraint_t::check(this);
    return true;
}


std::ostream& operator << (std::ostream& os, const settings_t& s)
{
    os << "<analysis name=\"" << s.name << "\">" << std::endl;
    os << "    <notes>" << s.notes << "</notes>" << std::endl;
#if 0
    if (s.design) {
        os << "    <breeding-design file=\"" << s.design->filename << "\" />" << std::endl;
    }
    os << "    <consensus-map file=\"" << s.map_filename << "\" format=\"mapmaker\" />" << std::endl;
    for (const std::string& f: s.marker_observation_specs_filenames) {
        os << "    <format-specification file=\"" << f << "\" />" << std::endl;
    }
    for (auto& np: s.populations) {
        const population& p = np.second;
        os << "    <dataset name=\"" << np.first << "\">" << std::endl;
        os << "        <population generation=\"" << p.qtl_generation_name << "\" traits=\"" << p.observed_traits_filename << "\">" << std::endl;
        if (p.pedigree_filename != "") {
            os << "            <pedigree file=\"" << p.pedigree_filename << "\" />" << std::endl;
        }
        for (auto& kv: p.observed_mark) {
            os << "            <markers generation=\"" << kv.first << "\" file=\"" << kv.second.filename << "\" />" << std::endl;
        }
        os << "            <noise value=\"" << p.noise << "\" />" << std::endl;
        os << "        </population>" << std::endl;
        os << "    </dataset>" << std::endl;
    }
#endif
    os << "    <settings>" << std::endl;
    os << "        <step value=\"" << s.step << "\" />" << std::endl;
    os << "        <parallel value=\"" << s.parallel << "\" />" << std::endl;
    os << "        <work-directory path=\"" << s.work_directory << "\" />" << std::endl;
    os << "        <model>" << std::endl;
    os << "            <connected enabled=\"" << (s.connected ? "true" : "false") << "\" />" << std::endl;
    os << "            <epistasis enabled=\"" << (s.epistasis ? "true" : "false") << "\" />" << std::endl;
    os << "            <pleiotropy enabled=\"" << (s.pleiotropy ? "true" : "false") << "\" tolerance=\"" << s.pleiotropy_tolerance << "\" />" << std::endl;
    os << "        </model>" << std::endl;
    os << "        <working-sets>" << std::endl;
    if (s.chromosome_selection.size()) {
        os << "            <chromosome-selection all=\"false\">" << std::endl;
        for (auto& k: s.chromosome_selection) {
            os << "                <chromosome name=\"" << k << "\" />" << std::endl;
        }
        os << "            </chromosome-selection>" << std::endl;
    } else {
        os << "            <chromosome-selection all=\"true\" />" << std::endl;
    }
    if (s.cofactor_marker_selection_filename != "") {
        os << "            <cofactor-marker-selection automatic=\"false\" file=\"" << s.cofactor_marker_selection_filename << "\" />" << std::endl;
    } else {
        os << "            <cofactor-marker-selection automatic=\"true\" distance-threshold=\"" << s.cofactor_marker_selection_distance << "\" />" << std::endl;
    }
    os << "            <epistasis-initial-qtl-selection file=\"TODO\"/>" << std::endl;
    os << "        </working-sets>" << std::endl;
    os << "    </settings>" << std::endl;

    os << "</analysis>" << std::endl;
    return os;
}


#include "io/output_impl.h"
