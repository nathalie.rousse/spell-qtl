/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "eigen.h"
#include "input.h"
//#include "input/read_map.h"
#include "io/output_impl.h"

namespace read_data {


std::vector<chromosome> read_map(file& is) {
    if (!is.good()) {
        throw input_exception(is, "couldn't read from file");
    }
    std::vector<chromosome> ret;
    std::string s;
    double d;
    int n;
    is >> std::noskipws;
    while (is.good() && !is.eof()) {
        ret.emplace_back();
        ret.back().name = read_star_name(is);
        ws_nonl(is);
        n = 0;
        is >> n;
        if (n <= 0) {
            throw input_exception(is, "expected the number of markers after the marker name.");
        }
        ret.back().raw.marker_name.reserve(n);
        ret.back().raw.marker_locus.reserve(n);
        ws_nonl(is);
        d = 0;
        s.clear();
        is >> s;
        if (s.size() == 0) {
            throw input_exception(is, "expected a marker name");
        }
        ret.back().raw.marker_name.push_back(s);
        ret.back().raw.marker_locus.push_back(0);
        while (--n && !is.eof()) {
            ws_nonl(is);
            d = -1;
            is >> d;
            if (d < 0) {
                throw input_exception(is, "expected a positive distance");
            }
            ws_nonl(is);
            s.clear();
            is >> s;
            if (s.size() == 0) {
                throw input_exception(is, "expected a marker name");
            }
            ret.back().raw.marker_name.push_back(s);
            ret.back().raw.marker_locus.push_back(ret.back().raw.marker_locus.back() + d);
        }
        ws_nl(is);
        ret.back().compute_haplo_sizes();
    }
    return ret;
}


} /* namespace read_data */

#ifdef TEST_MAP
//#include "file.h"

int main(int argc, char** argv)
{
    for (int i = 1; i < argc; ++i) {
        ifile ifs(argv[i]);
        std::cout << read_data::read_map(ifs) << std::endl;
    }
    return 0;
}

#endif

