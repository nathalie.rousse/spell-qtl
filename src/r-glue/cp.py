#!/usr/bin/env python3

import os
import sys
from glob import glob

def do_cp(src, dest):
    #print('do_cp', src, dest)
    ta = os.path.getmtime(src)
    if not os.path.isfile(dest) or os.path.getmtime(dest) < ta:
        try:
            os.makedirs(os.path.dirname(dest))
        except:
            pass
        print("Updating file", dest)
        open(dest, 'wb').write(open(src, 'rb').read())
        with open('.copy_sources', 'a'):
            os.utime('.copy_sources')

basedir = os.path.normpath(sys.argv[1])
destdir = os.path.normpath(sys.argv[-1])

for pat in sys.argv[2:-1]:
    for srcfile in glob(os.path.join(basedir, pat)):
        if not os.path.isfile(srcfile):
            continue
        destfile = os.path.join(destdir, srcfile[len(basedir) + 1:])
        do_cp(srcfile, destfile)
