assign('[.Spell2PtMatrix', function(what, i, j) what$get2PtValue(i, j))

dim.Spell2PtMatrix <- function(m) m$getDim()

as.matrix.Spell2PtMatrix <- function (m) {
    d <- dim(m)
    ret <- sapply(1:d[1], function(i) sapply(1:d[2], function(j) m[i, j]))
    rownames(ret) <- m$getRownames()
    colnames(ret) <- m$getColnames()
    ret
}

as.matrix.Spell2PtMatrix <- function (m) {
    ret <- m$as_matrix()
    rownames(ret) <- m$getRownames()
    colnames(ret) <- m$getColnames()
    ret
}

rownames.Spell2PtMatrix <- function(m) m$getRownames()

colnames.Spell2PtMatrix <- function(m) m$getColnames()
