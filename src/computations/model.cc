/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "eigen.h"
#include "input.h"
//#include "data/geno_matrix.h"
#include "cache/md5.h"
#include "computations/base.h"
#include "cache2.h"
#include "computations/basic_data.h"
#include "computations/probabilities.h"
#include "io/model_print.h"
#include "model/model.h"
#include "model/tests.h"
#include "computations/model.h"
//#include "io/output_impl.h"

#if 0
MatrixXd
traits_to_matrix()
{
	std::function<std::vector<std::string>(const qtl_pop_type&)>
		get_labels = [](const qtl_pop_type& pop)
		{
			std::vector<std::string> labels;
			labels.reserve(pop.observed_traits.size());
			for (auto& ot: pop.observed_traits) {
				labels.push_back(ot.name);
			}
			return labels;
		};
	std::function<int(const qtl_pop_type&)>
		get_pop_size = [](const qtl_pop_type& pop)
		{
			return pop.observed_traits.front().values.size();
		};
	block_builder<std::string> bb;
	init_connected_block_builder(bb, get_pop_size, get_labels, all_populations());
	MatrixXd ret(bb.n_rows, bb.n_columns);
	auto builder = bb.begin(ret);
	for (auto& pop: active_settings->populations) {
		auto& observed_traits = pop.second.observed_traits;
		MatrixXd tmp(observed_traits.front().values.size(),
					 observed_traits.size());
        for (size_t i = 0; i < observed_traits.front().values.size(); ++i) {
            for (size_t j = 0; j < observed_traits.size(); ++j) {
                tmp(i, j) = observed_traits[j].values[i];
            }
        }
		builder += tmp;
	}
	/*MSG_DEBUG("traits" << std::endl << ret);*/
	return ret;
}
#endif



const MatrixXd&
trait_by_name(population_value pop, const std::string& name)
{
	static MatrixXd none;
    for (auto& ot: pop->observed_traits) {
        if (ot.name == name) {
            return ot.values;
        }
    }
    return none;
}


template <typename M>
void
random_permutation(M&& block, const MatrixXd& values) {
    PermutationMatrix<Dynamic,Dynamic> perm(block.rows());
    perm.setIdentity();
    std::random_shuffle(perm.indices().data(), perm.indices().data()+perm.indices().size());
    block = perm * values; // permute rows
}


value<MatrixXd>
trait_permutations(const std::string& trait_name, int n)
{
	size_t n_rows = 0;
	std::vector<MatrixXd> traits;
    const auto& populations = active_settings->linked_pops[trait_name];

    /* FIXME: are linked pops inter-permutable? */
    for (const auto& pop_vec: populations) {
//        traits.emplace_back();
        for (const auto& pop_ptr: pop_vec) {
            n_rows += pop_ptr->size();
//            const auto& trait = pop_ptr->observed_traits.front().values;
//            traits.back().insert(traits.back().end(), trait.begin(), trait.end());
            traits.emplace_back(pop_ptr->observed_traits.front().values);
        }
    }

    value<MatrixXd> ret = MatrixXd();
    int block_cols = traits[0].cols();
	ret->resize(n_rows, block_cols * n);
    for (int j = 0; j < ret->cols(); j += block_cols) {
        n_rows = 0;
    	for (size_t t = 0; t < traits.size(); ++t) {
            random_permutation(ret->block(n_rows, j, traits[t].rows(), block_cols), traits[t]);
            n_rows += traits[t].rows();
        }
//		size_t ts = traits[t].size();
//		auto slice = ret->middleRows(n_rows, ts);
//		for (int j = 0; j < n; ++j) {
//			auto col = slice.col(j);
//			for (size_t i = 0; i < traits[t].size(); ++i) {
//				col(i) = traits[t][i];
//			}
//			std::random_shuffle(col.data(), col.data() + ts);
//		}
//		n_rows += ts;
	}
    return ret;
}


value<MatrixXd>
trait_matrix(const std::string& trait_name, const collection<population_value>& all_pops)
{
	size_t n_rows = 0;
	std::vector<const MatrixXd*> traits;
	traits.reserve(all_pops.size());
	for (auto& pop: all_pops) {
		traits.push_back(&trait_by_name(*pop, trait_name));
        MSG_DEBUG("have single_trait from sub-pop with " << traits.back()->rows() << " values");
		n_rows += traits.back()->rows();
	}
    MSG_DEBUG("have a total of " << n_rows << " rows");
    value<MatrixXd> ret = MatrixXd();
	ret->resize(n_rows, traits[0]->cols());
	n_rows = 0;
	int t = 0;
	for (auto& pop: all_pops) {
		size_t ts = traits[t]->rows();
		ret->middleRows(n_rows, ts) = *traits[t];
        n_rows += ts;
        ++t;
        (void)pop;
	}
    return ret;
}


#if 0
value<MatrixXd>
residuals_permutations(const model& M, int n)
{
	size_t n_rows = 0;
	std::vector<size_t> sizes;
	sizes.reserve(active_settings->populations.size());
	for (auto& pop: active_settings->populations) {
		sizes.push_back(pop.second.observed_traits.front().values.size());
		n_rows += sizes.back();
	}
    value<MatrixXd> ret = MatrixXd();
	ret->resize(n_rows, n);
	n_rows = 0;
	/*int t = 0;*/
    /* FIXME: only works for ONE single_trait at a time */
    /*std::cerr << "ret$dim(" << ret->innerSize() << ',' << ret->outerSize() << ')' << std::endl;*/
    /*std::cerr << "residuals$dim(" << M.residuals().innerSize() << ',' << M.residuals().outerSize() << ')' << std::endl;*/
    ret->colwise() = M.residuals().col(0);
	/*for (auto& pop: active_settings->populations) {*/
	for (size_t t = 0; t < active_settings->populations.size(); ++t) {
		size_t ts = sizes[t];
		auto slice = ret->middleRows(n_rows, ts);
		for (int j = 0; j < n; ++j) {
			auto col = slice.col(j);
			std::random_shuffle(col.data(), col.data() + ts);
		}
		n_rows += ts;
		/*++t;*/
	}
    return ret;
}
#endif


model_block_type
/*cross_indicator(const collection<population_value>& pops)*/
cross_indicator(const std::string& trait)
{
    const auto& pops = active_settings->linked_pops[trait];
//     MSG_DEBUG("[Building Cross Indicator] Have " << pops.size() << " distinct pops");
    int n_cols = pops.size();
    int n_rows = 0;
    for (const auto& pop_vec: pops) {
//         MSG_DEBUG("[Building Cross Indicator] Pop has " << pop_vec.size() << " subpops");
        for (const auto& pop: pop_vec) {
//             MSG_DEBUG("[Building Cross Indicator] Subpop has " << pop->size() << "     individuals");
            n_rows += pop->size();
        }
    }
	model_block_type val;
    /*int p = 0;*/
    std::vector<int> columns;
    const auto& lp = active_settings->linked_pops;
    std::vector<int> pop_col(lp.size(), -1);
    /*for (int i = 0; i < pop_col.size(); ++i) { pop_col[i] = i; }*/
    std::vector<int> block_size_by_col(lp.size(), 0);
    size_t col = 0, row = 0;
    val.data = MatrixXd::Zero(n_rows, n_cols);
    for (const auto& pop_vec: pops) {
        for (const auto& pop: pop_vec) {
            val.data.block(row, col, pop->size(), 1) = VectorXd::Ones(pop->size());
            row += pop->size();
        }
        ++col;
    }

    val.column_labels.clear();
    val.column_labels.reserve(val.data.outerSize());
    for (const auto& pop_vec: pops) {
//         for (const auto& pop: pop_vec) {
            val.column_labels.emplace_back(pop_vec.front()->qtl_generation_name.begin(), pop_vec.front()->qtl_generation_name.end());
//         }
    }

    /*for (const auto& pop: pops) {*/
        /*int sz = (*pop)->observed_traits.front().values.size();*/
        /*val.data.block(n_rows, p, sz, 1) = VectorXd::Ones(sz);*/
        /*val.column_labels.emplace_back();*/
        /*val.column_labels.back().push_back(' ');*/
        /*n_rows += sz;*/
        /*++p;*/
    /*}*/
// 	MSG_DEBUG("[Building Cross Indicator] cross indicator " << MATRIX_SIZE(val) << std::endl << val);
    /*MSG_QUEUE_FLUSH();*/
    return val;
}


value<model_block_type>
make_covariables_block(const std::string& trait, const std::vector<std::string>& covar_names) {
    if (covar_names.size() == 0) {
        return model_block_type{};
    }
    collection<model_block_type> blocks;
//    MSG_DEBUG("make_covariables_block");
    for (const auto& vqp: active_settings->linked_pops[trait]) {
        for (const auto& qp: vqp) {
            blocks.emplace_back(model_block_type());
//            MSG_DEBUG("COVAR QP " << qp);
//            MSG_QUEUE_FLUSH();
            blocks.back()->data = qp->covariables.values;
            for (const auto& l: qp->covariables.dim_names) {
                blocks.back()->column_labels.emplace_back(l.begin(), l.end());
            }
            blocks.back()->row_labels.assign(qp->covariables.good_indices.begin(), qp->covariables.good_indices.end());
//            MSG_DEBUG("COVAR BLOCK " << MATRIX_SIZE(blocks.back()->data) << " CLAB " << blocks.back()->column_labels << " RLAB " << blocks.back()->row_labels);
        }
    }
//    MSG_DEBUG("make_covariables_block assemble");
    return assemble_block_multipop(blocks);
}


#if 0
model
basic_model()
{
	value<MatrixXd> traits = make_value<Sync|Mem>(traits_to_matrix);
    model M(*traits, all_populations());
    M.add_block({}, cross_indicator(all_populations()));
    const char* decomp_method = getenv("DECOMP_METHOD");
    std::string s(decomp_method ? decomp_method : "");
    /*std::cout << "Xt" << std::endl << M.X().transpose() << std::endl;*/
    /*std::cout << "Yt" << std::endl << M.Y().transpose() << std::endl;*/
    /*std::cout << "XtX^-1" << std::endl << M.XtX_pseudo_inverse() << std::endl;*/
    /*std::cout << "coefs" << std::endl << M.coefficients() << std::endl;*/
    /*auto r = M.residuals().array();*/
    /*std::cout << "residuals" << std::endl << (r * r).colwise().sum() << std::endl;*/
	M.compute();
    return M;
}
#endif

#if 0
VectorXd
f_tester(const model& M, const model& M0, const parental_origin_per_locus_type& popl)
{
    model M1 = extend(M0, popl);
	M1.compute();
    return f_test(*const_cast<model*>(&M), *const_cast<model*>(&M1));
}

MatrixXd
f_test_along_chromosome(const value<model>& Mcurrent, const value<model>& M0,
                                 const collection<parental_origin_per_locus_type>& popl)
{
                DUMP_FILE_LINE();
    collection<VectorXd> aft
        = make_collection(f_tester, Mcurrent, M0, popl);

                DUMP_FILE_LINE();
    MatrixXd ret(aft[0]->innerSize(), aft.size());

    for (size_t i = 0; i < aft.size(); ++i) {
                DUMP_FILE_LINE();
        ret.col(i) = *aft[i];
    }
    return ret;
}
#endif

model extend_model(const model& m0, const model_block_key& k, const parental_origin_per_locus_type& block)
{
    model ret = m0.extend(k, block);
    ret.compute();
    return ret;
}

#if 0
VectorXd compute_ftest(const model& m0, const model& m1)
{
    return f_test(m0, m1);
}

VectorXd compute_r2(const model& m0, const model& m1) { return r2(m0, m1); }
VectorXd compute_chi2(const model& m0, const model& m1)
{
    VectorXd ret(1);
    ret(0) = chi2(m0, m1);
    return ret;
}

void
consolidate_computation_result(MatrixXd& ret,
                               const MatrixXd& (model::* func) () const,
                               const collection<model>& models)
{
    ret.resize(((*models[0]).*func)().innerSize(),
               ((*models[0]).*func)().outerSize() * models.size());
    size_t bl_size = ((*models[0]).*func)().outerSize();
    if (bl_size > 1) {
        size_t ofs = 0;
        for (size_t i = 0; i < models.size(); ++i) {
            ret.middleCols(ofs, bl_size) = ((*models[i]).*func)();
            ofs += bl_size;
        }
    } else {
        for (size_t i = 0; i < models.size(); ++i) {
            ret.col(i) = ((*models[i]).*func)().col(0);
        }
    }
}

void
consolidate_computation_result(MatrixXd& ret,
                               const VectorXd& (model::* func) () const,
                               const collection<model>& models)
{
    ret.resize(((*models[0]).*func)().innerSize(), models.size());
    for (size_t i = 0; i < models.size(); ++i) {
        ret.col(i) = ((*models[i]).*func)();
    }
}

void
consolidate_computation_result(VectorXd& ret,
                               int (model::* func) () const,
                               const collection<model>& models)
{
    ret.resize(models.size());
    for (size_t i = 0; i < models.size(); ++i) {
        ret(i) = ((*models[i]).*func)();
    }
}
#endif

void
one_computation_result(int i, MatrixXd& ret, const MatrixXd& model_member)
{
    size_t bl_size = model_member.outerSize();
    if (bl_size > 1) {
        ret.middleCols(bl_size * i, bl_size) = model_member;
    } else {
        ret.col(i) = model_member.col(0);
    }
}

void
one_computation_result(int i, MatrixXd& ret, const VectorXd& model_member)
{
    ret.col(i) = model_member.col(0);
}

void
one_computation_result(int i, VectorXd& ret, int model_member)
{
    ret(i) = model_member;
}


struct dump_model {
    std::string name;
    const model& m;

    friend
        std::ostream&
        operator << (std::ostream& os, const dump_model& dm)
        {
            return os << '[' << dm.name << " {" << dm.m.keys() << "} rank=" << dm.m.rank() << " rss=" << dm.m.rss().transpose() << ']';
        }
};


int compute_one(int i, computation_along_chromosome* ret,
                ComputationType ct, ComputationResults cr,
                size_t y_block_size,
                const model& Mcurrent, const model& M0,
                const model_block_key& k,
                const parental_origin_per_locus_type& popl)
{
    /*DUMP_FILE_LINE();*/
//    MSG_DEBUG("one popl" << std::endl << popl.transpose());
    model Mext = M0.extend(k, popl);
    Mext.compute();
//    MSG_DEBUG("Rank(Mext " << Mext.keys() << ") = " << Mext.rank());
//    MSG_DEBUG('[' << Mext.keys() << " rank=" << Mext.rank() << " rss=" << Mext.rss().transpose() << ']');

//    DUMP_FILE_LINE();
    if (cr & RSS) {
        one_computation_result(i, ret->rss, Mext.rss());
    }
//    DUMP_FILE_LINE();
    if (cr & Residuals) {
        one_computation_result(i, ret->residuals, Mext.residuals());
    }
//    DUMP_FILE_LINE();
    if (cr & Coefficients) {
        one_computation_result(i, ret->coefficients, Mext.coefficients());
    }

//    DUMP_FILE_LINE();
    if (cr & Rank) {
        one_computation_result(i, ret->rank, Mext.rank());
    }
//    DUMP_FILE_LINE();
    if (ct & (FTest|FTestLOD)) {
        f_test(Mcurrent, Mext, i,
               ct & FTest ? &ret->ftest_pvalue : NULL,
               ct & FTestLOD ? &ret->ftest_lod : NULL);

#if 0
        if ((ct & FTest) && Mcurrent.Y().outerSize() <= 5) {
            dump_model m1{"Mcurrent", Mcurrent}, m2{"Mext", Mext};
            MSG_DEBUG(m1 << " / " << m2 << " ftest=" << ret->ftest_pvalue.col(i).transpose());
        }
#endif
    }
//    DUMP_FILE_LINE();
    if (ct & Mahalanobis) {
        mahalanobis(Mcurrent, Mext, i, &ret->mahalanobis);
    }
//    DUMP_FILE_LINE();
    if (ct & R2) {
        r2(Mcurrent, Mext, i, &ret->r2);
    }
//    DUMP_FILE_LINE();
    if (ct & Chi2) {
//        auto tmp = chi2(Mcurrent, Mext, y_block_size);
//        MSG_DEBUG("HAVE CHI2 COLUMN " << tmp.rows() << 'x' << tmp.cols());
//        MSG_DEBUG("HAVE recipient   " << ret->chi2.rows() << 'x' << ret->chi2.cols());
        ret->chi2.col(i) = chi2(Mcurrent, Mext, y_block_size);
//        ret->chi2.col(i) = chi2(Mcurrent, Mext, y_block_size).transpose();
    }
    if (ct & Chi2LOD) {
//        auto tmp = chi2(Mcurrent, Mext, y_block_size);
//        MSG_DEBUG("HAVE CHI2 COLUMN " << tmp.rows() << 'x' << tmp.cols());
//        MSG_DEBUG("HAVE recipient   " << ret->chi2.rows() << 'x' << ret->chi2.cols());
//        MSG_DEBUG("Chi2LOD");
        ret->chi2_lod.col(i) = chi2(Mcurrent, Mext, y_block_size, true);
//        ret->chi2.col(i) = chi2(Mcurrent, Mext, y_block_size).transpose();
    }
//    DUMP_FILE_LINE();

    return 0;
}


model
init_model(const value<MatrixXd>& Y, const value<model_block_type>& indicator, const value<model_block_type>& covars, double threshold)
{
    model M(Y, threshold, all_populations());
//    const char* decomp_method = getenv("DECOMP_METHOD");  /* FIXME get rid of this sh\bt. */
//    std::string s(decomp_method ? decomp_method : "");
    M.m_max_order = 1;
    MSG_DEBUG("[NEW MODEL] indicator labels: " << indicator->column_labels);
    M.add_block(model_block_key_struc::cross_indicator(), indicator);
    MSG_DEBUG("[NEW MODEL] init_model with CI " << M.keys());
//    MSG_DEBUG("covariables " << active_settings->with_covariables << " covars.cols=" << covars->data.cols());
    if (covars && covars->data.cols() > 0) {
        M.add_block(model_block_key_struc::covariables(), covars);
    }
	/*std::cout << "indicator" << std::endl << indicator->transpose() << std::endl;*/
    /*std::cout << "Xt" << std::endl << M.X().transpose() << std::endl;*/
    /*std::cout << "Yt" << std::endl << M.Y().transpose() << std::endl;*/
    /*std::cout << "XtX^-1" << std::endl << M.XtX_pseudo_inverse() << std::endl;*/
    /*std::cout << "coefs" << std::endl << M.coefficients() << std::endl;*/
    /*auto r = M.residuals().array();*/
    /*std::cout << "residuals" << std::endl << (r * r).colwise().sum() << std::endl;*/
	M.compute();
    return M;
}


VectorXd
row_max(const MatrixXd& mat)
{
    return mat.rowwise().maxCoeff();
}


std::vector<double>
get_quantiles(const VectorXd& values, const std::vector<double>& quantiles)
{
    std::function<bool(double, double)> comp = [] (double a, double b) { return a > b; };
    /* quantiles should be in range [0;1) */
    size_t size = values.innerSize();
    size_t max_interesting_index
        = (size_t) (size * *std::max_element(quantiles.begin(),
                                             quantiles.end()));
    std::vector<double> sorted_values(values.data(),
                                      values.data() + size);
    std::partial_sort(sorted_values.begin(),
                      sorted_values.begin() + max_interesting_index + 1,
                      sorted_values.end(),
                      comp);
    std::vector<double> qvec;
    for (double q: quantiles) {
        qvec.push_back(sorted_values[(size_t) (q * size)]);
    }
    MSG_DEBUG("quantiles: " << qvec);
    return qvec;
}

/*
collection<block_builder<std::vector<char>>>
compute_block_builders(const collection<population_value>& all_pop,
					   const value<qtl_chromosome_value>& qtl_chr,
					   const value<MatrixXd>& LD_matrix)
{
	collection<block_builder<std::vector<char>>> ret;
	return ret;
}
*/


collection<model_block_type>
assemble_parental_origins_multipop(
        const value<chromosome_value>& chr,
        const value<locus_key>& lk,
        const std::vector<collection<parental_origin_per_locus_type>>& all_popl,
        const collection<population_value>& all_pop,
        bool block_is_POP)
{
    /* FIXME take into account the ACTUAL list of populations used! */
    /*MSG_DEBUG("ALL_POPL " << (*chr)->name << " " << lk);*/
    /*for (const auto& popl: all_popl) {*/
    /*MSG_DEBUG("======================================");*/
    /*MSG_DEBUG("" << popl);*/
    /*}*/
    /*MSG_QUEUE_FLUSH();*/
    static
    std::function<int(const qtl_pop_type&)>
            get_pop_size = [](const qtl_pop_type& pop)
    {
        return pop.size();
    };

    std::function<std::vector<std::vector<char>>(const qtl_pop_type&)>
            get_labels;

    if (block_is_POP) {
        get_labels
                = [=](const qtl_pop_type& pop)
        {
            context_key ck(new context_key_struc(&pop, *chr, std::vector<double>()));
            /*MSG_DEBUG(ck);*/
            return make_value<Sync|Mem>(compute_state_to_parental_origin,
                                        value<context_key>{ck},
                                        lk)->row_labels;
        };
    } else {
        get_labels
                = [=](const qtl_pop_type& pop)
        {
            context_key ck(new context_key_struc(&pop, *chr, std::vector<double>()));
            /*MSG_DEBUG(ck);*/
            return make_value<Sync|Mem>(compute_state_to_dominance,
                                        value<context_key>{ck},
                                        lk)->row_labels;
        };
    }

    /*DUMP_FILE_LINE();*/
    block_builder<std::vector<char>> bb;
    if (active_settings->connected) {
        /*MSG_DEBUG("CONNECTED!");*/
        init_connected_block_builder(bb, get_pop_size, get_labels, all_pop);
    } else {
        /*MSG_DEBUG("DISCONNECTED!");*/
        init_disconnected_block_builder(bb, get_pop_size, get_labels, all_pop);
    }
    /*DUMP_FILE_LINE();*/
    collection<model_block_type> all_blocks;
    all_blocks.reserve(all_popl.front().size());
    size_t n = all_popl.front().size();
    for (size_t i = 0; i < n; ++i) {
        /*DUMP_FILE_LINE();*/
        all_blocks.emplace_back(new immediate_value<model_block_type>(model_block_type()));
        /*DUMP_FILE_LINE();*/
        all_blocks.back()->column_labels = bb.labels;
        auto builder = bb.begin(all_blocks.back()->data);
        /*DUMP_FILE_LINE();*/
        /*MSG_DEBUG("building at #" << i);*/
        for (auto& popl: all_popl) {
            /*MSG_DEBUG("popl" << std::endl << popl);*/
            /*DUMP_FILE_LINE();*/
            /*MSG_DEBUG("  block " << std::endl << popl[i]->data);*/
            /*MSG_QUEUE_FLUSH();*/
            builder += popl[i]->data;
        }
        /*MSG_DEBUG('#' << i << std::endl << all_blocks.back());*/
    }

    /*MSG_DEBUG("ALL BLOCKS" << std::endl << all_blocks);*/

    return all_blocks;
}


collection<model_block_type>
assemble_parental_origins_multipop(
        const value<chromosome_value>& chr,
        const std::vector<locus_key>& lk,
        const std::vector<collection<parental_origin_per_locus_type>>& all_popl,
        const collection<population_value>& all_pop,
        bool block_is_POP)
{
    /* FIXME take into account the ACTUAL list of populations used! */
    /*MSG_DEBUG("ALL_POPL " << (*chr)->name << " " << lk);*/
    /*for (const auto& popl: all_popl) {*/
    /*MSG_DEBUG("======================================");*/
    /*MSG_DEBUG("" << popl);*/
    /*}*/
    /*MSG_QUEUE_FLUSH();*/
    static
    std::function<int(const qtl_pop_type&)>
            get_pop_size = [](const qtl_pop_type& pop)
    {
        return pop.size();
    };

    std::function<std::vector<std::vector<char>>(const qtl_pop_type&)>
            get_labels;

    /*DUMP_FILE_LINE();*/
    collection<model_block_type> all_blocks;
    all_blocks.reserve(all_popl.front().size());
    size_t n = all_popl.front().size();
    for (size_t i = 0; i < n; ++i) {
        if (block_is_POP) {
            get_labels
                    = [&](const qtl_pop_type& pop)
            {
                context_key ck(new context_key_struc(&pop, *chr, std::vector<double>()));
                /*MSG_DEBUG(ck);*/
                return make_value<Sync|Mem>(compute_state_to_parental_origin,
                                            value<context_key>{ck},
                                            as_value(lk[i]))->row_labels;
            };
        } else {
            get_labels
                    = [=](const qtl_pop_type& pop)
            {
                context_key ck(new context_key_struc(&pop, *chr, std::vector<double>()));
                /*MSG_DEBUG(ck);*/
                return make_value<Sync|Mem>(compute_state_to_dominance,
                                            value<context_key>{ck},
                                            as_value(lk[i]))->row_labels;
            };
        }

        /*DUMP_FILE_LINE();*/
        block_builder<std::vector<char>> bb;
        if (active_settings->connected) {
            /*MSG_DEBUG("CONNECTED!");*/
            init_connected_block_builder(bb, get_pop_size, get_labels, all_pop);
        } else {
            /*MSG_DEBUG("DISCONNECTED!");*/
            init_disconnected_block_builder(bb, get_pop_size, get_labels, all_pop);
        }
        /*DUMP_FILE_LINE();*/
        all_blocks.emplace_back(new immediate_value<model_block_type>(model_block_type()));
        /*DUMP_FILE_LINE();*/
        all_blocks.back()->column_labels = bb.labels;
        auto builder = bb.begin(all_blocks.back()->data);
        /*DUMP_FILE_LINE();*/
        /*MSG_DEBUG("building at #" << i);*/
        for (auto& popl: all_popl) {
            /*MSG_DEBUG("popl" << std::endl << popl);*/
            /*DUMP_FILE_LINE();*/
            /*MSG_DEBUG("  block " << std::endl << popl[i]->data);*/
            /*MSG_QUEUE_FLUSH();*/
            builder += popl[i]->data;
        }
        /*MSG_DEBUG('#' << i << std::endl << all_blocks.back());*/
    }

    /*MSG_DEBUG("ALL BLOCKS" << std::endl << all_blocks);*/

    return all_blocks;
}



MatrixXd
contrast_groups(const collection<population_value>& all_pops, const locus_key& lk)
{
	static
        std::function<int(const qtl_pop_type&)>
		get_pop_size = [](const qtl_pop_type&)
		{
			return 2;
		};

    std::function<std::vector<std::vector<char>>(const qtl_pop_type&)>
		get_labels = [=](const qtl_pop_type& pop)
		{
            static chromosome dummy;
            context_key ck(new context_key_struc(&pop, NULL, std::vector<double>()));
            /*MSG_DEBUG(ck);*/
            return make_value<Mem>(compute_state_to_parental_origin,
                                   value<context_key>{ck},
                                   as_value(lk->parent))->row_labels;
		};

    /*DUMP_FILE_LINE();*/
	block_builder<std::vector<char>> bb;
	if (active_settings->connected) {
		/*MSG_DEBUG("CONNECTED!");*/
		init_connected_block_builder(bb, get_pop_size, get_labels, all_pops);
	} else {
		/*MSG_DEBUG("DISCONNECTED!");*/
		init_disconnected_block_builder(bb, get_pop_size, get_labels, all_pops);
	}

    MatrixXb big;
    auto builder = bb.begin(big);
    for (const auto& p: all_pops) {
        builder += get_contrast_groups((*p)->gen.get(), lk);
    }
    /*MSG_DEBUG("big" << std::endl << big);*/

    MatrixXb tmp = big.transpose() * big;
    MatrixXb test = MatrixXb::Zero(tmp.innerSize(), tmp.outerSize());
    while (tmp != test) {
        test = tmp;
        tmp = test * test;
    }
    /*MSG_DEBUG("test" << std::endl << test);*/

    bool_row_set uniq;
    for (int i = 0; i < test.innerSize(); ++i) {
        uniq.insert(test.row(i));
    }

    test.resize(uniq.size(), test.outerSize());
    int r = 0;
    for (const auto& row: uniq) {
        test.row(r++) = row;
    }

    return test.cast<double>();
}



collection<parental_origin_per_locus_type>
disassemble_parental_origins_multipop(
        const chromosome_value& chr,
        const locus_key& lk,
        model_block_type& block,
        const collection<population_value>& all_pop)
{
	static
        std::function<int(const qtl_pop_type&)>
		get_pop_size = [](const qtl_pop_type& pop)
		{
			return pop.observed_traits.front().values.size();
		};
        
    std::function<std::vector<std::vector<char>>(const qtl_pop_type&)>
		get_labels = [=](const qtl_pop_type& pop)
		{
            context_key ck(new context_key_struc(&pop, chr, std::vector<double>()));
            /*MSG_DEBUG(ck);*/
            return make_value<Mem>(compute_state_to_parental_origin,
                                   value<context_key>{ck},
                                   as_value(lk))->row_labels;
		};

    /*DUMP_FILE_LINE();*/
	block_builder<std::vector<char>> bb;
	if (active_settings->connected) {
		/*MSG_DEBUG("CONNECTED!");*/
		init_connected_block_builder(bb, get_pop_size, get_labels, all_pop);
	} else {
		/*MSG_DEBUG("DISCONNECTED!");*/
		init_disconnected_block_builder(bb, get_pop_size, get_labels, all_pop);
	}
    collection<parental_origin_per_locus_type> ret;
    ret.reserve(bb.elements.size());
    auto builder = bb.disasm_begin(block.data);
    for (size_t i = 0; i < bb.elements.size(); ++i) {
        ret.emplace_back(new immediate_value<model_block_type>(parental_origin_per_locus_type()));
        builder -= ret.back()->data;
    }
    return ret;
}


std::vector<double>
compute_effective_loci(const locus_key& lk, const std::vector<double>& loci)
{
    if (lk && lk->locus != locus_key_struc::no_locus) {
        std::vector<double> test_loci;
        test_loci.reserve(loci.size());
        for (double k: loci) {
            if (!lk->has(k)) { test_loci.push_back(k); }
        }
        return test_loci;
    } else {
        return loci;
    }
}


template <typename Ret, typename... Args>
collection<model_block_type>
_compute_parental_origins_multipop(Ret (&Joint_POP_at_locus)(Args...),
                                   const collection<population_value>& all_pop,
                                   const value<chromosome_value>& chr,
                                   const value<locus_key>& lk,
                                   const value<std::vector<double>>& loci,
                                   const std::vector<double>& test_loci)
{
    /*DUMP_FILE_LINE();*/
	std::vector<collection<parental_origin_per_locus_type>> all_popl;
    /*DUMP_FILE_LINE();*/
	all_popl.reserve(all_pop.size());
    /*DUMP_FILE_LINE();*/

    /*MSG_DEBUG("loci are " << loci);*/
    /*MSG_DEBUG("given " << lk << ", test loci are " << test_loci);*/

    collection<locus_key> lkeys;
    lkeys.reserve(test_loci.size());
    for (double l: test_loci) {
        lkeys.emplace_back(*lk + l);
    }

	for (auto& p: all_pop) {
        /*DUMP_FILE_LINE();*/
        context_key ck(new context_key_struc(*p, *chr, *loci));
        all_popl.emplace_back(make_collection<Disk>(Joint_POP_at_locus,
                                                    value<context_key>{ck}, lkeys));
        /*std::cout << all_popl << std::endl;*/
		/*all_popl.emplace_back(compute_parental_origins(p, (*qtl_chr)->chr, loci));*/

	}
    for (auto& pl: all_popl) {
        for (auto& l: pl) {
            (void) *l;
        }
    }
    return assemble_parental_origins_multipop(chr, lk, all_popl, all_pop, &Joint_POP_at_locus == &joint_parental_origin_at_locus);
}



collection<model_block_type>
compute_dominance_multipop(const collection<population_value>& all_pop,
                           const value<chromosome_value>& chr,
                           const value<locus_key>& lk,
        /*const value<qtl_chromosome_value>& qtl_chr,*/
                           const value<std::vector<double>>& loci,
                           const std::vector<double>& test_loci)
{
    /*DUMP_FILE_LINE();*/
    std::vector<collection<parental_origin_per_locus_type>> all_popl;
    /*DUMP_FILE_LINE();*/
    all_popl.reserve(all_pop.size());
    /*DUMP_FILE_LINE();*/

    /*MSG_DEBUG("loci are " << loci);*/
    /*MSG_DEBUG("given " << lk << ", test loci are " << test_loci);*/

    collection<locus_key> lkeys;
    lkeys.reserve(test_loci.size());
    for (double l: test_loci) {
        lkeys.emplace_back(*lk + l);
    }

    for (auto& p: all_pop) {
        /*DUMP_FILE_LINE();*/
        context_key ck(new context_key_struc(*p, *chr, *loci));
        all_popl.emplace_back(make_collection<Disk>(joint_dominance_at_locus,
                                                    value<context_key>{ck}, lkeys, as_value(false)));
        /*std::cout << all_popl << std::endl;*/
        /*all_popl.emplace_back(compute_parental_origins(p, (*qtl_chr)->chr, loci));*/

    }
    for (auto& pl: all_popl) {
        for (auto& l: pl) {
            (void) *l;
        }
    }
    return assemble_parental_origins_multipop(chr, lk, all_popl, all_pop, false);

}


collection<model_block_type>
compute_dominance_multipop(const collection<population_value>& all_pop,
                           const value<chromosome_value>& chr,
                           const std::vector<locus_key>& lk,
        /*const value<qtl_chromosome_value>& qtl_chr,*/
                           const value<std::vector<double>>& loci,
                           const std::vector<double>& test_loci)
{
    /*DUMP_FILE_LINE();*/
    std::vector<collection<parental_origin_per_locus_type>> all_popl;
    /*DUMP_FILE_LINE();*/
    all_popl.reserve(all_pop.size());
    /*DUMP_FILE_LINE();*/

    /*MSG_DEBUG("loci are " << loci);*/
    /*MSG_DEBUG("given " << lk << ", test loci are " << test_loci);*/

    collection<locus_key> lkeys;
    lkeys.reserve(test_loci.size());
    auto lki = lk.begin();
    for (double l: test_loci) {
        lkeys.emplace_back(*lki++ + l);
    }

    for (auto& p: all_pop) {
        /*DUMP_FILE_LINE();*/
        context_key ck(new context_key_struc(*p, *chr, *loci));
        all_popl.emplace_back(make_collection<Disk>(joint_dominance_at_locus,
                                                    value<context_key>{ck}, lkeys, as_value(false)));
        /*std::cout << all_popl << std::endl;*/
        /*all_popl.emplace_back(compute_parental_origins(p, (*qtl_chr)->chr, loci));*/

    }
    for (auto& pl: all_popl) {
        for (auto& l: pl) {
            (void) *l;
        }
    }
    return assemble_parental_origins_multipop(chr, lk, all_popl, all_pop, false);
}


collection<model_block_type>
compute_parental_origins_multipop(const collection<population_value>& all_pop,
                                  const value<chromosome_value>& chr,
                                  const value<locus_key>& lk,
        /*const value<qtl_chromosome_value>& qtl_chr,*/
                                  const value<std::vector<double>>& loci,
                                  const std::vector<double>& test_loci)
{
#if 1
    /*locus_key lk(new locus_key_struc((*qtl_chr)->qtl));*/
    /*int n_qtl = (*qtl_chr)->qtl.size();*/

    /*DUMP_FILE_LINE();*/
    std::vector<collection<parental_origin_per_locus_type>> all_popl;
    /*DUMP_FILE_LINE();*/
    all_popl.reserve(all_pop.size());
    /*DUMP_FILE_LINE();*/

#if 0
    std::vector<double> test_loci;
    /*MSG_DEBUG("have lk=" << lk);*/
    if (lk && *lk && (*lk)->locus != locus_key_struc::no_locus) {
        test_loci.reserve(loci->size());
        for (double k: *loci) {
            if (!(*lk)->has(k)) { test_loci.push_back(k); }
        }
    } else {
        test_loci = *loci;
    }
#endif
//    MSG_DEBUG("loci are " << loci);
//    MSG_DEBUG("given " << lk << ", test loci are " << test_loci);

    collection<locus_key> lkeys;
    lkeys.reserve(test_loci.size());
    for (double l: test_loci) {
        lkeys.emplace_back(*lk + l);
    }

    for (auto& p: all_pop) {
        /*DUMP_FILE_LINE();*/
        context_key ck(new context_key_struc(*p, *chr, *loci));
        all_popl.emplace_back(make_collection<Disk>(joint_parental_origin_at_locus,
                                                    value<context_key>{ck}, lkeys));
#if 0
        size_t n_parents = make_value<Sync|Mem>(compute_state_to_parental_origin,
                                                value<context_key>{ck}, as_value(locus_key()))->innerSize();
        auto& popl = all_popl.back();
        for (size_t i = 0; i < popl.size(); ++i) {
            size_t loc_ord = (*lk)->locus_order(test_loci[i]);
            auto vpermut = make_value<Mem>(permutation_matrix,
                                           as_value(n_parents), as_value(1 + (*lk)->depth()),
                                           as_value(loc_ord));
            popl[i]->data *= *vpermut;
        }
#endif
        /*std::cout << all_popl << std::endl;*/
        /*all_popl.emplace_back(compute_parental_origins(p, (*qtl_chr)->chr, loci));*/

    }
    for (auto& pl: all_popl) {
        for (auto& l: pl) {
            (void) *l;
        }
    }
    return assemble_parental_origins_multipop(chr, lk, all_popl, all_pop, true);
#else
    return _compute_parental_origins_multipop(joint_parental_origin_at_locus, all_pop, chr, lk, loci, test_loci);
#endif
}


collection<model_block_type>
compute_parental_origins_multipop(const collection<population_value>& all_pop,
                                  const value<chromosome_value>& chr,
                                  const std::vector<locus_key>& lk,
        /*const value<qtl_chromosome_value>& qtl_chr,*/
                                  const value<std::vector<double>>& loci,
                                  const std::vector<double>& test_loci)
{
#if 1
    /*locus_key lk(new locus_key_struc((*qtl_chr)->qtl));*/
    /*int n_qtl = (*qtl_chr)->qtl.size();*/

    /*DUMP_FILE_LINE();*/
    std::vector<collection<parental_origin_per_locus_type>> all_popl;
    /*DUMP_FILE_LINE();*/
    all_popl.reserve(all_pop.size());
    /*DUMP_FILE_LINE();*/

#if 0
    std::vector<double> test_loci;
    /*MSG_DEBUG("have lk=" << lk);*/
    if (lk && *lk && (*lk)->locus != locus_key_struc::no_locus) {
        test_loci.reserve(loci->size());
        for (double k: *loci) {
            if (!(*lk)->has(k)) { test_loci.push_back(k); }
        }
    } else {
        test_loci = *loci;
    }
#endif
//    MSG_DEBUG("loci are " << loci);
//    MSG_DEBUG("given " << lk << ", test loci are " << test_loci);

    collection<locus_key> lkeys;
    lkeys.reserve(test_loci.size());
    auto lki = lk.begin();
    for (double l: test_loci) {
        lkeys.emplace_back(*lki++ + l);
    }

    for (auto& p: all_pop) {
        /*DUMP_FILE_LINE();*/
        context_key ck(new context_key_struc(*p, *chr, *loci));
        all_popl.emplace_back(make_collection<Disk>(joint_parental_origin_at_locus,
                                                    value<context_key>{ck}, lkeys));
#if 0
        size_t n_parents = make_value<Sync|Mem>(compute_state_to_parental_origin,
                                                value<context_key>{ck}, as_value(locus_key()))->innerSize();
        auto& popl = all_popl.back();
        for (size_t i = 0; i < popl.size(); ++i) {
            size_t loc_ord = (*lk)->locus_order(test_loci[i]);
            auto vpermut = make_value<Mem>(permutation_matrix,
                                           as_value(n_parents), as_value(1 + (*lk)->depth()),
                                           as_value(loc_ord));
            popl[i]->data *= *vpermut;
        }
#endif
        /*std::cout << all_popl << std::endl;*/
        /*all_popl.emplace_back(compute_parental_origins(p, (*qtl_chr)->chr, loci));*/

    }
    for (auto& pl: all_popl) {
        for (auto& l: pl) {
            (void) *l;
        }
    }
    return assemble_parental_origins_multipop(chr, lk, all_popl, all_pop, true);
#else
    return _compute_parental_origins_multipop(joint_parental_origin_at_locus, all_pop, chr, lk, loci, test_loci);
#endif
}



#if 0
/* TODO: use stfopom.haplo[12] to determine the 1 or 2 contrast groups per generation */
collection<std::vector<char>>
contrast_groups(const std::set<char>& vertices,
				const std::set<std::pair<char, char>>& edges)
{
	std::vector<char> parents;
	parents.reserve(vertices.size());
	parents.assign(vertices.begin(), vertices.end());
	/*MSG_DEBUG("parents: " << parents);*/

	std::map<char, int> indices;
	for (char c: parents) {
		int sz = indices.size();
		indices.insert({c, sz});
	}

	MatrixXb links = MatrixXb::Zero(indices.size(), indices.size());
	for (auto& e: edges) {
		int i = indices.find(e.first)->second;
		int j = indices.find(e.second)->second;
		links(i, j) = true;
	}

	MatrixXb cnx = links;
	MatrixXb tmp;
	/*MSG_DEBUG("** initial links **" << std::endl << cnx);*/
	do {
		tmp = cnx;
		cnx *= links;
		/*MSG_DEBUG("** iterating links **" << std::endl << cnx);*/
	} while (tmp != cnx);

	collection<std::vector<char>> ret;

	std::vector<bool> visited(parents.size(), false);
	for (size_t i = 0; i < parents.size(); ++i) {
		if (visited[i]) { continue; }
		auto col = cnx.col(i);
		std::vector<char> tmp;
		for (size_t p = 0; p < parents.size(); ++p) {
			if (col(p)) {
				tmp.push_back(parents[p]);
				visited[p] = true;
			}
		}
		ret.push_back(tmp);
	}

	return ret;
}


template <typename T>
std::ostream& operator << (std::ostream& os, const std::set<T>& s)
{
	os << '{';
	for (auto& t: s) {
		os << ' ' << t;
	}
	return os << " }";
}

collection<std::vector<char>>
contrast_groups_connected()
{
	std::set<std::pair<char, char>> edges;
	std::set<char> vertices;
	std::set<char> v1, v2;
	for (auto& kv: active_settings->populations) {
		generation_value gen = qtl_generation(&kv.second);
		v1.clear();
		v2.clear();
		for (auto ap: gen->main_process().row_labels) {
			v1.insert(ap.first.ancestor);
			v2.insert(ap.second.ancestor);
			vertices.insert(ap.first.ancestor);
			vertices.insert(ap.second.ancestor);
		}
		/*MSG_DEBUG("v1 " << v1);*/
		/*MSG_DEBUG("v2 " << v2);*/
		for (auto v: v1) {
			for (auto u: v1) {
				edges.insert({u, v});
				/*MSG_DEBUG("edge " << u << "->" << v);*/
			}
		}
		for (auto v: v2) {
			for (auto u: v2) {
				edges.insert({u, v});
				/*MSG_DEBUG("edge " << u << "->" << v);*/
			}
		}
	}

	return contrast_groups(vertices, edges);
}
#endif

locus_probabilities_type
locus_probabilities(const context_key& ck, const locus_key& lk,
                    /*const MatrixXd& mgo,*/
                    int ind,
                    const std::vector<double>& loci);

VectorXd
test_with_permutations_for_model(const std::string &trait, int, chromosome_value chr, const locus_key &lk,
                                 const model &Mperm, size_t y_block_size)
{
    /*const auto& pbt = active_settings->pops_by_trait.find(single_trait)->second;*/
    collection<population_value> all_pop;
    for (const auto& popv: active_settings->linked_pops[trait]) {
        for (const auto& pop_ptr: popv) {
            all_pop.emplace_back(as_value<population_value>(pop_ptr.get()));
        }
    }
    /*collection<population_value> all_pop(pbt.begin(), pbt.end());*/
    value<std::vector<double>> testpos = test_loci(chr);

    /* precompute locus probabilities first */
    for (const auto& vpop: all_pop) {
        context_key ck(new context_key_struc(*vpop, chr, *testpos));
        for (auto& x: make_collection<Disk>(locus_probabilities,
                    as_value(ck), as_value(lk), range<int>(0, (*vpop)->size(), 1),
                    as_value(compute_effective_loci(lk, *testpos)))) {
            (void)*x;
        }
    }

    collection<model_block_type> all_blocks = compute_parental_origins_multipop(all_pop, chr, lk, testpos, compute_effective_loci(lk, *testpos));
    /*MatrixXd ftac = f_test_along_chromosome(Mperm, Mperm, all_blocks);*/
    computation_along_chromosome cac;
    /*model_block_key mbk = model_block_key_struc::pop(chr, lk);*/
    /*mbk.selection[chr] = lk;*/
    compute_along_chromosome<>(cac, y_block_size == 1 ? FTest : Chi2, Test, Mperm, Mperm, y_block_size, lk, chr, *testpos, all_blocks);
    /*MSG_DEBUG("ftac" << std::endl << cac.test);*/
//    MSG_DEBUG(MATRIX_SIZE(cac.ftest_pvalue));
    return row_max(y_block_size == 1 ? cac.ftest_pvalue : cac.chi2);
}


VectorXd
test_with_permutations(const std::string &trait, int n_permut, size_t y_block_size, chromosome_value chr, const locus_key &lk)
{
	/*collection<population_value> all_pop = all_populations();*/
    /*const auto& pbt = active_settings->pops_by_trait.find(single_trait)->second;*/
    collection<population_value> all_pop;
    for (const auto& popv: active_settings->linked_pops[trait]) {
        for (const auto& pop_ptr: popv) {
            all_pop.emplace_back(as_value<population_value>(pop_ptr.get()));
        }
    }
    /*collection<population_value> all_pop(pbt.begin(), pbt.end());*/
    auto covar_block = make_value<Mem>(make_covariables_block, as_value(trait), as_value(active_settings->with_covariables));
    value<model> Mperm = init_model(trait_permutations(trait, n_permut), cross_indicator(trait), *covar_block, 0);
    return *make_value<Disk | Sync>(test_with_permutations_for_model, as_value(trait), as_value(n_permut),
                                    as_value(chr), as_value(lk), Mperm, as_value(y_block_size));
}


double
qtl_threshold(const std::string& trait_name, chromosome_value chr, const locus_key& lk, double quantile, int n_permut, size_t y_block_size)
{
    value<VectorXd> maxima = make_value<Disk>(test_with_permutations,
                                              as_value(trait_name), as_value(n_permut), as_value(y_block_size), as_value(chr), as_value(lk));
    return get_quantiles(*maxima, {quantile, .1, .15, .2, .25, .5, .8})[0];
}



/* FIXME: inject y_block_size throughout the whole chain */
double qtl_threshold_all_chromosomes_for_model(const std::string& trait_name, double quantile, int n_permut, size_t y_block_size, const model& Mperm)
{
    collection<VectorXd> tmp;
    model Mtmp = Mperm;
    Mtmp.m_computed = false;
    Mtmp.m_Y = trait_permutations(trait_name, n_permut);
    Mtmp.compute();
    for (auto& qc: active_settings->working_set) {
        value<const qtl_chromosome*> qtl_chr = &qc;
        tmp.push_back(make_value<Disk | Sync>(test_with_permutations_for_model,
                                              as_value(trait_name), as_value(n_permut),
                                              as_value((*qtl_chr)->chr), as_value(locus_key(new locus_key_struc())),
                                              as_value(Mtmp), as_value(y_block_size)));
    }
    MatrixXd max_per_chrom(n_permut, tmp.size());
    for (size_t i = 0; i < tmp.size(); ++i) {
        max_per_chrom.col(i) = *tmp[i];
    }

    VectorXd maxima = row_max(max_per_chrom);

#if 0
    size_t sz = 0;
    for (auto& v: tmp) {
        sz += v->innerSize();
    }
    /*MSG_DEBUG("sz=" << sz);*/
    VectorXd maxima(sz);
    sz = 0;
    for (auto& v: tmp) {
        for (int i = 0; i < v->innerSize(); ++i) {
            maxima(sz + i) = (*v)(i);
        }
        sz += v->innerSize();
    }
#endif
    return get_quantiles(maxima, {quantile, .1, .15, .2, .25, .5, .8})[0];
}



double qtl_threshold_all_chromosomes(const std::string& trait_name, size_t y_block_size, double quantile, int n_permut)
{
    /*const auto& pbt = active_settings->pops_by_trait.find(trait_name)->second;*/
    /*collection<population_value> all_pop(pbt.begin(), pbt.end());*/
    collection<population_value> all_pop;
    for (const auto& popv: active_settings->linked_pops[trait_name]) {
        for (const auto& pop_ptr: popv) {
            all_pop.emplace_back(as_value<population_value>(pop_ptr.get()));
        }
    }
    auto covar_block = make_value<Mem>(make_covariables_block, as_value(trait_name), as_value(active_settings->with_covariables));
    model Mperm = init_model(trait_matrix(trait_name, all_pop), cross_indicator(trait_name), *covar_block, 0);
    return *make_value<Disk|Sync>(qtl_threshold_all_chromosomes_for_model,
                                  as_value(trait_name), as_value(quantile), as_value(n_permut),
                                  as_value(y_block_size), as_value(Mperm));
}


#if 0
collection<model_block_type>
compute_parental_origins_multipop_extension(
        const collection<population_value>& all_pop,
        const value<qtl_chromosome_value>& qtl_chr,
        const value<std::vector<double>>& loci,
        const value<model_block_type>& source,
        int sz)
{
    collection<model_block_type> all_blocks = compute_parental_origins_multipop(all_pop, qtl_chr, loci);
    ArrayXd mult = kroneckerProduct(source->data, MatrixXd::Ones(1, sz)).array();
    /*MSG_DEBUG("DEBUG BLOCK MULT" << std::endl << mult << std::endl);*/

    for (auto& b: all_blocks) {
        /*MSG_DEBUG("DEBUG BLOCK BEFORE" << std::endl << b->data << std::endl);*/
        b->data.array() = b->data.array() * mult;
        /*MSG_DEBUG("DEBUG BLOCK AFTER" << std::endl << b->data << std::endl);*/
    }

	return all_blocks;
}
#endif


#include "io/output_impl.h"

