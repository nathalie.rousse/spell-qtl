/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*#include "all.h"*/
// #include "geno_matrix.h"
//#include "error.h"
//#include "util/malloc.h"
#include "eigen.h"
#include "input.h"
//#include "cache/md5.h"
//#include "cache2.h"
#include "computations.h"
//#include "util/chrono.h"
#include "io/output_impl.h"
#include "io/python.h"

#include <chrono>

double settings_t::get_threshold(const std::string& trait, size_t y_block_size)
{
    double& thres = qtl_thresholds[trait];
    if (thres == 0) {
        set_title(SPELL_STRING("Computing threshold for single_trait '" << trait << "' with " << n_permutations << " permutations and quantile " << qtl_threshold_quantile));
        thres = *make_value<Disk|Mem>(qtl_threshold_all_chromosomes,
                                  as_value(trait),
                                  as_value(y_block_size),
                                  as_value(qtl_threshold_quantile),
                                  as_value(n_permutations));
    }
    return thres;
}


int foo(int i) { return i << 3; }
int baz(int a, int b)
{
    std::this_thread::sleep_for(std::chrono::duration<double, std::milli>{1000.});
    value<int> z = make_value<Disk|Mem>(foo, as_value(a)), y = make_value<Disk|Mem>(foo, as_value(b));
    return *y + *z;
}



void
hangs_in_python(model_manager& mm)
{
//     def test(mm):
//         for e in mm.search_new_best_per_chromosome(False, ""):
//             if e.data().over_threshold:
//                 e.data().select(mm)
//         mm.challenge_qtl("ch1", 114.96)
    chromosome_value chr = NULL;
    // double locus;
    for (const auto& kv: mm.search_new_best_per_chromosome(false, NULL)) {
        if (kv.second.over_threshold) {
            kv.second.select(mm);
            chr = kv.second.chrom;
            // locus = kv.second.locus;
        }
    }
    if (chr) {
//         mm.challenge_qtl(chr, locus);  // TODO FIXME snap locus to test positions.
        mm.challenge_qtl(chr, 114.96);
//         mm.challenge_qtl(active_settings->find_chromosome("ch1"), 114.96);
    }
}



#if 0
std::ostream& operator << (std::ostream& os, const std::vector<char>& v)
{
    if (v.begin() == v.end()) {
        return os << '.';
    }

    auto i = v.begin(), j = v.end();
    os << (*i++);
    while (i != j) {
        os << '.' << (*i++);
    }
    return os;
}
#endif

void dump_pm(int np, int nl, int o)
{
    MatrixXd pm = permutation_matrix(np, nl, o);
    MSG_DEBUG("permutation_matrix(n_parents=" << np << ", n_loci=" << nl << ", order=" << o << ") ="
              << std::endl << pm);
    MSG_DEBUG("is permutation matrix? " << ((pm * pm.transpose()) == MatrixXd::Identity(pm.innerSize(), pm.innerSize())));
    MSG_DEBUG("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
}


extern "C" void delete_settings() { if (active_settings) { /*msg_handler_t::instance().queue.m_stop = true;*/ delete active_settings; } }

//#include "computations/probabilities.h"

locus_probabilities_type
locus_probabilities(const context_key& ck, const locus_key& lk,
                    /*const MatrixXd& mgo,*/
                    int ind,
                    const std::vector<double>& loci);

std::string
marker_names_and_increment(chromosome_value chr, chromosome::haplotype_iterator& hi, double locus) {
    auto haplo = *hi;
    double haplocus = chr->raw.marker_locus[haplo.first];
    if (locus == haplocus) {
        std::stringstream ss;
        ss << chr->raw.marker_name[haplo.first];
        for (size_t i = haplo.first + 1; i < haplo.second; ++i) {
            ss << ',' << chr->raw.marker_name[i];
        }
        ++hi;
        return ss.str();
    }
    return "";
}

int
dump_locus_probabilities(const context_key& ck, const locus_key& lk,
                         int ind, const std::vector<double>& loci) {
//    value<locus_probabilities_type> lp = make_value<Disk>(locus_probabilities,
//                                                          as_value(ck), as_value(lk),
//                                                          as_value(ind),
//                                                          as_value(loci));
    locus_probabilities_type _lp = locus_probabilities(ck, lk, ind, loci);
    locus_probabilities_type* lp = &_lp;
    std::string dirname = SPELL_STRING(active_settings->work_directory << '/' << active_settings->name << ".n-point/" << ck->chr->name << '/' << ck->pop->gen->name);
    ensure_directories_exist(dirname);
    std::string filename = SPELL_STRING(dirname << '/' << active_settings->name << '.' << ck->chr->name << '.' << ck->pop->gen->name << '.' << ind << ".csv");
    std::ofstream ofs(filename);
    auto chr = ck->chr;
    auto hi = chr->begin();
    ofs << "markers";
    for (double l: loci) {
        ofs << ';' << marker_names_and_increment(chr, hi, l);
    }
    ofs << std::endl;
    ofs << "locus";
    for (double l: loci) {
        ofs << ';' << l;
    }
    ofs << std::endl;
    for (int row = 0; row < lp->rows(); ++row) {
        ofs << lp->row_labels[row];
        for (int col = 0; col < lp->cols(); ++col) {
            ofs << ';' << lp->data(row, col);
        }
        ofs << std::endl;
    }
    return 0;
}



void report_full_map();
void finish_report();


#if 0
struct py_task : Task {
    py_task(model_manager& _) : mm(_), started(false) {}
    std::thread::id
    run() override
    {
        started.store(true);
        python_environment mypy;
        std::string algo(active_settings->qtl_algorithm.begin() + 3,
                        active_settings->qtl_algorithm.end());
        mypy.run_script(algo, mm);
        return {};
    }
    
    bool has_run() const override { return started.load(); }

private:
    model_manager& mm;
    std::atomic_bool started;
};
#endif



int
main(int argc, const char** argv)
{
    (void)msg_handler_t::instance();

    atexit(delete_settings);

    chrono::display() = true;
    chrono::start("*** initialization");

    active_settings = settings_t::from_args(argc, argv);

    if (!active_settings) {
        exit(0);
    }

    /*msg_handler_t::check(true);*/

    active_settings->finalize();

    
//     auto v = make_value<Disk|Mem>(baz, as_value(23), as_value(42));
//     MSG_DEBUG("Have " << (*v));
//     return 0;
    
    
    
    active_settings->set_title("Checking the validity of the configuration");
    if (!active_settings->sanity_check()) {
        MSG_QUEUE_FLUSH();
        exit(-1);
    }

    msg_handler_t::check(true);

    chrono::stop("*** initialization");

    /* some screen management */
    if (msg_handler_t::color() && active_settings->parallel > 1) {
        MSG_DEBUG("\x1b[2J\x1b[0;0H" << std::endl << std::endl << std::endl << std::endl);
        for (int i = 0; i < active_settings->parallel; ++i) { MSG_DEBUG(""); }
    }

    if (active_settings->output_npoint) {
        auto all_loci = full_search_intervals(std::map<chromosome_value, locus_key>());

        collection<int> all_lp;
        for (const auto& chr_interval: all_loci) {
            chromosome_value chr = chr_interval.first;
            const auto& loci = chr_interval.second./*front().*/all_positions;
            locus_key empty_lk;
            ensure_directories_exist(SPELL_STRING(active_settings->work_directory << '/' << active_settings->name << ".n-point"));
//            for (const std::string& gen: active_settings->npoint_gen) {
            for (auto pop: active_settings->populations) {
                context_key ck(new context_key_struc(pop.get(), chr, loci));
                auto alp = make_collection<>(dump_locus_probabilities,
                                             as_value(ck), as_value(empty_lk),
                                             range<int>(0, pop->size(), 1),
                                             as_value(loci));
                all_lp.insert(all_lp.end(), alp.begin(), alp.end());
            }
        }
        for (auto& x: all_lp) { *x; }
        return 0;
    }

    {
        std::stringstream sargs;
        for (const char** a = argv + 1; *a; ++a) {
            sargs << " " << (*a);
        }
        MSG_INFO("Command:" << sargs.str());
    }

    /* FIXME working set? */
    for (auto& k: active_settings->map) {
        active_settings->working_set.emplace_back(&k);
    }

#if 0
    /* Build lists of populations per observed single_trait */
    std::map<std::string, collection<population_value>> pops_by_trait;
    for (const auto& kv_pop: active_settings->populations) {
        for (const auto& single_trait: kv_pop.second.observed_traits) {
            pops_by_trait[single_trait.name].emplace_back(&kv_pop.second);
        }
    }
#endif

    /*std::map<std::string, std::map<double, std::string>> poi;*/
    /*std::map<std::string, std::map<double, std::pair<double, double>>> roi;*/

//     analysis_report ar(SPELL_STRING(active_settings->work_directory << '/' << active_settings->name << ".report"), AR::All);

    try {

    for (const auto& trait_pops: active_settings->linked_pops) {
        collection<population_value> pops;
        for (const auto& pvec: trait_pops.second) {
            for (const auto& pptr: pvec) {
                pops.emplace_back(pptr.get());
            }
        }

#if 0
        if (trait_pops.first == "VAR2_LUP") {
            auto chr = active_settings->find_chromosome("chr7");
            auto steps = compute_steps(chr->condensed.marker_locus, active_settings->step);
            context_key ck(new context_key_struc(*pops.front(),
                                                 chr,
                                                 std::vector<double>()));
            locus_key lk;
            auto mat = locus_probabilities(ck, lk, 3, steps);
            const geno_matrix* gen = (*pops.front())->gen.get();
            MSG_DEBUG("GENO_MATRIX" << std::endl << (*gen));
            MSG_DEBUG("" << mat);
        } else {
            continue;
        }
        break;
#endif

//         active_settings->
        
        model_manager mm(trait_pops.first, pops,
                         trait_matrix(trait_pops.first, pops),
                         //active_settings->get_threshold(trait_pops.first) * .9, /* FIXME, get_threshold needs pops_by_trait, so maybe move pops_by_trait to settings */
                         /*ComputationType::FTest,*/
                         active_settings->max_order,  /* FIXME make max_order configurable in settings_t */
                         SolverType::QR);
        init_analysis_report(mm);
        MSG_DEBUG("initial selection " << active_settings->initial_selection << " qtl detection algorithm " << active_settings->qtl_algorithm);
        if (active_settings->initial_selection != "" && active_settings->qtl_algorithm == "none") {
            mm.threshold = 0;
        } else {
            report_algo_phase(SPELL_STRING("Computing threshold for trait " << trait_pops.first));
            mm.threshold = active_settings->get_threshold(trait_pops.first, mm.y_block_cols) * .9;
        }
//         ar.attach_model_manager(mm);

#if 1

        /* STEP 1
         * Skeleton
         */
        /*init_skeleton(mm);*/
        mm.search_intervals = skeleton_search_intervals();

        /* STEP 2
         * Cofactors
         */
        //*
        if (active_settings->initial_selection == "") {
            active_settings->set_title(
                SPELL_STRING("Cofactor detection (" << active_settings->cofactor_algorithm << ')'));
            MSG_INFO("Cofactor acceptance threshold set to " << mm.threshold);
            MSG_QUEUE_FLUSH();
            if (active_settings->cofactor_algorithm == "forward") {
                forward(mm);
            } else if (active_settings->cofactor_algorithm == "backward") {
                backward(mm);
            } else if (active_settings->cofactor_algorithm == "all") {
                mm.select_all_loci();
            }
            MSG_QUEUE_FLUSH();
            mm.compute_model();
        } else {
            MSG_INFO("Skipping skeleton and cofactor detection.");
            mm.search_intervals = full_search_intervals({});
            mm.set_selection(active_settings->initial_selection);
            mm.compute_model();
            report_algo_phase(SPELL_STRING("Initial selection: " << mm.vMcurrent->keys()));
        }
        //*/
//         MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
        mm.compute_model();
//         MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
        /*MSG_DEBUG("Trait " << trait_pops.first << std::endl << mm.Mcurrent);*/

        /* STEP 3
         * QTL detection
         */

        /*mm.init_loci_by_step_with_exclusion(active_settings->step, active_settings->cofactor_exclusion_window_size);*/

        MSG_DEBUG("initial selection " << active_settings->initial_selection << " qtl detection algorithm " << active_settings->qtl_algorithm);
        MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());

        auto sel = mm.get_selection();
        MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
        mm.search_intervals = full_search_intervals(sel);
        MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());

        if (active_settings->qtl_algorithm != "none") {
            active_settings->set_title(
                SPELL_STRING("QTL detection (" << active_settings->qtl_algorithm << ')'));
            MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());

            mm.threshold = active_settings->get_threshold(trait_pops.first, mm.y_block_cols);
            MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
            MSG_INFO("QTL acceptance threshold set to " << mm.threshold);
            MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
            MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());

            //*
#if 0
            if (!active_settings->qtl_algorithm.compare(0, 3, "py:")) {
//                 static py_task task(mm);
//                 TaskPool::enqueue(&task);
//                 task.run();
//                 std::thread py([&]() {
//                 hangs_in_python(mm);
                   python_environment mypy;
                   std::string algo(active_settings->qtl_algorithm.begin() + 3,
                                    active_settings->qtl_algorithm.end());
                   mypy.run_script(algo, mm);
//                 });
//                 py.join();
            } else
#endif
            if (active_settings->qtl_algorithm == "CIM") {
                qtl_detect_cim(mm);
            } else if (active_settings->qtl_algorithm == "CIM-") {
                qtl_detect_cim_minus(mm);
            } else if (active_settings->qtl_algorithm == "iQTLm") {
                qtl_detect_iqtlm(mm);
            } else if (active_settings->qtl_algorithm == "iQTLm-GW") {
                qtl_detect_iqtlm_gw(mm);
            }
            MSG_QUEUE_FLUSH();
        } else {
            MSG_INFO("Skipping QTL detection.");
            report_algo_phase("Skipping QTL detection.");
        }
        /*/
        mm.select_chromosome("chr7");
        mm.add(active_settings->find_chromosome("chr7"), 104.87);

        mm = mm.model_for_estimation();
        mm.vMcurrent->enable_constraints();
        mm.vMcurrent->compute();

        ar.report_final_model(mm);

        mm.remove(104.87);
        mm.select_chromosome("chr7");
        mm.add(active_settings->find_chromosome("chr7"), 96.89);
        //*/

//         MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
        mm.vMcurrent->compute();

        /*mm = mm.model_for_estimation();*/
//         if (active_settings->initial_selection == "") {
//             MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
//        MSG_DEBUG("MODEL BEFORE CONSTRAINTS" << std::endl << (*mm.vMcurrent));

//             MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
            mm.vMcurrent->enable_constraints();
//             MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
            mm.vMcurrent->compute();
//             MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());

//        MSG_DEBUG("MODEL AFTER CONSTRAINTS" << std::endl << (*mm.vMcurrent));

//             MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
//             ar.report_final_model(mm);
//             ar.report_final_model_excel(mm);
            make_report(mm, std::vector<std::string>{argv, argv + argc});
//             MSG_INFO(__FILE__ << ':' << __LINE__ << " mm.selection = " << mm.get_selection());
//         } else {
//             model raw(*mm.vMcurrent);
//             for (const auto& chr: active_settings->map) {
//                 raw.remove_blocks_with_chromosome(&chr);
//             }
//             raw.compute();
//             double test = 0;
//             if (mm.vMcurrent->Y().cols() > 1) {
//                 VectorXd tmp = chi2(raw, *mm.vMcurrent, mm.vMcurrent->Y().cols());
//                 test = tmp(0);
//             } else {
//                 MatrixXd tmp(1, 1);
//                 f_test(raw, *mm.vMcurrent, 0, &tmp, NULL);
//                 test = tmp(0, 0);
//             }
//             MSG_DEBUG("TEST: " << test);
//         }

//         ar.detach_model_manager(mm);

#endif
        if (0)
        {  /* testing test_manager */
            test_all_domain tad;
            test_manager tm(trait_pops.first, 1, 0, true);
            std::vector<genome_search_domain> vgsd(1);
            for (const chromosome& c: active_settings->map) {
                vgsd.back().emplace_back(&c, compute_steps(c.condensed.marker_locus, active_settings->step));
            }
            tm.compute(vgsd, tad);

            MSG_DEBUG("TEST MANAGER SAYS");
            MSG_DEBUG("" << tm.results);

            auto ri = tm.results.begin(); //, rj = tm.results.end();
            double v0, v1, l0, l1;
            for (const chromosome& c: active_settings->map) {
                v0 = v1 = 0;
                l0 = l1 = 0;
                for (double l: c.condensed.marker_locus) {
                    l0 = l1;
                    l1 = l;
                    v0 = v1;
                    locus_set point = {{&c, l}};
                    v1 = tm.results.find(point)->second;
                    std::vector<double> values;
                    for (; ri->first < point; ++ri) {
                        values.emplace_back(ri->second - std::min(v0, v1));
                    }
                    MSG_DEBUG("Between " << (locus_set{{&c, l0}}) << " and " << (locus_set{{&c, l1}}));
                    MSG_DEBUG("" << values);
                }
                vgsd.back().emplace_back(&c, compute_steps(c.condensed.marker_locus, active_settings->step));
            }
        }

#if 0
        mm.vMcurrent->output_X_to_file();
        mm.vMcurrent->output_XtX_inv_to_file();
        /*mm.QTLs_to_cofactors();*/
        /*mm.vMcurrent->compute();*/
        mm.vMcurrent->output_X_to_file();
        mm.vMcurrent->output_XtX_inv_to_file();
        /*MSG_DEBUG("Trait " << trait_pops.first << std::endl << mm.Mcurrent);*/
        MSG_DEBUG("Final model");
        MSG_DEBUG("" << mm.Mcurrent);
        MSG_QUEUE_FLUSH();
        MSG_DEBUG("pmode = " << mm.pmode);
        model::xtx_printable xtx(mm.Mcurrent, true);
        MSG_DEBUG("XtX^-1" << std::endl << xtx);
        MSG_DEBUG("Coefficients " << mm.vMcurrent->coefficients().transpose());
        /*MSG_DEBUG("Residuals " << mm.vMcurrent->residuals().transpose());*/
        MSG_DEBUG("RSS " << mm.vMcurrent->rss());
        /*MSG_DEBUG("Model " << std::endl << mm.Mcurrent);*/
        MSG_QUEUE_FLUSH();

        auto QTLs = mm.QTLs();

        for (const auto& qtl: QTLs) {
            if (poi[qtl.chromosome][qtl.locus].size()) {
                poi[qtl.chromosome][qtl.locus] = SPELL_STRING(poi[qtl.chromosome][qtl.locus] << ',' << trait_pops.first);
            } else {
                poi[qtl.chromosome][qtl.locus] = trait_pops.first;
            }
            roi[qtl.chromosome][qtl.locus] = qtl.confidence_interval();
        }

#if 0
        for (const auto& mbk: mm.vMcurrent->m_blocks) {
            for (const auto& chr_lk: mbk.first.selection) {
                for (double l: chr_lk.second) {
                    if (poi[chr_lk.first->name][l].size()) {
                        poi[chr_lk.first->name][l] = SPELL_STRING(poi[chr_lk.first->name][l] << ',' << trait_pops.first);
                    } else {
                        poi[chr_lk.first->name][l] = trait_pops.first;
                    }
                }
            }
        }

        for (const auto& cqi: mm.qtl_confidence_interval) {
            for (const auto& qi: cqi.second) {
                roi[cqi.first][qi.first] = qi.second;
            }
        }
#endif
    }

    size_t width = msg_handler_t::termcols();
    if (width > 80) {
        width = width * .8;
    }
    for (const auto& chr: active_settings->map) {
        for (const auto& ckv: poi) {
            for (const auto& kv: ckv.second) {
                MSG_DEBUG("QTL at " << kv.first << ':' << kv.second << " interval {" << roi[ckv.first][kv.first].first << ':' << roi[ckv.first][kv.first].second << '}');
            }
        }
        /*MSG_DEBUG(chr.pretty_print(width, poi[chr.name], roi[chr.name]));*/
#endif
    }

    report_full_map();
    finish_report();
    
    } catch (file::error& fe) {
        MSG_DEBUG("FILE ERROR! " << fe.what());
        for (const auto& kv: file::debug_open_files) {
            if (kv.second) {
                MSG_DEBUG(kv.first << '\t' << kv.second);
            }
        }
        MSG_QUEUE_FLUSH();
        throw fe;
    }

    return 0;
}

