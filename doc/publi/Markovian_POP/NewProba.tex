\documentclass[10pt,a4paper]{nature}
\usepackage{lscape,latexsym}
\usepackage{amsfonts,amsmath,amssymb,mathrsfs}
\DeclareMathOperator{\rank}{rank}

\usepackage{pbox}
\usepackage{bbm}
\usepackage{authblk}

\usepackage{hyperref}
\hypersetup{                  % clean display of URLs, nothing else. 
    colorlinks=false,
    pdfborder={0 0 0},
}


\usepackage{longtable}
\setlength{\LTcapwidth}{17cm} % longtable caption for landscape tables display

\usepackage{booktabs}

\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{calc,fit,matrix,arrows,automata,positioning}
\newcommand\x{\times}
\newcommand\y{\colorbox{red}{$\times$}}
\newcommand\z{\colorbox{gray}{$\times$}}


\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture] \node (#1) {};}
\newcommand{\tikzdrawbox}[3][(0pt,0pt)]{%
    \tikz[overlay,remember picture]{
    \draw[#3]
      ($(left#2)+(-0.3em,0.9em) + #1$) rectangle
      ($(right#2)+(0.2em,-0.4em) - #1$);}
}




\title{True Markovian Parental origin probability for any breeding design.}
\date{}
\author{Sylvain Jasson, Damien Leroux}
\affil{INRA, UR MIA-T, 24 Chemin de Borde Rouge, CS 52627, 31326 Castanet-Tolosan cedex, France }
\begin{document}

\maketitle



\begin{abstract}
The mapping of Quantitative Traits Loci (QTL) require computing the Parental origin probabilities (POP) along the genome. 
The Markov chain is a desirable model for these probabilities, since it allows their fast and genome-wide computation using all the available marker scores. 
Although such a modeling is straightforward for single meiosis populations (Back-cross, F2, Doubled Haploid), fine QTL dissecting population (such as Advanced Intercross (AI) populations) genetic structure  was hindering Markovian modeling. 
We present here a new method to derive a discrete state space and a Markov transition matrix for any cross within any breeding design. (That is any number of parents and any number of successive population random crossing or selfing). 
Moreover, the theoretical causes of previously supposed impossibility of AI populations Markovian modeling are explained. 
The exposed method capability to compute exact joint multi point POP given the markers score, will empower any QTL mapping method used afterwards, regardless of its nature. 

\end{abstract}


Detecting and mapping the Quantitative Traits Loci, is the first step toward the identification and characterization of genes that polymorphism is important in agriculture and human medicine. Therefore a wide range of statistical tools aimed at QTL detection were developed over years\cite{Jansen2008}. 
These analysis methods rely on  highlighting linkage between measured traits value and genetic information along the genome. Is is  then mandatory to fill gaps in  genetic data everywhere needed, that is between molecular markers when the genetic map is sparse, and at marker's position for any missing, ambiguous, partly informative or not absolutely certain marker score. 

In this report Parent (with capital P) means an ancestral Parent inbred line, originating the breeding design, (especially in acronym POP), while parent (with lowercase p) means a particular population's father and/or mother. 


Browsing the successive populations of a design, the exposed method first builds  a very simple Markov model for ancestors  and then derives representative elements for progeny.
For successive crosses within the experimental design, a finite state space $St$ is defined from the state space of its parents (two parents for inter-population random crossing or only one parent for selfing). Any state in this state space can be labeled with a pair of phased Parental origin. For an example, the states in a $F2$ cross from Parents $P_1$ and $P_2$ are labeled with either $(P_1,P_1)$, $(P_1,P_2)$, $(P_2,P_1)$ or $(P_2,P_2)$. A back-cross (BC) from Parents $P_3$ (reentrant) and $P_4$ (donor)  has its states labeled with either $(P_3,P_3)$ or $(P_3,P_4)$. 
More often than not, these labels do not carry enough information to fully characterize the state. Therefore labels may be not unique, multiple states may have the same label and $|St|$ (the cardinality of $St$) may be greater than the number of possible genotypes.

In order to obtain the POP we will compute the probabilities of every state at locus of interest and putative QTL $q$. These probabilities can be stored in $p(q)$, a normed $|St|$-dimensional vector. The POP are then obtained by summing the probabilities of the states sharing the same label. 

The states's probabilities along the genome follow a stochastic process with transition matrix $T_d$ between two loci. This transition matrix depends on the distance $d$ between the loci and reflects the recombination probability during successive meiosis. We have then $p(l+d)=T_d.p(l)$.

This transition matrix must fulfill two requirement : the Chapman-Kolmogorov (CK) equation and existence of a unique stationary distribution $\pi$.
The CK equation ($T_{d+d'}=T_d.T_{d'}$), is a necessary and sufficient condition ensuring the Markov property along genome.
The unique stationary distribution existence will ensure that two loci very far away do not influence each other probabilities vectors. That is $\lim_{d\rightarrow\infty}(T_{d}.p(l))$ exists and its value does not depend on $p(l)$. This particular probability vector $\pi$ displays the states relative abundance away from any marker score. Its value is used in actual POP value computation. 

\section{Results}

\subsection{State space and transition matrix for Parental inbred lines}
Since our purpose is to obtain probabilities of Parental origin, the state space for any Parental line $P_i$ is reduced to a single state ${(P_i,P_i)}$. The probability vector is then reduced to a single component vector $p(q)=(1)$. The Markov transition matrix is obviously reduced to a $1\times 1$ matrix: $T^{P}_d=(1)=I_1$.  

CK equation verification is immediate : $T^{P}_d.T^{P}_{d'}=I_1.I_1=I_1=T^{P}_{d+d'}$
Unique stationary distribution existence too, since this whole Markov chain is stationary.  



\subsection{State Space and transition matrix for gametes}
Given a cross $C$ (with state space $St^C$ and transition matrix $T^C_d$) the meiosis produces a single strand gametes population G with random recombination process along the two parental DNA strands. For any state in $St^C$, two states are created in the gametes population state space $St^G$. Each state in $St^G$ is labeled with a unique Parental origin. For an example, if a state is labeled $(P_1,P_2)$ in $St^C$, two states respectively labeled $P_1$ and $P_2$  appear in $St^G$.

  The transition probabilities between these states result of two independent processes: the transition between states in parental population C and the recombination during the meiosis. 
  
  
The transition matrix is then obtained by the Kronecker product (also named direct product) :
\begin{equation} T^G_d=T^C_d \otimes T_d \mbox{ with } T_d=\left( \begin{array}{cc}
1-r_d     &  r_d\\
r_d      &    1-r_d \end{array} \right) \end{equation}

 $r_d$ stands for the meiosis recombination probability depending of distance d, and using the Haldane distance $r_d=1/2 (1-e^{-2d})$. 
 

 The matrix $T_d$ contains information about the transition probabilities during meiosis and enjoy both KC and stationary distribution properties (see appendix~\ref{app:proof_KC_one}). The matrix $T^C_d$ contains information about all the transition probabilities in C. Both matrix verify the required properties, then $T^G_d$ verifies these properties too. (see proof in appendix~\ref{app:proof_KC_product})


\subsection{Crossing and gametes mating}
The elemental breeding operation is crossing between populations. A new population is produced from random mating between the gametes of each parental cross. (In the case of a selfing operation, mating is only allowed between gametes from the same individual.)

After the mating of gametes populations $FG$ (female gamete with state space $St^{FG}$ and transition matrix $T^{FG}_d$) and $MG$ (male gamete with state space $St^{MG}$ and transition matrix $T^{MG}_d$), the state space of the new population is the Cartesian product of their respective state spaces.
The new states are labeled with juxtaposition of their compound states labels. 

The transition probabilities between these states are the result of two independent processes : the transition between states in the maternal gamete population and the transition between states in the paternal gamete population.  

\begin{equation}St^C=St^{FG} \times St^{MG} \mbox{ and } T^C_d=T^{FG}_d \otimes T^{MG}_d \end{equation}

Both $T^{FG}_d$ and $T^{MG}_d$ verify KC and stationary distribution properties, then $T^C_d$ verifies them too.(see appendix~\ref{app:proof_KC_product})



\subsection{Approximation  of RIL recombination probability} 


The size of state space increase with successive crossing. The Recombinant Inbred Line (RIL) model as a limit of infinite selfing process would then require a space state set of infinite size and a transition matrix of infinite order. 


In order to bypass this problem the classic RIL model\cite{haldane1931inbreeding} uses a space state with only two states (say for a RIL from Parents $P_1$ and $P_2$, $\{(P_1,P_1),(P_2,P_2)\}$) and a pseudo transition matrix \begin{equation}\tau^{RIL}_d=\left( \begin{array}{cc}
1-R_d     &  R_d\\
R_d      &    1-R_d \end{array} \right) \mbox{ with } R_d=\frac{2r_d}{1+2r_d}\end{equation}

 Since the multiple meiosis increase the number of recombinant zygotes (and therefore $R_d\geq r_d$), it is often said that using a RIL design is equivalent to expanding the genetic map. But obviously, this model is not Markovian\cite{Martin01052006}, due to non linearity in definition of $R_d$. Using $\tau^{RIL}_d$ in order to compute POP implies to deal with many problems, including the lack of additivity and the loss of symmetry inside intervals. 
 

 In order to preserve Markov chain properties, and capability of rapid calculation on large datasets, the RIL process can be approximated with any required precision, by a mixture of independent recombination processes at different rates by (see appendix~\ref{app:proof_Taylor}): 
 
\begin{equation} \label{eq:RIL_expansion} R_d=\sum_{k=1}^n \frac{r_{kd}}{2^k}+\epsilon_{d,n} \mbox{ and } |\epsilon_{d,n} | \leq \frac{1}{2^{n+1}} \end{equation}

Each Markov processes is weighted with weight $w_k=1/2^k$, and has a 2-states space state and transition matrix $T_{kd}=\left( \begin{array}{cc}
1-r_{kd}     &  r_{kd}\\
r_{kd}      &    1-r_{kd} \end{array} \right)$. 

For implementation ease, it is possible to build a product space state for these independent processes and a block diagonal transition matrix. Each diagonal block is then $T_{kd}$ (see table~\ref{table:examples}).




\subsection{Multiple loci probabilities}

Since every process in this model verifies KC property, multiple loci probabilities are obtained from matrix multiplication. This allow very fast computation of POP, even when numerous partly informative marks are involved.  


For every observed mark $\mathcal{M}_m$ at locus $m$, it is possible to define a projection matrix $M_m$, its range being the states allowed by the observation and its kernel being the states forbidden by the observation. When the observation is partially informative, the rank of the projection can be very large, on the opposite, when the observation is fully informative this rank is much smaller. (If the score is missing at locus $m$, $M_m=I$). 

If markers were observed in successive populations or in some ancestral populations (for an example the markers were observed in $F2$ and the POP are needed for $F3$), the projection matrix are easily derived remarking that we can build some pedigree for every state in any population state space. In other words, we know every state's label and the labels from its parents, grandparents... Comparing these labels with actual observations allow the construction of projection matrix. 

Suppose the locus of interest $q$ being preceded by $n_l$ observed loci and followed by $n_r$ observed loci, the loci sequence being 
 $[l_{n_l},...,l_2,l_1,q,r_1,r_2,...,r_{n_r}]$. The parental origin probabilities vector at locus $q$ is then :



\begin{multline} \label{eq:pr_qtl1} p(q|\mathcal{M}_{l_1},...,\mathcal{M}_{l_{n_l}},\mathcal{M}_{r_1},...,\mathcal{M}_{r_{n_r}}) =\\
\frac{ \left(T_{q-l_1}\displaystyle\left(\prod_{i=1}^{n_l-1}     M_{l_i}T_{l_{i}-l_{i+1}} \right)      M_{l_{n_l}} \pi \right) \odot 
       \left(T_{r_1-q}\displaystyle\left(\prod_{i=1}^{n_r-1}M_{r_i}T_{r_{i+1}-r_i} \right) M_{r_{n_r}} \pi  \right) }  
     {\left| \left(T_{q-l_1}\displaystyle\left(\prod_{i=1}^{n_l-1}     M_{l_i}T_{l_{i}-l_{i+1}} \right)      M_{l_{n_l}} \pi \right) \odot 
       \left(T_{r_1-q}\displaystyle\left(\prod_{i=1}^{n_r-1}M_{r_i}T_{r_{i+1}-r_i} \right) M_{r_{n_r}} \pi  \right) \right| }
       \end{multline}


Where $\odot$ stands for the Hadamard product (also named Schur or entrywise product), and using convention that a void matrix product equals identity matrix. (see appendix \ref{app:Multiple_loci} for more detailed explanations about this formula).


\section{Discussion}


\subsection{Model in practical use}

Of course equation~(\ref{eq:pr_qtl1}) first usage is the computation of Parental Origin probabilities knowing the markers scores. But since it is a true multi point equation, it is far more versatile. It makes possible the computation of exact joint probabilities by successive use. To achieve this, it can  compute Parental Origin probabilities at some locus, given the markers scores and given  other locus genotype.  

In order to become genotype score error resilient, the projection matrices $M_m$ may be replaced with less stringent operators, modifying their diagonal terms (off diagonal terms remain $0$ since genotype observation has nothing to do with state transition), the $1$ being replaced by $1-\varepsilon_m$ and the $0$ by $\varepsilon_m$.  The (small) adjustment parameter $\varepsilon_m$ reflecting the possibility that this marker score contains erroneous information. This strategy is especially effective with high throughput genotype scores when detailed human inspection and cleaning is out of reach. Since equation~(\ref{eq:pr_qtl1}) is fully multi point, the information is gathered from multiple neighboring marks, a single genotype error will then be less likely to produce some sharp spike in POP and therefore to fool the QTL detection method. 



\subsection{Lumping}

From a theoretical point of view, the  states space and transition matrix constructed this way are satisfying. They allow correct Markov chain representation and effective Parental Origin probabilities computation. But every cross increases the number of states. From a practical point of view, merging states that share the same label is useful as long this merging does not break the Markov property of the system. This state merging operation, called lumping produces a quotient Markov chain representing the Parental origin of genome as exactly  as the initial Markov chain\cite{buchholz1994exact}. And it is able to preserve any initial partition of states\cite{DBLP:journals/ipl/DerisaviHS03,DBLP:conf/qest/Derisavi07}.  Note that these lumping algorithms find the coarsest partition of a given Markov chain. Every further state merging is then inappropriate for Markov properties preservation. 

\subsection{Previous work on AI populations}

The seminal paper by Fisch, Ragot and Gay (FRG)\cite{Fisch01051996} describe a way to compute POP for multiple selfing breeding. They derive exact three point probabilities from an intergenerational Markov process. But it is not possible to reconstruct a space Markov process from their probabilities for $F_i$, as soon as  $i\geq 3$.\cite{GRH:Kao5486696} 

This can be checked by computing the 36 Marker-QTL-Marker configurations' probabilities  with fixed finite distance between the three loci.(Note that in the matrix page 575 of their published work\cite{Fisch01051996} the value "$1$" at line 4, column 3 and the value "$0$" at line 3, column 3 have been inverted) Then reconstruct three transition matrix by conditioning as exposed in their appendix C: $T^{(FRG)}_{M_A-M_B}$, $T^{(FRG)}_{M_A-M_Q}$ and $T^{(FRG)}_{M_Q-M_B}$. These matrix do not satisfy the expected KC property since 
\begin{equation} T^{(FRG)}_{M_Q-M_B}T^{(FRG)}_{M_A-M_Q} - T^{(FRG)}_{M_A-M_B} \neq 0
\end{equation} (see table~\ref{table:Compare}). The eight three position haploid and the 36 three position diploid configuration they used, lead to inappropriate lumping. 

In other words, an homozygous marker score in $F3$ (or higher order selfing) is not a fully informative score. As can be seen on the table~\ref{table:examples}, the optimal lumped state space for $F3$ cross has not less than three $(P1,P1)$ labeled states. This is related to the fact that from a 'A' score (homozygous from Parent A) at some position for any individual in $F3$ we can derive the information that its parent's score at the same position is  a 'D' (or 'not-B'), and no more. Only from a 'H' (heterozygous) mark score we can derive information that its parent was heterozygous too. The amount of information is higher in heterozygous marks scores, but their number is halved by every self fecundation: therefore enumeration of every possible haploid between two fully informative marks will be computationally unfordable. 

The three point probabilities obtained using FRG mathod are an approximation of multi point probabilities, making assumption that no information from markers scores outside their working interval AB is available.  

The lack of Markovian properties forbids any generalization form two points probabilities to three point probabilities, from three points probabilities to four point probabilities, and so on. It is yet possible to compute these probabilities as a function of all the possible recombination rates\cite{Broman01022012}. The 4 points computation of exact POP\cite{kao2010} uses this approach at the cost of enormous algebraic effort.   



\subsection{Take over the QTL mapper's dilemma}

The QTL mapper for advanced population was then facing a dilemma : Either he could use a Markovian model that is using the Parental Origin probabilities computed for $F2$ cross and treating the observed traits in order to take account of supplementary crosses\cite{Zhang01042004,Kao01112006}, having then to deal with erroneous recombination rates. Or he could use correct recombination rates\cite{Teuscher01032007,Broman01022012} but renouncing to Markovian properties and therefore using only information at flanking markers. 

The method described here takes the good sides of both approach. The Markov properties allow the usage of all available linked markers scores, allowing further comparison of several competing multiple-QTL models\cite{Jansen2008}, and it takes account of multiple meiosis and higher recombination rates, allowing effective ghost QTL resolution capabilities of advanced inter cross. As the transition matrix are constructed only from browsing the breeding design cross by cross, any kind of population is addressed : multiple selfing, multiple (back)-crossing, any combination between selfing and crossing. The result produced by our method will then feed and empower any subsequent QTL detection and mapping  method.



\subsubsection*{URL}
The values in table \ref{table:Compare} were computed using a Scilab script available at \url{https://mulcyber.toulouse.inra.fr/download//spel/supplmentary_material/1.0/Compare_Transition_Matrix.sce} 


\subsubsection*{Acknowledgments}

The authors thank D. Robelin for suggestions about mixture models, B. Mangin for numerous discussions and B. Gilbert for report correction. 

\subsubsection*{Author contributions}
Both authors contributed equally to this work. 

\subsubsection*{Competing financial interest}
The authors declare no competing financial interests.


\bibliographystyle{nature}
\bibliography{ProbaPop}


\appendix
\part*{Online Methods}


\section{Kolmogorov Chapman and stationary distribution properties for matrix $T_d$}
\label{app:proof_KC_one}

The discrete space transition matrix $T_d=\left( \begin{array}{cc}
1-r_d     &  r_d\\
r_d      &    1-r_d \end{array} \right) $ describes the recombination during meiosis process. 


\subsection{KC property}
It is possible to prove it verifies the KC property by explicit calculation of the product $T_d.T_{d'}$ after substitution of $r_d$ by $1/2(1-e^{-2d})$. But better understanding of the underlying mechanisms is achieved using  the infinitesimal generator matrix $Q$. The discrete space transition matrix and the infinitesimal generator matrix are related together by $T_d=e^{dQ}$ and $Q=\frac{dT_d}{dd}\big|_{d=0}$

Therefore $Q=\left( \begin{array}{cc} 
-1     &  1\\
1      &   -1 \end{array} \right) $ and  $T_d.T_{d'}=e^{dQ}e^{d'Q}=e^{(d+d')Q}=T_{d+d'}$.

\subsection{Stationary distibution}
 When genetic distance increases the limit is explicit :  $T_{\infty}=\lim_{d\rightarrow\infty}T_d=\left( \begin{array}{cc}
1/2    &  1/2\\
1/2   &    1/2\end{array} \right) $ and therefore the stationary distribution is $\pi=\begin{pmatrix} 1/2 \\ 1/2 \end{pmatrix} $. 


\section{Kolmogorov Chapman and infinite distance properties in product space}
\label{app:proof_KC_product}


Suppose two Markov process say $A$ and $B$ with respective state space $St^A$ and $St^B$, respective transition matrix $T^A_d$ and $T^B_d$ and respective unique stationary distribution $\pi^A$ and $\pi^B$. We build a new process, called $A\times B$, embedding all $A$ and $B$ features. This new process product state space is the Cartesian product $St^{A\times B}=St^A\times St^B$ and its transition matrix in this new state space is the Kronecker product $T^{A\times B}_d=T^A_d \otimes T^B_d$. 

\subsection{KC property}
 Let us prove that this new transition matrix does enjoy the KC property.  

By definition of the product state space: 
\begin{equation} T^{A\times B}_d.T^{A\times B}_{d'}=(T^A_d\otimes T^B_d).(T^A_{d'}\otimes T^B_{d'})\end{equation}
The mixed product property applied to right hand side leads to: 
\begin{equation} T^{A\times B}_d.T^{A\times B}_{d'}=(T^A_d.T^A_{d'})\otimes(T^B_d.T^B_{d'})\end{equation}
The KC property for both chains A and B in their respective state space allows: 
\begin{equation}T^{A\times B}_d.T^{A\times B}_{d'}=(T^A_{d+d'})\otimes(T^B_{d+d'})\end{equation}
The definition of the product space implies:
\begin{equation} T^{A\times B}_d.T^{A\times B}_{d'}=T^{A\times B}_{d+d'}\end{equation}
That proves the KC property in the product state space for the Kronecker product matrix and that the new process is Markovian. 

\subsection{Stationary distribution}
The process $A$ and $B$ both have a unique stationary distribution, then the limits $\lim_{d\rightarrow\infty}(T_d^A)$ and $\lim_{d\rightarrow\infty}(T_d^B)$ exist. 
Since the number of states is finite, $\lim_{d\rightarrow\infty}(T_d^A\otimes T_d^B)$ exists too and its value is $\lim_{d\rightarrow\infty}(T_d^A\otimes T_d^B)=\lim_{d\rightarrow\infty}(T_d^A)\otimes \lim_{d\rightarrow\infty}(T_d^B)$. This ensure existence of some stationary distributions.

Furthermore the uniqueness $\pi^A$ and $\pi^B$ implies that $\rank(\lim_{d\rightarrow\infty}(T_d^A))=1$ and $\rank(\lim_{d\rightarrow\infty}(T_d^B))=1$.
The Kronecker product's rank is the product of its operand ranks, therefore $\rank(\lim_{d\rightarrow\infty}(T_d^A\otimes T_d^B)=1$.This ensure the uniqueness of $\pi^{A\times B}$.




\section{$R_d$ Taylor expansion}
\label{app:proof_Taylor}

It is possible to prove equation~(\ref{eq:RIL_expansion}) $R_d$ approximation by explicit calculation of the difference $\epsilon_{d,n}$. 

But in order to extend the method to other repeated crossing design, consider the Taylor expansion of $R_d$ at point $r=1/2$ (here with exact remainder $\rho_{d,n}$) : 
 
\begin{equation} R_d=1-\frac{1}{2}\sum_{k=0}^{n}\left(\frac{1}{2}-r_d\right)^{k} + \rho_{d,n} \mbox{ with } \rho_{d,n}=-\frac{\left(1/2-r_d\right)^{n+1}}{1+2r_d}\end{equation}

it is possible to linearize this expansion using $(1-2r_d)^k=e^{-2kd}=1-2r_{kd}$.

\begin{equation}R_d=\sum_{k=1}^n \frac{r_{kd}}{2^k}+\rho_{d,n}-\rho_{0,n}\end{equation}

When $r_d$ is in the range $[0,1/2]$, $\rho_{d,n}$ increases from $-2^{-n-1}$ to $0$, then:  
\begin{equation}|\epsilon_{d,n}|=|\rho_{d,n}-\rho_{0,n}|\leq -\rho_{0,n} = \frac{1}{2^{n+1}}\end{equation}


The same way, it is possible to use the Taylor expansion of the full-sib mating RIL:  $R_d^{(SIB)}=\frac{4r_d}{1+6r_d}$ 
 in order to derive an approximation. The weights of the $n$ independent Markov processes are then $w_k^{(SIB)}=3^{k-1}/2^{2k} $ and the error is bounded by $| \epsilon_{d,n}^{(SIB)} | \leq 3^n/2^{2n+1}$

\section{Multiple loci Probabilities}
\label{app:Multiple_loci}

With sequence loci, $[l_{n_l},...,l_2,l_1,q,r_1,r_2,...,r_{n_r}]$ the POP vector at the left flanking locus $l_1$, knowing all the left marks is :

\begin{equation} \label{eq:flanking_left}p(l_1|\mathcal{M}_{l_1},...,\mathcal{M}_{l_{n_l}}) \propto
   \displaystyle\left(\prod_{i=1}^{n_l-1}     M_{l_i}T_{l_{i}-l_{i+1}} \right)      M_{l_{n_l}} \pi \end{equation}

 

If the distance between the two loci $l_{k+1}$ and $l_{k}$ is $0$ then the transition matrix used is $T_0=I_{|St|}$, the identity matrix. 
This leads to a product of the successive projection matrices $M_{l_{k}}.M_{l_{k+1}}$. If the observation at both loci are consistent (or identical), this is equivalent to use only one projection with both observation merged information. If the observations at both loci are inconsistent, the product $M_{l_{k}}.M_{l_{k+1}}$ is zero, 
and the whole result of equation~(\ref{eq:flanking_left}) is zero. 

The same way the Parental origin probability vector at locus $r_1$ knowing all the right marks is : 


\begin{equation} \label{eq:flanking_right}
p(r_1|\mathcal{M}_{r_1},...,\mathcal{M}_{r_{n_r}}) \propto
   \displaystyle\left(\prod_{i=1}^{n_r-1}M_{r_i}T_{r_{i+1}-r_i} \right) M_{r_{n_r}} \pi \end{equation}



The POP vector at locus of interest $q$ is easily expressed using these flanking probabilities. The benefit or Markovian modeling is clear here, as any useful information about the left (respectively right) marks is available is the left (respectively right) flanking locus probability. 

\begin{multline} \label{eq:pr_qtl0} p(q|\mathcal{M}_{l_1},...,\mathcal{M}_{l_{n_l}},\mathcal{M}_{r_1},...,\mathcal{M}_{r_{n_r}}) =\\
\frac{ \left(T_{q-l_1}p(l_1|\mathcal{M}_{l_1},...,\mathcal{M}_{l_{n_l}})\right) \odot 
       \left(T_{r_1-q}p(r_1|\mathcal{M}_{r_1},...,\mathcal{M}_{r_{n_r}})  \right) }  
     {\left| \left(T_{q-l_1}p(l_1|\mathcal{M}_{l_1},...,\mathcal{M}_{l_{n_l}})\right) \odot 
       \left(T_{r_1-q}p(r_1|\mathcal{M}_{r_1},...,\mathcal{M}_{r_{n_r}})  \right) \right| }\end{multline}

The only denominator role is to normalize the resulting vector. 

Substituting  equation (\ref{eq:flanking_left}) and equation (\ref{eq:flanking_right}) right hand sides in equation (\ref{eq:pr_qtl0}) provides equation~(\ref{eq:pr_qtl1}).




\begin{landscape}

\part*{Tables}

\tiny

\begin{longtable}{l c c c }
\caption{}
\endfirsthead
\caption[]{(continued)}
\endhead
\toprule
Cross  (C) &  
%||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| (vline)
State Space ($St^C$) &  
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Transition Matrix ($T_d^C$) & 
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\begin{array}{c} \mbox{Stationary}\\ \mbox{distribution }(\pi^C) \end{array}$ \\
\toprule %----------------------------------------------------------- (hline)
\pbox{20cm}{Parental line $P_1$} & 
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$ \begin{matrix} \{(P_1,P_1)\}\end{matrix}$ & 
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\left( 1 \right)$ 
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
& (1) \\ 
\midrule %-----------------------------------------------------------
\pbox{20cm}{BC:\\$(P_1\times P_2)\times P_1$}    &  
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$ \begin{matrix} \{(P_1,P_1),\\ \phantom{\}}(P_1,P_2) \}\end{matrix}$ & 
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\left( \begin{array}{cc}
	s_d     &  r_d\\
	r_d      &    s_d 
	\end{array} \right)$ &  
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\left( \begin{array}{c}
	1/2\\
	1/2\end{array} \right)$ \\ 
\midrule %-----------------------------------------------------------
\pbox{20cm}{F2:\\$Selfing(P_1\times P_2)$     }&
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\begin{matrix}\{ (P_1,P_1), \\ \phantom{\}}(P_1,P_2),\\ \phantom{\}} (P_2,P_1),\\ \phantom{\}}(P_2,P_2) \} \end{matrix}$ &
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\left( \begin{array}{cccc}
	s_d^2   &  r_ds_d   &  r_ds_d   &  r_d^2  \\
	r_ds_d    &  s_d^2  &  r_d^2      &  r_ds_d\\
	r_ds_d    &  r_d^2      &  s_d^2  &  r_ds_d\\
	r_d^2       &  r_ds_d   &  r_ds_d   &  s_d^2 \end{array} \right)$ & 
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\left( \begin{array}{c}
	1/4\\
	1/4\\
	1/4\\
	1/4\end{array} \right)$ \\ 
\midrule %-----------------------------------------------------------
\pbox{20cm}{BC2: \\$BC \times P_1$ }    &  
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$ \begin{matrix}\{(P_1,P_1),\\ \phantom{\}}(P_1,P_1),\\ \phantom{\}}(P_1,P_2)\} \end{matrix}$  &  
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\left( \begin{array}{ccc}
	2r_d^2-2r_d+1  &  2r_ds_d   &  2r_ds_d\\
	r_ds_d   &  s_d^2   &  r_d^2   \\
	r_ds_d   &  r_d^2       &  s_d^2 \end{array} \right)$ & 
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\left( \begin{array}{c}
	1/2\\
	1/4\\
	1/4\end{array} \right)$ \\ 
\midrule %-----------------------------------------------------------
\pbox{20cm}{F3:\\$Selfing(F2)$     }&  
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\begin{matrix}\{ (P_1,P_1), \\ \phantom{\}} (P_1,P_1), \\ \phantom{\}} (P_1,P_1), \\ \phantom{\}} (P_1,P_2),\\ \phantom{\}} (P_2,P_1),\\ \phantom{\}}(P_2,P_2),\\ \phantom{\}}(P_2,P_2),\\ \phantom{\}}(P_2,P_2) \} \end{matrix}$ &  
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\begin{matrix}
	\left( \begin{matrix}
     	s_d^2(2r_d^2-2r_d+1)&	 2r_ds_d^3 & r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2\\ 
    	2r_ds_d^3& s_d^2(2r_d^2-2r_d+1)& 2r_d^2s_d^2& r_ds_d(2r_d^2-2r_d+1)\\ 
    	r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2& 2r_d^4-4r_d^3+6r_d^2-4r_d+1& r_ds_d(2r_d^2-2r_d+1)\\
    	2r_d^2s_d^2& r_ds_d(2r_d^2-2r_d+1)& r_ds_d(2r_d^2-2r_d+1)& 2r_d^4-4r_d^3+6r_d^2-4r_d+1\\
    	2r_d^2s_d^2& r_ds_d(2r_d^2-2r_d+1)& r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2\\
    	r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2& 2r_d^2s_d^2& r_ds_d(2r_d^2-2r_d+1)\\ 
    	r_d^2(2r_d^2-2r_d+1)& 2r_d^3s_d& r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2\\ 
    	2r_d^3s_d& r_d^2(2r_d^2-2r_d+1)& 2r_d^2s_d^2& r_ds_d(2r_d^2-2r_d+1)\\ 
	\end{matrix}  \phantom{\dots} \dots\right.\\ \\
 	\left. \dots \phantom{\dots}  \begin{matrix} 2r_d^2s_d^2 & r_ds_d(2r_d^2-2r_d+1)& r_d^2(2r_d^2-2r_d+1)& 2r_d^3s_d\\ 
 			           r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2& 2r_d^3s_d& r_d^2(2r_d^2-2r_d+1)\\
 			           r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2& r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2\\
 			           2r_d^2s_d^2& r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2& r_ds_d(2r_d^2-2r_d+1)\\
 			           2r_d^4-4r_d^3+6r_d^2-4r_d+1& r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2& r_ds_d(2r_d^2-2r_d+1)\\
 			           r_ds_d(2r_d^2-2r_d+1)& 2r_d^4-4r_d^3+6r_d^2-4r_d+1& r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2\\
 			           2r_d^2s_d^2& r_ds_d(2r_d^2-2r_d+1)& s_d^2(2r_d^2-2r_d+1)& 2r_ds_d^3\\
 			           r_ds_d(2r_d^2-2r_d+1)& 2r_d^2s_d^2& 2r_ds_d^3& s_d^2(2r_d^2-2r_d+1)\\
 	\end{matrix} \right)  \\
\end{matrix}$ & 
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\left( \begin{array}{c}
	\tikzmark{left5}1/8\\
	1/8\\
	1/8\tikzmark{right5}\\
	\tikzmark{left6}1/8\\
	1/8\tikzmark{right6}\\
	\tikzmark{left7}1/8\\
	1/8\\
	1/8\tikzmark{right7} \end{array} 
	\tikzdrawbox{5}{thin,dashed}
 	\tikzdrawbox{6}{thin,dashed}
 	\tikzdrawbox{7}{thin,dashed}\right)$ \\ 
\midrule %-----------------------------------------------------------
\pbox{20cm}{RIL:\\ $\lim_{g\rightarrow \infty} Fg$ \\ $Fg=Selfing(F_{g-1})$ \\ \\weights vector: \\$(1/2,1/4,...,1/2^n)$   } &  
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$ \begin{matrix}\{(P_1,P_1),\\ \phantom{\}}(P_2,P_2)\} \\ \{(P_1,P_1),\\ \phantom{\}}(P_2,P_2)\}\\ \phantom{\}} \vdots \\ \{(P_1,P_1),\\ \phantom{\}}(P_2,P_2)\} \end{matrix}$ &
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
${ \left( \begin{array}{c@{}c@{}c c c}
		\boxed{ \begin{matrix}  s_d    &  r_d  \\  r_d     &  s_d \\ 
			\end{matrix} }                                                               & & & \\
		&    \boxed{ \begin{matrix}  s_{2d}    &  r_{2d}  \\  r_{2d}     &  s_{2d} \\ 
			     \end{matrix}}                                                           &  &{\raisebox{0pt}[0pt][0pt]{\Huge 0}} \\
		{\raisebox{0pt}[0pt][0pt]{\Huge 0}} & centering parbox& \ddots & \\
		& & & \boxed{ \begin{matrix}  s_{nd}    &  r_{nd}  \\  r_{nd}     &  s_{nd} \\ 
		              \end{matrix}} \\
 	    \end{array} \right)}$ &  
%|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$\begin{array}{c} \begin{pmatrix}
	1/2\\
	1/2\end{pmatrix} \\  \begin{pmatrix}
	1/2\\
	1/2\end{pmatrix} \\  
	\vdots \\ \begin{pmatrix}
	1/2\\
	1/2\end{pmatrix} \\  
\end{array}$\\  
\bottomrule %-----------------------------------------------------------
\caption{ \label{table:examples} Some examples of state space, transition matrix (optimally lumped\cite{DBLP:conf/qest/Derisavi07}, using notation $s_{kd}=1-r_{kd}$) and stationary distribution.\\ 
Note that different states in the $BC2$, $F3$ and $RIL$ approximation model share the same label. By summing the stationary probabilities of states sharing the same label we recover the expected abundances (for an example in $F3$ the sums of the 3 dashed boxes are the well known $3/8$, $1/4$ and $3/8$). However it is not possible to merge them while keeping the Markov properties. Only the simplest breeding designs have state unique states' label.\\
Therefore and contrary to intuition, in many populations (e.g. $Fi$ with $i\geq3$, $BCi$ with $i\geq2$) the homozygous marks are not fully informative. From a practical point of view this implies to deal efficiently with partially informative marks.  }
\end{longtable}

\newpage

\begin{longtable}{ c c c }
\caption{}
\endfirsthead
\caption[]{(continued)}
\endhead
\toprule
  &  FRG  Model &  True Markovian Model  \\
\toprule
 State Space & $\left\{(P_1,P_1),[(P_1,P_2)\,or\,(P_2,P_1)],(P_2,P_2)\right\}$ & $\left\{(P_1,P_1),(P_1,P_1)(P_1,P_1),(P_1,P_2),(P_2,P_1),(P_2,P_2),(P_2,P_2),(P_2,P_2)\right\}$\\
\midrule
 $T^{F3}_{30\,cM}$ 
 	 &   $\left(\phantom{0}{\begin{matrix}\tikzmark{left4}0.637\tikzmark{right4} & 0.288 & 0.171\\ 0.192 & 0.423 & 0.192\\ 0.171 & 0.288 & 0.637\\ \end{matrix}} \tikzdrawbox{4}{thin,dotted} \phantom{0}\right)$              
 	 &   $\left(\phantom{0}{\begin{matrix}\tikzmark{left1}0.39\phantom{0} & 0.21\phantom{0} & 0.114 \tikzmark{right1}& 0.061 & 0.061 & 0.114 & 0.033 & 0.018 \\ 
 	                      \tikzmark{left2}0.21\phantom{0} & 0.39\phantom{0} & 0.061 \tikzmark{right2}& 0.114 & 0.114 & 0.061 & 0.018 & 0.033\\ 
 	                      \tikzmark{left3}0.114 & 0.061 & 0.362 \tikzmark{right3}& 0.114 & 0.114 & 0.061 & 0.114 & 0.061\\ 
 	                      0.061 & 0.114 & 0.114 & 0.362 & 0.061 & 0.114 & 0.061 & 0.114\\
 	                      0.061 & 0.114 & 0.114 & 0.061 & 0.362 & 0.114 & 0.061 & 0.114\\ 
 	                      0.114 & 0.061 & 0.061 & 0.114 & 0.114 & 0.362 & 0.114 & 0.061\\ 
 	                      0.033 & 0.018 & 0.114 & 0.061 & 0.061 & 0.114 & 0.39\phantom{0} & 0.21\phantom{0}\\ 
 	                      0.018 & 0.033 & 0.061 & 0.114 & 0.114 & 0.061 & 0.21\phantom{0} & 0.39\phantom{0}\\ \end{matrix}} 
 	                      \tikzdrawbox{1}{thin,dashed}
 	                      \tikzdrawbox{2}{thin,dashed}
 	                      \tikzdrawbox{3}{thin,dashed}
 	                      \phantom{0}\right)$  \\    
\midrule
  $T^{F3}_{60\,cM}$ 
  	 & $\left(\phantom{0}{\begin{matrix}0.498 & 0.351 & 0.267\\ 0.234 & 0.297 & 0.234\\ 0.267 & 0.351 & 0.498\\ \end{matrix}}\phantom{0}\right)$   
  	 & $\left(\phantom{0}{\begin{matrix}0.231 & 0.192 & 0.124 & 0.103 & 0.103 & 0.124 & 0.067 & 0.056\\ 
  	                     0.192 & 0.231 & 0.103 & 0.124 & 0.124 & 0.103 & 0.056 & 0.067\\ 
  	                     0.124 & 0.103 & 0.194 & 0.124 & 0.124 & 0.103 & 0.124 & 0.103\\ 
  	                     0.103 & 0.124 & 0.124 & 0.194 & 0.103 & 0.124 & 0.103 & 0.124\\ 
  	                     0.103 & 0.124 & 0.124 & 0.103 & 0.194 & 0.124 & 0.103 & 0.124\\ 
  	                     0.124 & 0.103 & 0.103 & 0.124 & 0.124 & 0.194 & 0.124 & 0.103\\ 
  	                     0.067 & 0.056 & 0.124 & 0.103 & 0.103 & 0.124 & 0.231 & 0.192\\ 
  	                     0.056 & 0.067 & 0.103 & 0.124 & 0.124 & 0.103 & 0.192 & 0.231\\ \end{matrix}}\phantom{0}\right)$         \\
\midrule
$T^{F3}_{60\,cM} - {T^{F3}_{30\,cM}}^2 $
	 &  ${\begin{pmatrix}-0.008 & \phantom{-}0.004 & \phantom{-}0.006\\ \phantom{-}0.002 & -0.007 & \phantom{-}0.002\\ \phantom{-}0.006 & \phantom{-}0.004 & -0.008\\ \end{pmatrix}}$   
& $ \%eps \times { \left(\phantom{0} \begin{matrix} 
    0  &   0  &    1/16 &     1/8 &    0  & -  1/8 &     1/16 &  -  1/16 \\           
    0  &   0  &    1/8 &     1/16 &  -  1/8 &    0  &   0  &   0  \\       
     1/16 &     1/8 &    0  & -  1/8 &  -  1/16 &  -  1/8 &    0  & -  1/16 \\          
     1/8 &     1/16 &  -  1/8 &    0  & -  1/8 &  -  1/8 &     1/16 &  -  1/8 \\         
     1/16 &     1/16 &    0  & -  1/16 &  -  1/4 &  -  1/4 &     1/16 &  -  1/8 \\         
     1/16 &     1/16 &  -  1/16 &     1/16 &  -  1/4 &  -  1/4 &    0  & -  1/16 \\         
     1/16 &    0  &   0  &    1/16 &  -  1/16 &  -  1/8 &    0  & -  1/4 \\         
  -  1/32 &     1/16 &     1/16 &    0  & -  1/8 &  -  1/16 &    0  & - 3/8  \\  
\end{matrix}\phantom{0}\right)} $ \\
\bottomrule
\caption{\label{table:Compare} Comparison between the $F3$ transition matrix using FRG model\cite{Fisch01051996} (second column) and a Markovian model (third column) at genetic distance $30\,cM$ and $60\,cM$. The Markovian model was optimally lumped\cite{DBLP:conf/qest/Derisavi07}. (The special Scilab value $\%eps=2.22\times10^{-16}$ is Scilab numerical relative precision.) \\  
The last row shows that FRG model does not enjoy the KC property, and therefore is not a Markovian model. (The KC property would imply $ T^{F3}_{60\,cM} = {T^{F3}_{30\,cM}}^2 $ )
Note that the sums of the probabilities in the three dashed boxes in the third column are respectively $0.714$, $0.661$ and $0.537$. They differ and according to lumping criteria\cite{buchholz1994exact} the corresponding states can not be lumped while keeping Markov property. \\	
Lumping the states anyway leads to pool the three values, that is to use their mean ($0.637$) as the transition probability standing in the dotted box in the second column. The FRG model is equivalent to True Markovian model which an inappropriate lumping was performed. }

\end{longtable}



\end{landscape}

\end{document}
