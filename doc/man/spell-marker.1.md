% SPELL-MARKER(1) Spell-QTL software suite
% Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
% December 2016

# NAME

spell-marker – Compute the 1-point Parental Origin Probabilities in a pedigree given genotype or allelic observations

# SYNOPSIS

**spell-marker** [options...] [**-wd** *PATH*] **-n** *NAME* **-m** *GEN:FORMAT* *FILE* [**-m**...]

# DESCRIPTION

**spell-marker** computes the 1-point Parental Origin Probabilities using Bayesian inference.

It outputs a data file that can be used with **spell-qtl** to compute the n-point Parental Origin Probabilities and
perform the actual QTL analysis.
 
**spell-marker** expects that **spell-pedigree** has been run  beforehand with the same working directory and configuration name.

Because each marker is supposed to be independent, **spell-marker** can perform the computations in parallel in a
variety of ways. See the *Job control* subsection of the options for details.

**-wd**,**--work-directory** *PATH*
: Path to directory for cache files and outputs. Defaults to the **current directory**.

**-n**,**--name** *NAME*
: User-friendly name for this configuration

**-m**,**--marker-obs** *GEN:FORMAT* *FILE*
: Path to the marker observations file of generation *GEN* with given format *FORMAT*. This file must have as many
individuals as the pedigree has for that generation.

    **spell-marker** knows three marker observation formats by default. Bi-allelic SNP observations encoded as 0, 1, 2 
    (*02*), bi-parental genotype observations as in the Mapmaker format (*ABHCD*), and phased outbred parental observations
    as in carthagene (*CP*). You can define other formats using the **-mos** option.

    You can direct **spell-marker** to use only a slice of an observation file using the following syntaxes:
     
    *FILE*:**single_column_index**
    
    *FILE*:**first_column_index**:**last_column_index**
    
    When using genotype observations in a pedigree with more than two ancestors, you can specify the format for each
    generation as **Parent1_letter**/**Parent2_letter** or **Parent1_generation**/**Parent2_generation**. The format
    will be *ABHCD* with *a* and *b* replaced with the corresponding letters. 

# OPTIONS

## Miscellaneous

**-h**, **--help**
: Display usage.

**-z**,**--noise** *level*
: Set the noise level for marker observations. Defaults to **0**.

## Job control

Select and configure the job control scheme

**-mt**,**--dispatch-multithread** *n_threads*
: Use single-machine, multi-threading.

**-ssh**,**--dispatch-SSH** *HOSTS*
: Use SSH for job dispatch. *HOSTS* is a comma-separated list of hostnames. **spell-marker** expects to find the same
file system structure on all hosts.

**-sge**,**--dispatch-SGE** *n_jobs* *qsub options*
: Use SGE for job dispatch. Use '-' for *qsub options* if you don't wish to provide any specific option.

## Inputs

Input files and configuration of observations.
There are two essential parameters to compute the genotype probabilities: the number of ancestors and the number
of observed alleles (for SNP observations). The number of ancestors is automatically computed from the given pedigree
or breeding design specification. The number of alleles is computed from the marker observation specifications.

**-mos**,**--marker-observation-spec** *path*
: Path to a marker observation specification file.

**-o**,**--output-generations** *comma-separated list*
: Specifies the list of variables to extract after the computation.
The state probabilities for all individuals in the given generations will be extracted and made available for spell-qtl.
Defaults to all generations.

## Output modes

Set the output mode. By default, only the population data file will be written. If you specify -O1, only the 1-point
Parental Origin Probabilities will be written, unless you also specify -Op.

**-Op**,**--output-population-data**
: Output the population data file for use in spell-qtl. This is the default behaviour. The output will be named
*NAME***.spell-marker.data** in the directory **WORK_DIRECTORY/*NAME*.cache**.


**-O1**,**--output-one-point-prob**
: Output the 1-point Parental Origin Probabilities. This will disable the output of the population data file
unless -Op is also explicitly used.

# MARKER OBSERVATION FORMAT SPECIFICATION

A format specification file is a JSON object (dictionary) where each key is a format name. Each corresponding value is
a JSON object containing the following keys:

**"domain"**
: either *"allele"* or *"ancestor"*.

**"alphabet-from"**
: a string containing all the characters (alleles or ancestor letters) that can be observed.

**"scores"**
: an object where each key is an observation and each value an array of all the possible genotype/allelic pairs it
encompasses.


## Example: the *02*, *ABHCD*, and *CP* formats


~~~~
{
    "02": {
        "domain": "allele",
        "alphabet_from": "01",
        "scores": {
            "0": ["00"],
            "1": ["01", "10"],
            "2": ["11"],
            "-": ["00", "01", "10", "11"]
        }
    },
    "ABHCD": {
        "domain": "ancestor",
        "alphabet_from": "ab",
        "scores": {
            "A": ["aa"],
            "H": ["ab", "ba"],
            "B": ["bb"],
            "-": ["aa", "ab", "ba", "bb"],
            "C": ["ab", "ba", "bb"],
            "D": ["aa", "ab", "ba"]
        }
    },
    "CP": {
        "domain": "ancestor",
        "alphabet_from": "abcd",
        "scores": {
            "0": ["ac", "ad", "bc", "bd"],
            "1": ["ac"],
            "2": ["ad"],
            "3": ["ac", "ad"],
            "4": ["bc"],
            "5": ["ac", "bc"],
            "6": ["ad", "bc"],
            "7": ["ac, ad", "bc"],
            "8": ["bd"],
            "9": ["ac", "bd"],
            "A": ["ad", "bd"],
            "B": ["ac", "ad", "bd"],
            "C": ["bc", "bd"],
            "D": ["ac", "bc", "bd"],
            "E": ["ad", "bc", "bd"],
            "F": ["ac", "ad", "bc", "bd"],
            "a": ["ad", "bd"],
            "b": ["ac", "ad", "bd"],
            "c": ["bc", "bd"],
            "d": ["ac", "bc", "bd"],
            "e": ["ad", "bc", "bd"],
            "f": ["ac", "ad", "bc", "bd"],
            "-": ["ac", "ad", "bc", "bd"]
        }
    } 
}
~~~~
# EXAMPLES

See `spell-qtl-examples` (1) for complete examples of the spell-qtl pipeline.

# SEE ALSO

`spell-pedigree` (1), `spell-qtl` (1), `spell-qtl-examples` (1).
