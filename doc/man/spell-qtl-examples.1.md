% SPELL-QTL-EXAMPLES(1) Spell-QTL software suite
% Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
% December 2016

# NAME

spell-qtl-examples – Example datasets for the spell-qtl software suite.

# DESCRIPTION

The datasets are located in /usr/share/spell-qtl/examples if it was installed system-wide, or share/spell-qtl/examples
in the installation directory.

The example datasets each consist in a set of files :
 - the pedigree in a **.ped** file,
 - the genetic map in a **.map** file,
 - one or more genotypic or allelic observation files in **.gen** files,
 - one or more sets of single_trait observations in **.phen** files.

Additionally, a **README** file in each directory describes the dataset features and the commands to run to process them.

# SEE ALSO

`spell-pedigree` (1), `spell-marker` (1), `spell-qtl` (1)
