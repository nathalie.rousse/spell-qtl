% SPELL-PEDIGREE(1) Spell-QTL software suite
% Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
% December 2016

# NAME

spell-pedigree – Precompute the Markov Models for a pedigree

# SYNOPSIS

**spell-pedigree** [**-h**] [**-wd** *PATH*] **-n** *NAME* [**-s** *CHAR*] **-p** *FILE*

# DESCRIPTION

**spell-pedigree** computes Markov Models representing the evolution of the genotype on all the individuals in a
pedigree.

It outputs a data file that can be used with **spell-marker** to compute the Parental Origin
Probabilities for this pedigree given allelic or genotype observations on a set of markers. 

**-wd**,**--work-directory** *PATH*
: Path to directory for cache files and outputs. Defaults to the **current directory**.

**-n**,**--name** *NAME*
: User-friendly name for this configuration

**-p**, **--pedigree-file** *FILE*
: Path to the genetic map file.

    | The expected pedigree file must be a CSV file with each row in the following format:
    | **GENERATION_NAME** ; **Individual number** ; **Parent1 number** ; **Parent2 number**
    | Any additional column will be silently ignored by **spell-pedigree**.
    |
    | Individual numbers are expected to increase and all GREATER than zero, and parent numbers for a given individual are expected to be LESSER than the individual number.
    |
    | Breeding lines are encoded with Parent1 = Parent2 = 0.
    | Selfings are encoded with Parent1 = Parent2.
    | Doubled haploids are encoded with Parent2 = 0.
    |
    | The generation names will be used when specifying genotype and phenotype observations in the later steps.
    |
    | The first line is expected to be a header line and will be ignored.

# OPTIONS

**-h**, **--help**
: Display usage.

**-s**,**--separator** *CHAR*
: Column delimiter character used in the pedigree file. Defaults to ";".

# OUTPUT

**spell-pedigree** will create a file named *NAME***.spell-pedigree.data** in the directory **WORK_DIRECTORY/*NAME*.cache**.

# EXAMPLES

See `spell-qtl-examples` (1) for complete examples of the spell-qtl pipeline.

# SEE ALSO

`spell-marker` (1), `spell-qtl` (1), `spell-qtl-examples` (1).
