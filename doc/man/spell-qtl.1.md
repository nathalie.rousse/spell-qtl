% SPELL-QTL(1) Spell-QTL software suite
% Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
% December 2016

# NAME

spell-qtl – Compute n-point Parental Origin Probabilities and perform QTL analysis on modern genetic datasets.

# SYNOPSIS

**spell-qtl** [*options...*] [**-wd** *PATH*] **-n** *NAME* **-gm** *MAP* **-p** *GEN* *TRAITS* [**-p**...] [model and algorithms configuration...]

# DESCRIPTION

**spell-qtl** computes the n-point Parental Origin Probabilities along the linkage groups using the data provided by
**spell-marker**.

**spell-qtl** expects that **spell-pedigree** and **spell-marker** have been run beforehand with the same working directory and configuration name.

**-wd**,**--work-directory** *PATH*
: Path to directory for cache files and outputs. Defaults to the **current directory**.

**-n**,**--name** *NAME*
: User-friendly name for this configuration

**-gm**,**--genetic-map** *MAP*
: Path to the genetic map file

**-p**,**--population** *GEN* *TRAITS*
: Specify a new population (dataset) to work on.

    *GEN* is the name of the phenotyped generation. The *TRAITS* path must point to a single_trait observation file with the
    **same** number of individuals as defined for the given generation in the pedigree for this population.

# OPTIONS

## Miscellaneous

**-v**,**--version**
: Display version and exit

**-h**,**--help**
: Display usage and exit

**-N**,**--notes** *TEXT*
: Optional free text

**-P**,**--parallel** *N_CORES*
: Setup parallel computations (number of cores to use or 'auto'). Defaults to **0**.

**--clean**
: Clears all cached files in the specified working directory (the **-wd** parameter **MUST** appear before **--clean**).

**-a**,**--ansi**
: Use ANSI escape sequences to display colors and realtime progress information at the top of the terminal.
Enabled by default only if output is on a terminal.

**-na**,**--no-ansi**
: Don't use ANSI escape sequences, don't display colors or realtime progress information.

**-rj**,**--join-radius** *DISTANCE*
: Specify the maximum distance from a selected locus to compute joint probabilities. Default is **10**.

**-rs**,**--skip-radius** *DISTANCE*
: Specify the maximum distance from a selected locus to skip tests. Default is **1**.

## Input datasets

The following configures the construction of the linear model.
The following specifies the datasets you want processed. A dataset specification starts with argument -p, followed by one or more arguments -m.

**-gm**,**-genetic-map** *PATH*
: Path to the genetic map file.

**-p**,**-population** *QTL_generation_name* *PHEN_PATH*
: Specify a new population (dataset) to work on.

## Model options

The following configures the construction of the linear model.

**connected**
: Select connected mode. Disabled by default.

    In connected mode, the same ancestors in two datasets share the same column in the linear model.
    
## Working set options

The following configures the analysis domain.

**lg** *LINKAGE GROUP NAMES*
: Specify the list of linkage groupe to study.

**covar** *COVARIABLE NAMES*
: Specify the list of covariables to put in the model.

**traits** *TRAIT NAMES*
: Specify the list of traits to analyse.

**pleiotropy** *PLEIOTROPIC_TRAIT_NAME* *TOLERANCE* *TRAIT_NAMES*
: Specify a pleiotropic trait. This trait will be added to the list of traits to analyze. 1.e-3 is a good default value for *TOLERANCE*.

## Processing options

The following configures the QTL analysis.
The standard pipeline is:
1. skeleton creation
2. cofactors detection
3. QTLs detection
4. effects estimation

**output-nppop**
: Compute the n-point Parental Origin Probabilities and exit. The results will be written under the **WORK_DIRECTORY/NAME.n-point** directory.

**qtl-threshold-permutations** *VALUE*
: Set the number of permutations to compute the QTL threshold value in automatic mode. Default is **10000**.

**qtl-threshold-quantile** *VALUE*
: Set the quantile value in range [0:1] to select the QTL threshold value in automatic mode. Default is **0.05**. 

**qtl-threshold-value** *single_trait=value,...*
: Set the QTL threshold value manually for some traits. If not specified, will be automatically computed using the above settings.

**cofactor-threshold** *single_trait=value,...*
: Set the cofactor threshold value manually for some traits. Defaults to **value of QTL threshold * .9**.

**cofactor-exclusion-window** *DISTANCE*
: Set the half-size (in cM) of the exclusion window around cofactors. No detection will be performed inside this window. Defaults to **30**.

**step** *VALUE*
: Step size in cM. Defaults to **1**.

**lod-support** *VALUE*
: LOD support value. Defaults to **1**.

**skeleton** *MODE* *marker,...\ OR \ distance*
: Setup the cofactor detection skeleton. Mode can be either *manual*, *auto* or *none*.

    If *manual*, specify a comma-separated marker list.
    If *auto*, specify the minimum interval between markers in cM.

    By default, mode is *auto* and interval is *20*.
    
**cofactor-detection** **ALGORITHM**
: Specify the cofactor detection algorithm. Available algorithms are *forward*, *backward*, *none*, and *all*. Default is *forward*.

**initial-selection** *SELECTION*
: Specify the initial selection of QTLs for the detection algorithm.

    The selection is a comma-separated list of CHROMOSOME:POSITION values.
    Setting an initial selection overrides and cancels skeleton generation and cofactor detection.

**QTL-detection** *ALGORITHM*
: Specify the QTL detection algorithm. Available algorithms are *none*, *CIM*, *CIM-*, *iQTLm*, and *iQTLm-GW*. The default algorithm is *iQTLm*.

# EXAMPLES

See `spell-qtl-examples` (1) for complete examples of the spell-qtl pipeline.

# SEE ALSO

`spell-pedigree` (1), `spell-qtl` (1), `spell-qtl-examples` (1).
