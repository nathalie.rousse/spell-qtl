/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// #include "geno_matrix.h"
#include "data/genoprob_computer2.h"
#include "pedigree.h"
#include "pedigree_tree.h"


inline
std::vector<size_t> operator | (const std::vector<size_t>& s1, const std::vector<size_t>& s2)
{
    std::vector<size_t> ret(s1.size() + s2.size());
    auto it = std::set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}


inline
std::vector<size_t> operator ^ (const std::vector<size_t>& s1, const std::vector<size_t>& s2)
{
    std::vector<size_t> ret(s1.size() + s2.size());
    auto it = std::set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}


inline
std::vector<size_t> operator | (const std::vector<size_t>& s1, size_t p)
{
    std::vector<size_t> ret(s1.size() + 1);
    auto i = s1.begin();
    auto j = s1.end();
    auto o = ret.begin();
    while (i != j && *i < p) {
        *o++ = *i++;
    }
    if (i != j && *i == p) {
        ++i;
    }
    *o++ = p;
    while (i != j) {
        *o++ = *i++;
    }
    ret.resize(o - ret.begin());
    return ret;
}


inline
std::vector<size_t> operator / (const std::vector<size_t>& s1, const std::vector<size_t>& s2)
{
    std::vector<size_t> ret(s1.size());
    auto it = std::set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}




label_type empty = {GAMETE_EMPTY, GAMETE_EMPTY};


geno_matrix
    gamete = {
        Gamete,
        {{GAMETE_L, GAMETE_EMPTY}, {GAMETE_R, GAMETE_EMPTY}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        /* BEWARE these matrices (p and p_inv) SHOULD be * 1/sqrt(2) */
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         -P_NORM_FACTOR, P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         -P_NORM_FACTOR, P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << -2, 0).finished(),
        (VectorXd(2) << .5, .5).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
#ifdef WITH_OVERLUMPING
        {},
            /*{{{GAMETE_L, GAMETE_L}, {GAMETE_R, GAMETE_R}}, {{0, 0}, {1, 1}}},*/
            /*{{{GAMETE_L, GAMETE_R}, {GAMETE_R, GAMETE_L}}, {{0, 1}, {1, 0}}}*/
            /*symmetry_group_type().insert({permutation_type::anti_identity(2), letter_permutation_type{}})*/
        /*},*/
        {}
#endif

    },
    doubling_gamete = {
        DoublingGamete,
        {{GAMETE_L, GAMETE_L}, {GAMETE_R, GAMETE_R}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         -P_NORM_FACTOR, P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         -P_NORM_FACTOR, P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << -2, 0).finished(),
        (VectorXd(2) << .5, .5).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
        (MatrixXd(2, 2) <<
         1, 0,
         0, 1).finished(),
#ifdef WITH_OVERLUMPING
        {},
            /*{{{GAMETE_L, GAMETE_L}, {GAMETE_R, GAMETE_R}}, {{0, 1}, {1, 0}}}*/
            /*{{{0, 1}, {1, 0}}}*/
            /*symmetry_group_type().insert({permutation_type::identity(2), letter_permutation_type{}})*/
        /*},*/
        {}
#endif
    };

geno_matrix
    selfing_gamete = kronecker(gamete, gamete);





labelled_matrix<MatrixXd, label_type, double>
run_gpc(const geno_matrix* gen, const std::vector<double>& LV_loci, const MatrixXd& LV, const std::vector<double>& test_loci)
{
    geno_prob_computer gpc;
    gpc.LV = &LV;
    gpc.locus = &LV_loci;
    gpc.init(gen);
    return gpc.fast_compute_over_segment(test_loci);
}


labelled_matrix<MatrixXd, label_type, double>
run_on_segment(const geno_matrix* gen, double delta_M2, double delta_M3,
               const std::vector<label_type>& obs_M1, const std::vector<label_type>& obs_M2, const std::vector<label_type>& obs_M3,
               std::vector<double> steps, const std::set<subset>& P)
{
    auto obs2LV = gen->LV_map();

    MatrixXd LV = MatrixXd::Zero(gen->rows(), 3);

    for (auto l: obs_M1) { LV.col(0) += obs2LV[l]; }
    for (auto l: obs_M2) { LV.col(1) += obs2LV[l]; }
    for (auto l: obs_M3) { LV.col(2) += obs2LV[l]; }

    double dist_M3 = delta_M2 + delta_M3;

    std::vector<double> LV_loci = {0, delta_M2, dist_M3};
    MSG_DEBUG("steps " << steps);
    MSG_DEBUG("LV_loci " << LV_loci);
    MSG_DEBUG("LV");
    MSG_DEBUG(LV);

    auto ret = run_gpc(gen, LV_loci, LV, steps);
    auto collect = experimental_lumper::make_collect(P, ret.data.rows());
    ret.data = collect * ret.data;
    auto pi = P.begin();
    auto pj = P.end();
    for (size_t i = 0; pi != pj; ++i, ++pi) {
        ret.row_labels[i] = ret.row_labels[pi->front()];
    }
    ret.row_labels.resize(P.size());
    return ret;
}


/*
labelled_matrix<MatrixXd, label_type, double>
run_on_segment(const geno_matrix* gen, double delta_M2, double delta_M3, const std::vector<label_type>& obs_M1, const std::vector<label_type>& obs_M2, const std::vector<label_type>& obs_M3, double step)
{
    std::vector<double> LV_loci = {0, delta_M2, delta_M3 + delta_M3};
    auto steps = compute_steps(LV_loci, step, {});
    return run_on_segment(gen, delta_M2, delta_M3, obs_M1, obs_M2, obs_M3, steps);
}
*/


void proba_plot(braille_plot& p, const labelled_matrix<MatrixXd, label_type, double>& LM)
{
    std::vector<double> y;
    struct col { int r, g, b; };
    std::vector<col> palette = {
        {255, 0, 0},
        {0, 255, 0},
        {0, 255, 0},
        {0, 0, 255}
    };
    for (int r = 0; r < LM.data.rows(); ++r) {
        y.clear();
        y.reserve(LM.data.cols());
        for (int c = 0; c < LM.data.cols(); ++c) {
            y.push_back(LM.data(r, c));
        }
        p.plot(LM.column_labels, y, palette[r].r, palette[r].g, palette[r].b);
    }
}


void proba_plot(const labelled_matrix<MatrixXd, label_type, double>& LM)
{
    braille_plot p(300, 50, LM.column_labels.front(), LM.column_labels.back(), -.02, 1.02);
    proba_plot(p, LM);
    p.haxis(0, 0, .5, .1);
    p.vaxis(0, 0, .5, .25);
    MSG_DEBUG("" << p);
}


template <typename T>
    T to(const std::string& s) { std::stringstream ss(s); T x; ss >> x; return x; }

template <typename T>
    std::string to_string(T&& x) { std::stringstream ss; ss << x; return ss.str(); }

struct sim_data_table {
    std::vector<std::string> colnames;
    std::vector<std::string> rownames;
    std::vector<std::vector<double>> data;

    sim_data_table(const std::string& filename)
        : colnames(), rownames(), data()
    {
        std::string line, word;
        ifile in(filename);
        std::getline(in, line);
        std::stringstream header(line);
        /*MSG_DEBUG("HEADER: " << line);*/
        header >> word;
        while (!header.eof()) {
            colnames.emplace_back();
            header >> colnames.back();
            data.emplace_back();
        }
        while (!in.eof()) {
            std::getline(in, line);
            if (line.size() == 0) {
                continue;
            }
            /*MSG_DEBUG("row: " << line);*/
            std::stringstream inl(line);
            rownames.emplace_back();
            inl >> rownames.back();
            for (int i = 0; !inl.eof(); ++i) {
                data[i].emplace_back();
                inl >> data[i].back();
            }
        }
    }

    sim_data_table(const labelled_matrix<MatrixXd, label_type, double>& result)
        : colnames(), rownames(), data()
    {
        for (const auto& l: result.column_labels) { rownames.push_back(to_string(l)); }
        for (const auto& l: result.row_labels) { colnames.push_back(to_string(l)); }
        data.resize(result.data.rows(), std::vector<double>(result.data.cols(), 0));
        for (int c = 0; c < result.data.cols(); ++c) {
            for (int r = 0; r < result.data.rows(); ++r) {
                data[r][c] = result.data(r, c);
            }
        }
    }

    sim_data_table() : colnames(), rownames(), data() {}

    size_t
        find_col(const std::string& name) const
        {
            return std::find(colnames.begin(), colnames.end(), name) - colnames.begin();
        }

    size_t
        find_row(const std::string& name) const
        {
            return std::find(rownames.begin(), rownames.end(), name) - rownames.begin();
        }

    const std::vector<double>&
        col(size_t n) const { return data[n]; }

    std::vector<double>
        row(size_t n) const { std::vector<double> ret; ret.reserve(data.size()); for (const auto& col: data) { ret.push_back(col[n]); } return ret; }

    double
        sample_count() const { auto x = row(0); double accum = 0; for (double i: x) { accum += i; } return accum; }

    size_t rows() const { return data[0].size(); }
    size_t cols() const { return data.size(); }

    sim_data_table
        operator + (const sim_data_table& other) const
        {
            /*MSG_DEBUG("ADDING");*/
            /*MSG_DEBUG("" << other);*/
            /*MSG_DEBUG("TO");*/
            /*MSG_DEBUG("" << (*this));*/
            sim_data_table ret;
            ret.colnames = colnames;
            ret.rownames = rownames;
            ret.data = data;
            for (size_t c = 0; c < data.size(); ++c) {
                for (size_t r = 0; r < data[c].size(); ++r) {
                    ret.data[c][r] += other.data[c][r];
                }
            }
            /*MSG_DEBUG("=");*/
            /*MSG_DEBUG("" << ret);*/
            return ret;
        }

    sim_data_table
        operator - (const sim_data_table& other) const
        {
            /*MSG_DEBUG("ADDING");*/
            /*MSG_DEBUG("" << other);*/
            /*MSG_DEBUG("TO");*/
            /*MSG_DEBUG("" << (*this));*/
            sim_data_table ret;
            ret.colnames = colnames;
            ret.rownames = rownames;
            ret.data = data;
            for (size_t c = 0; c < data.size(); ++c) {
                for (size_t r = 0; r < data[c].size(); ++r) {
                    ret.data[c][r] -= other.data[c][r];
                }
            }
            /*MSG_DEBUG("=");*/
            /*MSG_DEBUG("" << ret);*/
            return ret;
        }

    sim_data_table
        normalized() const
        {
            double factor = 1. / sample_count();
            sim_data_table ret = *this;
            for (auto& col: ret.data) {
                for (auto& d: col) {
                    d *= factor;
                }
            }
            return ret;
        }

    sim_data_table
        operator | (const sim_data_table& other) const
        {
            sim_data_table ret(*this);
            ret.colnames.insert(ret.colnames.end(), other.colnames.begin(), other.colnames.end());
            ret.data.insert(ret.data.end(), other.data.begin(), other.data.end());
            return ret;
        }

    friend
        std::ostream&
        operator << (std::ostream& os, const sim_data_table& sd)
        {
            os << "marker";
            for (const auto& c: sd.colnames) { os << '\t' << c; }
            os << std::endl;
            for (size_t i = 0; i < sd.rownames.size(); ++i) {
                os << sd.rownames[i];
                for (size_t c = 0; c < sd.data.size(); ++c) {
                    os << '\t' << sd.data[c][i];
                }
                os << std::endl;
            }
            return os;
        }
};



#include <glob.h>
#include <regex>

/* taken from http://stackoverflow.com/a/24703135 */
std::vector<std::string>
globVector(const std::string& pattern){
    glob_t glob_result;
    glob(pattern.c_str(),GLOB_TILDE,NULL,&glob_result);
    std::vector<std::string> files;
    for(unsigned int i=0;i<glob_result.gl_pathc;++i){
        files.push_back(std::string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return files;
}


struct sim_data_map {
    std::map<int, std::map<int, std::map<std::string, sim_data_table>>> data;
    size_t counts;
    std::set<char> letters;

    sim_data_map(const std::string& base_dir)
        : data(), counts(0)
    {
        auto filelist = globVector(SPELL_STRING(base_dir << "/*/*/*/*.txt"));
        std::regex path_info(".*/counts_([0-9]+)/([0-9]+)/([0-9]+)/([a-zA-Z]{3}).txt", std::regex_constants::extended);
        MSG_DEBUG("Reading " << filelist.size() << " files...");
        for (const auto& path: filelist) {
            std::smatch match;
            if (std::regex_match(path.begin(), path.end(), match, path_info)) {
                /*MSG_DEBUG("MATCH FOUND! " << match[0] << ", " << match[1] << ", " << match[2] << ", " << match[3] << ", " << match[4]);*/
                if (counts == 0) {
                    counts = to<int>(match[1]);
                }
                int dm12 = to<int>(match[2]);
                int dm23 = to<int>(match[3]);
                std::string m123 = match[4];
                data[dm12][dm23].emplace(m123, sim_data_table{path});
                for (char c: m123) { letters.insert(c); }
            }
        }
        MSG_DEBUG("Done reading " << filelist.size() << " files.");
    }

    const sim_data_table&
        get(int m12, int m23, char M1, char M2, char M3) const
        {
            return data.find(m12)->second.find(m23)->second.find(std::string{M1, M2, M3})->second;
        }

    sim_data_table
        get_sums_over_M3(int m12, int m23, char M1, char M2) const
        {
            auto i = letters.begin();
            auto j = letters.end();
            sim_data_table ret = get(m12, m23, M1, M2, *i);
            for (++i; i != j; ++i) {
                ret = ret + get(m12, m23, M1, M2, *i);
            }
            return ret;
        }

    double
        index_to_distance(int i) const
        {
            return r_to_d(.5 * i / counts);
        }

private:
    
    /* ENSURE THAT THIS CODE IS EXACTLY COMPATIBLE WITH THE SIMULATOR CODE */

    inline
        static double r_to_d(double r)
        {
            if (r == .5) {
                r = .4999;
            }
            return -.5 * log(1. - 2. * r);
        }

};


std::vector<double>
simu_steps(size_t n_rows, double d12, double d23)
{
    std::vector<double> X(n_rows + 1);
    for (int i = 0; i < (int) n_rows; ++i) {
        X[i] = i * d12 / (n_rows - 1);
    }
    X.back() = X[n_rows] + d23;
    MSG_DEBUG("COMPUTED X WITH n_rows=" << n_rows << " = size(" << X.size() << ") " << X);
    return X;
}

braille_plot
proba_plot(const sim_data_table& table, double d12)
{
    auto X = simu_steps(table.rows() - 1, d12, 0);
    X.pop_back();
    /*MSG_DEBUG("X " << X);*/
    /*MSG_DEBUG("X.size() = " << X.size());*/
    struct rgb { int r, g, b; };
    std::vector<rgb> palette_tpl = {
        {255, 0, 0},
        {0, 255, 0},
        {0, 0, 255},
        {255, 128, 128},
        {128, 255, 128},
        {128, 128, 255},
        {255, 255, 0},
        {0, 255, 255},
        {255, 0, 255},
        {255, 255, 128},
        {128, 255, 255},
        {255, 128, 255},
        {255, 255, 255}
    };
    std::vector<std::pair<int, int>> styles_tpl = {
        {1, 0},
        {1, 1}
    };
    std::vector<rgb> palette;
    std::vector<std::pair<int, int>> styles;
    for (size_t i = 0; i < table.cols(); i += 2) {
        palette.push_back(palette_tpl[i >> 1]);
        styles.push_back(styles_tpl[0]);
    }
    for (size_t i = 0; i < table.cols(); i += 2) {
        palette.push_back(palette_tpl[i >> 1]);
        styles.push_back(styles_tpl[1]);
    }
    double max = -std::numeric_limits<double>::infinity(), min = std::numeric_limits<double>::infinity();
    for (int c = 0; c < (int) table.cols(); ++c) {
        for (int r = 0; r < (int) X.size(); ++r) {
            double x = table.col(c)[r];
            if (x > max) {
                max = x;
            }
            if (x < min) {
                min = x;
            }
        }
    }
    double pad_x = (X.back() - X.front()) * .05;
    double pad_y = (max - min) * .05;
    double min_x = X.front() - pad_x;
    double max_x = X.back() + pad_x;
    double min_y = min - pad_y;
    double max_y = max + pad_y;
    MSG_DEBUG("USING TABLE(" << table.rows() << ", " << table.cols() << ')');
    MSG_DEBUG("" << table);
    MSG_DEBUG("xlim=(" << min_x << ", " << max_x << ") ylim=(" << min_y << ", " << max_y << ')');
    braille_plot p(200, 75, X.front() - pad_x, X.back() + pad_x, min - pad_y, max + pad_y);
    for (int c = 0; c < (int) table.cols(); ++c) {
        /*MSG_DEBUG("C " << table.col(c));*/
        auto e = table.col(c).end();
        --e;
        MSG_DEBUG("PLOTTING COLUMN #" << c);
        p.plot(X, std::vector<double>(table.col(c).begin(), e), palette[c].r, palette[c].g, palette[c].b, styles[c].first, styles[c].second);
    }
    double xtick = 1;
    while (xtick > X.back()) { xtick *= .1; }
    double ytick = 1;
    while (ytick > max) { ytick *= .1; }
    p.haxis(0, 0, xtick, xtick * .25);
    p.vaxis(0, 0, ytick, ytick * .25);
    MSG_DEBUG("" << p);
    return p;
}



std::vector<std::string> parse_arg(const char* arg)
{
    std::vector<std::string> ret;
    std::string s(arg);
    auto beg = s.begin();
    while (true) {
        auto end = std::find(beg, s.end(), ',');
        ret.emplace_back(beg, end);
        if (end == s.end()) {
            break;
        }
        beg = end + 1;
    }
    return ret;
}



std::pair<pedigree_type, int>
build_selfing(int max_states, int n_gen)
{
    std::pair<pedigree_type, int> ped;
    ped.first.max_states = max_states;
    int A = ped.first.ancestor();
    int B = ped.first.ancestor();
    int F1 = ped.first.crossing(A, B);
    ped.second = F1;
    for (; n_gen >= 2; --n_gen) {
        ped.second = ped.first.selfing(ped.second);
    }
    return ped;
}


std::pair<pedigree_type, int>
build_sibling(int max_states, int n_gen)
{
    std::pair<pedigree_type, int> ped;
    ped.first.max_states = max_states;
    int p1 = ped.first.ancestor();
    int p2 = ped.first.ancestor();
    for (; n_gen >= 2; --n_gen) {
        int np1 = ped.first.crossing(p1, p2);
        int np2 = ped.first.crossing(p1, p2);
        p1 = np1;
        p2 = np2;
    }
    ped.second = ped.first.crossing(p1, p2);
    return ped;
}


std::pair<pedigree_type, int> build_pedigree(int max_states, int argc, int cur, const char** argv)
{
    if (std::string("selfing") == argv[cur]) {
        return build_selfing(max_states, to<int>(argv[cur + 1]));
    } else if (std::string("sibling") == argv[cur]) {
        return build_sibling(max_states, to<int>(argv[cur + 1]));
    }
    std::map<std::string, int> individuals;
    std::pair<pedigree_type, int> ret;
    ret.first.max_states = max_states;
    for (; cur < argc; ++cur) {
        auto cross = parse_arg(argv[cur]);
        const std::string& ind = cross[0];
        if (cross.size() == 1) {
            ret.second = individuals[ind] = ret.first.ancestor();
        } else if (cross.size() == 2) {
            ret.second = individuals[ind] = ret.first.dh(individuals[cross[1]]);
        } else {
            ret.second = individuals[ind] = ret.first.crossing(individuals[cross[1]], individuals[cross[2]]);
        }
    }
    return ret;
}


std::map<char, std::vector<label_type>>
parse_marker_obs_format(const char* arg)
{
    std::string s(arg);
    auto it = s.begin();
    auto end = s.end();
    std::map<char, std::vector<label_type>> ret;
    while (it != end) {
        char key = *it++;
        while (it != end && *it++ != ',') {
            char l1 = *it++;
            char l2 = *it++;
            ret[key].emplace_back(l1, l2);
        }
    }
    return ret;
}


std::set<subset> make_obs_partition(const std::map<char, std::vector<label_type>>& format, const std::vector<std::string>& columns, const std::vector<label_type>& labels)
{
    std::set<subset> ret;
    for (const auto& s: columns) {
        const auto& fmt_labels = format.find(s[0])->second;
        subset part;
        for (int i = 0; i < (int) labels.size(); ++i) {
            if (std::find(fmt_labels.begin(), fmt_labels.end(), labels[i]) != fmt_labels.end()) {
                part.push_back(i);
            }
        }
        ret.insert(part);
    }
    return ret;
}



int main(int argc, const char** argv)
{
    if (argc < 8) {
        MSG_DEBUG("Usage:");
        MSG_DEBUG(argv[0] << " <sim_data_dir> <max_states> <d12> <d23> <marker_obs_format> <M1M2M3_simu> <M1M2M3_comp> <pedigree...>");
        MSG_DEBUG("marker_obs_format: A=aa,B=bb,H=aa:ab,U=aa:ab:ba:bb");
        MSG_DEBUG("pedigree: A B F1,A,B F2,F1,F1 DH_F2,F2 ...");
        return 0;
    }
    std::vector<const char*> args(argv, argv + argc);
    auto ai = args.begin();
    ++ai;
    std::string sim_data_dir(*ai++);
    size_t max_states = to<size_t>(*ai++);
    int d12 = to<int>(*ai++);
    int d23 = to<int>(*ai++);
    std::map<char, std::vector<label_type>> marker_obs_format = parse_marker_obs_format(*ai++);
    std::string M123(*ai++);
    char M1 = M123[0];
    char M2 = M123[1];
    char M3 = M123[2];
    std::string M123_comp(*ai++);
    std::vector<label_type> M1_comp = marker_obs_format[M123_comp[0]];
    std::vector<label_type> M2_comp = marker_obs_format[M123_comp[1]];
    std::vector<label_type> M3_comp = marker_obs_format[M123_comp[2]];
    auto ped_ind = build_pedigree(max_states, argc, ai - args.begin(), argv);
    int ind = ped_ind.second;
    const auto& ped = ped_ind.first;

    sim_data_map simu(sim_data_dir);
    auto REF_MEANS_M12 = simu.get_sums_over_M3(d12, d23, M1, M2).normalized();
    auto REF_M12 = simu.get(d12, d23, M1, M2, M3).normalized() - REF_MEANS_M12;
    auto X = simu_steps(REF_M12.rows() - 1, simu.index_to_distance(d12), simu.index_to_distance(d23));
    auto pol = experimental_lumper(*ped.get_gen(ind)).partition_on_labels();
    std::vector<label_type> short_labels;
    for (const auto& part: pol) { short_labels.push_back(ped.get_gen(ind)->labels[part.front()]); }
    MSG_DEBUG("short_labels");
    MSG_DEBUG("" << short_labels);
    auto obs_P = make_obs_partition(marker_obs_format, REF_M12.colnames, short_labels);
    MSG_DEBUG("obs_P");
    MSG_DEBUG("" << obs_P);
    MSG_QUEUE_FLUSH();
    auto result = run_on_segment(ped.get_gen(ind).get(), simu.index_to_distance(d12), simu.index_to_distance(d23), M1_comp, M2_comp, M3_comp, X, obs_P);

    MSG_DEBUG(result);
    sim_data_table result_table(result);
    MSG_DEBUG(result_table);

    auto full_comp = REF_M12 | (result_table - REF_MEANS_M12);
    proba_plot(full_comp, simu.index_to_distance(d12));
    proba_plot(simu.get(d12, d23, M1, M2, M3).normalized() | result_table, simu.index_to_distance(d12));

    return 0;
}

