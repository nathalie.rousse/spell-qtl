/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "error.h"
#include "input/design.h"
#include "geno_matrix.h"

void read_possibly_unknown(const std::string& filename, std::istream& is, int& num)
{
    std::string tmp;
    is >> tmp;
    /*MSG_DEBUG("read " << tmp);*/
    if (!tmp.size() || (tmp != "-" && (tmp[0] < '0' || tmp[0] > '9'))) {
        MSG_ERROR("Reading " << filename << ": Expected a number but read \"" << tmp << '"', "Check the counts of individuals are correct in your pedigree file");
        num = 0;
    }
    if (tmp == "-" || !tmp.size()) {
        num = 0;
    } else {
        std::stringstream s(tmp);
        s >> num;
    }
}

pedigree_type read_pedigree(const design_type* design, const std::string& filename, std::istream& is)
{
    if (!design) {
        MSG_ERROR("Reading " << filename << ": No breeding design specified. Can't read pedigree file.", "Define a breeding design BEFORE specifying any pedigree file");
        return {};
    }

    pedigree_type ret;

    std::string gen_name;
    int n_ind;
    int ind, mother, father;

    while (!is.eof()) {
        is >> gen_name >> n_ind;
        auto it = design->generation.find(gen_name);
        if (it == design->generation.end()) {
            MSG_ERROR("Reading " << filename << ": Generation '" << gen_name << "' isn't defined.", "Fix your breeding design or pedigree file");
            return {};
        }
        const geno_matrix* g = it->second;
        std::pair<const geno_matrix*, const geno_matrix*> parents;
        parents = g->design->get_parents();
        int mother_gen_size, father_gen_size;
        mother_gen_size = ret[parents.first].size();
        father_gen_size = ret[parents.second].size();
        auto& vec = ret[g];
        vec.reserve(n_ind);
        /*MSG_DEBUG("generation " << gen_name);*/
        for (int i = 0; i < n_ind; ++i) {
            ind = mother = father = -1;
            is >> std::ws;
            if (is.eof()) {
                MSG_ERROR("Reading " << filename << ": unexpected end of file, expected " << n_ind << " individuals in generation " << gen_name << " and read only " << vec.size() << " so far", "Fix your pedigree file");
                return {};
            }
            is >> ind;
            read_possibly_unknown(filename, is, mother);
            read_possibly_unknown(filename, is, father);
            --ind;
            --mother;
            --father;
            if (ind != i) {
                MSG_ERROR("Reading " << filename << ": Individual numbers not sequential (expected " << (i + 1) << ", found " << ind << ") in pedigree of generation " << gen_name, "Fix your pedigree data");
                return {};
            }
            if (ind < 0) {
                MSG_ERROR("Reading " << filename << ": Invalid individual number " << ind << " in pedigree of generation " << gen_name, "Fix your pedigree data");
                return {};
            }
            if (parents.first && mother != -1 && mother >= mother_gen_size) {
                MSG_ERROR("Reading " << filename << ": Invalid mother number " << ind << " in pedigree of generation " << gen_name, "Fix your pedigree data");
                return {};
            }
            if (parents.second && father != -1 && father >= father_gen_size) {
                MSG_ERROR("Reading " << filename << ": Invalid father number " << ind << " in pedigree of generation " << gen_name, "Fix your pedigree data");
                return {};
            }
            vec.emplace_back(mother, father);
        }
        is >> std::ws;
    }
    return ret;
}

