/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model/print.h"

namespace model_print {
int precision = 4;
std::string field_sep = "  ";
std::string section_hsep = " | ";
char section_vsep = '-';
std::string section_csep = "-+-";
}

int main(int, char**)
{
    /*std::cout << std::setprecision(2) << 1.23456789 << std::endl;*/
    /*std::cout << std::setprecision(2) << 12.3456789 << std::endl;*/
    /*labelled_matrix<Eigen::MatrixXd, int, std::string> m;*/
    Eigen::MatrixXd m(3, 3), mk;
    m << 1, 2, 3, 4, 5, 6, 7, 8, 9;
    m *= 1.23;
    mk = kroneckerProduct(m, m);
    std::vector<std::string> labels = {"aa", "ab", "ac"};
    /*std::cout << std::setprecision(2) << 1234.56789 << std::endl;*/

    /*model_print::matrix_with_sections<std::string, void, std::string, std::string> mws(m.data);*/
    model_print::matrix_with_sections<std::string, std::string, std::string, std::string> mws(mk);
    std::string star("*");
    std::string toto("toto toto");
    std::string pouet("pouet");
    mws.add_column_section(toto, labels);
    mws.add_column_section(pouet, labels);
    mws.add_column_section(toto, labels);
    /*mws.add_row_section(pouet, 3);*/
    mws.add_row_section(pouet, labels);
    mws.add_row_section(toto, labels);
    mws.add_row_section(star, labels);

    auto fw = mws.compute_field_widths();
    auto sw = mws.compute_section_widths(fw, 1);
    auto sh = mws.compute_section_heights();
    /*std::cout << "field widths";*/
    /*for (auto x: fw) {*/
        /*std::cout << ' ' << x;*/
    /*}*/
    /*std::cout << std::endl;*/

    /*std::cout << "section widths";*/
    /*for (auto x: sw) {*/
        /*std::cout << " {" << x.label_length << ", " << x.size << '}';*/
    /*}*/
    /*std::cout << std::endl;*/

    /*std::cout << "section heights";*/
    /*for (auto x: sh) {*/
        /*std::cout << " {" << x.label_length << ", " << x.size << '}';*/
    /*}*/
    /*std::cout << std::endl;*/

    std::cout << mws << std::endl;

    return 0;
}

