/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "geno_matrix.h"
#include <iomanip>


int main(int argc, char** argv)
{
    permutation_type p0 = permutation_type::identity(10);
    MSG_DEBUG("p0" << std::endl << p0 << std::endl);
    std::vector<int> perm = {0, 2, 4, 1, 3, 5, 6, 8, 7, 9};
    permutation_type p1(perm);
    MSG_DEBUG("p1" << std::endl << p1 << std::endl);
    MSG_DEBUG("p1.transpose" << std::endl << (p1.transpose()) << std::endl);
    MSG_DEBUG("p1 * p1" << std::endl << (p1 * p1) << std::endl);
    MSG_DEBUG("p1 * p1.transpose" << std::endl << (p1 * p1.transpose()) << std::endl);
    std::vector<std::string> oups = {"pouet", "plop", "coin", "prout", "plof", "hop", "bouh", "chou", "genou", "caillou"};
    std::stringstream ss;
    ss << std::setw(10) << "vec:";
    for (const auto& s: oups) {
        ss << ' ' << std::setw(10) << s;
    }
    ss << std::endl;
    ss << std::setw(10) << "vec*p1:";
    for (const auto& s: p1 * oups) {
        ss << ' ' << std::setw(10) << s;
    }
    ss << std::endl;
    MSG_DEBUG(ss.str());

    Eigen::MatrixXi mat(3, 3);

    mat << 1, 2, 3, 4, 5, 6, 7, 8, 9;

    MSG_DEBUG(mat << std::endl);

    permutation_type p2({0,2,1});

    MSG_DEBUG(p2);

    MSG_DEBUG((p2 % mat));

    permutation_type p3({2, 1, 0});

    MSG_DEBUG("kronecker of" << std::endl << p2 << std::endl << "and" << std::endl << p3 << std::endl << "=");
    MSG_DEBUG(kronecker(p2, p3));

    MSG_DEBUG("kronecker of" << std::endl << p3 << std::endl << "and" << std::endl << p2 << std::endl << "=");
    MSG_DEBUG(kronecker(p3, p2));

    {
        permutation_type p6({4,5,1,2,0,3});
        permutation_type lz = permutation_type::lozenge(3, 2);
        MSG_DEBUG("==");
        MSG_DEBUG(lz);
        MSG_DEBUG("==");
        MSG_DEBUG((generate_lozenge(3, 2) - lz.matrix<bool>()));
        permutation_type lzp = permutation_type::lozenge(3, 2, p6);
        MSG_DEBUG("==");
        MSG_DEBUG(lzp);
        MSG_DEBUG("==");
        MSG_DEBUG((generate_lozenge(3, 2, p6.matrix<bool>()) - lzp.matrix<bool>()));
    }

    if (0)
    {
        braille_grid g1(1, 1);
        g1.put_pixel(0, 0);
        MSG_DEBUG("g1 " << g1);
        braille_grid g2(8, 8);
        for (int i = 0; i < 8; ++i) {
            for (int j = i & 1; j < 8; j += 2) {
                g2.put_pixel(i, j);
            }
        }
        MSG_DEBUG(g2);
    }

    if (1)
    {
        braille_plot plot(200, 20, -5, 5, -1.1, 1.1);
        plot.plot((double(&)(double)) cos, .025, 100, 100, 230);
        plot.plot((double(&)(double)) sin, .025, 255, 0, 0);
        plot.set_background(20, 20, 60);
        MSG_DEBUG(plot);
        MSG_DEBUG("");
        /*plot.line(-5, 0, 5, 0, 2, 1);*/
        /*plot.line(0, -1, 0, 1, 2, 1);*/
        plot.vline(-M_PI/4, 1, 0, 255, 0, 0);
        plot.vline(-M_PI/2, 1, 0, 0, 255, 0);
        plot.vline(-3*M_PI/4, 1, 0, 0, 0, 255);
        plot.haxis(0, 0, M_PI, M_PI / 4, 0, 255, 255);
        plot.vaxis(0, 0, 1, .25, 0, 255, 255);
        plot.add_label(0, 0, "\v 0");
        plot.add_label(0, 1, "\v 1");
        plot.add_label(0, -1, " -1");
        plot.add_label(M_PI, 0, "\vπ");
        plot.add_label(-M_PI, 0, "\v-π");
        MSG_DEBUG(plot);
        MSG_DEBUG("");
        braille_plot mini(1, 1, 0, 1, 0, 1);
        mini.put_pixel(0, 0);
        mini.put_pixel(.5, 0);
        mini.put_pixel(0, 1./3);
        mini.put_pixel(.5, 1./3);
        mini.put_pixel(0, 2./3);
        mini.put_pixel(.5, 2./3);
        mini.put_pixel(0, 1.);
        mini.put_pixel(.5, 1.);
        MSG_DEBUG(mini);

        MSG_DEBUG("x+ y+");
        MSG_DEBUG(braille_plot(5, 5, -1, 1, -1, 1).line(-1, -1, 1, 1, 1, 1));
        MSG_DEBUG("x+ y-");
        MSG_DEBUG(braille_plot(5, 5, -1, 1, -1, 1).line(-1, 1, 1, -1, 1, 1));
        MSG_DEBUG("x- y+");
        MSG_DEBUG(braille_plot(5, 5, -1, 1, -1, 1).line(1, -1, -1, 1, 1, 1));
        MSG_DEBUG("x- y-");
        MSG_DEBUG(braille_plot(5, 5, -1, 1, -1, 1).line(1, 1, -1, -1, 1, 1));
    }

    if (0)
    {
        constexpr size_t N = 100;
        std::vector<double> x(N + 1, 0.);
        std::vector<double> y(N + 1, 0.);
        double a = 0;
        for (int i = 0; i < x.size(); ++i, a += 1) {
            x[i] = cos(a * 6 * M_PI / N);
            y[i] = sin(a * 4 * M_PI / N);
        }
        /*for (size_t i = 0; i < N; ++i) {*/
            /*MSG_DEBUG(x[i] << " " << y[i]);*/
        /*}*/
        MSG_DEBUG(braille_plot(80, 40, -1.05, 1.05, -1.05, 1.05)
                  .plot(x, y, 255, 128, 128)
                  .set_background(0, 0, 128)
                  );
    }

    if (1)
    {
        constexpr size_t N = 200;
        std::vector<double> x(N + 1, 0.);
        std::vector<double> y(N + 1, 0.);
        braille_plot plot(160, 80, -251, 150, -201, 201);
        plot.set_background(0, 0, 24);
        double da = 2 * M_PI / N;
        double inner_radius = 75;
        for (double ia = -M_PI; ia < M_PI; ia += M_PI / 5000) {
            double cx = inner_radius * cos(ia);
            double cy = inner_radius * sin(ia);
            double r = sqrt((inner_radius - cx) * (inner_radius - cx) + cy * cy);
            double a = 0;
            for (int i = 0; i < x.size(); ++i, a += da) {
                x[i] = r * cos(a) + cx;
                y[i] = r * sin(a) + cy;
            }
            int col = (int) (1.7 * 1.7 / 255. * r * r);
            plot.plot(x, y, col, col, col);
            /*plot.plot(x, y, 0, 255, 0);*/
        }
        /*for (size_t i = 0; i < N; ++i) {*/
            /*MSG_DEBUG(x[i] << " " << y[i]);*/
        /*}*/
        MSG_DEBUG(plot);
    }

    if (0)
    {
        char buf[] = "\xE2\xA0\x00";
        MSG_DEBUG(buf);
        for (unsigned char i = 0; i < 0xFF; ++i) {
            /*std::cout.write((char*)&i, sizeof(char16_t));*/
            buf[sizeof buf - 1] = i;
            std::cout << buf;
        }
    }

    if (0)
    {
        std::vector<std::string> banner = {
/*"  #####                                     #####  ####### #       ",*/
/*" #     # #####  ###### #      #            #     #    #    #       ",*/
/*" #       #    # #      #      #            #     #    #    #       ",*/
/*"  #####  #    # #####  #      #      ##### #     #    #    #       ",*/
/*"       # #####  #      #      #            #   # #    #    #       ",*/
/*" #     # #      #      #      #            #    #     #    #       ",*/
/*"  #####  #      ###### ###### ######        #### #    #    ####### "*/

/*" .d8888b.                    888 888         .d88888b.  88888888888 888      ",*/
/*"d88P  Y88b                   888 888        d88P\" \"Y88b     888     888      ",*/
/*"Y88b.                        888 888        888     888     888     888      ",*/
/*" \"Y888b.   88888b.   .d88b.  888 888        888     888     888     888      ",*/
/*"    \"Y88b. 888 \"88b d8P  Y8b 888 888        888     888     888     888      ",*/
/*"      \"888 888  888 88888888 888 888 888888 888 Y8b 888     888     888      ",*/
/*"Y88b  d88P 888 d88P Y8b.     888 888        Y88b.Y8b88P     888     888      ",*/
/*" \"Y8888P\"  88888P\"   \"Y8888  888 888         \"Y888888\"      888     88888888 ",*/
/*"           888                                     Y8b                       ",*/
/*"           888                                                               ",*/
/*"           888                                                               "*/

"                                **    **                                              ",
" ********                     ****  ****          ********   ************ ******      ",
"***    **                       **    **        ****    **** **   **   **   **        ",
"*****     **********  ******    **    **        ***      ***      **        **        ",
"  *******   **   *** ***   **   **    **        **        **      **        **        ",
"*     ***   **    ** ********   **    **  ***** ***      ***      **        **      * ",
"**     **   **   *** ***    *   **    **        ****    ****      **        **     ** ",
"********    *******   ******* ************        ********      ******    *********** ",
"            **                                        ***                             ",
"          ******                                       ******                         "
        };
        braille_grid grid(banner[0].size(), banner.size());
        for (size_t y = 0; y < banner.size(); ++y) {
            for (size_t x = 0; x < banner[y].size(); ++x) {
                if (banner[y][x] != ' ') {
                    grid.put_pixel(x, y);
                }
            }
        }
        MSG_DEBUG(grid);
    }

    if (0)
    {
        MSG_DEBUG(braille_grid(200, 200)
                  .box(0, 0, 199, 199, 1, 1, 0, 255, 0)
                  .ellipse(100, 100, 80, 50, 5, 2, 0, 0, 255)
                  .line(100, 100, 20, 50, 2, 1, 255, 0, 0)
                  .line(100, 100, 180, 50, 1, 0, 255, 0, 0)
                  .set_background(140, 140, 140)
                  );
    }

    if (0)
    {
        MSG_DEBUG(braille_grid(200, 200)
                  .box(0, 0, 199, 199, 1, 1)
                  .add_label(8, 8, "toto")
                  .box(6, 6, 18, 12, 1, 0)
                  .add_label(108, 8, "titi")
                  .ellipse(111, 9, 7, 5, 1, 0)
                  .add_label(108, 108, "titi pouet coin plop")
                  .ellipse(108 + 20, 109, 23, 5, 1, 0)
                  );
    }

    if (0)
    {
        std::stringstream grid;
        grid << std::dec;
        for (int h = 0; h < 16; ++h) {
            for (int l = 0; l < 16; ++l) {
                int i = (h << 4) | l;
                grid << "\x1b[38;5;" << i << 'm' << std::setw(8) << i;
            }
            grid << std::endl;
        }
        grid << std::endl;
        MSG_DEBUG(grid.str());
    }

    if (0)
    {
        std::stringstream grid;
        grid << std::dec;
        for (int r = 0; r < 256; r += 16) {
            for (int g = 0; g < 256; g += 16) {
                for (int b = 0; b < 256; b += 16) {
                    /*grid << "\x1b[38;2;" << r << ';' << g << ';' << b << 'm' << std::setw(12) << dynamic_cast<std::stringstream&>(std::stringstream() << r << ':' << g << ':' << b).str();*/
                    grid << palette::color256(r, g, b) << std::setw(12) << dynamic_cast<std::stringstream&>(std::stringstream() << r << ':' << g << ':' << b).str();
                }
                grid << std::endl;
            }
        }
        grid << std::endl;
        MSG_DEBUG(grid.str());
    }

    return 0;
    (void) argc; (void) argv;
}

