/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "markov_population.h"
#include "input/read_map.h"
#include "input/read_mark.h"
#include "input/invoke_probapop.h"
#include "probapop_dtd.h"
#include <exception>

/*#include <unsupported/Eigen/MatrixFunctions>*/
/*#include <unsupported/Eigen/KroneckerProduct>*/
#include "eigen.h"

#include "generation_rs.h"
#include "outbred.h"
#include "banner.h"
#include "chrono.h"
#include "data/genoprob_computer.h"


Eigen::IOFormat genomatrix_fmt(/*precision*/StreamPrecision, /*flags*/DontAlignCols,
                               /*coef_sep*/" ", /*row_sep*/"\n",
                               /*row_pfx*/"", /*row_sfx*/"",
                               /*mat_pfx*/"", /*mat_sfx*/"");


void convert_ril_distances(std::vector<double>& loci, std::vector<double>& steps)
{
    std::vector<double> new_steps, new_loci;
    double base = 0;
    double last = 0;
    for (auto l: loci) {
        base = base + dist_RIL_to_additive(l - last);
        last = l;
        new_loci.push_back(base);
    }
    size_t l = 0;
    size_t s;
    for (s = 0; s < (steps.size() - 1); ++s) {
        if (steps[s] > loci[l + 1]) {
            ++l;
        }
        new_steps.push_back(new_loci[l] + dist_RIL_to_additive(steps[s] - loci[l]));
    }
    new_steps.push_back(new_loci.back());
    loci.swap(new_loci);
    steps.swap(new_steps);
}


template <typename V1, typename V2>
double delta(const V1& v1, const V2& v2)
{
    /*if (v1.innerSize() == 2) {*/
        /*Eigen::VectorXd tmp(4);*/
        /*tmp(0) = v1(0);*/
        /*tmp(3) = v1(1);*/
        /*tmp(1) = tmp(2) = 0;*/
        /*return acos(tmp.dot(v2) / (tmp.norm() * v2.norm()));*/
    /*}*/
    return acos(v1.dot(v2) / (v1.norm() * v2.norm()));
}


template <typename OutputType>
OutputType to(char* x)
{
    std::stringstream s(x);
    OutputType ret;
    s >> ret;
    return ret;
}

std::pair<chromosome, marker_data> from_argv(int argc, char** argv, double& step)
{
    chromosome chr;
    int i = 1;
    step = to<double>(argv[i++]);
    chr.name = "chr1";
    marker_data md;
    md.data_type =argv[i++];
    md.n_obs = 1;
    chr.marker_name.push_back(argv[i++]);
    chr.marker_locus.push_back(0);
    md.data[chr.marker_name.back()] = argv[i++];
    /*if (md.data_type == "ril") {*/
        /*step = dist_RIL_to_additive(step);*/
    /*}*/
    for (; i < argc;) {
        double d = to<double>(argv[i++]);
        chr.marker_name.push_back(argv[i++]);
        md.data[chr.marker_name.back()] = argv[i++];
        /*if (md.data_type == "ril") {*/
            /*d = dist_RIL_to_additive(d);*/
        /*}*/
        chr.marker_locus.push_back(chr.marker_locus.back() + d);
    }
    /*for (auto& l: chr.marker_locus) {*/
        /*l = dist_RIL_to_additive(l);*/
    /*}*/
    md.n_mark = md.data.size();
    return {chr, md};
}


std::pair<chromosome, marker_data> from_argv_outbred(int argc, char** argv, double& step)
{
    int i = 1;
    step = to<double>(argv[i++]);
    ++i;
    ifile chrif(argv[i++]);
    chromosome chr = read_data::read_map(chrif).front();
    ifile markif(argv[i++]);
    marker_data md = read_data::read_outbred(markif);
    chr.name = argv[i++]; /* ATTENTION C'EST LE CFG ! */
    return {chr, md};
}

#if 1
#endif




generation_rs* select_pop(const std::string& pop)
{
    generation_rs* A = generation_rs::ancestor("A", 'a');
    generation_rs* B = generation_rs::ancestor("B", 'b');
    std::cout << "Pop type: " << pop << std::endl;
    if (pop == "cp") {
        generation_rs* C = generation_rs::ancestor("C", 'c');
        generation_rs* D = generation_rs::ancestor("D", 'd');
        return A->crossing("AB", B)->crossing("CP", C->crossing("CD", D));
    }
    generation_rs* F1 = A->crossing("F1", B);
    if (pop == "bc") {
        return F1->crossing("BC", A);
    }
    if (pop == "bc2") {
        return F1->crossing("BC", A)->crossing("BC2", A)->selfing("BC2s1");
    }
    if (pop == "ril") {
        return F1->to_doubled_haploid("DH")->to_ril("RIL");
    }
    if (pop[0] == 'f') {
        std::stringstream s(pop);
        char _;
        int i;
        s >> _ >> i;
        generation_rs* ret = F1;
        for (int n = 2; n <= i; ++n) {
            std::stringstream popname;
            popname << "F" << n;
            ret = ret->selfing(popname.str());
        }
        return ret;
    }
    throw "unknown pop";
}


bool compare(const std::string& aname, const std::vector<double>& a,
             const std::string& bname, const std::vector<double>& b)
{
    bool ok = true;
    std::cout << "###############################################" << std::endl
              << "Comparing " << aname << " and " << bname << std::endl;
    if (a.size() < b.size()) {
        std::cout << aname << " is shorter (" << a.size() << ") than " << bname << " (" << b.size() << ')' << std::endl;
        ok = false;
    } else if (a.size() > b.size()) {
        std::cout << bname << " is shorter (" << b.size() << ") than " << aname << " (" << a.size() << ')' << std::endl;
        ok = false;
    }
    auto ai = a.begin();
    auto aj = a.end();
    auto bi = b.begin();
    auto bj = b.end();
    bool nl = false;
    while (ai != aj && bi != bj) {
        if (abs(*ai - *bi) < LOCUS_EPSILON) {
            std::cout << (*ai) << ' ';
            ++ai;
            ++bi;
            nl = true;
        } else if (*ai < *bi) {
            if (nl) {
                nl = false;
                std::cout << std::endl;
            }
            std::cout << "only in " << aname << ": " << (*ai) << " at #" << (ai - a.begin())
                      << " vs " << (*bi) << " in " << bname << " at #" << (bi - b.begin())
                      << std::endl;
            ++ai;
            ok = false;
        } else if (*ai > *bi) {
            if (nl) {
                nl = false;
                std::cout << std::endl;
            }
            std::cout << "only in " << bname << ": " << (*bi) << " at #" << (bi - b.begin())
                      << " vs " << (*ai) << " in " << aname << " at #" << (ai - a.begin())
                      << std::endl;
            ++bi;
            ok = false;
        }
    }
    if (nl) {
        nl = false;
        std::cout << std::endl;
    }
    if (ai != aj) {
        std::cout << (aj - ai) << " elements left in " << aname << std::endl;
    } else if (bi != bj) {
        std::cout << (bj - bi) << " elements left in " << bname << std::endl;
    }
    return ok;
}


int test_segment(int argc, char** argv) {
    std::vector<std::pair<char, std::vector<allele_pair>>> obs_spec = {
        {'A', {{'a', 'a'}}},
        {'B', {{'b', 'b'}}},
        {'H', {{'a', 'b'}, {'b', 'a'}}},
        {'-', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}}},
        {'C', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}}},
        {'D', {{'b', 'b'}, {'a', 'b'}, {'b', 'a'}}}
    };

    double step;
    chrono::start("read args");
    std::pair<chromosome, marker_data> data = from_argv(argc, argv, step);
    chrono::stop("read args");
    chromosome& chr1 = data.first;
    /*for (size_t i = 0; i < chr1.marker_locus.size(); ++i) {*/
        /*std::cout << "M " << chr1.marker_locus[i] << ' ' << chr1.marker_name[i] << std::endl;*/
    /*}*/
    marker_data& md = data.second;
    chrono::start("new[select_pop]");
    generation_rs& pop = *select_pop(md.data_type);
    chrono::stop("new[select_pop]");
    /*chrono::start("affichage");*/
    /*std::cout << pop << std::endl;*/
    /*chrono::stop("affichage");*/

    chrono::start("new[get_obs]");
    std::vector<char> obs = md.get_obs(chr1.marker_name.begin(), chr1.marker_name.end(), 0);
    chrono::stop("new[get_obs]");
    /*std::cout << "================================================================" << std::endl;*/
    /*std::cout << output << std::endl;*/
    /*std::cout << "################################################################" << std::endl;*/
    
    if (md.data_type != "bc2") {
#if 1
        chrono::start("test_probapop");
        labelled_matrix<MatrixXd, double, double> pp = test_probapop(chr1, md, step);
        chrono::stop("test_probapop");
#else
        ifile ifs("pimpam.xml");
        probapop_output* po = read_probapop(ifs);
        labelled_matrix<MatrixXd, double, double> pp = *po;
        delete po;
#endif
        /*std::cout << "================================================================" << std::endl;*/
        /*std::cout << pp.transpose() << std::endl;*/
        /*std::cout << "################################################################" << std::endl;*/
        chrono::start("new[compute_over_segment]");
        /*auto output = pop.compute_over_segment(chr1.marker_locus, obs, obs_spec, step, 0).transpose();*/
        generation_rs::segment_computer_t sc = pop.segment_computer(chr1.marker_locus, step, 0);
        auto output = sc.compute(obs, obs_spec).transpose();
        /*output = sc.compute(obs, obs_spec).transpose();*/
        chrono::stop("new[compute_over_segment]");

        std::cout << output << std::endl;

        size_t locus_idx = 0;

        if (!compare("output", output.row_labels, "probapop", pp.column_labels)) {
            std::cout << "PROBLEM: locus differ between probapop (" << pp.outerSize() << ") and us (" << output.innerSize() << ')' << std::endl;
        }

        for (size_t i = 0; i < pp.outerSize(); ++i) {
            std::cout << "at " << output.row_labels[i];
            while (locus_idx < chr1.marker_locus.size() && chr1.marker_locus[locus_idx] <= output.row_labels[i]) {
                std::cout << " marker " << chr1.marker_name[locus_idx] << ' ' << obs[locus_idx];
                ++locus_idx;
            }
            double err = delta(output.data.row(i), pp.data.col(i));
            if (isnan(err) || err < 1.e-5) {
                std::cout << "  OK" << std::endl;
            } else {
                std::cout << " error=" << err << std::endl;
                std::cout << "    new " << output.data.row(i) << std::endl << "    pp  " << pp.data.col(i).transpose() << std::endl;
            }
        }
        for (; locus_idx < chr1.marker_locus.size(); ++locus_idx) {
            std::cout << "supp " << chr1.marker_locus[locus_idx] << ' ' << chr1.marker_name[locus_idx] << std::endl;
        }
    } else {
        auto output = pop.compute_over_segment(chr1.marker_locus, obs, obs_spec, step, 0).transpose();
        std::cout << output << std::endl;
    }

    return 0;
}



int test_segment_outbred(int argc, char** argv) {
    allele_pair a0 = {'a', 'c'};
    allele_pair a1 = {'a', 'd'};
    allele_pair a2 = {'b', 'c'};
    allele_pair a3 = {'b', 'd'};
    std::vector<std::pair<char, std::vector<allele_pair>>> obs_spec = {
        {'0', {}},
        {'1', {a0}},
        {'2', {a1}},
        {'3', {a0, a1}},
        {'4', {a2}},
        {'5', {a2, a0}},
        {'6', {a2, a1}},
        {'7', {a2, a1, a0}},
        {'8', {a3}},
        {'9', {a3, a0}},
        {'a', {a3, a1}},
        {'b', {a3, a1, a0}},
        {'c', {a3, a2}},
        {'d', {a3, a2, a0}},
        {'e', {a3, a2, a1}},
        {'f', {a3, a2, a1, a0}}
    };

    double step;
    std::pair<chromosome, marker_data> data = from_argv_outbred(argc, argv, step);
    chromosome& chr1 = data.first;
    marker_data& md = data.second;
    generation_rs& pop = *select_pop(md.data_type);
    std::cout << pop << std::endl;

    std::vector<char> obs = md.get_obs(chr1.marker_name.begin(), chr1.marker_name.end(), 0);
    /*std::cout << "================================================================" << std::endl;*/
    /*std::cout << output << std::endl;*/
    /*std::cout << "################################################################" << std::endl;*/
    
    MatrixXd pp = test_probapop(chr1, md, step);
    /*std::cout << "================================================================" << std::endl;*/
    /*std::cout << pp.transpose() << std::endl;*/
    /*std::cout << "################################################################" << std::endl;*/

    auto output = pop.compute_over_segment(chr1.marker_locus, obs, obs_spec, step, 0).transpose();

    for (int i = 0; i < pp.outerSize(); ++i) {
        double d = delta(output.data.row(i), pp.col(i));
        if (d >= 1.e-4) {
            std::cout << "at " << output.row_labels[i] << std::endl;
            std::cout << "new " << output.data.row(i) << std::endl << "pp  " << pp.col(i).transpose() << std::endl;
            std::cout << " error=" << d << std::endl;
        }
    }

    return 0;
}






#if 0
void test_mat(const PolynomMatrix& M)
{
    /*std::cout << "M" << std::endl << M << std::endl;*/
    /*std::cout << "M²" << std::endl << (M * M) << std::endl;*/
    for (double d1 = 0; d1 < 100; d1 += 10) {
        for (double d2 = 0; d2 < 100; d2 += 10) {
            double p1 = prob_recomb(d1);
            double p2 = prob_recomb(d2);
            double p = prob_recomb(d1 + d2);
            /*double p1 = reverse_prob(d1);*/
            /*double p2 = reverse_prob(d2);*/
            /*double p = reverse_prob(d1 + d2);*/
            MatrixXd M12 = eval_poly(M, p1) * eval_poly(M, p2);
            MatrixXd diff = eval_poly(M, p) - M12;
            if (diff.lpNorm<Eigen::Infinity>() > 1.e-15) {
                std::cout << "d1=" << d1 << " d2=" << d2 << "\tBAD" << std::endl;
                /*std::cout << "===============================================================" << std::endl;*/
                /*std::cout << "M(" << d1 << ")" << std::endl << eval_poly(M, p1) << std::endl;*/
                /*std::cout << "M(" << d2 << ")" << std::endl << eval_poly(M, p2) << std::endl;*/
                /*std::cout << "M(" << d1 << ") * M(" << d2 << ')' << std::endl << M12 << std::endl;*/
                /*std::cout << "M(" << d1 << " + " << d2 << ')' << std::endl << eval_poly(M, p) << std::endl;*/
                /*std::cout << "M(" << d1 << ") * M(" << d2 << ") - M(" << d1 << " + " << d2 << ')' << std::endl << diff << std::endl;*/
            } else {
                std::cout << "d1=" << d1 << " d2=" << d2 << "\tGOOD" << std::endl;
            }
        }
    }
}




Generation4 convert4(const GenoMatrix& gm)
{
    std::vector<allele_pair> labels;
    for (int i = 0; i < gm.innerSize(); ++i) {
        labels.push_back(gm(i, 0).genotype.first);
    }
    for (int i = 0; i < gm.outerSize(); ++i) {
        if (!(labels[i] == gm(0, i).genotype.second)) {
            std::cerr << "oops label mismatch @" << i << ": " << labels[i] << ' ' << gm(0, i).genotype.second << std::endl;
        }
    }
    Generation4 ret(labels);
    polynom r = {0, 1};
    polynom s = {1, -1};
    for (int i = 0; i < gm.innerSize(); ++i) {
        for (int j = 0; j < gm.outerSize(); ++j) {
            const rs_polynom& rs = gm(i, j).poly;
            int r_exp = rs.r_exp;
            int s_exp = rs.s_exp;
            ret.data(i, j) = (r ^ r_exp) * (s ^ s_exp) * rs.R;
        }
    }
    return ret;
}
#endif


std::map<allele_pair, std::vector<int>> locus_occurrences(const std::vector<allele_pair> labels)
{
    std::map<allele_pair, std::vector<int>> ret;
    int i = 0;
    for (auto l: labels) {
        ret[l].push_back(i++);
    }
    return ret;
}




void test_lump(const GenerationRS& bigOne)
{
    std::cout << "Generating kroneckered matrix" << std::endl;
    /*Generation4 G = convert4(bigOne);*/
    GenerationRS G = convert(bigOne);
    std::cout << "Initializing lumper" << std::endl;
    lumper<GenerationRS, exact_compare<algebraic_genotype>> l(G);
    /*std::vector<bitset> P = l.partition_on_labels();*/
    /*for (auto& C: P) {*/
        /*for (auto B: P) {*/
            /*std::cout << C << " -> " << B << std::endl;*/
            /*for (auto& k: l.compute_keys(C, B)) {*/
                /*std::cout << k.first << "    " << k.second << std::endl;*/
            /*}*/
        /*}*/
    /*}*/
    std::cout << "Computing refinement" << std::endl;
    auto P = l.refine_all();
    for (auto S: P) {
        std::cout << S << std::endl;
    }
    std::cout << "Resulting matrix" << std::endl << l.to_matrix(P) << std::endl;
}





#if 0
void test_rs()
{
    rs_polynom
        Un = {0, 0, {1}},
        Ixe = {1, 0, {1}},
        IxUn = {0, 1, {1}},
        Zero = {0, 0, {0}},
        A = {2, 3, {1}},
        B = {4, 2, {1}};

#define DODUMP(_l, _op, _r) \
    std::cout << _l << ' ' << #_op << ' ' << _r << " = " << (_l _op _r) << std::endl;

    DODUMP(Un, +, Ixe);
    DODUMP(Un, *, Ixe);
    DODUMP(A, +, B);
    DODUMP(A, *, B);
    DODUMP(IxUn, *, A);
    DODUMP(A, *, IxUn);

    DODUMP(A, <, B);
    DODUMP(B, <, A);
    DODUMP(A, <, A);
    DODUMP(Un, <, Un);
    DODUMP(Zero, <, Zero);
    DODUMP(A.R, <, A.R);
}



bool check_markov(const GenerationRS& G)
{
    bool ok = true;
    for (double d1 = 0; d1 < 100; d1 += 10) {
        for (double d2 = 0; d2 < 100; d2 += 10) {
            double r1 = prob_recomb(d1);
            double r2 = prob_recomb(d2);
            double r = prob_recomb(d1 + d2);
            MatrixXd m1 = eval_poly(G.data, r1);
            MatrixXd m2 = eval_poly(G.data, r2);
            MatrixXd m = eval_poly(G.data, r);
            double n1 = (m1 * m2 - m).norm();
            if (n1 > 1.e-10) {
                std::cout << "d1=" << d1 << " d2=" << d2 << " norm=" << n1 << std::endl;
                std::cout << "---------------------------------------------------" << std::endl;
                std::cout << m1 << std::endl;
                std::cout << "---------------------------------------------------" << std::endl;
                std::cout << m2 << std::endl;
                std::cout << "---------------------------------------------------" << std::endl;
                std::cout << m << std::endl;
                std::cout << "---------------------------------------------------" << std::endl;
                std::cout << (m1 * m2 - m) << std::endl;
                std::cout << "===================================================" << std::endl;
                ok = false;
            }
        }
    }
    return ok;
}




bool check_markov(const generation_rs& G)
{
    bool ok = true;
    std::cout << G << std::endl;
    for (auto& p: G.P) {
        ok &= check_markov(p.G);
    }
    return ok;
}


#endif



#define max__(_a, _b) ((_a) > (_b) ? (_a) : (_b))


void test_comp(const algebraic_genotype& a, const algebraic_genotype& b)
{
    fuzzy_compare<algebraic_genotype> fc;

    std::cout << "A = " << a << std::endl;
    std::cout << "B = " << b << std::endl;

    std::cout << "A  < B ? " << (a < b) << std::endl;
    std::cout << "A  > B ? " << (b < a) << std::endl;

    std::cout << "A f< B ? " << fc(a, b) << std::endl;
    std::cout << "A f> B ? " << fc(b, a) << std::endl;
}


typedef fuzzy_compare<algebraic_genotype> FC;

void test_comp(const FC::key_type& a, const FC::key_type& b)
{
    fuzzy_compare<algebraic_genotype> fc;

    std::cout << "A = " << a << std::endl;
    std::cout << "B = " << b << std::endl;

    std::cout << "A f< B ? " << fc(a, b) << std::endl;
    std::cout << "A f> B ? " << fc(b, a) << std::endl;
}



void test_3V()
{
    std::vector<std::pair<char, std::vector<allele_pair>>> obs_spec = {
        {'1', {{'a', 'a'}}},
        {'2', {{'a', 'b'}}},
        /*{'3', {{'a', 'c'}}},*/
        {'3', {{'a', 'c'}, {'c', 'a'}}},
        {'4', {{'b', 'a'}}},
        {'5', {{'b', 'b'}}},
        /*{'6', {{'b', 'c'}}},*/
        {'6', {{'b', 'c'}, {'c', 'b'}}},
        {'7', {{'c', 'a'}}},
        {'8', {{'c', 'b'}}},
        {'9', {{'c', 'c'}}}
    };

    generation_rs* A = generation_rs::ancestor("A", 'a');
    generation_rs* B = generation_rs::ancestor("B", 'b');
    generation_rs* C = generation_rs::ancestor("C", 'c');
    generation_rs* F1 = A->crossing("AB_3V", B)->crossing("F1_3V", C);
    generation_rs* F2 = F1->selfing("F2_3V");
    std::cout << (*F1) << std::endl << (*F2) << std::endl;

    multi_generation_observations mgo(5);
    chromosome chr1
        /*= {*/
        /*"chr1",*/
        /*{"M1", "M2", "M3", "M4", "M5"},*/
        /*{0, 17, 17+14, 17+14+19, 17+14+19+14}*/
    /*}*/
        ;
    std::vector<char> obs_f1 = {'6', '6', '3', '6', '6'};
    std::vector<char> obs_f2 = {'5', '9', '3', '6', '6'};
    auto lvf1 = F1->observation_vectors(obs_spec);
    auto lvf2 = F2->observation_vectors(obs_spec);
    mgo[F1] = F1->raw_observations(lvf1, obs_f1);
    mgo[F2] = F2->raw_observations(lvf2, obs_f2);
    F2->update_locus_vectors(mgo);
    auto output = F2->compute_over_segment(chr1.marker_locus, mgo[F2], 1., 0).transpose();
    std::cout << output << std::endl;
}



int main(int argc, char** argv)
{
    if (argc > 2 && std::string("cp") == argv[2]) {
        test_segment_outbred(argc, argv);
    } else {
        test_segment(argc, argv);
    }
#if 0
    std::map<char, std::vector<allele_pair>> obs_spec = {{'-', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}}}};

    generation_rs A('a'), B('b');
    generation_rs F1 = A.crossing(B);
    generation_rs BCbizarre = F1.crossing(A).crossing(B);
    std::cout << BCbizarre << std::endl;
    for (int x = 1; x < 5; ++x) {
        for (int y = 1; y < 5; ++y) {
            generation_rs F = F1;
            for (int k = 0; k < x; ++k) { F = F.crossing(A); }
            for (int k = 0; k < y; ++k) { F = F.selfing(); }
            std::cout << "BC" << x << "S" << y << ": " << F.P.front().G.innerSize();
            std::cout << "   (x+1)*2^(y+1)=" << ((x+1)*(2<<y)) << std::endl;
        }
    }
    /*std::cout << F.compute_over_segment({0, 10}, {'-', '-'}, {{'-', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}}}}, 10.) << std::endl;*/
    /*F = F.crossing(F);*/
    /*std::cout << F.compute_over_segment({0, 10}, {'-', '-'}, {{'-', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}}}}, 10.) << std::endl;*/
    /*F = F.crossing(F);*/
    /*std::cout << F.compute_over_segment({0, 10}, {'-', '-'}, {{'-', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}}}}, 10.) << std::endl;*/
    /*F = F.crossing(F);*/
    /*std::cout << F.compute_over_segment({0, 10}, {'-', '-'}, {{'-', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}}}}, 10.) << std::endl;*/
#endif

#if 0
    auto F3 = select_pop("f3");
    auto F4 = select_pop("f4");
    std::cout << "F3 size: " << F3.P.front().G.innerSize() << std::endl;
    std::cout << "F4 size: " << F4.P.front().G.innerSize() << std::endl;
    auto F34 = F3.crossing(F4);
    auto F33 = F3.crossing(F3);
    auto F2 = select_pop("f2");
    auto F22 = F2.crossing(F2);
    auto F222 = F22.crossing(F2);
    std::cout << "F3xF4 size: " << F34.P.front().G.innerSize() << std::endl;
    std::cout << "F3xF3 size: " << F33.P.front().G.innerSize() << std::endl;
    std::cout << "F2xF2 size: " << F22.P.front().G.innerSize() << std::endl;
    std::cout << "F2xF2xF2 size: " << F222.P.front().G.innerSize() << std::endl;
#endif

#if 0
    generation_rs* A = generation_rs::ancestor("A", 'a');
    generation_rs* B = generation_rs::ancestor("B", 'b');

    auto F1 = A->crossing("F1", B);
    auto BC1 = F1->crossing("BC1", A);
    auto BC2 = BC1->crossing("BC2", A);

    std::cout << (*F1) << std::endl;
    std::cout << (*BC1) << std::endl;
    std::cout << (*BC2) << std::endl;
    /*auto f3 = select_pop("f3");*/
    /*std::cout << (*f3) << std::endl;*/
    /*auto p = f3->main_process();*/
    auto p = BC2->main_process();
    std::cout << "colsums" << p.data.colwise().sum() << std::endl;
    std::cout << "rowsums" << p.data.rowwise().sum().transpose() << std::endl;
#endif

#if 0
    auto F2 = select_pop("f2");
    auto F3 = select_pop("f3");

    std::vector<std::pair<char, std::vector<allele_pair>>> obs_spec = {
        {'A', {{'a', 'a'}}},
        {'B', {{'b', 'b'}}},
        {'H', {{'a', 'b'}, {'b', 'a'}}},
        {'-', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}}},
        {'C', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}}},
        {'D', {{'b', 'b'}, {'a', 'b'}, {'b', 'a'}}}
    };

    auto lvf2 = F2->observation_vectors(obs_spec);
    auto lvf3 = F3->observation_vectors(obs_spec);

    multi_generation_observations mgo(3);
    std::vector<char> obs2 = {'A', 'H', 'B'};
    std::vector<char> obs3 = {'A', 'A', 'B'};
    mgo[F2] = F2->raw_observations(lvf2, obs2);
    mgo[F3] = F3->raw_observations(lvf3, obs3);

    std::cout << (*F2) << std::endl << std::endl << std::endl;
    std::cout << (*F3) << std::endl << std::endl << std::endl;

    std::cout << "obs F2" << std::endl << mgo[F2] << std::endl << std::endl;
    std::cout << "obs F3" << std::endl << mgo[F3] << std::endl << std::endl;

    F3->update_locus_vectors(mgo);

    std::cout << "LV F2" << std::endl << mgo[F2] << std::endl << std::endl;
    std::cout << "LV F3" << std::endl <<mgo[F3] << std::endl << std::endl;

    test_3V();
#endif

    std::cout << "Species Loci Perscrutandi Collocabuntur." << std::endl;

    std::cout << BANNER << std::endl;

    return 0;
}

