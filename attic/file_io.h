/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef _SPEL_COMPUTATIONS_FILE_IO_H_
#define _SPEL_COMPUTATIONS_FILE_IO_H_

template <typename CACHE_DIRECTION>
cache_file<CACHE_DIRECTION>& operator & (cache_file<CACHE_DIRECTION>& cf, allele_pair& ap)
{
    return cf & ap.first & ap.second;
}

static inline
cache_input& operator & (cache_input& ci, generation_rs*& g)
{
    std::string s;
    ci & s;
    g = active_settings->design->generation[s];
    return ci;
}

static inline
cache_input& operator & (cache_input& ci, const impl::generation_rs*& g)
{
    std::string s;
    ci & s;
    g = active_settings->design->generation[s];
    return ci;
}

static inline
cache_output& operator & (cache_output& ci, const generation_rs*& g)
{
    return ci & g->name;
}

static inline
cache_output& operator & (cache_output& ci, const impl::generation_rs*& g)
{
    return ci & g->name;
}

static inline
cache_input& operator & (cache_input& ci, const ::chromosome*& chr)
{
    std::string s;
    ci & s;
    chr = _find_chromosome(s);
    return ci;
}

static inline
cache_output& operator & (cache_output& co, const ::chromosome*& chr)
{
    return co & chr->name;
}


static inline
cache_input& operator & (cache_input& ci, const ::population*& pop)
{
    std::string pname;
    ci & pname;
    pop = &active_settings->populations[pname];
    return ci;
}


static inline
cache_output& operator & (cache_output& co, const ::population*& pop)
{
    return co & pop->name;
}

template <typename CACHE_DIRECTION>
cache_file<CACHE_DIRECTION>& operator & (cache_file<CACHE_DIRECTION>& co, impl::generation_rs::segment_computer_t& sc)
{
    return co & sc.g_this & sc.steps & sc.noise;
}

#endif

