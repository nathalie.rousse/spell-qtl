/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_GENEALOGY_H_
#define _SPEL_GENEALOGY_H_

#include "generation_rs.h"

typedef std::pair<std::vector<int>, std::vector<int>> ancestor_index;


struct generation_ancestors {
    generation_rs* g;  /* links to design and generation name */
    std::vector<ancestor_index> ancestors;
};

/*typedef std::map<int, ancestor_index> generation_ancestors;*/

typedef std::map<std::string, generation_ancestors> pedigree;


namespace read_data {
    pedigree read_genealogy(std::istream&);
}

#endif

