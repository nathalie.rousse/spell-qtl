#ifndef _SPEL_GENOPROB_COMPUTER_H_
#define _SPEL_GENOPROB_COMPUTER_H_

#include "markov_population.h"
#include "algebraic_genotype.h"


#ifndef USE_RIL_SERIES
#if 0
template <typename GENERATION>
struct geno_prob_computer {
    const std::vector<double>* locus;
    const MatrixXd* LV;
    GENERATION* this_gen;
    /*std::unordered_map<double, MatrixXd> tr_cache;*/
    eval_poly_optim::cache epo;

    std::vector<MatrixXd> TR;
    MatrixXd init_kernel;
    /*MatrixXd redux;*/
    /*std::map<int, MatrixXd> left, right;*/
    MatrixXd left, right;

    geno_prob_computer()
        : locus(0), LV(0), this_gen(0), epo(), TR(), init_kernel(), left(), right()
    {}

    geno_prob_computer(geno_prob_computer<GENERATION>&& tmp)
        : locus(tmp.locus)
        , LV(tmp.LV)
        , this_gen(tmp.this_gen)
        , epo(std::forward<eval_poly_optim::cache>(tmp.epo))
        , left(std::forward<MatrixXd>(tmp.left))
        , right(std::forward<MatrixXd>(tmp.right))
    {}

    geno_prob_computer<GENERATION>&
        operator = (geno_prob_computer<GENERATION>&& tmp)
        {
            locus = tmp.locus;
            LV = tmp.LV;
            this_gen = tmp.this_gen;
            epo = std::move(tmp.epo);
            TR.swap(tmp.TR);
            left.swap(tmp.left);
            right.swap(tmp.right);
            return *this;
        }

    MatrixXd& compute_tr(double dist, int order)
    {
        return epo.eval(dist, 1);
    }

    MatrixXd normalize_by_col(const MatrixXd& m)
    {
        MatrixXd ret = MatrixXd::Zero(m.innerSize(), m.outerSize());
        VectorXd sums = m.colwise().sum();
        for (int i = 0; i < m.outerSize(); ++i) {
            if (sums(i) != 0) {
                ret.col(i) = m.col(i) / sums(i);
            }
        }
        return ret;
    }

    MatrixXd lv(int i)
    {
        return LV->col(i).asDiagonal();
    }

    void init(GENERATION* gen)
    {
        /*epo.init(break_matrix);*/
        /*tr_cache.clear();*/
        this_gen = gen;
        /*redux = make_redux(matrix_labels_to_cliques(break_matrix.column_labels));*/
        init_order(1);
    }

    void init_order(int order)
    {
        /*this_gen->epo.clear();*/
        epo = this_gen->epo.new_cache();
        TR.clear();
        for (size_t i = 1; i < locus->size(); ++i) {
            TR.emplace_back(compute_tr((*locus)[i] - (*locus)[i - 1], order));
        }

        init_kernel = epo.eval(std::numeric_limits<double>::infinity(), order);
    }

    bool compute_partial_kernels()
    {
        /*std::map<int, std::string> lstring, rstring;*/
        /*left.clear();*/
        /*right.clear();*/
        left = MatrixXd::Zero(LV->innerSize(), LV->outerSize());
        right = MatrixXd::Zero(LV->innerSize(), LV->outerSize() + 1);

        int l = 0, r = LV->outerSize() - 1;

        MatrixXd kernel;

        left.col(0) = kernel = LV->col(0);

        /*lstring[0] = "LV0.";*/
        for(++l; l <= r; ++l) {
            kernel = LV->col(l).asDiagonal() * TR[l - 1] * kernel;
            double s = kernel.sum();
            if (s == 0) {
                return false;
            }
            kernel /= s;
            left.col(l) = kernel;
            /*std::stringstream ss;*/
            /*ss << "LV" << l << ".TR" << (l-1) << '.' << lstring[l-1];*/
            /*lstring[l] = ss.str();*/
        }

        right.col(r+1) = VectorXd::Ones(this_gen->main_process().innerSize()) / this_gen->main_process().innerSize();
        /*rstring[r+1] = "";*/
        right.col(r) = kernel = LV->col(r) / LV->col(r).sum();
        if (r > 0) {
            right.col(r-1) = right.col(r);
        }
        /*{ std::stringstream s; s << ".LV" << r; rstring[r-1] = rstring[r] = s.str(); }*/
        for (--r; r > 0; --r) {
            kernel = LV->col(r).asDiagonal() * TR[r] * kernel;
            double s = kernel.sum();
            if (s == 0) {
                return false;
            }
            kernel /= s;
            right.col(r-1) = kernel;
            /*std::stringstream ss;*/
            /*ss << rstring[r] << ".TR" << r << ".LV" << r;*/
            /*rstring[r-1] = ss.str();*/
        }

        r = LV->outerSize() - 1;
        kernel = init_kernel;
        return true;
    }

    std::pair<size_t, size_t> find_lr(double loc)
    {
        size_t l = 0, r = LV->outerSize() - 1;
        if (loc < locus->front() || loc > locus->back()) {
            throw out_of_segment_exception();
        }

        for (; (l + 1) < r && (*locus)[l + 1] <= loc; ++l);
        for (; (*locus)[r - 1] > loc; --r);

        return {l, r};
    }

    VectorXd fast_compute_at(double loc, size_t l, size_t r, int order)
    {
        MatrixXd& TRL = compute_tr(loc - (*locus)[l], order);
        MatrixXd& TRR = compute_tr((*locus)[r] - loc, order);
        const MatrixXd& redux = this_gen->get_redux();
        auto L = (TRL * left.col(l)).array();
        auto R = (TRR * right.col(l)).array();
        auto vv = redux * (L * R).matrix();
        double s = vv.sum();
        if (s == 0) {
            return vv;
        }
        return vv / s;
    }

    labelled_matrix<MatrixXd, allele_pair, double>
        fast_compute_over_segment(const std::vector<double>& steps, int order)
        {
            init_order(order);
            if (!compute_partial_kernels()) {
                labelled_matrix<MatrixXd, allele_pair, double>
                    ret(this_gen->get_unique_labels(), steps);
                ret.data = MatrixXd::Zero(ret.row_labels.size(), ret.column_labels.size());
                return ret;
            }

#if 0
            std::vector<typename GENERATION::col_label_type> labels;
            size_t i = 0;
            size_t j = 0;
            while (i < redux.innerSize()) {
                while (j < breakmat.column_labels.size() && redux(i, j) != 1) ++j;
                labels.push_back(breakmat.column_labels[j]);
                while (j < breakmat.column_labels.size() && redux(i, j) == 1) ++j;
                ++i;
            }
            labelled_matrix<MatrixXd, typename GENERATION::col_label_type, double>
                ret(labels, steps);
#endif
            labelled_matrix<MatrixXd, allele_pair, double>
                ret(this_gen->get_unique_labels(), steps);
            size_t l = 0, last = steps.size() - 1, r = !!(LV->outerSize() - 1);
            for (size_t i = 0; i < steps.size(); ++i) {
                ret.data.col(i) = fast_compute_at(steps[i], l, r, order);
                if (steps[i] >= (*locus)[r]) {
                    ++l;
                    r += i < last;
                }
            }
            return ret;
        }
};
#endif
#else

template <typename GENERATION>
struct geno_prob_computer {
    const std::vector<double>* locus;
    const MatrixXd* LV;
    GENERATION* this_gen;
    /*std::unordered_map<double, MatrixXd> tr_cache;*/
    eval_poly_optim::cache epo;

    std::vector<MatrixXd> TR;
    MatrixXd init_kernel;
    /*MatrixXd redux;*/
    std::map<int, MatrixXd> left, right;

    geno_prob_computer()
        : locus(0), LV(0), this_gen(0), epo(), TR(), init_kernel(), left(), right()
    {}

    geno_prob_computer(geno_prob_computer<GENERATION>&& tmp)
        : locus(tmp.locus)
        , LV(tmp.LV)
        , this_gen(tmp.this_gen)
        , epo(std::forward<eval_poly_optim::cache>(tmp.epo))
        , left(std::forward<std::map<int, MatrixXd>>(tmp.left))
        , right(std::forward<std::map<int, MatrixXd>>(tmp.right))
    {}

    geno_prob_computer<GENERATION>&
        operator = (geno_prob_computer<GENERATION>&& tmp)
        {
            locus = tmp.locus;
            LV = tmp.LV;
            this_gen = tmp.this_gen;
            epo = std::move(tmp.epo);
            TR.swap(tmp.TR);
            left.swap(tmp.left);
            right.swap(tmp.right);
            return *this;
        }

    MatrixXd& compute_tr(double dist, int)
    {
        auto& ret = epo.eval(dist, 1);
        MSG_DEBUG("##DUMP TR(" << dist << ")");
        MSG_DEBUG(ret);
        return ret;
    }

    MatrixXd normalize_by_col(const MatrixXd& m)
    {
        MatrixXd ret = MatrixXd::Zero(m.innerSize(), m.outerSize());
        VectorXd sums = m.colwise().sum();
        for (int i = 0; i < m.outerSize(); ++i) {
            if (sums(i) != 0) {
                ret.col(i) = m.col(i) / sums(i);
            }
        }
        return ret;
    }

    MatrixXd lv(int i)
    {
        return LV->col(i).asDiagonal();
    }

    void init(GENERATION* gen)
    {
        /*epo.init(break_matrix);*/
        /*tr_cache.clear();*/
        this_gen = gen;
        /*redux = make_redux(matrix_labels_to_cliques(break_matrix.column_labels));*/
        /*init_order(0);*/
    }

    void init_order(int order)
    {
        /*this_gen->epo.clear();*/
        /*MSG_DEBUG("init_order(" << this_gen->name << ", " << order << ", " << this_gen->epo.size() << ')');*/
        epo = this_gen->epo[order].new_cache();
        TR.clear();
        for (size_t i = 1; i < locus->size(); ++i) {
            TR.emplace_back(compute_tr((*locus)[i] - (*locus)[i - 1], 1));
        }

        init_kernel = epo.eval(std::numeric_limits<double>::infinity(), 1);
    }

    bool compute_partial_kernels()
    {
        std::map<int, std::string> lstring, rstring;
        left.clear();
        right.clear();

        int l = 0, r = LV->outerSize() - 1;

        MatrixXd kernel;

        left[0] = kernel = LV->col(0);

        lstring[0] = "LV0.";
        /*MSG_DEBUG("0 " << lstring[0]);*/
        /*MSG_DEBUG(left[0]);*/
        for(++l; l <= r; ++l) {
            kernel = LV->col(l).asDiagonal() * TR[l - 1] * kernel;
            double s = kernel.sum();
            if (s == 0) {
                return false;
            }
            kernel /= s;
            left[l] = kernel;
            std::stringstream ss;
            ss << "LV" << l << ".TR" << (l-1) << '.' << lstring[l-1];
            lstring[l] = ss.str();
            /*MSG_DEBUG(l << " " << lstring[l]);*/
            /*MSG_DEBUG(left[l]);*/
        }

        right[r+1] = VectorXd::Ones(this_gen->main_process().innerSize()) / this_gen->main_process().innerSize();
        rstring[r+1] = "";
        right[r-1] = right[r] = kernel = LV->col(r) / LV->col(r).sum();
        { std::stringstream s; s << ".LV" << r; rstring[r-1] = rstring[r] = s.str(); }
        for (--r; r > 0; --r) {
            kernel = LV->col(r).asDiagonal() * TR[r] * kernel;
            double s = kernel.sum();
            if (s == 0) {
                return false;
            }
            kernel /= s;
            right[r-1] = kernel;
            std::stringstream ss;
            ss << rstring[r] << ".TR" << r << ".LV" << r;
            rstring[r-1] = ss.str();
        }

        r = LV->outerSize() - 1;
        kernel = init_kernel;
        return true;
    }

    std::pair<size_t, size_t> find_lr(double loc)
    {
        size_t l = 0, r = LV->outerSize() - 1;
        if (loc < locus->front() || loc > locus->back()) {
            throw out_of_segment_exception();
        }

        for (; (l + 1) < r && (*locus)[l + 1] <= loc; ++l);
        for (; r > 0 && (*locus)[r - 1] > loc; --r);

        return {l, r};
    }

    VectorXd fast_compute_at(double loc, size_t l, size_t r, int order)
    {
#if 0
        MSG_DEBUG("l=" << l);
        MSG_DEBUG("r=" << r);
        MSG_DEBUG("locus_l=" << (*locus)[l]);
        MSG_DEBUG("locus_r=" << (*locus)[r]);
        MSG_DEBUG("locus=" << loc);
#endif
        MatrixXd& TRL = compute_tr(loc - (*locus)[l], order);
        MatrixXd& TRR = compute_tr((*locus)[r] - loc, order);
#if 0
        MSG_DEBUG(MATRIX_SIZE(TRL));
        MSG_DEBUG(TRL);
        MSG_DEBUG(MATRIX_SIZE(TRR));
        MSG_DEBUG(TRR);
        MSG_DEBUG(MATRIX_SIZE(left[l]));
        MSG_DEBUG(left[l]);
        MSG_DEBUG(MATRIX_SIZE(right[l]));
        MSG_DEBUG(right[l]);
#endif
        auto L = (TRL * left[l]).array();
        auto R = (TRR * right[l]).array();
#if 0
        /*std::cout << loc << std::endl << (L * R).matrix().transpose() << std::endl;*/
        MSG_DEBUG(MATRIX_SIZE(L));
        MSG_DEBUG(L);
        MSG_DEBUG(MATRIX_SIZE(R));
        MSG_DEBUG(R);
        MSG_DEBUG(MATRIX_SIZE(this_gen->get_redux()));
        MSG_DEBUG(this_gen->get_redux());
#endif
        VectorXd vv = this_gen->get_redux() * (L * R).matrix();
        double s = vv.sum();
        if (s == 0) {
            return vv;
        }
        return vv / s;
    }

    labelled_matrix<MatrixXd, allele_pair, double>
        fast_compute_over_segment(const std::vector<double>& steps, int order)
        {
            init_order(order);
            if (!compute_partial_kernels()) {
                labelled_matrix<MatrixXd, allele_pair, double>
                    ret(this_gen->get_unique_labels(), steps);
                ret.data = MatrixXd::Zero(ret.row_labels.size(), ret.column_labels.size());
                return ret;
            }

#if 0
            std::vector<typename GENERATION::col_label_type> labels;
            size_t i = 0;
            size_t j = 0;
            while (i < redux.innerSize()) {
                while (j < breakmat.column_labels.size() && redux(i, j) != 1) ++j;
                labels.push_back(breakmat.column_labels[j]);
                while (j < breakmat.column_labels.size() && redux(i, j) == 1) ++j;
                ++i;
            }
            labelled_matrix<MatrixXd, typename GENERATION::col_label_type, double>
                ret(labels, steps);
#endif
            /*MSG_DEBUG("LOCUS VECTORS" << std::endl << *LV);*/
            labelled_matrix<MatrixXd, allele_pair, double>
                ret(this_gen->get_unique_labels(), steps);
            /*MSG_DEBUG(MATRIX_SIZE(ret));*/
            /*MSG_DEBUG(this_gen->get_unique_labels());*/
            size_t l = 0, last = steps.size() - 1, r = !!(LV->outerSize() - 1);
            for (size_t i = 0; i < steps.size(); ++i) {
                /* FIXME: optimize the CORRECT initialization of l & r */
                std::tie(l, r) = find_lr(steps[i]);
                ret.data.col(i) = fast_compute_at(steps[i], l, r, order);
                if ((ret.data.col(i).array() < 0).any()) {
                    MSG_DEBUG("Bad value at column #" << i << std::endl << ret.data.col(i).transpose());
                    MSG_DEBUG(this_gen->P[order].G);
#if 0
                    MSG_DEBUG("steps: " << steps);
                    MSG_DEBUG("LV:" << std::endl << (*LV));
                    MSG_DEBUG(*this_gen);
                    for (const auto& m: TR) {
                        MSG_DEBUG(m);
                    }
                    for (const auto& kv: left) {
                        MSG_DEBUG("left " << kv.first);
                        MSG_DEBUG(kv.second);
                    }
                    for (const auto& kv: right) {
                        MSG_DEBUG("right " << kv.first);
                        MSG_DEBUG(kv.second);
                    }
#endif
                    /*throw 0;*/
                }
                while (steps[i] >= (*locus)[r]) {
                    ++l;
                    if (i < last) {
                        ++r;
                    } else {
                        break;
                    }
                    /*r += i < last;*/
                }
            }
            /*MSG_DEBUG("STATE PROBABILITIES" << std::endl << ret);*/
            return ret;
        }
};
#endif

#endif

