#ifndef _SPEL_MARKOV_POP_H_
#define _SPEL_MARKOV_POP_H_

/*#include <Eigen/Core>*/
/*#include <Eigen/SVD>*/
/*#include <Eigen/Eigenvalues>*/
#include "eigen.h"
#include <sstream>
#include <fstream>
#include "fmin.h"
#include <set>
#include <unordered_map>
/*#include "polynom.h"*/
#include "fast_polynom.h"
#include "labelled_matrix.h"
#include "proba_config.h"
#include "fmin.h"

/*#define RIL_ORDER 10*/
extern int RIL_ORDER;
/*#define BROMAN_ORDER 5*/
extern int MAGIC8_ORDER;


using namespace Eigen;



struct ancestor_allele {
    char ancestor;
    ancestor_allele() : ancestor(0) {}
    ancestor_allele(int s) : ancestor((char) s) {}
    int operator ~ () const { return ancestor; }
    bool operator < (const ancestor_allele& aa) const { return ancestor < aa.ancestor; }
    bool operator == (const ancestor_allele& aa) const { return ancestor == aa.ancestor; }
    bool operator != (const ancestor_allele& aa) const { return ancestor != aa.ancestor; }
};

inline
std::ostream& operator << (std::ostream& os, const ancestor_allele& aa)
{
    return os << ((char)aa.ancestor);
}


struct allele {
    ancestor_allele symbol;
    bool parental_strand; /* 0 = from mother, 1 = from father */

    bool operator < (const allele& l) const { return symbol < l.symbol || (symbol == l.symbol && parental_strand < l.parental_strand); }
    bool operator == (const allele& l) const { return symbol == l.symbol && parental_strand == l.parental_strand; }
    ancestor_allele operator ~ () const { return symbol; }
};

struct locus;

struct allele_pair {
    ancestor_allele first, second;
    bool operator < (const allele_pair& ap) const { return first < ap.first || (first == ap.first && second < ap.second); }
    bool operator == (const allele_pair& ap) const { return first == ap.first && second == ap.second; }
    locus to_locus(bool m, bool p) const;
    allele_pair operator ~ () const { return *this; }
};

inline std::ostream& operator << (std::ostream& os, const allele_pair& a) { return os << a.first << a.second; }

/*inline std::ostream& operator << (std::ostream& os, const allele& a) { return os << a.symbol << (a.parental_strand ? "\u209A" : "\u2098"); }*/
inline std::ostream& operator << (std::ostream& os, const allele& a) { return os << a.symbol << (a.parental_strand ? "p" : "m"); }


struct haplo_type {
    /* note de SJ : la distance est volontairement abstraite de ces définitions. */
    allele first, second;

    bool operator < (const haplo_type& h) const { return first < h.first || (first == h.first && second < h.second); }
    bool operator == (const haplo_type& h) const { return first == h.first && second == h.second; }
};

inline std::ostream& operator << (std::ostream& os, const haplo_type& h) { return os << h.first << ',' << h.second; }


struct locus {
    allele mater, pater;

    bool operator < (const locus& l) const { return mater < l.mater || (mater == l.mater && pater < l.pater); }
    bool operator == (const locus& l) const { return mater == l.mater && pater == l.pater; }
    allele_pair operator ~ () const { return {~mater, ~pater}; }
    bool strand_is_ambiguous() const { return mater.parental_strand == pater.parental_strand; }
    operator allele_pair () const { return ~*this; }
};

inline std::ostream& operator << (std::ostream& os, const locus& l) { return os << l.mater << '|' << l.pater; }

inline locus allele_pair::to_locus(bool m, bool p) const { return {{first, m}, {second, p}}; }


struct locus_pair {
    allele_pair first, second;
    /*bool operator < (const locus_pair& lp) const { return first < lp.first || (first == lp.first && second < lp.second); }*/
    bool operator < (const locus_pair& lp) const { return *reinterpret_cast<const unsigned int*>(this) < *reinterpret_cast<const unsigned int*>(&lp); }
    bool operator == (const locus_pair& lp) const { return first == lp.first && second == lp.second; }
};


struct geno_type {
    locus first, second;

    bool operator < (const geno_type& g) const { return first < g.first || (first == g.first && second < g.second); }
    bool operator == (const geno_type& g) const { return first == g.first && second == g.second; }
    haplo_type mater() const { return {first.mater, second.mater}; }
    haplo_type pater() const { return {first.pater, second.pater}; }
    locus_pair operator ~ () const { return {~first, ~second}; }
};

inline std::ostream& operator << (std::ostream& os, const locus_pair& lp) { return os << lp.first << ',' << lp.second; }
inline std::ostream& operator << (std::ostream& os, const geno_type& g) { return os << g.first << ',' << g.second; }


/* SEMANTICS
 *
 * | is horizontal.
 * allele | allele => locus
 * haplo_type | haplo_type => geno_type
 *
 * + is vertical.
 * allele + allele => haplo_type
 * locus + locus => geno_type
 *
 * % is horizontal. (reciprocal to | )
 *
 * / is vertical. (reciprocal to + )
 */ 

inline geno_type operator | (const haplo_type& m, const haplo_type& p) { return {{m.first, p.first}, {m.second, p.second}}; }
inline geno_type operator + (const locus& first, const locus& second) { return {first, second}; }
inline locus operator | (const allele& m, const allele& p) { return {m, p}; }
inline haplo_type operator + (const allele& first, const allele& second) { return {first, second}; }
inline allele operator % (const locus& l, bool mp) { return mp ? (allele){l.pater.symbol, mp} : (allele){l.mater.symbol, mp}; }
/*inline haplo_type operator % (const geno_type& g, bool mp) { return mp ? g.pater() : g.mater(); }*/
inline allele operator / (const haplo_type& h, bool fs) { return fs ? h.second : h.first; }
inline locus operator / (const geno_type& g, bool fs) { return fs ? g.second : g.first; }
inline allele_pair operator / (const locus_pair& g, bool fs) { return fs ? g.second : g.first; }
inline ancestor_allele operator % (const allele_pair& ap, bool fs) { return fs ? ap.second : ap.first; }






template <typename ALLELE_PAIR_TYPE>
inline std::vector<int> matrix_labels_to_cliques(const std::vector<ALLELE_PAIR_TYPE>& labels, std::vector<ALLELE_PAIR_TYPE>& uniq)
{
    std::vector<int> cliques;
#if 1
    std::set<ALLELE_PAIR_TYPE> uniq_set(labels.begin(), labels.end());
    /*std::vector<ALLELE_PAIR_TYPE> uniq(uniq_set.begin(), uniq_set.end());*/
    uniq.assign(uniq_set.begin(), uniq_set.end());

    /*MSG_DEBUG("LABELS " << labels);*/
    /*MSG_DEBUG("UNIQ " << uniq);*/

    auto get_index = [&] (const ALLELE_PAIR_TYPE& ap) { return (int) (std::find(uniq.begin(), uniq.end(), ap) - uniq.begin()); };

    cliques.resize(labels.size());
    for (size_t i = 0; i < labels.size(); ++i) {
        cliques[i] = get_index(labels[i]);
    }

#else
    int clique = -1;
    cliques.resize(labels.size(), -1);
    for (size_t i = 0; i < labels.size(); ++i) {
        if (cliques[i] != -1) {
            continue;
        }
        cliques[i] = ++clique;
        for (size_t j = i + 1; j < labels.size(); ++j) {
            if (~labels[i] == ~labels[j]) {
                cliques[j] = cliques[i];
            }
        }
    }
    /*std::cout << "CLIQUES :: " << cliques << std::endl;*/
#endif
    return cliques;
}

inline std::vector<int> matrix_labels_to_cliques_sm(const std::vector<locus>& labels)
{
    std::vector<int> cliques;
    cliques.resize(labels.size(), -1);
    for (size_t i = 0; i < labels.size(); ++i) {
        cliques[i] = ~(labels[i] % 0) != ~(labels[i] % 1);
    }
    return cliques;
}

inline MatrixXd make_redux(std::vector<int> cliques)
{
    int max = 0;
    for (auto x: cliques) { max = max < x ? x : max; }
    MatrixXd reduxd = MatrixXd::Zero(max + 1, cliques.size());
    for (size_t i = 0; i < cliques.size(); ++i) {
        reduxd(cliques[i], i) = 1;
    }
    /*MSG_DEBUG("REDUX");*/
    /*MSG_DEBUG(reduxd);*/
    return reduxd;
}



#if 0
inline PolynomMatrix make_redux_poly(std::vector<int> cliques)
{
    int max = 0;
    for (auto x: cliques) { max = max < x ? x : max; }
    PolynomMatrix redux(max + 1, cliques.size());
    for (size_t i = 0; i < cliques.size(); ++i) {
        redux(cliques[i], i) = {1};
    }
    return redux;
}
#endif





typedef labelled_matrix<MatrixXd, haplo_type, double> GenoProbMatrix;


struct out_of_segment_exception : public std::exception {};





inline double prob_recomb(double dist, int order)
{
    return order == RIL_ORDER
           ? .5 - exp(-.02 * (RIL_ORDER + 1) * dist) * .5
           : .5 - exp(-.02 * order * dist) * .5;
}

inline double process_weight(int order)
{
    return order == RIL_ORDER
           ? pow(.5, RIL_ORDER - 1)
           : pow(.5, order);
}

inline double inverse_prob_recomb(double r)
{
    return -50. * log(1. - 2. * r);
}

inline double prob_no_recomb(double dist)
{
    return .5 + exp(-.02 * dist) * .5;
}


inline double dist_RIL_to_additive(double dist)
{
    double r = prob_recomb(dist, 1);
    return inverse_prob_recomb(2. * r / (1. + 2. * r));
}


inline double reverse_prob(double d)
{
    return exp(-.02 * d);
}



#ifdef TR_LEFT_IS_TRANSPOSED
#  define TR_left(x) (x.transpose())
#else
#  define TR_left(x) (x)
#endif

#ifdef TR_RIGHT_IS_TRANSPOSED
#  define TR_right(x) (x.transpose())
#else
#  define TR_right(x) (x)
#endif


/*extern void test_mat(const PolynomMatrix& M);*/



namespace std {
    template <>
        struct hash<ancestor_allele> {
            size_t operator () (const ancestor_allele& aa) const
            {
                return hash<decltype(ancestor_allele::ancestor)>()(aa.ancestor);
                /*static_assert(sizeof(ancestor_allele) == sizeof(short), "ancestor_allele must be packed into 16 bits");*/
                /*hash<unsigned short> h;*/
                /*return h(*reinterpret_cast<const unsigned short*>(&aa));*/
                /*static_assert(sizeof(ancestor_allele) == sizeof(int), "ancestor_allele must be packed into 32 bits");*/
                /*hash<unsigned int> h;*/
                /*return h(*reinterpret_cast<const unsigned int*>(&aa));*/
                /*hash<decltype(ancestor_allele::ancestor)> ha;*/
                /*hash<decltype(ancestor_allele::tag)> ht;*/
                /*size_t a = ha(aa.ancestor);*/
                /*size_t t = ht(aa.tag);*/
                /*return (a * 0xdeadbeef) ^ t;*/
            }
        };

    template <>
        struct hash<allele_pair> {
            size_t operator () (const allele_pair& ap) const
            {
                static_assert(sizeof(allele_pair) == sizeof(short), "allele_pair must be packed into 16 bits");
                return hash<unsigned short>()(*reinterpret_cast<const unsigned short*>(&ap));
                /*hash<ancestor_allele> h;*/
                /*return h(ap.first) ^ h(ap.second);*/
                /*static_assert(sizeof(allele_pair) == sizeof(int), "allele_pair must be packed into 32 bits");*/
                /*hash<unsigned int> h;*/
                /*return h(*reinterpret_cast<const unsigned int*>(&ap));*/
                /*static_assert(sizeof(allele_pair) == sizeof(long long), "allele_pair must be packed into 64 bits");*/
                /*hash<unsigned long long> h;*/
                /*return h(*reinterpret_cast<const unsigned long*>(&ap));*/
            }
        };

    template <>
        struct hash<locus_pair> {
            size_t operator () (const locus_pair& g) const
            {
                static_assert(sizeof(locus_pair) == sizeof(unsigned int), "locus_pair must be packed into 32 bits");
                /*MSG_DEBUG("LP SIZE " << sizeof(locus_pair)); MSG_QUEUE_FLUSH();*/
                /*MSG_DEBUG("size_t SIZE " << sizeof(size_t)); MSG_QUEUE_FLUSH();*/
                /*MSG_DEBUG("uint SIZE " << sizeof(unsigned int)); MSG_QUEUE_FLUSH();*/
                /*MSG_DEBUG("ulong SIZE " << sizeof(unsigned long)); MSG_QUEUE_FLUSH();*/
                return hash<unsigned int>()(*reinterpret_cast<const unsigned int*>(&g));
            }
        };
}



#endif

