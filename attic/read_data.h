/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file is a trimmed down implementation from CarthaGène, with some
 * obviously specific types removed (order, constraints).
 */

#ifndef _CARTAGENE_READ_DATA_H
#define _CARTAGENE_READ_DATA_H

#include <iostream>
#include "file.h"
#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <map>
#include <cstring>
#include <algorithm>


/* This enum is inherited from CarthaGène */
/*#include "CGtypes.h"*/
enum CrossType { Unk, BC, IC, RISib, RISelf, IRISelf, IRISib, RH, RHD, RHE, BS };


namespace read_data {


    typedef std::pair<std::string, CrossType> follow;
    typedef std::multimap<std::string, follow> follow_set_t;
    typedef std::pair<follow_set_t::iterator, follow_set_t::iterator> follow_set_range_t;
    typedef std::pair<CrossType, std::string> cross_metadata_t;




    typedef std::vector<char> MarkerRow;
    typedef std::vector<MarkerRow> MarkerData;
    typedef std::vector<std::string> MarkerNames;

    struct marker_data {
        /* for all */
        CrossType Cross;
        /* for bs */
        std::string design_string;
        /* for all but constraints and order */
        MarkerData observations;
        MarkerNames marker_names;
        int nm, ni;
        /* for all but constraints and order */
        char tr[256];

        marker_data()
            : Cross(Unk), design_string(), observations(), marker_names(), nm(0), ni(0)
        {}

        marker_data& operator = (const marker_data& md)
        {
            Cross = md.Cross;
            nm = md.nm;
            ni = md.ni;
            design_string = md.design_string;
            observations = md.observations;
            marker_names = md.marker_names;
            std::copy(md.tr, md.tr + 256, tr);
            return *this;
        }

        marker_data(CrossType c, std::string& ds)
            : Cross(c),
            design_string(ds),
            observations(),
            marker_names(),
            nm(0), ni(0)
        {
            memset(tr, 0, 256);
            tr[' '] = ' ';
            tr['-'] = '-';
            if(Cross != IC && Cross != BS) { // CN added the BS condition, 12.29.05
                tr['A'] = '0';
                tr['B'] = 'A';
                tr['H'] = 'A';
            } else {
                tr['A'] = 'A';
                tr['B'] = 'B';
                tr['C'] = 'C';
                tr['D'] = 'D';
                tr['H'] = 'H';
                tr['0'] = '0'; // 0000
                tr['1'] = 'A'; // 0001
                tr['2'] = '2'; // 0010
                tr['3'] = '3'; // 0011
                tr['4'] = '4'; // 0100
                tr['5'] = '5'; // 0101
                tr['6'] = 'H'; // 0110
                tr['7'] = 'D'; // 0111
                tr['8'] = 'B'; // 1000
                tr['9'] = '9'; // 1001
                tr['a'] = 'a'; // 1010
                tr['b'] = 'b'; // 1011
                tr['c'] = 'c'; // 1100
                tr['d'] = 'd'; // 1101
                tr['e'] = 'C'; // 1110
                tr['f'] = '-'; // 1111
            }
        }
    };

    marker_data read_file(const char* filename);
}

#endif
