/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*#include <Eigen/Core>*/
/*#include <iostream>*/
/*#include <vector>*/
/*#include <iomanip>*/
/*#include <unsupported/Eigen/KroneckerProduct>*/
#include "all.h"

using namespace Eigen;

struct bloc {
    std::vector<std::string> m_lines;
    int m_max_width;
    bool m_fill;

    bloc()
        : m_lines(), m_max_width(0), m_fill(false)
    {}

    bloc(const std::string& s)
        : m_lines(), m_max_width(0), m_fill(false)
    {
        init(s);
    }

    bloc(const std::string& s, bool fill)
        : m_lines(), m_max_width(0), m_fill(fill)
    {
        init(s);
    }

    template <typename Z>
        bloc(const Z& z)
            : m_lines(), m_max_width(0), m_fill(false)
        {
            init(z);
        }

    template <typename Z>
        void init(const Z& z)
        {
            std::stringstream ss;
            ss << z;
            init(ss.str());
        }

    void init(const std::string& s)
    {
        auto i = s.begin(), j = s.end();
        while (i != j) { /* for each line */
            auto eol = std::find(i, j, '\n');
            int width = eol - i;
            if (width > m_max_width) {
                m_max_width = width;
            }
            m_lines.emplace_back(i, eol);
            i = eol;
            if (eol != s.end()) {
                ++i;
            }
        }
        /*dump_bloc();*/
    }

    void dump_bloc() const
    {
        std::cout << "new bloc " << m_max_width << 'x' << height() << " fill=" << m_fill << std::endl;
        for (const auto& l: m_lines) {
            std::cout << '|' << l << '|' << std::endl;
        }
    }

#if 0
    bloc(const std::vector<bloc>& bvec, const std::string& sep)
        : m_lines(), m_max_width(0), m_fill(true)
    {
        int max_height = 0;
        for (const bloc& b: bvec) {
            m_max_width += b.width();
            if (b.height() > max_height) {
                max_height = b.height();
            }
            m_fill &= b.m_fill;
        }
        m_lines.reserve(max_height);
        std::vector<std::stringstream> ssvec(max_height);
        bool first = true;
        for (const bloc& b: bvec) {
            size_t i;
            for (i = 0; i < b.height(); ++i) {
                if (!first) {
                    ssvec[i] << sep;
                }
                ssvec[i] << b[i];
            }
            first = false;
            if (i < max_height) {
                if (b.m_fill) {
                    for (; i < max_height; ++i) {
                        ssvec[i] << b[i % b.height()];
                    }
                } else {
                    std::string space(b.width(), ' ');
                    for (; i < max_height; ++i) {
                        ssvec[i] << space;
                    }
                }
            }
        }
        for (auto& ss: ssvec) {
            m_lines.emplace_back(ss.str());
        }
        /*dump_bloc();*/
    }
#endif

    int width() const { return m_max_width; }
    int height() const { return m_lines.size(); }
    typedef std::vector<std::string>::const_iterator iterator;
    iterator begin() const { return m_lines.begin(); }
    iterator end() const { return m_lines.end(); }

    friend
        std::ostream& operator << (std::ostream& os, const bloc& b)
        {
            for (const std::string& l: b) {
                os << l << std::endl;
            }
            return os;
        }
};


struct block_matrix {
    std::vector<std::vector<bloc>> m_matrix;
    std::vector<int> m_widths, m_heights;
    int m_inner, m_outer;

    block_matrix(int inner, int outer)
        : m_matrix(inner), m_widths(outer), m_heights(inner), m_inner(inner), m_outer(outer)
    {
        for (auto& row: m_matrix) {
            row.resize(outer);
        }
    }

    bloc& operator () (int i, int j) { return m_matrix[i][j]; }

    void compute_dimensions()
    {
        for (int i = 0; i < m_inner; ++i) {
            for (int j = 0; j < m_outer; ++j) {
                bloc& b = m_matrix[i][j];
                if (b.width() > m_widths[j]) {
                    m_widths[j] = b.width();
                }
                if (b.height() > m_heights[i]) {
                    m_heights[i] = b.height();
                }
            }
        }
    }

    typedef std::pair<bloc::iterator, bloc::iterator> block_iterator;

    static bool output_line(std::ostream& os, int width, block_iterator& bi)
    {
        os << std::setw(width);
        if (bi.first == bi.second) {
            os << "";
            return false;
        } else {
            os << (*bi.first);
            ++bi.first;
            return true;
        }
    }

    void output_row(std::ostream& os, int r)
    {
        auto& row = m_matrix[r];
        std::vector<block_iterator> line_iter;
        for (auto& b: row) {
            line_iter.emplace_back(b.begin(), b.end());
        }
        bool keep_going;
        for (int lnum = 0; lnum < m_heights[r]; ++lnum) {
            keep_going = output_line(os, m_widths[0], line_iter.front());
            for (int i = 1; i < line_iter.size(); ++i) {
                os << " | ";
                keep_going |= output_line(os, m_widths[i], line_iter[i]);
            }
            os << std::endl;
        }
    }

    friend
        std::ostream& operator << (std::ostream& os, block_matrix& bm)
        {
            bm.compute_dimensions();
            char prev_fill = os.fill();
            /*for (auto& row: bm.m_matrix) {*/
            int row_max = bm.m_heights.size() - 1;
            for (int r = 0; r < row_max; ++r) {
                bm.output_row(os, r);
                os << std::setfill('-') << std::setw(bm.m_widths[0] + 1) << "" << std::setfill(prev_fill);
                int _max = bm.m_widths.size() - 1;
                for (int i = 1; i < _max; ++i) {
                    os << '+' << std::setfill('-') << std::setw(bm.m_widths[i] + 2) << "" << std::setfill(prev_fill);
                }
                os << '+' << std::setfill('-') << std::setw(bm.m_widths[_max] + 1) << "" << std::setfill(prev_fill) << std::endl;
            }
            bm.output_row(os, row_max);
            return os;
        }
};


template <typename X>
bloc
block_output(const X& x)
{
    std::stringstream ss;
    ss << x;
    return bloc(ss.str());
}






namespace Eigen {
    template<typename X>
    struct NumTraits<Matrix<Matrix<X, Dynamic, Dynamic>, Dynamic, Dynamic>> {
        typedef Matrix<X, Dynamic, Dynamic> Real;
        typedef Matrix<X, Dynamic, Dynamic> NonInteger;
        typedef Matrix<X, Dynamic, Dynamic> Nested;
        enum {
            IsComplex = 0,
            IsInteger = 1,
            ReadCost = 100,
            WriteCost = 100,
            MulCost = 1000,
            IsSigned = 0,
            RequireInitialization = 1
        };
        static Real epsilon() { return Real(); }
        static Real dummy_precision() { return Real(); }
        static Real highest() { return Real(); }
        static Real lowest() { return Real(); }
    };
}


typedef Matrix<MatrixXd, Dynamic, Dynamic> MatMat;
typedef Matrix<MatMat, Dynamic, Dynamic> MatMatMat;


#if 0
template <typename X>
std::ostream& operator << (std::ostream& os, const Matrix<Matrix<X, Dynamic, Dynamic>, Dynamic, Dynamic>& m)
{
    std::vector<bloc> brow;
    for (int j = 0; j < m.outerSize(); ++j) {
        std::vector<bloc> bcol;
        size_t width = 0;
        for (int i = 0; i < m.innerSize(); ++i) {
            bcol.emplace_back(m(i, j));
            if (bcol.back().width() > width) {
                width = bcol.back().width();
            }
        }
        brow.push_back(bcol.front());
        std::stringstream rowsep;
        rowsep << std::setfill('-') << std::setw(width) << "";
        bloc browsep(rowsep.str());
        for (int i = 1; i < bcol.size(); ++i) {
            brow.back().append_bottom(browsep);
            brow.back().append_bottom(bcol[i]);
        }
    }
    std::stringstream sep;
    char prev_fill = os.fill('-');
    int n_lines = m.outerSize() - 1;
    int i;
    for (i = 0; i < n_lines; ++i) {
        std::vector<bloc> bvec;
        for (int j = 0; j < m.innerSize(); ++j) {
            bvec.emplace_back(m(i, j));
        }
        bloc b(bvec, " | ");
        os << b;
        os << std::setw(bvec[0].width()) << "";
        for (size_t k = 1; k < bvec.size(); ++k) {
            os << "-+-" << std::setw(bvec[k].width()) << "";
        }
        os << std::endl;
    }
    std::vector<bloc> bvec;
    for (int j = 0; j < m.innerSize(); ++j) {
        bvec.emplace_back(m(i, j));
    }
    bloc b(bvec, " | ");
    os << b;
    os.fill(prev_fill);
    return os;
}
#endif


template <typename X>
std::ostream& operator << (std::ostream& os, const Matrix<Matrix<X, Dynamic, Dynamic>, Dynamic, Dynamic>& m)
{
    block_matrix bm(m.innerSize(), m.outerSize());
    for (int i = 0; i < m.innerSize(); ++i) {
        for (int j = 0; j < m.outerSize(); ++j) {
            bm(i, j).init(m(i, j));
        }
    }
    return os << bm;
}


int main(int argc, char** argv)
{
    MatMat m2(2, 2);
    MatMat m(1, 1);
    MatMatMat n(2, 2);

    m(0, 0) = MatrixXd::Zero(3, 3);
    m(0, 0)(0, 0) = 0;
    m(0, 0)(0, 1) = 1;
    m(0, 0)(0, 2) = 2;
    m(0, 0)(1, 0) = 3;
    m(0, 0)(1, 1) = 4;
    m(0, 0)(1, 2) = 5;
    m(0, 0)(2, 0) = 6;
    m(0, 0)(2, 1) = 7;
    m(0, 0)(2, 2) = 8;

    m2(0, 0) = MatrixXd::Zero(2, 2);
    m2(0, 1) = MatrixXd::Ones(2, 2);
    m2(1, 0) = MatrixXd::Ones(2, 2);
    m2(1, 1) = MatrixXd::Zero(2, 2);

    n(0, 0) = m;
    n(0, 1) = m2;
    n(1, 0) = m2;
    n(1, 1) = kroneckerProduct(m2, m2);

    std::cout << m2 << std::endl;
    std::cout << n << std::endl;
    std::cout << kroneckerProduct(n, n) << std::endl;

    return 0;
}

