/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "input.h"
#include <x2c/x2c.h>

using namespace x2c;

design_type* read_design(std::istream& is);

#ifdef TEST_DESIGN
int main(int argc, char** argv)
{
    for (int i = 1; i < argc; ++i) {
        ifile ifs(argv[i]);
        try {
            design_type* d = read_design(ifs);
            std::cout << *d << std::endl;
            delete d;
        } catch(const char* x) {
            std::cerr << x << std::endl;
        } catch(xml_exception x) {
            std::cerr << x.what() << std::endl;
        }
    }
    return 0;
}
#endif

