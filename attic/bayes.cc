/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>
#include <iomanip>
#include "file.h"
#include "script_parser.h"
#include "malloc.h"
#include "computations.h"
#include "chrono.h"
#include "input/read_mark.h"
#include "pedigree.h"

#include "bayes.h"

#include "bayes/factor_var2.h"

#include "generation_rs.h"

#include <boost/dynamic_bitset.hpp>
#include "generation_kronecker.h"


/* COMMAND-LINE HANDLING */

#include "commandline.h"

/* testing the implementation of bra|ket */
/*#include "bracket.h"*/



size_t novar = (size_t) -1;

/*
namespace direction_data {
#define V1 0, 1, 2, 3
#define V2 4, 5, 6, 7
#define V3 8, 9, 10, 11
    std::vector<std::vector<size_t>>
        normalize = {{V1}, {V2}, {V3}};

#define V11 {0, 0}, {1, 1}, {2, 2}, {3, 3}
#define V21 {4, 0}, {5, 1}, {6, 2}, {7, 3}
#define V22 {4, 4}, {5, 5}, {6, 6}, {7, 7}
#define V32 {8, 4}, {9, 5}, {10, 6}, {11, 7}
    std::vector<std::vector<std::pair<size_t, size_t>>>
        product_mapping_binary = {{V21}, {V11}},
        product_mapping_ternary = {{V21, V32}, {V11, V32}, {V11, V22}};

    std::vector<std::vector<size_t>>
        sum_mapping_binary = {{V2}, {V1}},
        sum_mapping_ternary = {{V2, V3}, {V1, V3}, {V1, V2}};

}
*/



inline std::ostream& operator << (std::ostream& os, const std::map<std::string, generation_rs*>& d)
{
    os << "Design:" << std::endl;
    auto g = d.begin();
    auto gend = d.end();
    for (; g != gend; ++g) {
        os << (*g->second) << std::endl;
    }
    return os;
}




pedigree_bayesian_network
make_bn(const std::vector<pedigree_item>& pedigree, const std::map<std::string, ObservationDomain>& obs_gen, /*const std::string& query_gen,*/ size_t n_alleles, double obs_noise, double tolerance)
{
    size_t n_par = 0;
    for (const auto& pi: pedigree) {
        if (pi.is_ancestor()) {
            ++n_par;
        }
        /*if (pi.gen_name == query_gen) {*/
            /*++n_q;*/
        /*}*/
    }

    pedigree_bayesian_network ret(n_par, n_alleles, obs_noise, tolerance);
    ret.pedigree = pedigree;

    /*ret.families = pedigree_families(pedigree, ret.generations, ret.gen_by_id);*/
    ped_design_type design;
    genealogy_type genealogy;
    std::map<size_t, individual_constraints> constraint_table;
    ret.families = pedigree_analysis(pedigree, design, ret.generations, ret.gen_by_id, genealogy, constraint_table);

    ret.var_to_state_helper.reserve(pedigree.size());

    bayesian_network& bn = ret.bn;
    auto& obs_map = ret.evidence_by_gen_name;

    auto
        get_parent_id
        = [&] (const branch_type& b, int id)
        {
            for (size_t i = 0; i < b.size(); ++i) {
                id = b.test(i) ? genealogy[id].second : genealogy[id].first;
            }
            return id;
        };

    for (const auto& pi: pedigree) {
        if (pi.is_ancestor()) {
            ret.vars[pi.id] = bn.ancestor();
        } else if (pi.is_self()) {
            ret.vars[pi.id] = bn.selfing(ret.vars[pi.p1]);
        } else if (pi.is_dh()) {
            ret.vars[pi.id] = bn.dhing(ret.vars[pi.p1]);
        } else if (pi.is_cross()) {
            ret.vars[pi.id] = bn.crossing(ret.vars[pi.p1], ret.vars[pi.p2]);
        }
        /* TODO: create state vector nodes here */
        auto ogi = obs_gen.find(pi.gen_name);
        if (ogi != obs_gen.end()) {
            if (ogi->second == ODAllele) {
                obs_map[pi.gen_name].push_back(bn.allele_obs(ret.vars[pi.id]));
            } else {
                obs_map[pi.gen_name].push_back(bn.ancestor_obs(ret.vars[pi.id]));
            }
        }

        /* Compute parents */
        MSG_DEBUG("Constraints for #" << pi.id);
        MSG_DEBUG(constraint_table[pi.id]);
        std::vector<branch_type> branches;
        std::vector<branch_type> taboo;
        for (const auto& cons: constraint_table[pi.id].constraints) {
            auto b = cons.first;
            b.resize(1);
            if (std::find(taboo.begin(), taboo.end(), b) == taboo.end()) {
                taboo.push_back(b);
            }
            b = cons.second;
            b.resize(1);
            if (std::find(taboo.begin(), taboo.end(), b) == taboo.end()) {
                taboo.push_back(b);
            }
        }
        for (const auto& cons: constraint_table[pi.id].constraints) {
            auto b = cons.first;
            if (std::find(taboo.begin(), taboo.end(), b) == taboo.end()) {
                branches.push_back(b);
                taboo.push_back(b);
                MSG_DEBUG("parent " << b);
            }
            b.flip(b.size() - 1);  /* other parent */
            if (std::find(taboo.begin(), taboo.end(), b) == taboo.end()) {
                branches.push_back(b);
                taboo.push_back(b);
                MSG_DEBUG("parent " << b);
            }
            b = cons.second;
            b.flip(b.size() - 1);
            if (std::find(taboo.begin(), taboo.end(), b) == taboo.end()) {
                branches.push_back(b);
                taboo.push_back(b);
                MSG_DEBUG("parent " << b);
            }
            taboo.push_back(cons.second);
        }
        /* adding direct parents if not already taboo'd */
        {
            branch_type b(1, 0);
            if (std::find(taboo.begin(), taboo.end(), b) == taboo.end()) {
                branches.push_back(b);
                MSG_DEBUG("parent " << b);
            }
            b.flip(0);
            if (std::find(taboo.begin(), taboo.end(), b) == taboo.end()) {
                branches.push_back(b);
                MSG_DEBUG("parent " << b);
            }
        }

        MSG_DEBUG("lincomb");
        MSG_DEBUG(ret.gen_by_id[pi.id]->this_lincomb);

        std::sort(branches.begin(), branches.end());
        std::vector<size_t> parents;
        parents.reserve(branches.size());
        for (const auto& b: branches) {
            parents.push_back(ret.vars[get_parent_id(b, pi.id)]);
        }

        /*ret.var_to_state_helper.emplace_back(ret.vars[pi.id], std::vector<size_t>{ret.vars[pi.p1], ret.vars[pi.p2]}, ret.gen_by_id[pi.id]);*/
        ret.var_to_state_helper.emplace_back(ret.vars[pi.id], parents, ret.gen_by_id[pi.id]);
        bn.varname(ret.vars[pi.id]) = SPELL_STRING(pi.gen_name << ':' << pi.id);
    }

    size_t dom = n_par * n_par;

    for (const auto& ng: ret.generations) {
        const generation_rs* g = ng.second;
        const auto& labels = g->main_process().row_labels;
        /*SparseMatrix<double>& M = ret.geno_to_state[g] = MatrixXd::Zero(labels.size(), dom);*/
        ret.geno_to_state[g].resize(labels.size(), dom);
        SparseMatrix<double>& M = ret.geno_to_state[g];
        std::vector<Eigen::Triplet<double>> t;
        t.reserve(labels.size());
        std::vector<double> norm;
        norm.resize(dom, 0.);
        for (size_t i = 0; i < labels.size(); ++i) {
            int r = labels[i].first.ancestor - 'a';
            int c = labels[i].second.ancestor - 'a';
            norm[r + n_par * c] += 1.;
        }
        for (double& d: norm) { d = 1. / d; }
        for (size_t i = 0; i < labels.size(); ++i) {
            int r = labels[i].first.ancestor - 'a';
            int c = labels[i].second.ancestor - 'a';
            /*M(i, c * n_par + r) = 1;*/
            t.emplace_back(i, r + c * n_par, norm[r + n_par * c]);
        }
        M.setFromTriplets(t.begin(), t.end());
        /*VectorXd csum = M.array().colwise().sum();*/
        /*M.array().rowwise() /= (csum.array() == 0).select(VectorXd::Ones(csum.size()).array(), csum.array()).transpose();*/

        /*MSG_DEBUG(g->name);*/
        /*for (size_t i = 0; i < labels.size(); ++i) {*/
            /*MSG_DEBUG(labels[i] << "  " << M.row(i));*/
        /*}*/
    }

    bn.init_messages();
    return ret;
}


template <typename MATRIX_TYPE>
double mass(const MATRIX_TYPE& m)
{
    std::vector<size_t> d;
    size_t s = m.dimensions().size();
    d.reserve(s);
    for (size_t i = 0; i < s; ++i) { d.push_back(i); }
    auto x = sum_over(m, d);
    auto ac = x.accessor();
    return ac.get(0);
}


inline std::ostream& operator << (std::ostream& os, const impl::generation_rs* g)
{
    return os << g->name;
}

