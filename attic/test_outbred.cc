/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "outbred.h"
/*#include "all.h"*/


void test_obs(const segregation& s, const phase& p, const allele_pair& o)
{
    std::cout << s << ' ' << p << ' ' << o << " => " << to_genotypes(o % (s * p)) << std::endl;
}



int main(int argc, char** argv)
{
    std::cout << "considering (AxB)x(CxD), i.e. <abxcd>" << std::endl;
    test_obs({'a', 'a', 'b', 'c'}, {'-', '0'}, {'a', 'c'});
    test_obs({'a', 'a', 'b', 'c'}, {'-', '1'}, {'a', 'c'});
    test_obs({'a', 'b', 'a', 'b'}, {'0', '0'}, {'a', 'b'});
    test_obs({'a', 'b', 'a', 'b'}, {'0', '1'}, {'a', 'b'});
    test_obs({'a', 'b', 'a', 'b'}, {'1', '0'}, {'a', 'b'});
    test_obs({'a', 'b', 'a', 'b'}, {'1', '1'}, {'a', 'b'});
    test_obs({'a', 'b', 'a', 'b'}, {'0', '0'}, {'a', '*'});
    test_obs({'a', 'b', 'a', 'b'}, {'0', '0'}, {'b', '*'});
    
    return 0;
}

