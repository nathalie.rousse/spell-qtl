/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*#include <iostream>*/
/*#include <x2c/x2c.h>*/
/*#include "design.h"*/
/*#include "probabilities.h"*/
#include "all.h"

using namespace x2c;

struct probapop_output {
    std::vector<std::vector<double>> prob;
    operator MatrixXd () const
    {
        MatrixXd ret(4, prob.size());
        for (size_t j = 0; j < prob.size(); ++j) {
            for (int i = 0; i < 4; ++i) {
                ret(i, j) = prob[j][i];
            }
        }
        return ret;
    }
};

struct matrix_p {
    std::string mark;
    double p1, p2, p3;
    void operator () (probapop_output& po)
    {
        /*if (mark == "yes") {*/
            std::vector<double> col;
            col.push_back(p1);
            col.push_back(p2*.5);
            col.push_back(p2*.5);
            col.push_back(p3);
            po.prob.push_back(col);
        /*}*/
    }
};

struct matrix_p_list {
    std::vector<matrix_p> list;
    void operator () (probapop_output& po) {
        for (auto m: list) {
            m(po);
        }
    }
};

DTD_START(probapop_dtd, POPULATION, probapop_output)
    ELEMENT(CHROMOSOME, matrix_p_list);
    ELEMENT(POSITION, matrix_p);
    ELEMENT(MATRIX_P, matrix_p);
    ELEMENT(LINE, matrix_p);
    ELEMENT(PROBABILITY, double);

    PROBABILITY = A("value");
    LINE = A("number", &ignore::entity) & E(PROBABILITY, &matrix_p::p1) & E(PROBABILITY, &matrix_p::p2) & E(PROBABILITY, &matrix_p::p3);
    MATRIX_P = A("number_rows", &ignore::entity) & A("number_columns", &ignore::entity) & E(LINE);
    POSITION = A("value", &ignore::entity) & A("marker", &matrix_p::mark) & A("name", &ignore::entity) & A("weight", &ignore::entity) & E(MATRIX_P);
    CHROMOSOME = A("name", &ignore::entity) & A("number", &ignore::entity) & E(POSITION, &matrix_p_list::list);
    POPULATION = A("name", &ignore::entity) & A("type", &ignore::entity) & E(CHROMOSOME);
DTD_END(probapop_dtd);


using Eigen::Matrix4d;

#ifdef TEST_PROBA

int main(int argc, char** argv)
{
    std::vector<haplo_type> hap = {
        { 'a', 'a' },
        { 'a', 'b' },
        { 'b', 'a' },
        { 'b', 'b' }
    };
    auto breakmat = haplotype_break_matrix(hap);

    VectorXd A(4), B(4), C(4), H(4), D(4), U(4);
    A << 1, 0, 0, 0;
    B << 0, 0, 0, 1;
    H << 0, 1., 1., 0;
    C << 1., 1., 1., 0;
    D << 0, 1., 1., 1.;
    U << .25, .25, .25, .25;

    MatrixXd probapop, X;

    ifile is("/homeLocal/dleroux/test_probapop/data.long/pimpam.xml");
    probapop_output* po = probapop_dtd.parse(is);
    if (po) {
        probapop = *po;
        delete po;
    }

    segment_probability_2 sp2;
    sp2.locus.push_back(0);
    int N = 3;
    for (int i = 1; i < N; ++i) {
        sp2.locus.push_back(sp2.locus.back() + 5);
    }
    sp2.LV.resize(4, N);
    sp2.LV << U, U, U;
    sp2.init(breakmat);
    X = sp2.compute_over_segment(1);
    std::cout << "mine" << std::endl << X << std::endl << std::endl;
    std::cout << "probapop" << std::endl << probapop << std::endl << std::endl;
    std::cout << "abs(diff)" << std::endl << (X - probapop).array().abs() << std::endl << std::endl;
    std::cout << std::endl << "max(abs(diff)) = " << (X - probapop).lpNorm<Eigen::Infinity>() << std::endl;


    return 0;
}

#endif

