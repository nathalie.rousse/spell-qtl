/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_CACHE_H_
#define _SPEL_CACHE_H_

/*#include "cache/call_tuple.h"*/
#include <iostream>

struct md5_digest;

namespace cache {
    template <typename V> struct value;
    template <typename V> struct range;
    template <typename V> struct collection;
    template <typename C, typename T, typename... D> struct computed_value;
    template <typename V> struct computed_collection;

    template <typename X>
    struct is_value {
        static const X& _();
        template <typename V> static std::true_type check(const value<V>&);
        static std::false_type check(...);
        static const bool value = decltype(check(_()))::value;
    };

    struct with_output_func {
        virtual ~with_output_func() {}
        virtual std::ostream& output(std::ostream& os) const = 0;
        virtual md5_digest& to_md5(md5_digest&) const = 0;
        virtual size_t hash() const = 0;
        /*virtual cache_input& from_cache(cache_input&) = 0;*/
        /*virtual cache_output& to_cache(cache_output&) = 0;*/
    };
}

#include "cache/md5.h"
#include "cache/tuple_utils.h"
#include "cache/value.h"
#include "cache/cartesian_product.h"
#include "cache/file.h"
#include "cache/async.h"
#include "cache/factory.h"

#endif

