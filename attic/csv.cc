/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "error.h"
#include "pedigree.h"


std::vector<pedigree_item>
read_csv(const std::string& pedigree_file, char field_sep)
{
    std::vector<pedigree_item> ret;
    ifile pef(pedigree_file);
    if (!pef.good()) {
        MSG_DEBUG("prout");
    }
    std::string col_name, col_id, col_p1, col_p2;
    read_csv_line(pef, field_sep, col_name, col_id, col_p1, col_p2);
    /*MSG_DEBUG("col_name=" << col_name << " col_id=" << col_id << " col_p1=" << col_p1 << " col_p2=" << col_p2);*/
    while (!pef.eof()) {
        ret.emplace_back(pef, field_sep);
        if (ret.back().id == 0) {
            ret.pop_back();
        }
    }
    return ret;
}


