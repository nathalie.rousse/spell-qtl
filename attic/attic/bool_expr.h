/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_BOOL_EXPR_H_
#define _SPEL_BOOL_EXPR_H_

enum class BoolExprType { True, False, And, Or, Variable };

struct bool_expr {
    BoolExprType type;
    std::set<bool_expr> contents;

    bool_expr(bool state)
        : type(state ? BoolExprType::True : BoolExprType::False)
        , contents()
    {}

    bool_expr(BoolExprType t=BoolExprType::Variable)
        : type(t)
        , contents()
    {}

    bool_expr& operator |= (const bool_expr& be)
    {
        bool_expr tmp(type);
        std::swap(tmp.contents, contents);
        if (type == BoolExprType::Or) {
            ret.contents.insert(contents.begin(), contents.end())
        }
        if (be.type == BoolExprType::Or) {
            ret.contents.insert(be.contents.begin(), be.contents.end());
        }
        return ret;
    }

    bool_expr operator & (const bool_expr& be)
    {
        bool_expr ret(BoolExprType::And);
        if (type == BoolExprType::And) {
            ret.contents.insert(contents.begin(), contents.end())
        }
        if (be.type == BoolExprType::And) {
            ret.contents.insert(be.contents.begin(), be.contents.end());
        }
        return ret;
    }
};


#endif

