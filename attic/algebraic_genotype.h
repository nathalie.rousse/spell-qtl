#ifndef _SPEL_ALGEBRAIC_GENOTYPE_H_
#define _SPEL_ALGEBRAIC_GENOTYPE_H_

#include "fast_polynom.h"
#include "markov_population.h"

/* s = 1 - r
 */

#define RS_EPSILON (2.5e-1)
#define RS_PRUNE_EPSILON (1.e-4)
#define RS_MAX_EPS (1. + RS_EPSILON)
#define RS_MIN_EPS (1. - RS_EPSILON)
/*#define FUZZY_COMPARE*/
#define COMPARE_NORM

#define DEBUG_ALGEBRAIC_GENOTYPE

namespace impl {
template <typename... X> struct multi_compare;

template <typename Comparable1, typename... Comparable>
    struct multi_compare<Comparable1, Comparable...> {
        static bool lt(Comparable1 k1, Comparable... x1, Comparable1 k2, Comparable... x2)
        {
            return (k1 < k2) || ((k1 == k2) && multi_compare<Comparable...>::lt(x1..., x2...));
        }
        static bool gt(Comparable1 k1, Comparable... x1, Comparable1 k2, Comparable... x2)
        {
            return (k1 > k2) || ((k1 == k2) && multi_compare<Comparable...>::lt(x1..., x2...));
        }
        static bool eq(Comparable1 k1, Comparable... x1, Comparable1 k2, Comparable... x2)
        {
            return (k1 == k2) && multi_compare<Comparable...>::eq(x1..., x2...);
        }
    };

template <>
    struct multi_compare<> {
        static bool le() { return true; }
        static bool ge() { return true; }
        static bool lt() { return false; }
        static bool gt() { return false; }
        static bool eq() { return true; }
        static bool ne() { return false; }
    };

}


#if 0
inline int min_non_null_coef(const polynom& P)
{
    size_t i = 0;
    while (i < P.size() && P[i] == 0) {
        ++i;
    }
    return (int)i;
}
#endif


#define min__(_a, _b) ((_a) < (_b) ? (_a) : (_b))

static inline int compute_epsilon_exp() {
    static double epsilon = 0, tol1 = 0;
    static int exp = 0;
    if (tol1 == 0) {
        epsilon = 1.;
        do {
            epsilon *= .5;
            ++exp;
            tol1 = 1. + epsilon;
            /*printf("tol1 = %e\n", tol1);*/
        } while (tol1 > 1.);
        /*epsilon = sqrt(epsilon);*/
        exp = exp >> 1;
        /*printf("epsilon = %e\n", epsilon);*/
    }
    return exp;
}



#if 0
struct rs_polynom {
    /* r^i * s^j * P(X) with r=X and s=1-X */
    unsigned short int r_exp;
    unsigned short int s_exp;
    polynom P;

    static const polynom r_poly;
    static const polynom s_poly;

    rs_polynom() : r_exp(0), s_exp(0), P() {}
    rs_polynom(double x) : r_exp(0), s_exp(0), P(x) {}
    rs_polynom(unsigned short r, unsigned short s, double x) : r_exp(r), s_exp(s), P(x) {}
    rs_polynom(unsigned short r, unsigned short s, const polynom& x) : r_exp(r), s_exp(s), P(x) {}

    rs_polynom denormalized(unsigned short int new_r_exp, unsigned short int new_s_exp) const
    {
        return {new_r_exp, new_s_exp, P * (r_poly ^ (r_exp - new_r_exp)) * (s_poly ^ (s_exp - new_s_exp))};
    }

    double norm() const
    {
        return denormalized(0, 0).P.norm();
    }

    rs_polynom& normalize()
    {
        int r_fac = min_non_null_coef(P);
        polynom tmp_r;
        tmp_r.assign(P.begin() + r_fac, P.end());
        tmp_r = tmp_r.o(s_poly);
        int s_fac = min_non_null_coef(tmp_r);
        polynom tmp_s;
        tmp_s.assign(tmp_r.begin() + s_fac, tmp_r.end());
        P = tmp_s.o(s_poly);
        r_exp += r_fac;
        s_exp += s_fac;
        P.prune(RS_PRUNE_EPSILON);
        return *this;
    }

    friend rs_polynom operator * (const rs_polynom& a, const rs_polynom& b)
    {
        return {(unsigned short int) (a.r_exp + b.r_exp),
                (unsigned short int) (a.s_exp + b.s_exp),
                a.P * b.P};
    }

    friend rs_polynom operator + (const rs_polynom& a, const rs_polynom& b)
    {
        unsigned short int min_r_exp = min__(a.r_exp, b.r_exp), min_s_exp = min__(a.s_exp, b.s_exp);
        rs_polynom denorm_a = a.denormalized(min_r_exp, min_s_exp);
        rs_polynom denorm_b = b.denormalized(min_r_exp, min_s_exp);
        rs_polynom ret = {min_r_exp, min_s_exp, denorm_a.P + denorm_b.P};
        return ret.normalize();
    }

    friend rs_polynom operator - (const rs_polynom& a, const rs_polynom& b)
    {
        unsigned short int min_r_exp = min__(a.r_exp, b.r_exp), min_s_exp = min__(a.s_exp, b.s_exp);
        rs_polynom denorm_a = a.denormalized(min_r_exp, min_s_exp);
        rs_polynom denorm_b = b.denormalized(min_r_exp, min_s_exp);
        rs_polynom ret = {min_r_exp, min_s_exp, denorm_a.P - denorm_b.P};
        return ret.normalize();
    }

    int valuation() const
    {
        int v = P.valuation();
        return v >= 0 ? r_exp + v : v;
    }

#ifdef RS_APPROX_RELATIVE

    int compare_point(const rs_polynom& a, double x) const
    {
        double v1 = (*this)(x);
        double v2 = a(x);
        if (v2 == 0) {
            return v1 <= -RS_EPSILON
                    ? -1
                    : v1 >= RS_EPSILON
                        ? 1
                        : 0;
        } else if (v1 == 0) {
            return v2 <= -RS_EPSILON
                    ? 1
                    : v2 >= RS_EPSILON
                        ? -1
                        : 0;
        }
        if (v1 < v2) {
            double delta = v2 / v1;
            return delta < RS_MAX_EPS ? 0 : -1;
        } else {
            double delta = v1 / v2;
            return delta < RS_MAX_EPS ? 0 : 1;
        }
    }

#else

    int compare_point(const rs_polynom& a, double x) const
    {
        /* murzistique */
        double degree = r_exp + s_exp + P.size();
        /*double epsilon = 1. - pow(1. - RS_EPSILON, degree);*/
        double epsilon = RS_EPSILON;
        /*double epsilon = P[0] * (1. - pow(1. - RS_EPSILON, degree * .5));*/
        /*double epsilon = P[0] * RS_EPSILON;*/
        double v1 = (*this)(x);
        double v2 = a(x);
        if (v2 == 0) {
            return v1 <= -epsilon
                    ? -1
                    : v1 >= epsilon
                        ? 1
                        : 0;
        } else if (v1 == 0) {
            return v2 <= -epsilon
                    ? 1
                    : v2 >= epsilon
                        ? -1
                        : 0;
        }
        if (v1 < v2) {
            return pow(v2 - v1, degree) < epsilon ? 0 : -1;
        } else {
            return pow(v1 - v2, degree) < epsilon ? 0 : 1;
        }
    }

#endif


#ifdef COMPARE_NORM
#ifdef LULZ
    bool compare(const rs_polynom& a) const
    {
        double anorm = a.norm();
        return fabs(norm() - anorm) < (RS_MAX_EPS * anorm);
    }

    bool inf(const rs_polynom& a) const
    {
        double anorm = a.norm();
        double n = norm();
        return (anorm - n) >= (RS_MAX_EPS * n);
    }
#else
    bool compare(const rs_polynom& a) const
    {
        rs_polynom diff = a - *this;
        double eps = (norm() + a.norm()) * RS_EPSILON;
        return diff.norm() < eps;
    }

    bool inf(const rs_polynom& a) const
    {
        return !compare(a) && *this < a;
    }
#endif
#else
    bool compare(const rs_polynom& a) const
    {
        if (r_exp == a.r_exp && s_exp == a.s_exp && P == a.P) {
            return true;
        }
        for (double x = 0; x <= .375; x += .125) {
            if (compare_point(a, x)) {
                return false;
            }
        }
        return true;
    }

    bool inf(const rs_polynom& a) const
    {
        for (double x = 0; x <= .375; x += .125) {
            switch(compare_point(a, x)) {
                case -1: return true;
                case 1: return false;
                default:;
            };
        }
        return false;
    }
#endif


    bool operator < (const rs_polynom& a) const
    {
#ifdef FUZZY_COMPARE
        /*return !compare(a) &&*/
        return inf(a);
#else
        return
            impl::multi_compare<unsigned short int, unsigned short int, polynom>
            ::lt(r_exp,   s_exp,   P,
                 a.r_exp, a.s_exp, a.P);
#endif
    }

    bool operator == (const rs_polynom& a) const
    {
#ifdef FUZZY_COMPARE
        return compare(a);
#else
        return impl::multi_compare<unsigned short int, unsigned short int, polynom>
            ::eq(r_exp, s_exp, P, a.r_exp, a.s_exp, a.P);
#endif
    }

    rs_polynom& operator *= (const rs_polynom& a)
    {
        r_exp += a.r_exp;
        s_exp += a.s_exp;
        P *= a.P;
        return *this;
    }

    rs_polynom& operator += (const rs_polynom& a)
    {
        unsigned short int min_r_exp = min__(a.r_exp, r_exp), min_s_exp = min__(a.s_exp, s_exp);
        rs_polynom denorm_a = a.denormalized(min_r_exp, min_s_exp);
        rs_polynom denorm_b = denormalized(min_r_exp, min_s_exp);
        r_exp = min_r_exp;
        s_exp = min_s_exp;
        P = denorm_a.P + denorm_b.P;
        return normalize();
    }

    rs_polynom o(const rs_polynom& q) const
    {
        rs_polynom ret = {0, 0, denormalized(0, 0).P.o(q.denormalized(0, 0).P)};
        ret.normalize();
        return ret;
    }

    friend std::ostream& operator << (std::ostream& os, const rs_polynom& rs)
    {
        if (rs.r_exp > 0) {
            os << "X^" << rs.r_exp << '*';
        }
        if (rs.s_exp > 0) {
            os << "(1-X)^" << rs.s_exp << '*';
        }
        return os << rs.P;
    }

    double operator () (double x) const
    {
        return pow(x, r_exp) * pow(1 - x, s_exp) * P(x);
    }
};


namespace std {
    template <>
        struct hash<const rs_polynom&> {
            size_t operator () (const rs_polynom& rs) const
            {
                hash<int> hi;
                hash<double> hd;
                size_t accum = hi(rs.r_exp) ^ hi(rs.s_exp);
                for (auto& d: rs.P) {
                    accum ^= hd(d);
                }
                /*std::cout << "hashing " << rs << " => " << accum << std::endl;*/
                return accum;
            }
        };
};
#endif


struct error : public std::exception {
    std::string msg;
    error(const char* w) : msg(w) {}
    error(const std::string& w) : msg(w) {}
    virtual ~error() {}
    const char* what() const throw() { return msg.c_str(); }
};


struct algebraic_genotype { /* must fit in a QWORD */
    static const algebraic_genotype null;

    locus_pair genotype;
    enum class Type: short {Null, Genotype, Gamete, DoublingGamete, Haplotype, Cross, Polynom, RilPolynom} type;
    /*rs_polynom poly;*/
    fast_polynom poly;
    /*int __align[2];*/
#ifdef DEBUG_ALGEBRAIC_GENOTYPE
    /*std::pair<const algebraic_genotype*, const algebraic_genotype*> history;*/
#endif
    /*
     * Genotype * Genotype : boom
     * Genotype * Gamete : Haplotype
     * Genotype * Haplotype : boom
     * Genotype * Cross : Genotype
     *
     * Gamete * Gamete : Cross
     * Gamete * Haplotype : boom
     * Gamete * Cross : boom
     *
     * Haplotype * Haplotype : Genotype
     * Haplotype * Cross : boom
     *
     * Cross * Cross : Cross (sibling)
     */

    algebraic_genotype()
#ifdef DEBUG_ALGEBRAIC_GENOTYPE
        /*: history(NULL, NULL)*/
#endif
    {
        genotype = {{0, 0}, {0, 0}};
        type = Type::Null;
        /*r_exp = s_exp = 0;*/
        /*poly = {0, 0, {1}};*/
        poly = fast_polynom::zero;
    }

    algebraic_genotype(locus_pair lp, Type t, const fast_polynom& rs)
        /*: genotype(lp), type(t), r_exp(r), s_exp(s)*/
        : genotype(lp), type(t), poly(rs)
    {
        /*MSG_DEBUG("NEW " << t); MSG_QUEUE_FLUSH();*/
    }

    algebraic_genotype(double constant)
        : genotype({{0, 0}, {0, 0}}), type(Type::Polynom), poly()
    { poly = {constant}; }

    algebraic_genotype(const fast_polynom& rs)
        : genotype({{0, 0}, {0, 0}}), type(Type::Polynom), poly(rs)
    {}

    static algebraic_genotype Polynom(ancestor_allele ff, ancestor_allele fs, ancestor_allele sf, ancestor_allele ss, const fast_polynom& rs)
    {
        return {{{ff, fs}, {sf, ss}}, Type::Polynom, rs};
    }

    static algebraic_genotype Genotype(ancestor_allele ff, ancestor_allele fs, ancestor_allele sf, ancestor_allele ss, const fast_polynom& rs)
    {
        return {{{ff, fs}, {sf, ss}}, Type::Genotype, rs};
    }

    static algebraic_genotype Cross(ancestor_allele ff, ancestor_allele fs, ancestor_allele sf, ancestor_allele ss, const fast_polynom& rs)
    {
        return {{{ff, fs}, {sf, ss}}, Type::Cross, rs};
    }

    static algebraic_genotype Gamete(ancestor_allele ff, ancestor_allele sf, const fast_polynom& rs)
    {
        return {{{ff, 0}, {sf, 0}}, Type::Gamete, rs};
    }

    static algebraic_genotype Haplotype(ancestor_allele ff, ancestor_allele sf, const fast_polynom& rs)
    {
        return {{{ff, 0}, {sf, 0}}, Type::Haplotype, rs};
    }

    bool operator < (const algebraic_genotype& ag) const
    {
        return impl::multi_compare<Type, locus_pair, fast_polynom>
                   ::lt(type,    genotype,    poly,
                        ag.type, ag.genotype, ag.poly);
    }

    /*bool operator > (algebraic_genotype ag) const*/
    /*{*/
        /*return impl::multi_compare<Type, locus_pair, fast_polynom>*/
                   /*::gt(type,    genotype,    poly,*/
                        /*ag.type, ag.genotype, ag.poly);*/
    /*}*/

    /*bool operator == (const algebraic_genotype& ag) const*/
    /*{*/
        /*return impl::multi_compare<Type, locus_pair, fast_polynom>*/
                   /*::eq(type,    genotype,    poly,*/
                        /*ag.type, ag.genotype, ag.poly);*/
    /*}*/

    algebraic_genotype& operator = (const algebraic_genotype ag)
    {
        genotype = ag.genotype;
        type = ag.type;
        /*r_exp = ag.r_exp;*/
        /*s_exp = ag.s_exp;*/
        poly = ag.poly;
        return *this;
    }

    algebraic_genotype& operator += (const algebraic_genotype ag)
    {
        /*MSG_DEBUG("AG_ACCUM " << (*this) << " += " << ag);*/
        if (type == Type::Null) { return *this = ag; }
        /*if (ag.type == Type::Null) { return *this; }*/
        /*if (ag.type == type && ag.genotype == genotype) {*/
        if (type == Type::Polynom) {
            type = ag.type;
            genotype = ag.genotype;
        }
            poly += ag.poly;
            return *this;
        /*}*/
        std::cerr << "Error: " << (*this) << std::endl;
        std::cerr << "Error: " << ag << std::endl;
        throw error("can't + two non-null algebraic_genotype");
    }

    bool operator == (const algebraic_genotype ag) const { return type == ag.type && genotype == ag.genotype && poly == ag.poly; }

    friend std::ostream& operator << (std::ostream& os, algebraic_genotype::Type t)
    {
        switch(t) {
            /*case algebraic_genotype::Type::Genotype: os << "Genotype"; break;*/
            case algebraic_genotype::Type::Genotype: break;
            case algebraic_genotype::Type::Haplotype: os << "Haplotype"; break;
            case algebraic_genotype::Type::Gamete: os << "Gamete"; break;
            case algebraic_genotype::Type::DoublingGamete: os << "DoublingGamete"; break;
            case algebraic_genotype::Type::Cross: os << "GameteCross"; break;
            case algebraic_genotype::Type::Null: os << "Null"; break;
            case algebraic_genotype::Type::Polynom: os << "Polynom"; break;
            case algebraic_genotype::Type::RilPolynom: os << "RilPolynom"; break;
        };
        return os;
    }

    friend std::ostream& operator << (std::ostream& os, algebraic_genotype ag)
    {
        os << '{' << ag.type << ' ';
        switch(ag.type) {
            case algebraic_genotype::Type::Haplotype:
                os << ag.genotype.first.first << ',' << ag.genotype.second.first;
                break;
            case algebraic_genotype::Type::DoublingGamete:
            case algebraic_genotype::Type::Gamete:
                os << (char)('0' + ag.genotype.first.first.ancestor);
                os << ',';
                os << (char)('0' + ag.genotype.second.first.ancestor);
                break;
            case algebraic_genotype::Type::Cross:
                os << (char)('0' + ag.genotype.first.first.ancestor);
                os << (char)('0' + ag.genotype.first.second.ancestor);
                os << ',';
                os << (char)('0' + ag.genotype.second.first.ancestor);
                os << (char)('0' + ag.genotype.second.second.ancestor);
                break;
            case Type::RilPolynom:
                break;
            case Type::Polynom:
            default:
                os << ag.genotype;
        };
        /*return os << " r^" << ((int)ag.r_exp) << "*s^" << ((int)ag.s_exp) << '}';*/
        return os << ' ' << ag.poly << '}';
    }

    const algebraic_genotype operator + (const algebraic_genotype ag) const
    {
        /*MSG_DEBUG("AG_ADD " << (*this) << " + " << ag);*/
        if (type == Type::Null) { return ag; }
        if (ag.type == Type::Null) { return *this; }
        /*if (ag.type == type && ag.genotype == genotype) {*/
            /*return {genotype, type, poly + ag.poly};*/
        /*} else {*/
            /*return algebraic_genotype(poly + ag.poly);*/
        /*}*/
        /*if (poly != fast_polynom::zero) {*/
        if (type != Type::Polynom) {
            algebraic_genotype ret = {genotype, type, poly + ag.poly};
            /*MSG_DEBUG("AG_ADD " << (*this) << " + " << ag << " => " << ret);*/
            return ret;
        } else {
            algebraic_genotype ret = {ag.genotype, ag.type, poly + ag.poly};
            /*MSG_DEBUG("AG_ADD " << (*this) << " + " << ag << " => " << ret);*/
            return ret;
        }
        /*std::cerr << "Error: " << (*this) << std::endl;*/
        /*std::cerr << "Error: " << ag << std::endl;*/
        /*throw error("can't + two non-null algebraic_genotype");*/
    }

    algebraic_genotype operator * (algebraic_genotype other) const
    {
        /*MSG_DEBUG(type << " * " << other.type);*/
        if (other.type == Type::Polynom) {
            if (type == Type::RilPolynom) {
                if (poly == fast_polynom::zero) {
                    return {other.genotype, other.type, fast_polynom::zero};
                }
                return {other.genotype, other.type, other.poly.o(poly)};
            }
            return {genotype, type, poly * other.poly};
        } else if (other.type == Type::RilPolynom) {
            if (poly == fast_polynom::zero) {
                return {genotype, type, fast_polynom::zero};
            }
            return {genotype, type, poly.o(other.poly)};
        }
        switch (type) {
            case Type::DoublingGamete:
                switch (other.type) {
                    case Type::Genotype:
                        return Genotype(other.genotype.first % (bool)genotype.first.first.ancestor,
                                        other.genotype.first % (bool)genotype.first.first.ancestor,
                                        other.genotype.second % (bool)genotype.second.first.ancestor,
                                        other.genotype.second % (bool)genotype.second.first.ancestor,
                                        /*other.poly.o(poly));*/
                                        poly * other.poly);
                        break;
                    case Type::Gamete:
                        if (genotype == other.genotype) {
                            return *this;
                        }
                        return null;
                    default:;
                        /*throw error("Unsupported DoublingGamete*WhateverButGenotype");*/
                        {
                            static std::string tmp_msg;
                            tmp_msg = MESSAGE("Unsupported DoublingGamete*" << other.type);
                            throw error(tmp_msg.c_str());
                        }
                };
                break;
            case Type::Polynom:
                /*return Polynom(genotype.first.first, genotype.first.second,*/
                               /*other.genotype.second.first, other.genotype.second.second,*/
                               /*poly * other.poly);*/
                /*switch (other.type) {*/
                    /*case Type::Polynom:*/
                    /*default:*/
                {
                    algebraic_genotype ret = {other.genotype, other.type, poly * other.poly};
                    /*MSG_DEBUG("POLY_MULT " << (*this) << " * " << other << " => " << ret);*/
                    return ret;
                }
                /*};*/
                break;
            case Type::Genotype:
                switch (other.type) {
                    case Type::Genotype:
                        /* This is a "hack abject" as per SJ standards. */
                        /*MSG_DEBUG("[Geno*Geno] " << (*this) << " * " << other);*/
                        /*throw error("Unsupported Genotype*Genotype");*/
                        if (genotype.second == other.genotype.first && other.poly.degree() == 0) {
                            return Genotype(genotype.first.first, genotype.first.second,
                                            other.genotype.second.first, other.genotype.second.second,
                                            poly * other.poly);
                        } else if (genotype.first == other.genotype.first && poly.degree() == 0) {
                            return Genotype(genotype.second.first, genotype.second.second,
                                            other.genotype.second.first, other.genotype.second.second,
                                            poly * other.poly);
                        } else {
                            MSG_QUEUE_FLUSH();
                            throw error(MESSAGE("Inconsistent Genotype*Genotype: " << genotype << " * " << other.genotype << " degree=" << poly.degree() << " other.degree=" << other.poly.degree()));
                        }
                    case Type::Haplotype:
                        throw error("Unsupported Genotype*Haplotype");
                        break; /* make SJ happy */
                    case Type::DoublingGamete:
                        return Genotype(genotype.first % (bool)other.genotype.first.first.ancestor,
                                        genotype.first % (bool)other.genotype.first.first.ancestor,
                                        genotype.second % (bool)other.genotype.second.first.ancestor,
                                        genotype.second % (bool)other.genotype.second.first.ancestor,
                                        /*poly.o(other.poly));*/
                                        poly * other.poly);
                    case Type::Gamete:
                        return Haplotype(genotype.first % (bool)other.genotype.first.first.ancestor,
                                         genotype.second % (bool)other.genotype.second.first.ancestor,
                                         poly * other.poly);
                        /*break;*/
                    case Type::Cross:
                        return Genotype(genotype.first % (bool)other.genotype.first.first.ancestor,
                                        genotype.first % (bool)other.genotype.first.second.ancestor,
                                        genotype.second % (bool)other.genotype.second.first.ancestor,
                                        genotype.second % (bool)other.genotype.second.second.ancestor,
                                        poly * other.poly);
                        /*break;*/
                    case Type::Null:
                        throw error("Unsupported Genotype*Null");
                    case Type::Polynom:  // break;  /* can't happen */
                        /*return Polynom(genotype.first.first, genotype.first.second,*/
                                       /*other.genotype.second.first, other.genotype.second.second,*/
                                       /*poly * other.poly);*/
                        return {genotype, type, poly * other.poly};
                    case Type::RilPolynom: break; /* can't happen */
                };
                break;
            case Type::Gamete:
                switch (other.type) {
                    case Type::Gamete:
                        return Cross(genotype.first.first, other.genotype.first.first,
                                     genotype.second.first, other.genotype.second.first,
                                     poly * other.poly);
                    case Type::Genotype:
                        return Haplotype(other.genotype.first % (bool)genotype.first.first.ancestor,
                                         other.genotype.second % (bool)genotype.second.first.ancestor,
                                         poly * other.poly);
                    case Type::DoublingGamete:
                        if (genotype == other.genotype) {
                            return other;
                        }
                        return null;
                    case Type::Haplotype: throw error("Unsupported Gamete*Haplotype");
                    case Type::Cross: throw error("Unsupported Gamete*Cross");
                    case Type::Null: throw error("Unsupported Gamete*Null");
                    case Type::Polynom: break;  /* can't happen */
                    case Type::RilPolynom: break; /* can't happen */
                };
                break;
            case Type::Haplotype:
                switch (other.type) {
                    case Type::Haplotype:
                        /*std::cout << "H1" << std::endl << (*this) << std::endl;*/
                        /*std::cout << "H2" << std::endl << other << std::endl;*/
                        return Genotype(genotype.first.first, other.genotype.first.first,
                                        genotype.second.first, other.genotype.second.first,
                                        poly * other.poly);
                    case Type::Genotype: throw error("Unsupported Haplotype*Genotype");
                    case Type::Gamete: throw error("Unsupported Haplotype*Gamete");
                    case Type::Cross: throw error("Unsupported Haplotype*Cross");
                    case Type::Null: throw error("Unsupported Haplotype*Null");
                    case Type::DoublingGamete: throw error("Unsupported Haplotype*DoublingGamete");
                    case Type::Polynom: break;  /* can't happen */
                    case Type::RilPolynom: break; /* can't happen */
                };
                break;
            case Type::Cross:
                switch (other.type) {
                    case Type::Genotype:
                        return Genotype(other.genotype.first % (bool)genotype.first.first.ancestor,
                                        other.genotype.first % (bool)genotype.first.second.ancestor,
                                        other.genotype.second % (bool)genotype.second.first.ancestor,
                                        other.genotype.second % (bool)genotype.second.second.ancestor,
                                        poly * other.poly);
                    case Type::Haplotype: throw error("Unsupported Cross*Haplotype");
                    case Type::Gamete: throw error("Unsupported Cross*Gamete");
                    /*case Type::Cross: throw error("Unsupported Cross*Cross");*/
                    case Type::Cross:
                        return Cross(other.genotype.first % (bool)genotype.first.first.ancestor,
                                     other.genotype.first % (bool)genotype.first.second.ancestor,
                                     other.genotype.second % (bool)genotype.second.first.ancestor,
                                     other.genotype.second % (bool)genotype.second.second.ancestor,
                                     poly * other.poly);
                    case Type::Null: throw error("Unsupported Cross*Null");
                    case Type::DoublingGamete: throw error("Unsupported Cross*DoublingGamete");
                    case Type::Polynom: break;  /* can't happen */
                    case Type::RilPolynom: break; /* can't happen */
                };
                break;
            case Type::Null:
                return *this;
            case Type::RilPolynom: break; /* can't happen */
        };
        return null;
    }
};




struct algebraic_locus {
    enum class Type { AllelePair, Allele, Bool };

    allele_pair locus;
    bool possible;
    Type type;

    algebraic_locus(const algebraic_locus& al)
        : locus(al.locus), possible(al.possible), type(al.type)
    {}

    algebraic_locus(bool x=false)
        : locus({0, 0}), possible(x), type(Type::Bool)
    {}

    algebraic_locus(const allele_pair& ap, bool pos=true)
        : locus(ap), possible(pos), type(Type::AllelePair)
    {}

    algebraic_locus(char allele, bool pos=true)
        : locus({allele, 0}), possible(pos), type(Type::Allele)
    {}

    algebraic_locus& operator *= (const algebraic_locus& al)
    {
        switch (type) {
            case Type::AllelePair:
                switch (al.type) {
                    case Type::Bool:
                        /* gametification */
                        type = Type::Allele;
                        locus.first = al.possible ? locus.second : locus.first;
                        locus.second = 0;
                        break;
                    case Type::AllelePair:
                        throw error("Can't multiply AllelePair with AllelePair.");
                    case Type::Allele:
                        throw error("Can't multiply AllelePair with Allele.");
                };
                break;
            case Type::Allele:
                switch(al.type) {
                    case Type::Bool:
                        throw error("Can't multiply Allele with Bool.");
                    case Type::AllelePair:
                        throw error("Can't multiply Allele with AllelePair.");
                    case Type::Allele:
                        locus.second = al.locus.first;
                        type = Type::AllelePair;
                        possible &= al.possible;
                        break;
                };
                break;
            case Type::Bool:
                switch(al.type) {
                    case Type::Bool:
                        possible &= al.possible;
                        break;
                    case Type::AllelePair:
                        locus.first = possible ? al.locus.second : al.locus.first;
                        locus.second = 0;
                        type = Type::Allele;
                        possible = al.possible;
                        break;
                    case Type::Allele:
                        throw error("Can't multiply Bool with Allele.");
                };
                break;
        };
        return *this;
    }

    algebraic_locus& operator += (const algebraic_locus&)
    {
        /* only authorized when perfect equality between *this and al. Does nothing. */
        /*if (*this == al) {*/
            /*return *this;*/
        /*}*/
        throw error("Can't add two different algebraic_locus.");
    }

    algebraic_locus operator + (const algebraic_locus& al)
    {
        algebraic_locus ret(*this);
        return ret += al;
    }

    algebraic_locus operator * (const algebraic_locus& al)
    {
        algebraic_locus ret(*this);
        return ret *= al;
    }
};




namespace Eigen {
    template<>
    struct NumTraits<algebraic_genotype> {
        typedef algebraic_genotype Real;
        typedef algebraic_genotype NonInteger;
        typedef algebraic_genotype Nested;
        enum {
            IsComplex = 0,
            IsInteger = 1,
            ReadCost = 10,
            WriteCost = 10,
            MulCost = 100,
            AddCost = 1000,
            IsSigned = 0,
            RequireInitialization = 1
        };
        static Real epsilon() { return {{{0, 0}, {0, 0}}, algebraic_genotype::Type::Genotype, fast_polynom::zero}; }
        static Real dummy_precision() { return {{{0, 0}, {0, 0}}, algebraic_genotype::Type::Genotype, fast_polynom::zero}; }
        static algebraic_genotype highest() { return {{{0, 0}, {0, 0}}, algebraic_genotype::Type::Genotype, fast_polynom::infinity}; }
        static algebraic_genotype lowest() { return {{{0, 0}, {0, 0}}, algebraic_genotype::Type::Genotype, fast_polynom::zero}; }
    };

    template<>
    struct NumTraits<algebraic_locus> {
        typedef algebraic_locus Real;
        typedef algebraic_locus NonInteger;
        typedef algebraic_locus Nested;
        enum {
            IsComplex = 0,
            IsInteger = 1,
            ReadCost = 10,
            WriteCost = 10,
            MulCost = 100,
            AddCost = 1000,
            IsSigned = 0,
            RequireInitialization = 1
        };
        /*static Real epsilon() { return {{0, 0}, algebraic_locus::Type::Bool, false}; }*/
        /*static Real dummy_precision() { return {{0, 0}, algebraic_locus::Type::Bool, false}; }*/
        /*static algebraic_locus highest() { return {{0, 0}, algebraic_locus::Type::Bool, true}; }*/
        /*static algebraic_locus lowest() { return {{0, 0}, algebraic_locus::Type::Bool, false}; }*/
    };
}

typedef Eigen::Matrix<algebraic_genotype, Eigen::Dynamic, Eigen::Dynamic> GenoMatrix;
typedef Eigen::Matrix<algebraic_locus, Eigen::Dynamic, Eigen::Dynamic> LocusMatrix;


template <typename T1, typename T2>
struct tie_struc {
    T1& first;
    T2& second;

    tie_struc& operator = (const std::pair<T1, T2>& p)
    {
        first = p.first;
        second = p.second;
        return *this;
    }
};

template <typename T1, typename T2>
static inline tie_struc<T1, T2> tie(T1& a, T2& b)
{
    return {a, b};
}



struct eval_poly_optim {
    std::map<fast_polynom, int> indices;
    std::vector<fast_polynom> polynoms;
    Matrix<int, Dynamic, Dynamic> rebuild;
    size_t pow_max;

    /*GenoMatrix debug_G;*/

    eval_poly_optim()
        : indices(), polynoms(), rebuild(), pow_max(0)
    {}

    void init(const GenoMatrix& G)
    {
        indices.clear();
        rebuild.resize(G.innerSize(), G.outerSize());
        std::set<fast_polynom> uniq;
        int index = 0;
        for (int i = 0; i < G.innerSize(); ++i) {
            for (int j = 0; j < G.outerSize(); ++j) {
                auto is_new = uniq.insert(G(i, j).poly);
                if (is_new.second) {
                    indices.insert({*is_new.first, index});
                    rebuild(i, j) = index;
                    ++index;
                    polynoms.push_back(G(i, j).poly);
                    size_t d = G(i, j).poly.degree();
                    if (d > pow_max) {
                        pow_max = d;
                    }
                } else {
                    rebuild(i, j) = indices[*is_new.first];
                }
            }
        }
        ++pow_max;
        /*pow_max = polynoms[0].degree() + 1;*/
        /*std::cout << "There are " << uniq.size() << " dictinct polynoms in " << (G.innerSize() * G.outerSize()) << " cells. pow_max=" << pow_max << std::endl;*/
        /*debug_G = G;*/
        /*std::cout << debug_G << std::endl;*/
        /*std::cout << rebuild << std::endl;*/
    }

    struct cache {
        const eval_poly_optim* m_epo;
        std::unordered_map<double, MatrixXd> m_cache;
        std::vector<double> r_pow, s_pow;
        std::vector<double> values;

        cache(const eval_poly_optim* et)
            : m_epo(et), m_cache(), r_pow(), s_pow(), values()
        {
            /*std::cout << "new cache for" << std::endl << m_epo->debug_G << std::endl;*/
            values.resize(m_epo->polynoms.size());
            r_pow.resize(m_epo->pow_max);
            s_pow.resize(m_epo->pow_max);
            r_pow[0] = 1.;
            s_pow[0] = 1.;
        }

        cache() : m_epo(0), m_cache(), r_pow(), s_pow(), values() {}

        cache(cache&& tmp)
            : m_epo(tmp.m_epo)
            , m_cache(std::forward<std::unordered_map<double, MatrixXd>>(tmp.m_cache))
            , r_pow(std::forward<std::vector<double>>(tmp.r_pow))
            , s_pow(std::forward<std::vector<double>>(tmp.s_pow))
            , values(std::forward<std::vector<double>>(tmp.values))
        {}

        cache& operator = (cache&& tmp)
        {
            m_epo = tmp.m_epo;
            m_cache.swap(tmp.m_cache);
            r_pow.swap(tmp.r_pow);
            s_pow.swap(tmp.s_pow);
            values.swap(tmp.values);
            return *this;
        }

        MatrixXd& eval(double dist, int order)
        {
            MatrixXd& ret = m_cache[dist];

            if (ret.innerSize() != 0) {
                return ret;
            }

            double r = prob_recomb(dist, order);
            double s = 1. - r;
            double r_tmp = r, s_tmp = s;

            ret.resize(m_epo->rebuild.innerSize(), m_epo->rebuild.outerSize());

            for (size_t i = 1; i < m_epo->pow_max; ++i) {
                r_pow[i] = r_tmp;
                s_pow[i] = s_tmp;
                r_tmp *= r;
                s_tmp *= s;
            }

            for (size_t p = 0; p < m_epo->polynoms.size(); ++p) {
                const impl::f_polynom& poly = m_epo->polynoms[p];
                if (poly.P.size() == 0) {
                    values[p] = 0;
                    continue;
                }
                double accum = r_pow[poly.r_exp] * s_pow[poly.s_exp];
                double tmp = poly.P[0];

                if (r_pow.size() < poly.P.size()) {
                    MSG_DEBUG("poly.P.size = " << poly.P.size() << " r_pow.size = " << r_pow.size());
                    MSG_DEBUG(m_epo->polynoms[p]);
                    throw 0;
                }

                for (size_t i = 1; i < poly.P.size(); ++i) {
                    tmp += poly.P[i] * r_pow[i];
                }
                values[p] = accum * tmp;
            }

            for (int i = 0; i < m_epo->rebuild.innerSize(); ++i) {
                for (int j = 0; j < m_epo->rebuild.outerSize(); ++j) {
                    ret(i, j) = values[m_epo->rebuild(i, j)];
                }
            }

            /*std::cout << "Evaluating" << std::endl << m_epo->debug_G << std::endl;*/
            /*std::cout << "with X=" << dist << std::endl;*/
            /*std::cout << ret << std::endl;*/

            return ret;
        }
    };

    cache new_cache() const { return {this}; }
};




#endif

