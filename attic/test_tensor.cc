/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tensor.h"
#include <iostream>

template <typename S, int N>
void do_chip1(const tensor_view<S, N>& t, size_t d, size_t i)
{
    auto tchip = t.chip(d, i);
    std::cout << tchip << std::endl << tchip.as_matrix() << std::endl << std::endl;
}



int main(int argc, char** argv)
{
    tensor_view<int, 3> t0(std::array<size_t, 3>{2, 3, 4});
    auto x = t0.chip<1>(std::array<size_t, 1>{0}, std::array<size_t, 1>{0});
    auto m = x.as_matrix();
    for (size_t i = 0; i < 2; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            for (size_t k = 0; k < 4; ++k) {
                t0({i, j, k}) = 1 + i + 10 * (j + 1) + 100 * (k + 1);
            }
        }
    }
    std::cout << t0;
    std::cout << x;
    std::cout << m << std::endl << std::endl;
    do_chip1(t0, 0, 1);
    do_chip1(t0, 1, 0);
    do_chip1(t0, 1, 1);
    do_chip1(t0, 1, 2);
    do_chip1(t0, 2, 0);
    do_chip1(t0, 2, 1);
    do_chip1(t0, 2, 2);
    do_chip1(t0, 2, 3);
    return 0;
    (void) argc; (void) argv;
}

