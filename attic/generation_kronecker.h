#ifndef _SPELL_GENERATION_KRONECKER_H_
#define _SPELL_GENERATION_KRONECKER_H_

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "chrono.h"
#include "generation_rs.h"
#include "pedigree.h"

#include <boost/dynamic_bitset.hpp>


GenoMatrix lump_gen(const GenoMatrix& gm);


template <typename MATRIX_TYPE>
struct iterable_generation_base {
    typedef typename MATRIX_TYPE::Scalar scalar_type;
    virtual void reset() = 0;
    virtual bool next() = 0;
    virtual size_t rows() const = 0;
    virtual size_t cols() const = 0;
    virtual size_t row() const = 0;
    virtual size_t col() const = 0;
    virtual const scalar_type& value() const = 0;

    friend std::ostream& operator << (std::ostream& os, std::shared_ptr<iterable_generation_base<MATRIX_TYPE>> igb)
    {
        return os << '[' << igb->row() << ',' << igb->col() << ' ' << igb->value() << ']';
    }
};


template <typename MATRIX_TYPE>
struct iterable_generation : iterable_generation_base<MATRIX_TYPE> {
    typedef typename MATRIX_TYPE::Scalar scalar_type;
    const MATRIX_TYPE* m_gen;
    int m_row;
    int m_col;
    iterable_generation(const MATRIX_TYPE* g) : m_gen(g), m_row(0), m_col(0) {}
    void reset() { m_row = m_col = 0; }
    bool next()
    {
        ++m_row;
        if (m_row >= m_gen->rows()) {
            m_row = 0;
            ++m_col;
            if (m_col >= m_gen->cols()) {
                m_col = 0;
                return true;
            }
        }
        return false;
    }
    size_t rows() const { return m_gen->rows(); }
    size_t cols() const { return m_gen->cols(); }
    size_t row() const { return m_row; }
    size_t col() const { return m_col; }
    const scalar_type& value() const { return (*m_gen)(m_row, m_col); }
};

template <>
struct iterable_generation<VectorLC> : iterable_generation_base<VectorLC> {
    typedef typename VectorLC::Scalar scalar_type;
    VectorLC m_gen;
    int m_row;
    int m_col;
    iterable_generation(const VectorLC& g) : m_gen(g), m_row(0), m_col(0) {}
    void reset() { m_row = m_col = 0; }
    bool next()
    {
        ++m_row;
        if (m_row >= m_gen.rows()) {
            m_row = 0;
            ++m_col;
            if (m_col >= m_gen.cols()) {
                m_col = 0;
                return true;
            }
        }
        return false;
    }
    size_t rows() const { return m_gen.rows(); }
    size_t cols() const { return m_gen.cols(); }
    size_t row() const { return m_row; }
    size_t col() const { return m_col; }
    const scalar_type& value() const { return m_gen(m_row, m_col); }
};


template <typename MATRIX_TYPE> struct generation_gamete;
template <typename MATRIX_TYPE> struct generation_doubling_gamete;
template <typename MATRIX_TYPE> struct generation_proxy;

template <>
struct generation_gamete<GenoMatrix> : iterable_generation<GenoMatrix> {
    generation_gamete() : iterable_generation(&gametes_factory<algebraic_genotype::Type::Gamete>()) {}
};


template <>
struct generation_gamete<VectorLC> : iterable_generation<VectorLC> {
    /*static VectorLC _get_matrix() { static VectorLC _ = Matrix<gencomb_type, 2, 1>::Constant(.5); return &_; }*/
    generation_gamete() : iterable_generation(Matrix<gencomb_type, 2, 1>::Constant(.5)) {}
};


template <>
struct generation_doubling_gamete<GenoMatrix> : iterable_generation<GenoMatrix> {
    generation_doubling_gamete() : iterable_generation(&gametes_factory<algebraic_genotype::Type::DoublingGamete>()) {}
};

template <>
struct generation_doubling_gamete<VectorLC> : iterable_generation<VectorLC> {
    /*static const VectorLC* _get_matrix() { static VectorLC _ = Matrix<gencomb_type, 2, 1>::Constant(.5); return &_; }*/
    generation_doubling_gamete() : iterable_generation(Matrix<gencomb_type, 2, 1>::Constant(.5)) {}
};

template <>
struct generation_proxy<GenoMatrix> : iterable_generation_base<GenoMatrix> {
    std::shared_ptr<iterable_generation_base> target;
    algebraic_genotype tmp;
    generation_proxy(std::shared_ptr<iterable_generation_base> ig) : target(ig) {}
    void reset() {}
    bool next() { return true; }
    size_t rows() const { return 1; }
    size_t cols() const { return 1; }
    size_t row() const { return 0; }
    size_t col() const { return 0; }
    const algebraic_genotype& value() const
    {
        const algebraic_genotype& ag = target->value();
        /*MSG_DEBUG("proxy " << ag);*/
        return *const_cast<algebraic_genotype*>(&tmp) = {ag.genotype, ag.type, fast_polynom::one};
    }
};


template <>
struct generation_proxy<VectorLC> : iterable_generation_base<VectorLC> {
    std::shared_ptr<iterable_generation_base> target;
    algebraic_genotype tmp;
    generation_proxy(std::shared_ptr<iterable_generation_base> ig) : target(ig) {}
    void reset() {}
    bool next() { return true; }
    size_t rows() const { return 1; }
    size_t cols() const { return 1; }
    size_t row() const { return 0; }
    size_t col() const { return 0; }
    const typename VectorLC::Scalar& value() const
    {
        return target->value();
    }
};


enum Operation { PUSH, KRON };

template <typename MATRIX_TYPE, bool DERIVED>
struct generation_kronecker {
    typedef std::vector<std::shared_ptr<iterable_generation_base<MATRIX_TYPE>>> matrix_vector_type;
    typedef typename MATRIX_TYPE::Scalar scalar_type;
    matrix_vector_type matrices;
    std::vector<Operation> ops;
    std::vector<size_t> row_strides;
    std::vector<size_t> col_strides;
    MATRIX_TYPE result;

    generation_kronecker(const matrix_vector_type& vm, const std::vector<Operation>& vo)
        : matrices(vm)
        , ops(vo)
        , result()
    {
        row_strides.reserve(matrices.size() + 1);
        col_strides.reserve(matrices.size() + 1);
        row_strides.push_back(1);
        col_strides.push_back(1);
        for (const auto& m: matrices) {
            row_strides.push_back(row_strides.back() * m->rows());
            col_strides.push_back(col_strides.back() * m->cols());
            m->reset();
            /*MSG_DEBUG("matrix has size (" << m->rows() << ", " << m->cols() << ')');*/
        }
        compute_result();
        /*MSG_DEBUG(MATRIX_SIZE(result));*/
    }
    void reset() { for (auto& m: matrices) { m->reset(); } }

    bool next_cell()
    {
        bool ret = false;
        for (auto& i: matrices) {
            /*MSG_DEBUG("incr");*/
            if (i->next()) {
                /*MSG_DEBUG("overflow");*/
                /*i->reset();*/
            } else {
                /*MSG_DEBUG("ok " << i->row() << ',' << i->col());*/
                ret = true;
                break;
            }
        }
        /*MSG_DEBUG(matrices);*/
        return ret;
    }

    size_t compute_col()
    {
        size_t accum = 0;
        for (size_t i = 0; i < matrices.size(); ++i) {
            accum += col_strides[i] * matrices[i]->col();
        }
        return accum;
    }

    size_t compute_row()
    {
        size_t accum = 0;
        for (size_t i = 0; i < matrices.size(); ++i) {
            accum += row_strides[i] * matrices[i]->row();
        }
        return accum;
    }

    void compute_cell()
    {
        size_t m_i = 0;
        std::vector<scalar_type> stack;
        stack.reserve(matrices.size());
        size_t tmp;
        for (Operation o: ops) {
            switch (o) {
                case PUSH:
                    stack.emplace_back(matrices[m_i]->value());
                    ++m_i;
                    break;
                case KRON:
                    tmp = stack.size() - 2;
                    stack[tmp] = stack[tmp] * stack.back();
                    stack.pop_back();
                    break;
            };
        }
        /* assert(stack.size() == 1); */
        size_t r = compute_row();
        size_t c = compute_col();
        /*MSG_DEBUG("row=" << r << " col=" << c << " ag=" << stack.back());*/
        result(r, c) = stack.back();
    }

    void compute_cell_derived()
    {
        size_t m_i = 0;
        std::vector<scalar_type> stack;
        stack.reserve(matrices.size());
        size_t tmp;
        scalar_type accum = 0;
        for (size_t der_i = 0; der_i < matrices.size(); ++der_i) {
            bool skip = false;
            for (size_t i = 0; i < matrices.size(); ++i) {
                skip |= (der_i != i && matrices[m_i]->row() != matrices[m_i]->col());
            }
            if (skip) {
                continue;
            }
            for (Operation o: ops) {
                switch (o) {
                    case PUSH:
                        if (m_i == der_i) {
                            stack.emplace_back(matrices[m_i]->value());
                        } else {
                            stack.emplace_back(1);
                        }
                        ++m_i;
                        break;
                    case KRON:
                        tmp = stack.size() - 2;
                        stack[tmp] = stack[tmp] * stack.back();
                        stack.pop_back();
                        break;
                };
            }
            accum += stack.back();
        }
        /* assert(stack.size() == 1); */
        size_t r = compute_row();
        size_t c = compute_col();
        /*MSG_DEBUG("row=" << r << " col=" << c << " ag=" << accum);*/
        result(r, c) = accum;
    }

    void compute_result();
};


template <typename MATRIX_TYPE>
    generation_kronecker<MATRIX_TYPE, false>::compute_result()
    {
        result.resize(row_strides.back(), col_strides.back());
        /*MSG_DEBUG(matrices);*/
        /*reset();*/
        do {
            compute_cell();
        } while (next_cell());
        /*if (strides.back() < 1024) {*/
            /*result = lump_gen(result);*/
        /*}*/
    }


template <typename MATRIX_TYPE>
    generation_kronecker<MATRIX_TYPE, true>::compute_result()
    {
        result.resize(row_strides.back(), col_strides.back());
        /*MSG_DEBUG(matrices);*/
        /*reset();*/
        do {
            compute_cell_derived();
        } while (next_cell());
        /*if (strides.back() < 1024) {*/
            /*result = lump_gen(result);*/
        /*}*/
    }


template <bool DERIVED=false>
struct generation_kronecker<geno_matrix, DERIVED> {
    geno_matrix result;
    generation_kronecker(const std::vector<const geno_matrix*>& vm, const std::vector<Operation>& vo)
    {
        std::vector<geno_matrix_variant_type> stack;
        auto i = vm.begin();
        for (auto op: vo) {
            if (op == PUSH) {
                stack.push(i->variant);
                ++i;
            } else {
                auto v2 = stack.back();
                stack.pop_back();
                auto v1 = stack.back();
                stack.pop_back();
                stack.push_back(geno_matrix_variant_table[v2][v1]);
            }
        }
        result.variant = stack.back();

        
    }
};


typedef std::map<std::string, generation_rs*> generation_table;
struct kron_component_description_base;
typedef std::vector<std::pair<std::string, size_t>> individual_list;


/*
bool operator < (const std::pair<std::string, size_t>& p1, const std::pair<std::string, size_t>& p2)
{
    return p1.first < p2.first || (p1.first == p2.first && p1.second < p2.second);
}
//*/

template <typename MATRIX_TYPE>
struct kron_builder {
    std::vector<std::shared_ptr<iterable_generation_base<MATRIX_TYPE>>> iterators;
    std::map<std::string, std::shared_ptr<iterable_generation_base<MATRIX_TYPE>>> proxy_targets;
    std::vector<Operation> operations;
    void push_iterator(const MATRIX_TYPE* gen, const std::string& name)
    {
        if (name == "") {
            iterators.emplace_back(std::make_shared<iterable_generation<MATRIX_TYPE>>(&*gen));
        } else {
            auto it = proxy_targets.find(name);
            if (it == proxy_targets.end()) {
                iterators.emplace_back(std::make_shared<iterable_generation<MATRIX_TYPE>>(&*gen));
                proxy_targets[name] = iterators.back();
            } else {
                iterators.emplace_back(std::make_shared<generation_proxy<MATRIX_TYPE>>(it->second));
            }
        }
    }
    void push_gamete() { iterators.emplace_back(std::make_shared<generation_gamete<MATRIX_TYPE>>()); }
    void push_doubling_gamete() { iterators.emplace_back(std::make_shared<generation_doubling_gamete<MATRIX_TYPE>>()); }
    void op_push() { operations.push_back(PUSH); }
    void op_kron() { operations.push_back(KRON); }
};


template <>
struct kron_builder<VectorLC> {
    std::vector<std::shared_ptr<iterable_generation_base<VectorLC>>> iterators;
    std::map<std::string, std::shared_ptr<iterable_generation_base<VectorLC>>> proxy_targets;
    std::vector<Operation> operations;
    void push_iterator(const VectorLC& gen, const std::string& name)
    {
        if (name == "") {
            iterators.emplace_back(std::make_shared<iterable_generation<VectorLC>>(gen));
        } else {
            auto it = proxy_targets.find(name);
            if (it == proxy_targets.end()) {
                iterators.emplace_back(std::make_shared<iterable_generation<VectorLC>>(gen));
                proxy_targets[name] = iterators.back();
            } else {
                iterators.emplace_back(std::make_shared<generation_proxy<VectorLC>>(it->second));
            }
        }
    }
    void push_gamete() { iterators.emplace_back(std::make_shared<generation_gamete<VectorLC>>()); }
    void push_doubling_gamete() { iterators.emplace_back(std::make_shared<generation_doubling_gamete<VectorLC>>()); }
    void op_push() { operations.push_back(PUSH); }
    void op_kron() { operations.push_back(KRON); }
};


template <typename MATRIX_TYPE>
std::ostream& operator << (std::ostream& os, const kron_builder<MATRIX_TYPE>& kb)
{
    int i = -1;
    for (Operation o: kb.operations) {
        switch (o) {
            case PUSH:
                os << " PUSH ";
                ++i;
                if (dynamic_cast<const generation_gamete<MATRIX_TYPE>*>(&*kb.iterators[i])) {
                    os << "g";
                } else if (dynamic_cast<const generation_doubling_gamete<MATRIX_TYPE>*>(&*kb.iterators[i])) {
                    os << "d";
                } else if (dynamic_cast<const iterable_generation<MATRIX_TYPE>*>(&*kb.iterators[i])) {
                    os << "i";
                } else if (dynamic_cast<const generation_proxy<MATRIX_TYPE>*>(&*kb.iterators[i])) {
                    os << "p";
                } else {
                    os << '?';
                }
                break;
            case KRON:
                os << " KRON";
                break;
        };
    }
    return os;
}


struct ascendant_categories {
    std::vector<int> bound;
    std::vector<int> free;
    ascendant_categories(int m, const std::vector<int>& M, int p, const std::vector<int>& P)
        : bound(M.size() + P.size() + 2), free(M.size() + P.size() + 2)
    {
        std::vector<int> Mi = M;
        if (m) {
            Mi.push_back(m);
        }
        std::vector<int> Pi = P;
        if (p) {
            Pi.push_back(p);
        }
        auto bi = std::set_intersection(Mi.begin(), Mi.end(), Pi.begin(), Pi.end(), bound.begin());
        bound.resize(bi - bound.begin());
        auto fi = std::set_symmetric_difference(Mi.begin(), Mi.end(), Pi.begin(), Pi.end(), free.begin());
        free.resize(fi - free.begin());
    }
};


typedef std::map<int, std::pair<int, int>> genealogy_type;
typedef std::map<int, std::vector<int>> ancestor_set_map_type;
typedef boost::dynamic_bitset<> branch_type;

bool find_branch_rec(int start, const std::set<int>& target, const genealogy_type& genealogy, int depth, branch_type& path);
bool find_branch_rec(int start, int target, const genealogy_type& genealogy, int depth, branch_type& path);


branch_type find_branch(int start, const std::set<int>& target, const genealogy_type& genealogy);
branch_type find_branch(int start, int target, const genealogy_type& genealogy, bool init);

inline
std::ostream& operator << (std::ostream& os, const branch_type& b)
{
    /*for (size_t i = b.find_first(); i != branch_type::npos; i = b.find_next(i)) {*/
    for (size_t i = 0; i < b.size(); ++i) {
        os << (b[i] ? 'P' : 'M');
    }
    return os;
}


std::vector<int> keep_only_nearest_degree(const ancestor_set_map_type& ancestor_sets, const std::vector<int>& set);


struct individual_constraints {
    std::vector<std::pair<branch_type, branch_type>> constraints;
    individual_constraints(const genealogy_type& genealogy, const ancestor_set_map_type& ancestor_sets, int p1, int p2)
        : constraints()
    {
        ascendant_categories ac(p1, ancestor_sets.find(p1)->second, p2, ancestor_sets.find(p2)->second);
        /* pick only "first-degree" common and free ancestors */
        /*MSG_DEBUG("ANCESTORS OF " << p1 << ": " << ancestor_sets.find(p1)->second);*/
        /*MSG_DEBUG("ANCESTORS OF " << p2 << ": " << ancestor_sets.find(p2)->second);*/
        /*MSG_DEBUG("ac.free " << ac.free);*/
        /*MSG_DEBUG("ac.bound " << ac.bound);*/
        ac.bound = keep_only_nearest_degree(ancestor_sets, ac.bound);
        ac.free = keep_only_nearest_degree(ancestor_sets, ac.free);
        /*MSG_DEBUG("nearest ac.free " << ac.free);*/
        /*MSG_DEBUG("nearest ac.bound " << ac.bound);*/
        /* compute constraints */
        for (int a: ac.bound) {
            constraints.emplace_back(find_branch(p1, a, genealogy, false), find_branch(p2, a, genealogy, true));
        }
    }

    individual_constraints() : constraints() {}

    individual_constraints(const std::vector<std::pair<std::string, std::string>>& cons)
        : constraints()
    {
        /* TODO: compute constraint bitsets from strings */
        constraints.resize(cons.size());
        for (size_t c = 0; c < cons.size(); ++c) {
            const auto& ps = cons[c];
            branch_type& b1 = constraints[c].first;
            branch_type& b2 = constraints[c].second;
            b1.resize(ps.first.size());
            b2.resize(ps.second.size());
            for (size_t i = 0; i < ps.first.size(); ++i) { b1.set(i, ps.first[i] == 'P'); }
            for (size_t i = 0; i < ps.second.size(); ++i) { b2.set(i, ps.second[i] == 'P'); }
        }
    }

    friend std::ostream& operator << (std::ostream& os, const individual_constraints& ic)
    {
        if (ic.constraints.size() > 0) {
            auto i = ic.constraints.begin();
            auto j = ic.constraints.end();
            os << i->first << '=' << i->second;
            for (++i; i != j; ++i) {
                os << ", " << i->first << '=' << i->second;
            }
        }
        return os;
    }
};


enum CrossType { Ancestor, Cross, DH };

struct generation_spec_type : individual_constraints {
    std::string name;
    CrossType type;
    std::string fp1, fp2;
    generation_spec_type(const std::string& n, const genealogy_type& genealogy, const ancestor_set_map_type& ancestor_sets, int p1, int p2, const std::map<int, std::string>& family_by_id)
        : individual_constraints(genealogy, ancestor_sets, p1, p2)
        , name(n)
        , type(p2 == 0 ? CrossType::DH : CrossType::Cross)
        , fp1(family_by_id.find(p1)->second), fp2(p2 > 0 ? family_by_id.find(p2)->second : "")
    {}

    generation_spec_type(const std::string& n, char haplo)
        : individual_constraints()
        , name(n)
        , type(CrossType::Ancestor), fp1(std::string() + haplo), fp2()
    {}
};


typedef std::map<std::string, generation_spec_type> ped_design_type;



struct gen_tree_node_type;
typedef std::shared_ptr<gen_tree_node_type> gen_tree_node;
struct gen_tree_node_type {
    std::string gen_name;
    gen_tree_node p1, p2;
    const generation_rs* pop;
    std::string name;
};

gen_tree_node make_tree(const ped_design_type& design, const generation_table& generations, const std::string& entrypoint, bool do_constraints=false);

generation_rs* tree_to_kron(const std::string& name, gen_tree_node node);

std::map<std::string, std::vector<int>>
pedigree_analysis(const std::vector<pedigree_item>& pedigree, ped_design_type& design,
                  generation_table& generations, std::map<size_t, const generation_rs*>& ped_gen,
                  genealogy_type& genealogy,
                  std::map<size_t, individual_constraints>& constraint_table);


#endif

