/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cache/base.h"
#include "geno_matrix.h"
#include "disk_hashtable.h"

int fibo(int n)
{
    if (n <= 2) {
        return 1;
    }
    return fibo(n - 1) + fibo(n - 2);
}


int fast_fibo(int n)
{
    static disk_hashtable<decltype(fibo)> dht("/tmp/fibo");
    std::function<int(int)> rec_fibo
        = [&] (int n) { return n < 1
                               ? 0
                               : n < 3
                                 ? 1
                                 : fast_fibo(n - 1) + fast_fibo(n - 2); };
    return dht.get_or_compute(rec_fibo, std::move(n));
    /*std::pair<bool, int> ret = dht.get(std::move(n));*/
    /*if (!ret.first) {*/
        /*ret.second = fast_fibo(n - 1) + fast_fibo(n - 2);*/
        /*dht.put(ret.second, std::move(n));*/
    /*}*/
    /*return ret.second;*/
}

int main(int argc, char** argv)
{
    /*
    disk_hashtable<decltype(fibo)> dht("/tmp/fibo");
    for (int i = 1; i < 20; ++i) {
        dht.put(fibo(i), std::move(i));
    }

    for (int i = 1; i < 21; ++i) {
        std::pair<bool, int> ret = dht.get(std::move(i));
        MSG_DEBUG('#' << i << " = " << ret.second);
    }
    */
    for (int i = 20; i > 0; --i) {
        MSG_DEBUG("fibo(" << i << ") = " << fast_fibo(i));
    }
    MSG_DEBUG("* * *");
    for (int i = 0; i < 50; ++i) {
        MSG_DEBUG("fibo(" << i << ") = " << fast_fibo(i));
    }
    return 0;
}

