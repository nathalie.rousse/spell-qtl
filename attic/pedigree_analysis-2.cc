/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>
#include <iomanip>
#include "file.h"
#include "chrono.h"
#include "generation_rs.h"
#include "generation_kronecker.h"

#include <boost/dynamic_bitset.hpp>


GenoMatrix lump_gen(const GenoMatrix& gm)
{
    /*MSG_DEBUG("PRE LUMP " << MATRIX_SIZE(gm));*/
    /*MSG_DEBUG(gm);*/
    GenerationRS tmp = convert(gm);
    ::lumper<GenerationRS, exact_compare<algebraic_genotype>> l(tmp);
    auto partition = l.refine_all();
    /*auto partition2 = l.try_refine(partition);*/
    /*MSG_DEBUG(partition);*/
    /*MSG_DEBUG(partition2);*/
    auto ret_lump = l.to_matrix(partition);
    /*MSG_DEBUG("POST LUMP " << MATRIX_SIZE(ret_lump));*/
    return ret_lump.data;
}


void dump_mat(const generation_rs* grs)
{
    /*GenoMatrix lum = lump_gen(tmp);*/
    /*MSG_DEBUG("=========================================================");*/
    /*MSG_DEBUG("RAW " << MATRIX_SIZE(tmp));*/
    /*MSG_DEBUG(tmp);*/
    /*MSG_DEBUG("LUMPED " << MATRIX_SIZE(lum));*/
    /*MSG_DEBUG(lum);*/
    /*MSG_DEBUG("ROWSUMS " << lum.rowwise().sum().transpose());*/
    /*MSG_DEBUG("COLSUMS " << lum.colwise().sum());*/
    /*MSG_DEBUG("=========================================================");*/
    /*generation_rs* grs = generation_rs::from_matrix(name, tmp);*/
    MSG_DEBUG((*grs));
#if 0
    if (grs->name.size() < 60) {
        return;
    }
    std::map<char, VectorXd> LVtable;
    allele_pair A = {'a', 'a'};
    allele_pair B = {'b', 'b'};
    LVtable['A'] = VectorXd::Zero(grs->main_process().innerSize());
    LVtable['B'] = VectorXd::Zero(grs->main_process().innerSize());
    LVtable['H'] = VectorXd::Zero(grs->main_process().innerSize());
    for (size_t i = 0; i < grs->unique_labels.size(); ++i) {
        allele_pair ap = grs->unique_labels[i];
        if (ap == A) {
            LVtable['A'] = grs->unique_unphased_LV[i];
        } else if (ap == B) {
            LVtable['B'] = grs->unique_unphased_LV[i];
        } else {
            LVtable['H'] += grs->unique_unphased_LV[i];
        }
    }
    for (const auto& kv: LVtable) {
        MSG_DEBUG(kv.first << " => " << kv.second.transpose());
    }
    std::vector<char> obs = {'A', 'H', 'B'};
    chromosome chr = {
        "test_vs_sim",
        {"M_1_1", "M_1_9", "M_1_100"},
        {0., 20., 100.}
    };
    qtl_chromosome qtl_chr(&chr);
    auto sc = grs->segment_computer(&qtl_chr, 1, 0);
    MatrixXd LV(grs->main_process().innerSize(), 3);
    for (char m1: obs) {
        for (char m2: obs) {
            for (char m3: obs) {
                LV.col(0) = LVtable[m1];
                LV.col(1) = LVtable[m2];
                LV.col(2) = LVtable[m3];
                auto probs = sc.compute(LV);
                /*MSG_DEBUG("############# " << m1 << m2 << m3);*/
                /*MSG_DEBUG(probs.transpose());*/
                ofile o(SPELL_STRING("spell_" << grs->name << '_' << m1 << m2 << m3 << ".txt"));
                o << probs.transpose();
            }
        }
    }
#endif
}


inline
std::vector<int> merge(const std::vector<int>& s1, const std::vector<int>& s2)
{
    std::vector<int> ret(s1.size() + s2.size());
    auto it = std::set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), ret.begin());
    ret.resize(it - ret.begin());
    return ret;
}


bool find_branch_rec(int start, const std::set<int>& target, const genealogy_type& genealogy, int depth, branch_type& path)
{
    const auto& p = genealogy.find(start)->second;
    if (target.find(p.first) != target.end()) {
        path.resize(depth + 1);
        path.reset(depth);
        return true;
    } else if (target.find(p.second) != target.end()) {
        path.resize(depth + 1);
        path.set(depth);
        return true;
    } else if (find_branch_rec(p.first, target, genealogy, depth + 1, path)) {
        path.reset(depth);
        return true;
    } else if (find_branch_rec(p.second, target, genealogy, depth + 1, path)) {
        path.set(depth);
        return true;
    }
    return false;
}


bool find_branch_rec(int start, int target, const genealogy_type& genealogy, int depth, branch_type& path)
{
    if (start == 0) {
        return false;
    }
    const auto& p = genealogy.find(start)->second;
    if (target == p.first) {
        path.resize(depth + 1);
        path.reset(depth);
        return true;
    } else if (target == p.second) {
        path.resize(depth + 1);
        path.set(depth);
        return true;
    } else if (find_branch_rec(p.first, target, genealogy, depth + 1, path)) {
        path.reset(depth);
        return true;
    } else if (find_branch_rec(p.second, target, genealogy, depth + 1, path)) {
        path.set(depth);
        return true;
    }
    return false;
}


branch_type find_branch(int start, const std::set<int>& target, const genealogy_type& genealogy)
{
    branch_type branch;
    find_branch_rec(start, target, genealogy, 0, branch);
    return branch;
}

branch_type find_branch(int start, int target, const genealogy_type& genealogy, bool init)
{
    branch_type branch;
    if (start == target) {
        branch.resize(1);
        branch.set(0, init);
    } else {
        find_branch_rec(start, target, genealogy, 1, branch);
        branch.set(0, init);
    }
    return branch;
}

#if 0
std::ostream& operator << (std::ostream& os, const branch_type& b)
{
    /*for (size_t i = b.find_first(); i != branch_type::npos; i = b.find_next(i)) {*/
    for (size_t i = 0; i < b.size(); ++i) {
        os << (b[i] ? 'P' : 'M');
    }
    return os;
}
#endif


/*std::ostream& operator << (std::ostream& os, const std::vector<int>& v) { auto i = v.begin(), j = v.end(); if (i != j) { os << (*i); for (++i; i != j; ++i) { os << ' ' << (*i); } } return os; }*/


std::vector<int> keep_only_nearest_degree(const ancestor_set_map_type& ancestor_sets, const std::vector<int>& set)
{
    std::vector<int> all_ancestors;
    /*if (p1 == p2) { all_ancestors = {p1}; } else if (p1 < p2) { all_ancestors = {p1, p2}; } else { all_ancestors = {p2, p1}; }*/
    for (int a: set) {
        /*MSG_DEBUG("JOINING SUBTREE FOR " << a);*/
        std::vector<int> tmp(ancestor_sets.size());
        const auto& as = ancestor_sets.find(a)->second;
        /*MSG_DEBUG(as.size());*/
        auto it = std::set_union(as.begin(), as.end(), all_ancestors.begin(), all_ancestors.end(), tmp.begin());
        /*MSG_DEBUG("TMP SIZE IS " << (it - tmp.begin()));*/
        tmp.resize(it - tmp.begin());
        all_ancestors.swap(tmp);
    }
    /*MSG_DEBUG("all_ancestors " << all_ancestors);*/

    /* diff with set of ancestors */
    std::vector<int> tmp(ancestor_sets.size());
    auto it = std::set_difference(set.begin(), set.end(), all_ancestors.begin(), all_ancestors.end(), tmp.begin());
    tmp.resize(it - tmp.begin());
    return tmp;
}


template <typename... Args>
gen_tree_node make_tree_node(Args&&... args) { return std::make_shared<gen_tree_node_type>(args...); }

gen_tree_node make_tree(const ped_design_type& design, const generation_table& generations, const std::string& entrypoint, bool do_constraints)
{
    gen_tree_node ret = make_tree_node();
    const auto& d = design.find(entrypoint)->second;
    ret->gen_name = d.name;
    auto it = generations.find(entrypoint);
    if (it != generations.end() && it->second) {
        ret->pop = it->second;
        /*MSG_DEBUG("INIT POP " << entrypoint);*/
        /*MSG_DEBUG(it->second);*/
    } else {
        ret->pop = NULL;
        ret->p1 = make_tree(design, generations, d.fp1);
        if (d.fp2 != "") {
            ret->p2 = make_tree(design, generations, d.fp2);
        }
    }

    if (!do_constraints) {
        return ret;
    }

    int cons_index = 0;

    auto follow_branch = [&] (const branch_type& br)
    {
        gen_tree_node ptr = ret;
        for (size_t i = 0; i < br.size(); ++i) {
            const auto& ptrdesign = design.find(ptr->gen_name)->second;
            if (br[i]) {
                /* expand father */
                if (!ptr->p2) {
                    ptr->p2 = make_tree(design, generations, ptrdesign.fp2, false);
                }
                if (!ptr->p1) {
                    ptr->p1 = make_tree(design, generations, ptrdesign.fp1, false);
                }
                ptr = ptr->p2;
            } else {
                /* expand mother */
                if (!ptr->p1) {
                    ptr->p1 = make_tree(design, generations, ptrdesign.fp1, false);
                }
                if (!ptr->p2) {
                    ptr->p2 = make_tree(design, generations, ptrdesign.fp2, false);
                }
                ptr = ptr->p1;
            }
        }
        ptr->name = SPELL_STRING("constraint#" << cons_index);
    };

    for (const auto& cp: d.constraints) {
        follow_branch(cp.first);
        follow_branch(cp.second);
        ++cons_index;
    }
    return ret;
}


template <typename MATRIX_TYPE> struct node_data_getter;
template <> struct node_data_getter<GenoMatrix> { const GenoMatrix* operator () (gen_tree_node n, size_t) const { return n->pop ? &n->pop->main_process().data : NULL; } };
template <> struct node_data_getter<VectorLC> { VectorLC operator () (gen_tree_node n, size_t i) const { return n->pop->raw_lincomb(i); } };


template <typename MATRIX_TYPE>
size_t rec_build_kron(gen_tree_node node, kron_builder<MATRIX_TYPE>& kb, size_t parent_i = 0)
{
    if (node->p1 && node->p2) {
        parent_i = rec_build_kron(node->p1, kb, parent_i);
        kb.push_gamete();
        kb.op_push();
        kb.op_kron();
        parent_i = rec_build_kron(node->p2, kb, parent_i);
        kb.push_gamete();
        kb.op_push();
        kb.op_kron();
        kb.op_kron();
    } else if (node->p1) {
        parent_i = rec_build_kron(node->p1, kb, parent_i);
        kb.push_doubling_gamete();
        kb.op_push();
        kb.op_kron();
    } else {
        kb.push_iterator(node_data_getter<MATRIX_TYPE>()(node, parent_i), node->name);
        kb.op_push();
    }
    return parent_i + 1;
}


std::ostream& operator << (std::ostream& os, gen_tree_node node)
{
    if (node->p1 && node->p2) {
        os << '(' << node->p1 << " ⊗ G ⊗ " << node->p2 << " ⊗ G)";
    } else {
        os << node->gen_name;
    }
    if (node->name != "") {
        os << '#' << node->name;
    }
    return os;
}


generation_rs* tree_to_kron(const std::string& name, gen_tree_node node)
{
    MSG_DEBUG("TREE " << node);
    /*MSG_QUEUE_FLUSH();*/
    kron_builder<GenoMatrix> kb;
    kron_builder<VectorLC> kb_lc;
    rec_build_kron(node, kb);
    /*MSG_DEBUG("KB" << kb);*/
    /*MSG_QUEUE_FLUSH();*/
    rec_build_kron(node, kb_lc);
    auto ret = generation_kronecker<GenoMatrix>(kb.iterators, kb.operations).result;
    VectorLC lc = generation_kronecker<VectorLC>(kb_lc.iterators, kb_lc.operations).result;
    /*MSG_DEBUG("COMPUTED LC");*/
    /*MSG_DEBUG(lc.transpose());*/
    return generation_rs::from_matrix(name, ret, lc);
}


std::ostream& operator << (std::ostream& os, const std::set<std::string>& nl)
{
    os << '{';
    if (nl.size()) {
        auto i = nl.begin();
        auto j = nl.end();
        os << (*i);
        for (++i; i != j; ++i) {
            os << ' ' << (*i);
        }
    }
    return os << '}';
}

GenoMatrix
create_ancestor(char haplo) {
    GenoMatrix tmp(1, 1);
    tmp(0, 0) = {{{{haplo}, {haplo}}, {{haplo}, {haplo}}},
        algebraic_genotype::Type::Genotype,
        fast_polynom::one};
    return tmp;
}


std::map<std::string, std::vector<int>>
pedigree_analysis(const std::vector<pedigree_item>& pedigree, ped_design_type& design,
                  generation_table& generations, std::map<size_t, const generation_rs*>& ped_gen,
                  genealogy_type& genealogy,
                  std::map<size_t, individual_constraints>& constraint_table)
{
    /*genealogy_type*/ genealogy = {{0, {0, 0}}};
    ancestor_set_map_type ancestor_sets = {{0, {}}};
    std::map<std::string, std::vector<int>> ret;
    std::map<int, int> depth;
    std::map<int, std::string> family_by_id;
    std::string fname;
    auto new_gen = [&]() { return design.find(fname) == design.end(); };
    char haplo = 'a';

    std::set<std::string> families;
    std::set<std::string> nl;

    for (const auto& pi: pedigree) {
        genealogy[pi.id] = {pi.p1, pi.p2};
        MSG_DEBUG("INDIVIDUAL " << pi.id << " / " << pi.p1 << ',' << pi.p2);
        scoped_indent _si;
        individual_constraints& ic = constraint_table[pi.id] = {genealogy, ancestor_sets, pi.p1, pi.p2};
        /*MSG_DEBUG("constraints for " << pi.id << ": " << ic);*/
        ret[pi.gen_name].push_back(pi.id);
        /*if (pi.is_delete()) {*/
            /*generations.erase(family_by_id[pi.p1]);*/
        /*}*/
        if (pi.is_ancestor()) {
            ancestor_sets.insert({pi.id, {}});
            fname = family_by_id[pi.id] = pi.gen_name;
            if (new_gen()) {
                const auto& this_design = design.insert({fname, {fname, haplo}}).first->second;
                nl.clear();
                /*generations[fname] = generation_rs::from_matrix(fname, create_ancestor(haplo));*/
                generations[fname] = generation_rs::ancestor(fname, haplo, {{0, 0}});
                /*dump_mat(generations[fname]);*/
                ++haplo;
            }
        } else if (pi.is_self()) {
            ancestor_sets.insert({pi.id, merge(ancestor_sets[pi.p1], {pi.p1})});
            fname = family_by_id[pi.id] = SPELL_STRING('S' << family_by_id[pi.p1]);
            if (new_gen()) {
                const auto& this_design = design.insert({fname, {fname, genealogy, ancestor_sets, pi.p1, pi.p2, family_by_id}}).first->second;
                nl.clear();
                generations[fname] = tree_to_kron(fname, make_tree(design, generations, fname, true));
                /*dump_mat(generations[fname]);*/
            }
        } else if (pi.is_dh()) {
            ancestor_sets.insert({pi.id, merge(ancestor_sets[pi.p1], {pi.p1})});
            fname = family_by_id[pi.id] = SPELL_STRING("D" << family_by_id[pi.p1]);
            if (new_gen()) {
                /*design[fname] = design[family_by_id[pi.p1]]->to_doubled_haploid(fname);*/
                const auto& this_design = design.insert({fname, {fname, genealogy, ancestor_sets, pi.p1, pi.p2, family_by_id}}).first->second;
                nl.clear();
                /*generations[fname] = generation_computer_type(design, this_design, generations, nl).gen;*/
                generations[fname] = tree_to_kron(fname, make_tree(design, generations, fname, true));
                /*dump_mat(generations[fname]);*/
            }
        } else if (pi.is_cross()) {
            int a, b;
            a = pi.p1 < pi.p2 ? pi.p1 : pi.p2;
            b = pi.p1 + pi.p2 - a;
            ancestor_sets.insert({pi.id, merge(ancestor_sets[pi.p1], merge(ancestor_sets[pi.p2], {a, b}))});
            if (ic.constraints.size()) {
                fname = family_by_id[pi.id] = SPELL_STRING('(' << family_by_id[pi.p1] << " * " << family_by_id[pi.p2] << ')' << '[' << ic << ']');
                const auto& this_design = design.insert({fname, {fname, genealogy, ancestor_sets, pi.p1, pi.p2, family_by_id}}).first->second;
                nl.clear();
                /*generations[fname] = generation_computer_type(design, this_design, generations, nl).gen;*/
                generations[fname] = tree_to_kron(fname, make_tree(design, generations, fname, true));
                /*dump_mat(generations[fname]);*/
            } else {
                fname = family_by_id[pi.id] = SPELL_STRING('(' << family_by_id[pi.p1] << " * " << family_by_id[pi.p2] << ')');
                const auto& this_design = design.insert({fname, {fname, genealogy, ancestor_sets, pi.p1, pi.p2, family_by_id}}).first->second;
                nl.clear();
                /*generations[fname] = generation_computer_type(design, this_design, generations, nl).gen;*/
                generations[fname] = tree_to_kron(fname, make_tree(design, generations, fname, true));
                /*dump_mat(generations[fname]);*/
            }
            if (new_gen()) {
                /*design[fname] = design[family_by_id[pi.p1]]->crossing(fname, design[family_by_id[pi.p2]]);*/
            }
        }
        ped_gen[pi.id] = generations[fname];
        families.insert(fname);
    }
    /*MSG_DEBUG("FAMILIES");*/
    /*for (const std::string& f: families) {*/
        /*MSG_DEBUG(" * " << f);*/
        /*MSG_DEBUG((*generations[f]));*/
    /*}*/
    return ret;
}


#ifdef TESTING_PEDIGREE_ANALYSIS_ONLY
int main(int argc, char** argv)
{
    /*generation_rs::clear_dict();*/
    /*test_kron();*/
    std::vector<pedigree_item> ped = {
        {"B", 2, 0, 0},
        {"A", 1, 0, 0},
        {"F1", 3, 1, 2},
        {"F1", 4, 1, 2},
        {"F2", 5, 3, 4},
        {"F2", 6, 3, 4},
        {"F3", 7, 5, 6},
        {"F3", 8, 5, 6},
        {"F3", 9, 5, 5},
        /*{"F4", 10, 9, 9},*/
        /*{"del A", -1, 1, 0},*/
        /*{"del B", -1, 2, 0},*/
        {"F4", 20, 7, 8},
        /*{"F4", 21, 7, 8},*/
        /*{"F5", 30, 20, 21},*/
        /*{"F5", 31, 20, 21},*/
        /*{"F6", 40, 30, 31},*/
    };
    {
        /*generation_rs::clear_dict();*/
        ped_design_type design;
        generation_table generations;
        pedigree_analysis(ped, design, generations);
    }
    return 0;
    {
        (void) argc;
        auto ped = read_csv(argv[1]);
        ped_design_type design;
        generation_table generations;
        pedigree_analysis(ped, design, generations);
    }
    return 0;
}
#endif

