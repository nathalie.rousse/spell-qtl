/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPEL_FRONTENDS_H_
#define _SPEL_FRONTENDS_H_

#include "../include/cache2.h"
#include "../include/computations/basic_data.h"
#include "../include/computations/model.h"

std::pair<bool, double>
detect_strongest_qtl(chromosome_value chr, const locus_key& lk,
                     const model& M0, const std::vector<double> pos);

MatrixXd
ftest_along_chromosome(chromosome_value chr, const locus_key& lk,
                       const model& M0, const std::vector<double> pos);


struct locus_selection {
    std::map<std::string, locus_key> chromosome_selections;
    const chromosome* chromosome_under_study;

    void add_locus(const std::string& chr_name, double locus)
    {
        auto it = chromosome_selections.find(chr_name);
        if (it == chromosome_selections.end()) {
            /*chromosome_selections.insert({chr_name, locus_key(new locus_key_struc({locus})}));*/
            chromosome_selections[chr_name] = locus_key(new locus_key_struc({locus}));
        } else {
            it->second = it->second + locus;
        }
    }

    locus_key& get_lk(const std::string& name) { return chromosome_selections[name]; }
    locus_key& get_lk(const chromosome* chr) { return chromosome_selections[chr->name]; }

    void remove_locus(const std::string& chr_name, double locus)
    {
        auto it = chromosome_selections.find(chr_name);
        if (it != chromosome_selections.end()) {
            it->second = it->second - locus;
        }
    }

    bool operator == (const locus_selection& ls) const
    {
        return chromosome_under_study == ls.chromosome_under_study
            && chromosome_selections == ls.chromosome_selections;
    }

    bool operator != (const locus_selection& ls) const
    {
        return chromosome_under_study != ls.chromosome_under_study
            || chromosome_selections != ls.chromosome_selections;
    }

    locus_selection& operator += (const std::pair<std::string, double>& loc)
    {
        add_locus(loc.first, loc.second);
        return *this;
    }

    locus_selection& operator -= (const std::pair<std::string, double>& loc)
    {
        remove_locus(loc.first, loc.second);
        return *this;
    }

    locus_selection& operator += (double loc)
    {
        add_locus(chromosome_under_study->name, loc);
        return *this;
    }

    locus_selection& operator -= (double loc)
    {
        remove_locus(chromosome_under_study->name, loc);
        return *this;
    }
};



struct selection {
    std::vector<model_block_key> keys;
};




struct model_manager {
    locus_selection selection;
    /*std::unordered_map<const chromosome*, value<model_block_type>> model_blocks;*/
    /*std::unordered_map<const population*, collection<parental_origin_per_locus_type>> pop_blocks;*/
    /*std::unordered_map<const population*, size_t> n_par;*/
    collection<population_value> pops;
    std::vector<collection<parental_origin_per_locus_type>> pop_blocks;
    std::vector<size_t> n_par;
    model Mcurrent;
    double threshold;
    ComputationType test_type;
    std::map<const chromosome*, value<std::vector<double>>> all_loci;
    value<std::vector<double>> loci;
    /*collection<model_block_type> locus_blocks;*/
    model_block_collection locus_blocks;
    computation_along_chromosome cac;
    MatrixXd* last_computation;

    model_manager(const collection<population_value>& colpop,
                  const value<MatrixXd>& y, double th,
                  ComputationType ct = ComputationType::FTest,
                  SolverType st = SolverType::QR)
        : pops(colpop)
        , selection()
        , model_blocks()
        , pop_blocks()
        , n_par()
        , Mcurrent(y, st)
        , threshold(th)
        , test_type(ct)
        , all_loci()
        , loci()
        , locus_blocks()
        , cac()
        , last_computation(NULL)
    {
        Mcurrent.add_block({}, make_value(cross_indicator));
        for (const auto& kpop: active_settings->populations) {
            context_key ck(new context_key_struc(&kpop.second,
                                                 &active_settings->map[0],
                                                 std::vector<double>()));
            n_par.push_back(
                    make_value<Mem>(compute_state_to_parental_origin,
                                    value<context_key>{ck},
                                    value<locus_key>{locus_key()})->innerSize());
        }
        pop_blocks.resize(active_settings->populations.size());
    }

    void select_chromosome(const std::string& name)
    {
        select_chromosome(active_settings->find_chromosome(name));
    }

    /*model_block_collection remove_cofactors()*/
    /*{*/
        /*return Mcurrent.extract_blocks_with_chromosome(selection.chromosome_under_study);*/
    /*}*/

    /*void add_cofactors(const model_block_collection&)*/
    /*{*/

    /*}*/

    void select_chromosome(const chromosome* c)
    {
        if (selection.chromosome_under_study != c) {
            selection.chromosome_under_study = c;
            pop_blocks.clear();
            loci = all_loci[selection.chromosome_under_study];
        }
    }

    void init_loci_by_step()
    {
        for (const chromosome& c: active_settings->map) {
            all_loci[&c]
                = compute_steps(
                    selection.chromosome_under_study->condensed.marker_locus,
                    active_settings->step);
        }
    }

    void init_loci_by_marker_list(const std::map<std::string, std::vector<std::string>>& markers)
    {
        for (auto mark_list: markers) {
            const chromosome* c = active_settings->find_chromosome(mark_list.first);
            value<std::vector<double>>& vml = all_loci[c];
            vml = std::vector<double>();
            vml->reserve(mark_list.second.size());
            for (const auto& mn: mark_list.second) {
                vml->push_back(c->raw.locus_by_name(mn));
            }
        }
    }

    const chromosome* selected_chromosome() const
    {
        return selection.chromosome_under_study;
    }

    void add_locus(double loc)
    {
        _remove_chrom_block();
        locus_key& lk = selection.get_lk(selection.chromosome_under_study->name);
        lk = lk + loc;
        _recompute_with_added_locus();
        _rebuild();
        _add_chrom_block();
    }

    void remove_locus(double loc)
    {
        _remove_chrom_block();
        locus_key& lk = selection.get_lk(selection.chromosome_under_study->name);
        _recompute_with_removed_locus(loc);
        lk = lk - loc;
        _rebuild();
        _add_chrom_block();
    }

    void test_along_chromosome()
    {
        compute_along_chromosome(cac, test_type, Test,
                                 Mcurrent, Mcurrent,
                                 locus_blocks);
        switch(test_type) {
            case ComputationType::FTest:
                last_computation = &cac.ftest_pvalue;
                break;
            case ComputationType::FTestLOD:
                last_computation = &cac.ftest_lod;
                break;
            case ComputationType::Chi2:
                last_computation = &cac.chi2;
                break;
            default:
                last_computation = NULL;
        };
    }

    struct test_result {
        const chromosome* chrom;
        double locus;
        double test_value;
        int index;
        bool over_threshold;
        /* TODO include the associated model_block_key */
        value<model_block_type> block;
    };

    test_result find_max()
    {
        int i_max = -1;
        double max = -1;
        for (int i = 0; i < last_computation->outerSize(); ++i) {
            if ((*last_computation)(0, i) >= max) {
                max = (*last_computation)(0, i);
                i_max = i;
            }
        }
        return {selection.chromosome_under_study,
                (*loci)[i_max], max, i_max, max > threshold,
                locus_blocks[i_max]};
    }

    test_result
        find_max_over_all_chromosomes()
        {
            test_result ret = {NULL, 0., 0., 0, false, {}};
            for (const chromosome& c: active_settings->map) {
                select_chromosome(&c);
                test_along_chromosome();
                auto tmp = find_max();
                if (tmp.test_value > ret.test_value) {
                    ret = tmp;
                }
            }
            return ret;
        }

protected:
    void _remove_chrom_block()
    {
        Mcurrent.remove_block(model_blocks[selection.chromosome_under_study]);
    }

    void _add_chrom_block()
    {
        model_block_key k(
                /*model_block_key::selection_value_type*/
                {{selection.chromosome_under_study,
                 selection.chromosome_selections[selection.chromosome_under_study->name]}});
        Mcurrent.add_block(k, model_blocks[selection.chromosome_under_study]);
    }

    void _recompute_with_added_locus()
    {
        value<locus_key> lk(selection.get_lk(selection.chromosome_under_study->name));
        value<std::vector<double>>
            test_loci = make_value<Mem>(compute_effective_loci, lk, loci);
        auto pop_it = active_settings->populations.begin();
        for (auto& pb: pop_blocks) {
            value<context_key>
                ck(context_key{new context_key_struc(&pop_it->second,
                                                     selection.chromosome_under_study,
                                                     *loci)});
            pb = make_collection<Disk>(joint_parental_origin_at_locus,
                                       ck,
                                       lk,
                                       values_of(*test_loci));
            ++pop_it;
        }
    }

    void _recompute_with_removed_locus(double loc)
    {
        locus_key lk = selection.get_lk(selection.chromosome_under_study->name);
        locus_key lk2 = lk - loc;
        auto pop_it = active_settings->populations.begin();
        auto npar_it = n_par.begin();
        for (auto& pb: pop_blocks) {
            const population* pop = &pop_it->second;
            context_key ck(new context_key_struc(pop, selection.chromosome_under_study, std::vector<double>()));
            MatrixXd red = lk->reduce(*npar_it, loc);
            for (auto& vmat: pb) {
                vmat->data *= red;
                vmat->column_labels = get_stpom_data(ck, lk2)->row_labels;
            }
            ++npar_it;
            ++pop_it;
        }
    }

    void _rebuild()
    {
        locus_blocks
            = assemble_parental_origins_multipop(
                    selection.chromosome_under_study,
                    selection.get_lk(selection.chromosome_under_study->name),
                    pop_blocks);
    }
};

#endif

