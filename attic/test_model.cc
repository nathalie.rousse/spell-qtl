/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*#include "model.h"*/
/*#include "chrono.h"*/
#include "all.h"

MatrixXd test_comp(const MatrixXd& M, const MatrixXd& P)
{
    std::cout << "M" << std::endl << M << std::endl;
    std::cout << "P" << std::endl << P << std::endl;
    MatrixXd ret = components(M, P);
    std::cout << "components(M, P)" << std::endl << ret << std::endl;
    return ret;
}


bool closeTo(const MatrixXd& a, const MatrixXd& b, double prec = 1.e-10)
{
    return (a - b).isMuchSmallerThan(1., prec) || a.isApprox(b, prec);
}


int main(int argc, char** argv)
{
#if 0
    /*MatrixXd P = MatrixXd::Ones(5, 1);*/
    /*MatrixXd M = MatrixXd::Random(5, 2);*/
    MatrixXd tmp(5, 2);
    tmp << 1, 0,
           1, 3,
           2, 4,
           1, 1,
           0, 1;
    test_comp(tmp, MatrixXd::Ones(5, 1));
    MatrixXd zob(5, 3);
    zob << 0, 1, 2,
           0, 1, 2,
           3, 0, 0,
           0, 1, 3,
           0, 2, 3;
    test_comp(zob, MatrixXd::Ones(5, 1));
    MatrixXd test(5, 2);
    test.col(0) = MatrixXd::Random(5, 1);
    test.col(1) = MatrixXd::Ones(5, 1) - test.col(0);
    test_comp(test, MatrixXd::Ones(5, 1));
    MatrixXd P(5, 1);
    P << 1, 0, 0, 0, 0;
    MatrixXd M(5, 3);
    M << 1, 0, 0,
         0, 1, 0,
         0, 0, 1,
         1, 0, 0,
         0, 0, 1;
    MatrixXd bouh = test_comp(M, P);
    MatrixXd gros = concat_right({bouh, P});
    test_comp(M, gros);

    model m(tmp);
    /*model m(MatrixXd::Ones(5, 1));*/
    m.add_bloc(zob);
    std::cout << "MODEL Y" << std::endl << m.Y() << std::endl;
    std::cout << "MODEL X" << std::endl << m.X() << std::endl;
    m.use_SVD();
    std::cout << "MODEL X RANK " << m.rank() << std::endl;
    std::cout << "MODEL COEFFICIENTS" << std::endl << m.coefficients() << std::endl;
    std::cout << "MODEL RESIDUALS" << std::endl << m.residuals() << std::endl;
    m.use_QR();
    std::cout << "MODEL X RANK " << m.rank() << std::endl;
    std::cout << "MODEL COEFFICIENTS" << std::endl << m.coefficients() << std::endl;
    std::cout << "MODEL RESIDUALS" << std::endl << m.residuals() << std::endl;
#endif
#define NIND 1000
#define NTRUC 210
#define XRANK 200
#define Hij(_x_) (1./(_x_))

    MatrixXd Y = MatrixXd::Random(NIND, 20);
    model bigmodel(Y);
#if 0
    MatrixXd H(NIND, NIND);
    for (int i = 0; i < H.innerSize(); ++i) {
        for (int j = 0; j < H.outerSize(); ++j) {
            H(i, j) = 1. / (1 + i + j);
        }
    }
    bigmodel.add_bloc(H);
#else
    MatrixXd X(NIND, NTRUC);
    X.leftCols(XRANK) = MatrixXd::Random(NIND, XRANK);
    for (int i = XRANK; i < NTRUC; i += XRANK) {
        X.block(0, i, NIND, (i + XRANK) <= NTRUC ? XRANK : (NTRUC - i - XRANK)) = X.leftCols(XRANK);
    }
    /*MatrixXd X = MatrixXd::Random(NIND, NTRUC);*/
    bigmodel.add_bloc(X);
#endif
    bigmodel.X();  /* pour initialiser X avant le chrono */
    std::cout << "Model rank = " << bigmodel.rank() << std::endl;
    MatrixXd coefQR, coefSVD, residQR, residSVD;
    chrono::start("SVD gros modèle");
    bigmodel.use_SVD();
    coefSVD = bigmodel.coefficients();
    std::cout << "SVD coefficients (" << coefSVD.innerSize() << ',' << coefSVD.outerSize() << ')' << std::endl;
    residSVD = bigmodel.residuals();
    std::cout << "SVD residuals (" << residSVD.innerSize() << ',' << residSVD.outerSize() << ')' << std::endl;
    chrono::stop("SVD gros modèle");
    chrono::start("QR gros modèle");
    bigmodel.use_QR();
    coefQR = bigmodel.coefficients();
    std::cout << "QR coefficients (" << coefQR.innerSize() << ',' << coefQR.outerSize() << ')' << std::endl;
    residQR = bigmodel.residuals();
    std::cout << "QR residuals (" << residQR.innerSize() << ',' << residQR.outerSize() << ')' << std::endl;
    chrono::stop("QR gros modèle");

    std::cout << "coef QR == coef SVD ? " << closeTo(coefQR, coefSVD, 1.e-15) << closeTo(coefQR, coefSVD, 1.e-10) << closeTo(coefQR, coefSVD, 1.e-5) << closeTo(coefQR, coefSVD, 1.e-1) << std::endl;
    std::cout << "resid QR == resid SVD ? " << closeTo(residQR, residSVD, 1.e-15) << closeTo(residQR, residSVD, 1.e-10) << closeTo(residQR, residSVD, 1.e-5) << closeTo(residQR, residSVD, 1.e-1) << std::endl;

    /*std::cout << "resid QR" << std::endl << residQR << std::endl << std::endl;*/
    /*std::cout << "resid SVD" << std::endl << residSVD << std::endl << std::endl;*/

    /*std::cout << "coef QR" << std::endl << coefQR << std::endl << std::endl;*/
    /*std::cout << "coef SVD" << std::endl << coefSVD << std::endl << std::endl;*/

    {
        MatrixXd X1(6, 3);
        MatrixXd X2(6, 2);
        MatrixXd Y(6, 1);
        X1 << 1, 2, 3,
              2, 2, 2,
              1, 3, 4,
              2, 3, 5,
              1, 2, 1,
              0, 1, 2;
        X2 << 5, 1,
              3, 1,
              2, 2,
              -1, 1,
              1, 0,
              1, 1;
        Y << 12, 10, 12, 10, 5, 5;
        /*Y << 11, 11, 11, 11, 6, 4;*/
        model M(Y);
        M.add_bloc(X1);
        M.use_QR();
        std::cout << "M1 residuals " << M.residuals().transpose() << std::endl;
        auto M2 = M.extend(X2);
        auto F = f_test(M, M2);
        double Chi2 = chi2(M, M2);
        std::cout << "F(M1, M2) " << F.transpose() << std::endl;
        std::cout << "Chi2(M1, M2) " << Chi2 << std::endl;
        std::cout << "M2.residuals " << M2.residuals().transpose() << std::endl;

        MatrixXd test_inv1(3, 2);
        test_inv1 << 2, 0,
                     0, 1,
                     0, 0;
        MatrixXd prout;
        model inv1(prout);
        /*inv1.add_bloc(test_inv1);*/
        inv1.add_bloc(X2);
        inv1.add_bloc(X2);
        std::cout << "==========================================================" << std::endl;
        std::cout << inv1.X() << std::endl;
        std::cout << "----------------------------------------------------------" << std::endl;
        std::cout << inv1.XtX_pseudo_inverse() << std::endl;
    }
    return 0;
}

