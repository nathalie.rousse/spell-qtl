/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "error.h"
#include "input/invoke_probapop.h"
#include <unistd.h>
#include <iostream>
#include "file.h"
#include "chrono.h"
#include "probapop_dtd.h"

using namespace Eigen;

#define MCQTL_PATH "/home/daleroux/MCQTL/MultiCrossQTL/build/bin/Linux/"
//#define MCQTL_PATH "/home/damien/bl0b_dev/inra/MCQTL-5.2.1-Linux/bin/Linux/"
#define TRANSLATE_DATA MCQTL_PATH "TranslateData"
#define PROBAPOP MCQTL_PATH "ProbaPop"
#define REDIRECT_QUIET " > /dev/null"

labelled_matrix<MatrixXd, double, double>
/*MatrixXd*/
test_probapop(const chromosome& chr, const marker_data& md, double step)
{
    if (md.data_type == "cp") {
        std::stringstream pp;
        pp << PROBAPOP << ' ' << chr.name << ' ' << step << REDIRECT_QUIET;
        system(pp.str().c_str());
        std::stringstream xml_name;
        xml_name << chr.name.substr(0, chr.name.size() - 4) << ".xml";
        ifile is(xml_name.str());
        probapop_output* po = read_probapop(is);
        /*MatrixXd ret = *po;*/
        /*return ret;*/
        return *po;
    } else {
        chrono::start("probapop[génération fichiers]");
        ofile ofmap("/tmp/probapop.map");
        ofile ofinf("/tmp/probapop.inf");
        ofile ofgen("/tmp/probapop.gen");
        ofile ofphen("/tmp/probapop.phen");

        ofmap << "*chr1 " << chr.raw.marker_locus.size() << ' ' << chr.raw.marker_name[0];
        for (size_t i = 1; i < chr.raw.marker_locus.size(); ++i) {
            ofmap << ' ' << (chr.raw.marker_locus[i] - chr.raw.marker_locus[i - 1]) << ' ' << chr.raw.marker_name[i];
        }
        ofmap << std::endl << std::flush;

        ofgen << "data type " << md.data_type << std::endl;
        ofgen << md.n_obs << ' ' << md.n_mark << " 0 0 Z=A I=A J=A K=A L=A" << std::endl;
        for (auto kv: md.data) { ofgen << '*' << kv.first << ' ' << kv.second << std::endl; }
        ofgen << std::endl << std::flush;

        ofinf << "*lineA pim" << std::endl << "*lineB pam" << std::endl;
        if (md.data_type == "bc") {
            ofinf << "*recurrent_line pim";
        }
        ofinf << std::flush;

        ofphen << "*poum 0" << std::endl << std::flush;

        unlink("pimpam.cfg");
        chrono::stop("probapop[génération fichiers]");
        chrono::start("probapop[translate_data]");
        system(TRANSLATE_DATA " /tmp/probapop.map /tmp/probapop.phen /tmp/probapop.gen /tmp/probapop.inf" REDIRECT_QUIET);
        chrono::stop("probapop[translate_data]");
        std::stringstream pp;
        unlink("pimpam.xml");
        pp << PROBAPOP << " pimpam.cfg " << step << REDIRECT_QUIET;
        chrono::start("probapop[calculs]");
        system(pp.str().c_str());
        chrono::stop("probapop[calculs]");

        chrono::start("probapop[lecture sortie]");
        ifile is("pimpam.xml");
        probapop_output* po = read_probapop(is);
        chrono::stop("probapop[lecture sortie]");
        labelled_matrix<MatrixXd, double, double> ret = *po;
        /*std::cout << ret.transpose() << std::endl;*/
        if (md.data_type == "bc") {
            ret.data.row(1) += ret.data.row(2);
            ret.row_labels = {ret.row_labels[0], ret.row_labels[1]};
            MatrixXd tmp = ret.data.topRows(2);
            ret.data = tmp;
        } else if (md.data_type == "ril" || md.data_type == "dh") {
            ret.data.row(1) = ret.data.row(3);
            ret.row_labels = {ret.row_labels[0], ret.row_labels[3]};
            MatrixXd tmp = ret.data.topRows(2);
            ret.data = tmp;
        }
        delete po;
        return ret;
    }
}


