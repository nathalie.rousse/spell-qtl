/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "eigen.h"
#include "error.h"
#include "labelled_matrix.h"
#include "lumping2.h"
#include <limits>
#include <cmath>

using namespace Eigen;

typedef Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> MatrixXb;
typedef Eigen::Matrix<bool, Eigen::Dynamic, 1> VectorXb;


typedef std::pair<char, char> label_type;
enum geno_matrix_variant_type { Gamete, DoublingGamete, SelfingGamete, Haplo, Geno };


bool operator == (const label_type& l1, const label_type& l2)
{
    return (l1.first == l2.first && l1.second == l2.second)
        || ((l1.first == 0 || l2.first == 0) && (l1.second == 0 || l2.second == 0));
}


struct geno_matrix {
    geno_matrix_variant_type variant;
    std::vector<label_type> labels;
    MatrixXd inf_mat;
    MatrixXd p, p_inv, diag;
    VectorXd stat_dist;
    /*double norm_factor;*/

    size_t rows() const { return inf_mat.rows(); }
    size_t cols() const { return inf_mat.cols(); }
    size_t size() const { return rows(); }

    geno_matrix& operator = (const geno_matrix& gm)
    {
        variant = gm.variant;
        labels.assign(gm.labels.begin(), gm.labels.end());
        inf_mat = gm.inf_mat;
        p = gm.p;
        p_inv = gm.p_inv;
        diag = gm.diag;
        stat_dist = gm.stat_dist;
        return *this;
    }

    MatrixXd exp(double d) const
    {
        MatrixXd ret = p * ((d * diag).array().exp().matrix().asDiagonal()) * p_inv;
        /*MatrixXd check = (d * inf_mat).exp();*/
        /*if (!ret.isApprox(check)) {*/
            /*MSG_ERROR("BAD EXP" << std::endl << "with diag:" << std::endl << check << std::endl << "with exp:" << std::endl << ret, "");*/
        /*} else {*/
            /*MSG_INFO("GOOD EXP");*/
        /*}*/
        return ret;
    }

    void cleanup(MatrixXd& m)
    {
        m = (m.array().abs() <= 1.e-10).select(MatrixXd::Zero(m.rows(), m.cols()), m);
    }

    void cleanup(VectorXd& v)
    {
        v = (v.array().abs() <= 1.e-10).select(VectorXd::Zero(v.size()), v);
    }

    geno_matrix& cleanup()
    {
        cleanup(inf_mat);
        cleanup(diag);
        cleanup(p);
        cleanup(p_inv);
        return *this;
    }

    MatrixXd lim_inf() const
    {
        /*MSG_DEBUG("LIM_INF ####### LIM_INF");*/
        /*MSG_DEBUG((*this));*/
        MatrixXd diag_inf = (diag.array() == 0).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();
        return p * diag_inf.asDiagonal() * p_inv;
    }

    bool check_not_nan() const
    {
        if (!(inf_mat == inf_mat)) {
            MSG_DEBUG("NAN INF_MAT");
            return false;
        }
        if (!(p == p)) {
            MSG_DEBUG("NAN P");
            return false;
        }
        if (!(p_inv == p_inv)) {
            MSG_DEBUG("NAN P_INV");
            return false;
        }
        if (!(diag == diag)) {
            MSG_DEBUG("NAN DIAG");
            return false;
        }
        if (!(stat_dist == stat_dist)) {
            MSG_DEBUG("NAN STAT_DIST");
            return false;
        }
        return true;
    }
};

inline
std::ostream& operator << (std::ostream& os, const label_type& l)
{
    return os << (l.first ? l.first : '*') << (l.second ? l.second : '*');
}

inline
std::ostream& operator << (std::ostream& os, const std::vector<label_type>& vl)
{
    for (const auto& l: vl) { os << ' ' << l; }
    return os;
}


std::ostream& operator << (std::ostream& os, const geno_matrix& gm)
{
    Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> tmp(gm.rows() + 1, gm.cols() + 1);
    for (size_t i = 0; i < gm.labels.size(); ++i) {
        tmp(0, 1 + i) = "  ";
        tmp(0, 1 + i)[0] = (gm.labels[i].first < 32 ? '0' : '\0') + gm.labels[i].first;
        tmp(0, 1 + i)[1] = (gm.labels[i].second < 32 ? '0' : '\0') + gm.labels[i].second;
        tmp(1 + i, 0) = tmp(0, 1 + i);
    }
    for (size_t i = 0; i < gm.rows(); ++i) {
        for (size_t j = 0; j < gm.rows(); ++j) {
            std::stringstream ss; ss << gm.inf_mat(i, j);
            tmp(i + 1, j + 1) = ss.str();
        }
    }
    os << tmp;
    os << std::endl;
    os << "P" << std::endl << gm.p << std::endl;
    os << "Pinv" << std::endl << gm.p_inv << std::endl;
    MatrixXd diag = gm.diag.asDiagonal();
    os << "D" << std::endl << diag << std::endl;
    os << "STATIONARY DISTRIBUTION " << gm.stat_dist.transpose() << std::endl;
    return os;
}



#define P_NORM_FACTOR (.707106781186547524400844362104849039284835937688474036588339868995366239231053519425193767163820786367506923115456148512462418027925)


label_type empty = {0, 0};


geno_matrix
    gamete = {
        Gamete,
        {{0, 0}, {1, 0}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        /* BEWARE these matrices (p and p_inv) SHOULD be * 1/sqrt(2) */
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << 0, -2).finished(),
        (VectorXd(2) << .5, .5).finished()
        /*.5*/
    },
    doubling_gamete = {
        DoublingGamete,
        {{0, 0}, {1, 1}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << 0, -2).finished(),
        (VectorXd(2) << .5, .5).finished()
        /*.5*/
    };

#define SELECT(__p, __b) ((__b) ? (__p).second : (__p).first)


geno_matrix kronecker(const geno_matrix& m1, const geno_matrix& m2)
{
    m1.check_not_nan();
    m2.check_not_nan();
    geno_matrix ret;
    ret.inf_mat.resize(m1.rows() * m2.rows(), m1.cols() * m2.cols());
    ret.labels.reserve(m1.labels.size() * m2.labels.size());
    switch (m1.variant) {
        case Gamete:
            if (m2.variant == Gamete) {
                for (const auto& l1: m1.labels) {
                    for (const auto& l2: m2.labels) {
                        ret.labels.emplace_back(l1.first, l2.first);
                    }
                }
                ret.variant = SelfingGamete;
                break;
            }
        case SelfingGamete:
        case DoublingGamete:
            MSG_ERROR("Gamete matrices can only be the right operand in a kronecker product", "");
            MSG_QUEUE_FLUSH();
            throw 0;
        case Haplo:
            if (m2.variant != Haplo) {
                MSG_ERROR("Only Haplo (x) Haplo is defined", "");
                MSG_QUEUE_FLUSH();
                throw 0;
            }
            ret.variant = Geno;
            for (const auto& l1: m1.labels) {
                for (const auto& l2: m2.labels) {
                    ret.labels.emplace_back(l1.first, l2.first);
                }
            }
            break;
        case Geno:
            switch (m2.variant) {
                case Gamete:
                    ret.variant = Haplo;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), 0);
                        }
                    }
                    break;
                case DoublingGamete:
                    ret.variant = Geno;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), SELECT(l1, (int)l2.first));
                        }
                    }
                    break;
                case SelfingGamete:
                    ret.variant = Geno;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), SELECT(l1, (int)l2.second));
                        }
                    }
                    break;
                default:
                    MSG_ERROR("Only Geno (x) {any gamete} is defined", "");
                    MSG_QUEUE_FLUSH();
                    throw 0;
            };
            break;
    };
    ret.inf_mat = kroneckerProduct(m1.inf_mat, MatrixXd::Identity(m2.rows(), m2.cols()))
                + kroneckerProduct(MatrixXd::Identity(m1.rows(), m1.cols()), m2.inf_mat);
    ret.p = kroneckerProduct(m1.p, m2.p);
    ret.p_inv = kroneckerProduct(m1.p_inv, m2.p_inv);
    ret.diag = (kroneckerProduct(m1.diag, VectorXd::Ones(m2.cols()))
             + kroneckerProduct(VectorXd::Ones(m1.cols()), m2.diag));
    /*ret.norm_factor = m1.norm_factor * m2.norm_factor;*/

    /* Check! */
    /*MSG_DEBUG("CHECK");*/
    /*MSG_DEBUG((ret.p * ret.diag.asDiagonal() * ret.p_inv * ret.norm_factor) - ret.inf_mat);*/
    /*MSG_DEBUG("END CHECK");*/
    ret.stat_dist = kroneckerProduct(m1.stat_dist, m2.stat_dist);
    return ret.cleanup();
}


geno_matrix
    selfing_gamete = kronecker(gamete, gamete);



geno_matrix ancestor_matrix(char a)
{
    std::vector<label_type> l;
    l.emplace_back(a, a);
    return {Geno, l, (MatrixXd(1, 1) << 0).finished(), (MatrixXd(1, 1) << 1).finished(), (MatrixXd(1, 1) << 1).finished(), (MatrixXd(1, 1) << 0).finished()/*, 1.*/, (VectorXd(1) << 1.).finished()};
}

namespace std {
    template <>
    struct hash<label_type> {
        size_t operator () (const label_type& l) const
        {
            return hash<unsigned short>()(*(unsigned short*)&l);
        }
    };
}

/*#define DEBUG_LUMPING*/
#ifdef DEBUG_LUMPING
#define LUMP_DEBUG(_expr_) MSG_DEBUG(_expr_)
#define LUMP_QUEUE_FLUSH() MSG_QUEUE_FLUSH()
#else
#define LUMP_DEBUG(_expr_)
#define LUMP_QUEUE_FLUSH()
#endif

double cosV(const VectorXd& v1, const VectorXd& v2)
{
    double n1 = v1.lpNorm<2>();
    double n2 = v2.lpNorm<2>();

    return n1 ? n2 ? abs(v1.transpose() * v2) / (n1 * n2)
                   : 0
              : 1;
}

double cosM(const VectorXd& v1, const MatrixXd& mat)
{
    MatrixXd tmp = v1.transpose() * mat;
    double ret = cosV(v1, mat * tmp.transpose());
    /*MSG_DEBUG("CosM");*/
    /*MSG_DEBUG(v1.transpose());*/
    /*MSG_DEBUG("-------------");*/
    /*MSG_DEBUG(mat);*/
    /*MSG_DEBUG("-------------");*/
    /*MSG_DEBUG(ret);*/
    return ret;
}



void compute_LR(const geno_matrix& m, const std::set<subset>& lumping_partition, MatrixXd& L1, MatrixXd& L2)
{
    /* Creation of L1 */
    L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    for (const auto& D: lumping_partition) {
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /* Creation of L2 */
    L2 = m.lim_inf().col(0).asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();
}



void subset_difference(const subset& s1, const subset& s2, subset& output)
{
    output.resize(s1.size() + s2.size());
    auto it = std::set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(), output.begin());
    output.resize(it - output.begin());
}



geno_matrix lump_using_partition_weighted(const geno_matrix& m, const std::set<subset>& lumping_partition);


VectorXd column_angles(const MatrixXd& g1, const MatrixXd& g2)
{
    /*MSG_DEBUG(MATRIX_SIZE(g1));*/
    /*MSG_DEBUG(g1);*/
    /*MSG_DEBUG(MATRIX_SIZE(g2));*/
    /*MSG_DEBUG(g2);*/
    VectorXd n1 = (g1.array() * g1.array()).colwise().sum().array().sqrt().matrix();
    /*MSG_DEBUG(MATRIX_SIZE(n1) << std::endl << n1.transpose());*/
    VectorXd n2 = (g2.array() * g2.array()).colwise().sum().array().sqrt().matrix();
    /*MSG_DEBUG(MATRIX_SIZE(n2) << std::endl << n2.transpose());*/
    VectorXd angles(n1.size());
    for (int i = 0; i < angles.size(); ++i) {
        double n = n1(i) * n2(i);
        if (n != 0.) {
            double dot = (g1.col(i).array() * g2.col(i).array()).sum();
            /*MSG_DEBUG("(" << g1.col(i).transpose() << ") . (" << g2.col(i).transpose() << ") = " << dot << ", n = " << n);*/
            if (dot < -n) {
                dot = -n;
            } else if (dot > n) {
                dot = n;
            }
            angles(i) = acos(dot / n);
        } else {
            angles(i) = 0.;
        }
    }
    /*MSG_DEBUG("ANGLES " << angles.transpose());*/
    return angles;
}



MatrixXd greedy_permute(const MatrixXd& m)
{
    VectorXd ret = VectorXd::Zero(m.cols());
    /* todo */
    MatrixXd tmp = m;
    /*tmp.diagonal().setConstant(std::numeric_limits<double>::infinity());*/

    int i;
    for (int n = 0; n < m.cols(); ++n) {
        /*MSG_DEBUG("angles(" << n << ") = " << tmp.col(n).transpose());*/
        double a = tmp.col(n).minCoeff(&i);
        /*MSG_DEBUG("   minimum at " << i << ": " << a);*/
        ret(n) = a;
        tmp.row(i).setConstant(std::numeric_limits<double>::infinity());
    }
    return ret;
}




VectorXd min_column_angles(const MatrixXd& g1, const MatrixXd& g2)
{
    VectorXd n1 = (g1.array() * g1.array()).colwise().sum().array().sqrt().matrix();
    VectorXd n2 = (g2.array() * g2.array()).colwise().sum().array().sqrt().matrix();
    MatrixXd angles(n1.size(), n2.size());
    for (int i1 = 0; i1 < n1.size(); ++i1) {
        for (int i2 = 0; i2 < n2.size(); ++i2) {
            double n = n1(i1) * n2(i2);
#if 1
            if (n != 0.) {
                double dot = (g1.col(i1).array() * g2.col(i2).array()).sum();
                if (dot < -n) {
                    dot = -n;
                } else if (dot > n) {
                    dot = n;
                }
                angles(i1, i2) = acos(dot / n);
            } else {
                angles(i1, i2) = 0.;
            }
#else
            double dot = (g1.col(i1).array() * g2.col(i2).array()).sum();
            angles(i1, i2) = n - dot;
#endif
        }
    }



    return greedy_permute(angles);
}



struct experimental_lumper {
    geno_matrix M;

    experimental_lumper(const geno_matrix& m)
        : M(m)
    {}

    double
        compute_sum(size_t s, const subset& B)
        {
            double accum = 0;
            for (int j: B) {
                accum += M.inf_mat(j, s);
            }
            return accum;
        }

    std::map<double, subset>
        compute_keys(const subset& C, const subset& B)
        {
            std::map<double, subset> ret;
            for (int i: C) {
                double sum = compute_sum(i, B);
                ret[sum].push_back(i);
            }
            return ret;
        }

    std::set<subset>
        partition_on_labels() const
        {
            std::set<subset> ret;
            std::map<label_type, subset> tmp;
            for (size_t i = 0; i < M.labels.size(); ++i) {
                tmp[M.labels[i]].push_back(i);
            }
            for (const auto& kv: tmp) {
                ret.insert(kv.second);
            }
            return ret;
        }

    struct conflict_type {
        subset C;
        subset B;
        std::map<double, subset> keys;
        double cost_;

        conflict_type(const experimental_lumper& el, const std::set<subset>& P0,
                      const subset& c, const subset& b, const std::map<double, subset>& k)
            : C(c), B(b), keys(k)
            /*, cost_(opportunity_cost(el, P0))*/
            , cost_(k.rbegin()->first - k.begin()->first)
        {}

        /*conflict_type(const subset& c, const subset& b, const std::map<double, subset>& k)*/
            /*: C(c), B(b), keys(k), cost_(0)*/
        /*{}*/

        friend
            std::ostream& operator << (std::ostream& os, const conflict_type ct)
            {
                os << "< " << ct.C << " vs " << ct.B;
                for (const auto& kv: ct.keys) {
                    os << ' ' << kv.first << ':' << kv.second;
                }
                return os << " >";
            }

        double
            magnitude() const
            {
                return keys.rbegin()->first - keys.begin()->first;
            }

        bool
            operator < (const conflict_type& other) const
            {
                return C.size() > other.C.size() || (C.size() == other.C.size() && cost_ < other.cost_);
                /*return C.size() > other.C.size() || (C.size() == other.C.size() && cost_ > other.cost_);*/
                /*return cost_ < other.cost_;*/
                /*double m1 = magnitude();*/
                /*double m2 = other.magnitude();*/
                /*return m1 < m2*/
                    /*|| (m1 == m2*/
                        /*&& (C < other.C*/
                            /*|| (C == other.C && B < other.B)))*/
                    /*;*/
            }

        double
            opportunity_cost0(const experimental_lumper& el, const std::set<subset>& P0) const
            {
                std::set<subset> P(P0);
                P.erase(C);
                for (const auto& kv: keys) { P.insert(kv.second); }
                double this_cost = el.compute_cost(P);
                /*double base_cost = el.compute_cost(P0);*/
                /*return this_cost - base_cost;*/
                return this_cost;
            }

        double
            opportunity_cost(const experimental_lumper& el, const std::set<subset>& P0) const
            {
                double accum = 0;
                size_t total_size = 0;
                for (const auto& kv: keys) {
                    accum += abs(kv.first) * kv.second.size();
                    total_size += kv.second.size();
                }
                double avg = accum / total_size;
                accum = 0;
                for (const auto& kv: keys) {
                    accum += abs(avg - abs(kv.first)) / avg;
                }
                return accum;
                (void) el; (void) P0;
            }

        std::multimap<double, subset>
            opportunity_cost_per_part(const experimental_lumper& el, const std::set<subset>& P0) const
            {
#if 0
                std::multimap<double, subset> ret;
                /*double base_cost = el.compute_cost(P0);*/
                subset C1;
                for (const auto& kv: keys) {
                    std::set<subset> P(P0);
                    subset_difference(C, kv.second, C1);
                    P.erase(C);
                    P.insert(kv.second);
                    P.insert(C1);
                    double this_cost = el.compute_cost(P);
                    /*ret.insert({this_cost - base_cost, kv.second});*/
                    ret.insert({this_cost, kv.second});
                }
#else
                std::multimap<double, subset> ret;
                subset C1;
                double accum = 0;
                size_t total_size = 0;
/*#define OP(_x_) ((_x_) * (_x_))*/
/*#define OP(_x_) ((_x_) * (_x_))*/
#define OP(_x_) (_x_)
                for (const auto& kv: keys) {
                    accum += OP(kv.first) * kv.second.size();
                    /*accum += OP(kv.first);*/
                    total_size += kv.second.size();
                }

                /*double avg = accum / keys.size();*/
                double avg = accum / total_size;
                double div;

                if (avg == 0.) {
                    div = 1.;
                } else {
                    div = 1. / avg;
                }

                for (const auto& kv: keys) {
                    /*double this_cost = (accum - OP(kv.first) * kv.second.size());*/
                    /*double this_cost = (accum - OP(kv.first));*/
                    double this_cost = abs(avg - OP(kv.first)) * div;
                    ret.insert({this_cost, kv.second});
                }
#endif
                return ret;
            }
    };

    double
        compute_cost0(const std::set<subset>& P) const 
        {
            MatrixXd PI, PI_inv;
            compute_LR(M, P, PI, PI_inv);
            MatrixXd Ptilde = PI * M.inf_mat * PI_inv;
            VectorXd stat_dist = PI * M.stat_dist;

#if 1
            VectorXd s = stat_dist.array().sqrt().inverse().matrix();
            MatrixXd U = s.array().matrix().transpose();
            MatrixXd Uinv = M.stat_dist.array().sqrt().matrix().asDiagonal();
#else
            auto Pl = partition_on_labels();
            MatrixXd U, discard;
            compute_LR(M, Pl, U, discard);
            U = (PI * U.transpose()).transpose();
            /*MatrixXd Uinv = M.stat_dist.array().sqrt().matrix();*/
            MatrixXd Uinv = M.stat_dist.asDiagonal();
#endif
            /*MatrixXd U = s.array().matrix().asDiagonal();*/

            /*MSG_DEBUG(MATRIX_SIZE(U));*/
            /*MSG_DEBUG(MATRIX_SIZE(Uinv));*/
            /*MSG_DEBUG(MATRIX_SIZE(PI));*/
            /*MSG_DEBUG(MATRIX_SIZE(M.inf_mat));*/
            /*MSG_DEBUG(MATRIX_SIZE(Ptilde));*/
            /*MSG_QUEUE_FLUSH();*/
            /*return (U * (PI * M.inf_mat - Ptilde * PI) * Uinv).lpNorm<Eigen::Infinity>();*/

            MatrixXd mat = (U * (PI * M.inf_mat - Ptilde * PI) * Uinv);
            double accum = 0;
            for (int i = 0; i < mat.cols(); ++i) {
                accum += mat.col(i).lpNorm<Eigen::Infinity>();
            }
            /*for (int i = 0; i < mat.cols(); ++i) {*/
                /*double tmp = mat.col(i).array().abs().sum();*/
                /*if (tmp > accum) {*/
                    /*accum = tmp;*/
                /*}*/
            /*}*/
            return accum;
            /*return mat.array().abs().colwise().max().sum();*/


            /*return (U * (PI * M.inf_mat - Ptilde * PI) * Uinv)(0, 0);*/
            /*return (PI * M.inf_mat - Ptilde * PI).lpNorm<2>();*/
        }

    double
        compute_cost(const std::set<subset>& P) const 
        {
            MatrixXd PI, PI_inv;
            compute_LR(M, P, PI, PI_inv);
            MatrixXd Gtilde1 = PI * M.inf_mat * PI_inv;
            VectorXd stat_dist = PI * M.stat_dist;

            auto Pl = partition_on_labels();
            MatrixXd U, Uinv;
            compute_LR(M, Pl, U, Uinv);

            U = (PI * U.transpose()).transpose();
            /*MatrixXd Uinv = M.stat_dist.asDiagonal();*/

            MatrixXd G = M.inf_mat;
            MatrixXd Gtilde = Gtilde1;
#if 0
            double accum = 0;
            int n = 1;
            double weight = .15;
            for (int i = 0; i < 10; ++i) {
                MatrixXd mat = (U * (PI * G - Gtilde * PI) * Uinv);
                accum += weight * mat.lpNorm<Eigen::Infinity>() / n;
                n *= (i + 2);
                G *= M.inf_mat;
                Gtilde *= Gtilde1;
                weight *= .15;
            }

            /*MSG_DEBUG(MATRIX_SIZE(U));*/
            /*MSG_DEBUG(MATRIX_SIZE(Uinv));*/
            /*MSG_DEBUG(MATRIX_SIZE(PI));*/
            /*MSG_DEBUG(MATRIX_SIZE(M.inf_mat));*/
            /*MSG_DEBUG(MATRIX_SIZE(Gtilde));*/
            /*MSG_QUEUE_FLUSH();*/
            /*return (U * (PI * M.inf_mat - Gtilde * PI) * Uinv).lpNorm<Eigen::Infinity>();*/

            return accum;
#elif 0
#define DIST_0 .3
            MatrixXd mat = (U * (PI * G - Gtilde * PI) * Uinv);
            double weight = DIST_0 / 2;
            MatrixXd accum = weight * mat;

            for (int n = 1; n < ORDER; ++n) {
                G *= M.inf_mat;
                Gtilde *= Gtilde1;
                weight *= DIST_0 / (n + 1);
                /*weight *= DIST_0 / (n + 2);*/
                /*weight *= DIST_0;*/
                mat = (U * (PI * G - Gtilde * PI) * Uinv);
                accum += weight * mat;
            }

            /*return accum.lpNorm<Eigen::Infinity>();*/
            return accum.lpNorm<1>();
#else
            /*return (U * (PI * G - Gtilde * PI) * Uinv).lpNorm<1>();*/
            /*MatrixXd g1 = U * PI * G * Uinv;*/
            /*MatrixXd g2 = U * Gtilde * PI * Uinv;*/
            MatrixXd g1 = PI * G;
            MatrixXd g2 = Gtilde * PI;
            return column_angles(g1, g2).lpNorm<2>();
#endif
        }

    std::vector<conflict_type>
        check_conflicts(std::set<subset>& P)
        {
            std::vector<conflict_type> ret;
            for (const auto& C: P) {
                for (const auto& B: P) {
                    auto keys = compute_keys(C, B);
                    if (keys.size() > 1) {
                        ret.emplace_back(*this, P, C, B, keys);
                    }
                }
            }
            return ret;
        }

    void
        dump_conflicts(const std::set<subset>& P, const std::multiset<conflict_type>& ordered)
        {
            MSG_DEBUG("===============================================================================================================================");
            MSG_DEBUG("base cost = " << compute_cost(P));
            MSG_DEBUG("conflicts");
            for (const auto& c: ordered) {
                MSG_DEBUG(c);
                /*MSG_DEBUG("opportunity_cost = " << c.opportunity_cost(*this, P));*/
                MSG_DEBUG("opportunity_cost = " << c.cost_);
                auto ocpp = c.opportunity_cost_per_part(*this, P);
                for (const auto& kv: ocpp) {
                    MSG_DEBUG("| cost for " << kv.second << " only: " << kv.first);
                }
            }
        }


    /*void*/
        /*pick_conflict(const std::vector<conflict>& conflicts, const std::map<label_type, size_t>& state_count)*/
        /*{*/
            /* TODO */
        /*}*/

    label_type state_label(const subset& s) { return M.labels[s.front()]; }

    geno_matrix
        do_lump(size_t max_size)
        {
            size_t count = 0;
            /*MSG_DEBUG(M);*/
            /*MSG_QUEUE_FLUSH();*/
            auto P = partition_on_labels();
            /*auto conflicts = check_conflicts(P);*/
            /*std::multiset<conflict_type> ordered(conflicts.begin(), conflicts.end());*/
            /*dump_conflicts(P, ordered);*/
            /*double max = ordered.rbegin()->opportunity_cost(*this, P);*/
            std::map<subset, std::pair<double, subset>> split_picker;
            while (P.size() < max_size && compute_cost(P) > 0.) {
                auto conflicts = check_conflicts(P);
                if (0) {
                    std::multiset<conflict_type> ordered(conflicts.begin(), conflicts.end());
                    dump_conflicts(P, ordered);
                }

                if (!conflicts.size()) {
                    break;
                }
#if 0
                auto best = std::min_element(conflicts.begin(), conflicts.end());

                auto ocpp = best->opportunity_cost_per_part(*this, P);
                /*auto bestppi = ocpp.rbegin();*/
                auto bestppi = ocpp.begin();

                P.erase(best->C);
                subset C1;
                subset_difference(best->C, bestppi->second, C1);
                P.insert(bestppi->second);
                P.insert(C1);
                ++count;
#else
                split_picker.clear();
                double min_cost = std::numeric_limits<double>::infinity();
                for (const auto& c: conflicts) {
                    auto& pick = split_picker[c.C];
                    pick.first = std::numeric_limits<double>::infinity();
                }
                for (const auto& c: conflicts) {
                    auto& pick = split_picker[c.C];
                    auto ocpp = c.opportunity_cost_per_part(*this, P);
                    auto bestppi = ocpp.begin();
                    if (bestppi->first < pick.first) {
                        pick.first = bestppi->first;
                        pick.second = bestppi->second;
                        if (min_cost > bestppi->first) {
                            min_cost = bestppi->first;
                        }
                    }
                }
                for (const auto& kv: split_picker) {
                    if (P.size() >= max_size) {
                        break;
                    }
                    if (kv.second.first - min_cost < 1.e-10) {
                        P.erase(kv.first);
                        subset C1;
                        subset_difference(kv.first, kv.second.second, C1);
                        P.insert(C1);
                        P.insert(kv.second.second);
                    }
                }
                ++count;
#endif
            }
            /*MSG_DEBUG("FINAL PARTITION after " << count << " iterations");*/
            /*MSG_DEBUG("final base cost = " << compute_cost(P));*/
            /*MSG_DEBUG("" << P);*/

            return lump_using_partition_weighted(M, P);
        }
};








/*
 * De lumpibus

(%i1) kill(all);
load(diag);
(%o0) done
(%o1) "/usr/share/maxima/5.32.1/share/contrib/diag.mac"
-->  / * création manuelle du BC2 * /
     bc2: matrix([-2, 2, 2],
                 [1, -2, 0],
                 [1, 0, -2]);
(%o2) matrix([-2,2,2],[1,-2,0],[1,0,-2])
(%i3) jbc2: jordan(bc2);
(%o3) [[-4,1],[-2,1],[0,1]]
(%i4) p: ModeMatrix(bc2, jbc2);
      p^^-1;
(%o4) matrix([1,0,1],[-1/2,1,1/2],[-1/2,-1,1/2])
(%o5) matrix([1/2,-1/2,-1/2],[0,1/2,-1/2],[1/2,1/2,1/2])
(%i6) pt: args(transpose(p));
(%o6) [[1,-1/2,-1/2],[0,1,-1],[1,1/2,1/2]]
(%i7) Ptmp:transpose(apply(matrix, map(uvect, pt)));
(%o7) matrix([sqrt(2)/sqrt(3),0,sqrt(2)/sqrt(3)],[-1/(sqrt(2)*sqrt(3)),1/sqrt(2),1/(sqrt(2)*sqrt(3))],[-1/(sqrt(2)*sqrt(3)),-1/sqrt(2),1/(sqrt(2)*sqrt(3))])
(%i8) float(Ptmp);
(%o8) matrix([0.81649658092772,0.0,0.81649658092772],[-0.40824829046386,0.70710678118654,0.40824829046386],[-0.40824829046386,-0.70710678118654,0.40824829046386])
-->  / * permutation pour être cohérent avec l'implémentation C++ de matrixexp (au 30/01/2015) * /
     zzz: matrix([1, 0, 0], [0, 0, 1], [0, 1, 0]);
     zzz2: matrix([-1, 0, 0], [0, 1, 0], [0, 0, 1]);
(%o9) matrix([1,0,0],[0,0,1],[0,1,0])
(%o10) matrix([-1,0,0],[0,1,0],[0,0,1])
(%i11) p: zzz . Ptmp . zzz . zzz2;
(%o11) matrix([-sqrt(2)/sqrt(3),sqrt(2)/sqrt(3),0],[1/(sqrt(2)*sqrt(3)),1/(sqrt(2)*sqrt(3)),-1/sqrt(2)],[1/(sqrt(2)*sqrt(3)),1/(sqrt(2)*sqrt(3)),1/sqrt(2)])
(%i12) float(p);
(%o12) matrix([-0.81649658092772,0.81649658092772,0.0],[0.40824829046386,0.40824829046386,-0.70710678118654],[0.40824829046386,0.40824829046386,0.70710678118654])
(%i13) pinv: p^^-1;
(%o13) matrix([-sqrt(3)/2^(3/2),sqrt(3)/2^(3/2),sqrt(3)/2^(3/2)],[sqrt(3)/2^(3/2),sqrt(3)/2^(3/2),sqrt(3)/2^(3/2)],[0,-1/sqrt(2),1/sqrt(2)])
(%i14) p . pinv;
(%o14) matrix([1,0,0],[0,1,0],[0,0,1])
-->  d:pinv.bc2.p;
     / * On est contents c'est bien la bonne diag, les bonnes p et pinv, tout va bien * /
(%o15) matrix([-4,0,0],[0,0,0],[0,0,-2])
(%i16) dinf: diag([0, 1, 0]);
(%o16) matrix([0,0,0],[0,1,0],[0,0,0])
(%i17) p.dinf.pinv;
(%o17) matrix([1/2,1/2,1/2],[1/4,1/4,1/4],[1/4,1/4,1/4])
(%i18) limit(matrixexp(bc2, ddd), ddd, inf);
(%o18) matrix([1/2,1/2,1/2],[1/4,1/4,1/4],[1/4,1/4,1/4])
-->  / * Note à benêts :
        l1 est la matrice de lumping.
        l2 est sa transposée pondérée par les abondances normalisées PAR SOUS-ENSEMBLE DE LA PARTITION.
        Il semble (sic) que cette magouille (sic) est nécessaire pour que
        l'abondance de la somme soit la même que la somme des abondances.
      * /
     l1: matrix([1, 1, 0], [0, 0, 1]);
     l2: matrix([2/3, 0], [1/3, 0], [0, 1]);
(%o19) matrix([1,1,0],[0,0,1])
(%o20) matrix([2/3,0],[1/3,0],[0,1])
(%i30) ab: [1/2, 1/4, 1/4];
(%o30) [1/2,1/4,1/4]
(%i31) l1 . ab;
(%o31) matrix([3/4],[1/4])
(%i21) l1 . l2;
(%o21) matrix([1,0],[0,1])
(%i22) bc2l: l1 . bc2 . l2;
(%o22) matrix([-2/3,2],[2/3,-2])
(%i23) eigenvectors(bc2l);
(%o23) [[[-8/3,0],[1,1]],[[[1,-1]],[[1,1/3]]]]
(%i24) pl: ModeMatrix(bc2l);
(%o24) matrix([1,1],[-1,1/3])
(%i25) plinv: pl^^-1;
(%o25) matrix([1/4,-3/4],[3/4,3/4])
(%i26) dl: plinv . bc2l . pl;
(%o26) matrix([-8/3,0],[0,0])
(%i27) limit(matrixexp(bc2l, dd), dd, inf);
(%o27) matrix([3/4,3/4],[1/4,1/4])
-->  / * Hou qu'il est beau le Youki * /
     wxplot2d(flatten(args(l1 . float(matrixexp(bc2, dd)) . l2 - float(matrixexp(bc2l, dd)))), [dd, 0, 5]);
(%t29)  << Graphics >> 
(%o29) 

 */


void removeZeroRows(Eigen::MatrixXd& mat);

void compute_LR2(const geno_matrix& m, const std::set<subset>& lumping_partition, MatrixXd& L1, MatrixXd& L2)
{
    /* Creation of L1 */
    L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    for (const auto& D: lumping_partition) {
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /* Creation of L2 */
    /*L2 = m.lim_inf().col(0).asDiagonal() * L1.transpose();*/
    /*VectorXd diag = m.inf_mat.fullPivLu().kernel().col(0);*/
    removeZeroRows(L1);
    VectorXd diag = m.stat_dist;
    L2 = diag.asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();

    if (!(L2 * L1 * diag - diag).isZero()) {
        MSG_DEBUG("PROUT");
        MSG_DEBUG("D" << std::endl << L2);
        MSG_DEBUG("C" << std::endl << L1);
        MSG_DEBUG("s" << std::endl << diag);
    }
}


geno_matrix lump_using_partition_weighted(const geno_matrix& m, const std::set<subset>& lumping_partition)
{
    m.check_not_nan();
    /*MSG_DEBUG("|| LUMPING");*/
    /*MSG_DEBUG("" << m);*/
    /*MSG_DEBUG("|| WITH PARTITION");*/
    /*MSG_DEBUG("" << lumping_partition);*/

    geno_matrix ret_lump;

    /* Creation of L1 */
    MatrixXd L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    ret_lump.labels.clear();
    ret_lump.labels.reserve(lumping_partition.size());
    for (const auto& D: lumping_partition) {
        ret_lump.labels.push_back(m.labels[D.front()]);
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /*MSG_DEBUG("L1");*/
    /*MSG_DEBUG(L1);*/
    /* Creation of L2 */
    MatrixXd L2 = m.stat_dist.asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();

    /* Let's be blunt. */
    ret_lump.inf_mat =
        /* <em>And bold.</em> */
        L1 * m.inf_mat * L2;

    /*MSG_DEBUG("## m.inf_mat" << std::endl << m.inf_mat);*/
    /*MSG_DEBUG("## L1" << std::endl << L1);*/
    /*MSG_DEBUG("## L2" << std::endl << L2);*/
    /*MSG_DEBUG("## ret_lump.inf_mat" << std::endl << ret_lump.inf_mat);*/

    /*MSG_DEBUG(MATRIX_SIZE(L1));*/
    /*MSG_DEBUG(L1);*/
    /*MSG_DEBUG(MATRIX_SIZE(m.stat_dist));*/
    /*MSG_DEBUG(m.stat_dist.transpose());*/
    /*MSG_QUEUE_FLUSH();*/

    ret_lump.stat_dist = L1 * m.stat_dist;

    VectorXd s = ret_lump.stat_dist.array().sqrt().matrix();
    /*MSG_DEBUG("## s = " << s.transpose());*/
    MatrixXd Uinv = s.asDiagonal();
    /*MSG_DEBUG("## Uinv" << std::endl << Uinv);*/
    MatrixXd U = s.array().inverse().matrix().asDiagonal();
    /*MSG_DEBUG("## U" << std::endl << U);*/
    MatrixXd tmp = U * ret_lump.inf_mat * Uinv;
    /*MSG_DEBUG("## tmp" << std::endl << tmp);*/

    if (!(tmp - tmp.transpose()).isZero()) {
        MSG_DEBUG("NOT SYMMETRIC!!!");
        MSG_DEBUG(tmp);
    }

    SelfAdjointEigenSolver<MatrixXd> saes(tmp);

    ret_lump.diag = saes.eigenvalues().real();
    MatrixXd p = saes.eigenvectors().real();
    MatrixXd p_inv = p.transpose();
    ret_lump.cleanup(p);
    ret_lump.cleanup(p_inv);
    /*MSG_DEBUG("================= p");*/
    /*MSG_DEBUG(p);*/
    /*MSG_DEBUG("================= p^-1");*/
    /*MSG_DEBUG(p_inv);*/
    /*MSG_DEBUG("================= diag");*/
    /*MSG_DEBUG(ret_lump.diag);*/
    /*MSG_DEBUG("================= p.p^-1");*/
    /*MSG_DEBUG((p * p_inv));*/

    ret_lump.p = Uinv * p;
    ret_lump.p_inv = p_inv * U;

    ret_lump.variant = m.variant;

    ret_lump.cleanup();

#if 0
    bool fail = false;

    if (!(p * p_inv - MatrixXd::Identity(ret_lump.p.rows(), ret_lump.p.cols())).isZero(1.e-6)) {
        MSG_DEBUG("@@ PROUT p.p^-1");
        MSG_DEBUG((p * p_inv - MatrixXd::Identity(p.rows(), p.cols())));
        fail = true;
    }

    if (!(p * ret_lump.diag.asDiagonal() * p_inv - tmp).isZero(1.e-6)) {
        MSG_DEBUG("@@ PROUT p.D.p^-1 == M");
        MSG_DEBUG((p * ret_lump.diag.asDiagonal() * p_inv));
        MSG_DEBUG("---");
        MSG_DEBUG(tmp);
        MSG_DEBUG("---");
        MSG_DEBUG((p * ret_lump.diag.asDiagonal() * p_inv - tmp));
        MSG_DEBUG("p^-1.M.p");
        MatrixXd prout = p_inv * tmp * p;
        ret_lump.cleanup(prout);
        MSG_DEBUG(prout);
        fail = true;
    }

    /*if (!(p_inv * ret_lump.diag.asDiagonal() * p - tmp).isZero(1.e-6)) {*/
        /*MSG_DEBUG("@@ PROUT P^-1.D.P == M");*/
        /*MSG_DEBUG((p_inv * ret_lump.diag.asDiagonal() * p - tmp));*/
        /*fail = true;*/
    /*}*/

    if (!(ret_lump.p * ret_lump.p_inv - MatrixXd::Identity(ret_lump.p.rows(), ret_lump.p.cols())).isZero(1.e-6)) {
        MSG_DEBUG("@@ PROUT P.P^-1");
        MSG_DEBUG((ret_lump.p * ret_lump.p_inv - MatrixXd::Identity(ret_lump.p.rows(), ret_lump.p.cols())));
        fail = true;
    }

    if (!(ret_lump.p * ret_lump.diag.asDiagonal() * ret_lump.p_inv - ret_lump.inf_mat).isZero(1.e-6)) {
        MSG_DEBUG("@@ LOSER (BAD DIAG)");
        MSG_DEBUG((ret_lump.p * ret_lump.diag.asDiagonal() * ret_lump.p_inv - ret_lump.inf_mat));
        fail = true;
    }

    MatrixXd eigtest = ret_lump.inf_mat * ret_lump.stat_dist;
    /*if (!(eigtest == MatrixXd::Zero(eigtest.rows(), eigtest.cols()) || eigtest.isZero(1.e-6))) {*/
    if (!eigtest.isZero(1.e-6)) {
        MSG_DEBUG("@@ LOSER AGAIN");
        MSG_DEBUG(eigtest);
        MSG_DEBUG("---");
        MSG_DEBUG(eigtest.isZero());
        MSG_DEBUG(eigtest.isZero(1.e-6));
        MSG_DEBUG(eigtest.sum());
        MSG_DEBUG(eigtest.lpNorm<1>());
        fail = true;
    }   

    if (0 && fail) {
        MSG_DEBUG("MEMENTO");
        MSG_DEBUG("U" << std::endl << U);
        MSG_DEBUG("U^-1" << std::endl << Uinv);
        MSG_DEBUG("tmp" << std::endl << tmp);
        MSG_DEBUG("p" << std::endl << p);
        MSG_DEBUG("p^-1" << std::endl << p_inv);
        MSG_DEBUG("- - - - - - - - - - - - - - - - - - - - - - - - - - -");
        MSG_DEBUG(ret_lump);
        exit(-1);
    }
#endif

    return ret_lump;
}


MatrixXd compute_lumping_costs(const geno_matrix& m, const std::set<subset>& P0);
std::set<subset> init_partition(const geno_matrix& m);

geno_matrix lump(const geno_matrix& m, bool harder=false)
{
    /*MSG_DEBUG("LUMPING=====================================================");*/
    /*MSG_DEBUG(MATRIX_SIZE(m.inf_mat));*/
    /*MSG_DEBUG(m);*/
    /*MSG_DEBUG("------------------------------------------------------------");*/
    ::lumper<MatrixXd, label_type> l(m.inf_mat, m.labels);
    auto lumping_partition = l.refine_all();
    /*if (harder) {*/
        /*l.try_harder(lumping_partition);*/
    /*}*/
    /*MSG_DEBUG("" << lumping_partition);*/

    /* TODO tester que selon la formule de Sylvain cette partition donne un joli 0 */

    /*MSG_DEBUG("PARTITION");*/
    /*MSG_DEBUG("" << lumping_partition);*/
#if 0
    if (0)
    {
        auto P = init_partition(m);
        MSG_DEBUG("LUMPING COSTS");
        MSG_DEBUG("" << compute_lumping_costs(m, P));
        MSG_DEBUG("" << lumping_partition);

        double delta = compute_delta(m, lumping_partition);
        MSG_DEBUG("LUMP_DELTA=" << delta);
        if (delta > 1.e-10 || delta != delta) {
            /*MSG_DEBUG(m.inf_mat);*/
            MSG_DEBUG("" << lumping_partition);
        }
    }
#endif
    return lump_using_partition_weighted(m, lumping_partition);
    (void) harder;
}


geno_matrix split_matrix(const geno_matrix& m)
{
    std::set<label_type> labels(m.labels.begin(), m.labels.end());
    int row, col;
    std::set<subset> partition;
    /*std::map<label_type, std::map<double, std::vector<int>>> lump_states;*/
    for (const auto& l: labels) {
        int rows = 0;
        for (const auto& ml: m.labels) {
            if (ml == l) { ++rows; }
        }
        MatrixXd splitter = MatrixXd::Zero(rows, m.cols());
        col = 0;
        row = 0;
        std::vector<int> state_index;
        for (const auto& ml: m.labels) {
            if (ml == l) {
                splitter(row, col) = 1;
                ++row;
                state_index.push_back(col);
            }
            ++col;
        }
        /*MSG_DEBUG("SPLITTER FOR " << l);*/
        /*MSG_DEBUG(splitter);*/
        /*MSG_DEBUG("SUB-MATRIX FOR " << l);*/
        MatrixXd mat = splitter * m.inf_mat * splitter.transpose();
        /*MSG_DEBUG(mat);*/
        VectorXd sums = mat.colwise().sum();
        /*MSG_DEBUG("SUMS " << sums);*/
        std::map<double, std::vector<int>> state_groups;
        for (int i = 0; i < sums.size(); ++i) {
            state_groups[sums[i]].push_back(state_index[i]);
            /*state_groups[0].push_back(state_index[i]);*/
        }
        for (const auto& kv: state_groups) {
            partition.insert(kv.second);
        }
    }
    /*MSG_DEBUG("OVER-LUMP");*/
    /*MSG_DEBUG("" << partition);*/
    /*for (const auto& C: partition) {*/
        /*std::stringstream msg;*/
        /*for (int s: C) {*/
            /*msg << ' ' << m.labels[s] << '[' << s << ']';*/
        /*}*/
        /*MSG_DEBUG(msg.str());*/
    /*}*/
    /*MSG_DEBUG_INDENT;*/

    geno_matrix ret = lump_using_partition_weighted(m, partition);

    /*MSG_DEBUG(ret);*/
    /*MSG_DEBUG_DEDENT;*/

    return ret;
}


void output_sur_son_balcon(const MatrixXd& sums, const std::set<subset>& P)
    /* manifestement parce que derisavi par procuration. */
{
    VectorXd part(sums.cols());
    int col = 0;
    for (const auto& C: P) {
        for (int s: C) {
            part(s) = col;
        }
        ++col;
    }

    MatrixXd transp = MatrixXd::Zero(sums.cols(), sums.cols());
    int row = 0;
    for (const auto& C: P) {
        for (int s: C) {
            transp(s, row) = 1;
            ++row;
        }
    }

    /*part = transp * part;*/

    /*MSG_DEBUG("=========" << std::endl << transp << std::endl << "=========");*/

    MatrixXd output(sums.rows() + 2, sums.cols());
    for (int i = 0; i < output.cols(); ++i) {
        output(0, i) = i;
    }
    output.row(1) = part.transpose();
    output.bottomRows(sums.rows()) = sums;
    output *= transp;
    /*MSG_DEBUG(output);*/
}


/*bool operator < (const VectorXd& v1, const VectorXd& v2)*/
namespace std {
    template <int L, typename _Scalar>
        struct less<Eigen::Matrix<_Scalar, L, 1>> {
            bool operator () (const Eigen::Matrix<_Scalar, L, 1>& v1, const Eigen::Matrix<_Scalar, L, 1>& v2) const
            {
                /*if ((v1 - v2).isZero()) {*/
                    /*return false;*/
                /*}*/
                for (int i = 0; i < v1.size(); ++i) {
                    if (v1(i) < v2(i)) return true;
                    if (v2(i) < v1(i)) return false;
                }
                return false;
            }
        };
}


std::set<subset> partition_on_labels(const std::vector<label_type>& labels)
{
    std::set<subset> P;
    lumper_map<label_type, subset> tmp;
    for (size_t i = 0; i < labels.size(); ++i) {
        auto& l = labels[i];
        /*if (tmp[l].size() == 0) {*/
            /*tmp[l].resize(G.column_labels.size());*/
        /*}*/
        /*tmp[l].set(i);*/
        tmp[l]./*insert*/push_back(i);
    }
    for (auto& kv: tmp) {
        /*std::cout << kv.first << "\t" << kv.second << std::endl;*/
        P.insert(kv.second);
    }

    return P;
}


void compute_U(const MatrixXd inf_mat, VectorXd& U, VectorXd& Uinv)
{
}


void subset_union(const subset& s1, const subset& s2, subset& output)
{
    output.resize(s1.size() + s2.size());
    auto it = std::set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), output.begin());
    output.resize(it - output.begin());
}


std::set<subset> init_partition(const geno_matrix& m)
{
    std::set<subset> ret;
    for (int i = 0; i < m.inf_mat.cols(); ++i) {
        ret.insert({i});
    }
    return ret;
}

std::set<subset>::const_iterator merge(const subset& S1, const subset& S2, std::set<subset>& P)
{
    P.erase(S1);
    P.erase(S2);
    subset Stot;
    subset_union(S1, S2, Stot);
    return P.insert(Stot).first;
}

void unmerge(const subset& S1, const subset& S2, std::set<subset>::const_iterator Stot, std::set<subset>& P)
{
    P.erase(Stot);
    P.insert(S1);
    P.insert(S2);
}




struct ClassLabels {
    std::vector<label_type> labels;
    const std::vector<label_type>* all_labels;

    ClassLabels(const geno_matrix& m)
        : labels(), all_labels(&m.labels)
    {}

    void recompute(const MatrixXd& C)
    {
        labels.clear();
        labels.resize(C.rows(), {0, 0});
        C.visit(*this);
        /*MSG_DEBUG("class labels: " << labels);*/
    }

    // called for the first coefficient
    void init(const double& value, int i, int j)
    {
        if (value != 0.0) {
            labels[i] = (*all_labels)[j];
        }
    }
    // called for all other coefficients
    void operator() (const double& value, int i, int j)
    {
        init(value, i, j);
    }
};


void removeZeroRows0(Eigen::MatrixXd& mat)
{
    Eigen::Matrix<bool, Eigen::Dynamic, 1> empty = (mat.array() == 0).rowwise().all();

    size_t last = mat.rows() - 1;
    while (empty(last)) { --last; }
    for (size_t i = 0; i < last + 1;)
    {
        if (empty(i))
        {
            mat.row(i).swap(mat.row(last));
            empty.segment<1>(i).swap(empty.segment<1>(last));
            while (empty(last)) { --last; }
        }
        else
            ++i;
    }
    mat.conservativeResize(last + 1, mat.cols());
}


void removeZeroRows(Eigen::MatrixXd& mat)
{
    Eigen::Matrix<bool, Eigen::Dynamic, 1> empty = (mat.array() == 0).rowwise().all();

    int rowsrc = 0;
    int rowdest = 0;

    /*MSG_DEBUG("removeZeroRows mat before");*/
    /*MSG_DEBUG(mat);*/

    for (rowsrc = 0; rowsrc < empty.size(); ++rowsrc) {
        if (empty(rowsrc)) {
            continue;
        }
        if (rowsrc > rowdest) {
            mat.row(rowdest) = mat.row(rowsrc);
        }
        ++rowdest;
    }
    mat.conservativeResize(rowdest, mat.cols());

    /*MSG_DEBUG("removeZeroRows mat after");*/
    /*MSG_DEBUG(mat);*/
}


void reorder(Eigen::MatrixXd& mat)
{
    Eigen::Matrix<bool, Eigen::Dynamic, 1> empty = (mat.array() == 0).rowwise().all();

    int rowsrc = 0;
    int rowdest = 0;

    /*MSG_DEBUG("removeZeroRows mat before");*/
    /*MSG_DEBUG(mat);*/

    for (rowsrc = 0; rowsrc < empty.size(); ++rowsrc) {
        if (empty(rowsrc)) {
            continue;
        }
        if (rowsrc > rowdest) {
            mat.row(rowdest) = mat.row(rowsrc);
        }
        ++rowdest;
    }
    /*mat.conservativeResize(rowdest, mat.cols());*/
    mat.bottomRows(mat.rows() - rowdest).setZero();

    /*MSG_DEBUG("removeZeroRows mat after");*/
    /*MSG_DEBUG(mat);*/
}


std::set<subset> matrix2partition(const MatrixXd& C)
{
    std::set<subset> P0;
    for (int i = 0; i < C.rows(); ++i) {
        subset s;
        for (int j = 0; j < C.cols(); ++j) {
            if (C(i, j) != 0.) {
                s.push_back(j);
            }
        }
        P0.insert(s);
    }
    return P0;
}


std::map<label_type, double> abundances(geno_matrix& m)
{
    Eigen::Map<Eigen::Array<label_type, Eigen::Dynamic, 1>> labels(m.labels.data(), m.labels.size(), 1);
    MatrixXd abund = m.lim_inf().col(0);
    std::set<label_type> unique(m.labels.begin(), m.labels.end());
    std::map<label_type, double> ret;
    for (const auto& l: unique) {
        ret[l] = ((labels == l).cast<double>().array() * abund.array()).sum();
    }
    return ret;
}


std::ostream& operator << (std::ostream& os, const std::map<label_type, double>& ab)
{
    os << "abundances:";
    for (const auto& kv: ab) {
        os << ' ' << kv.first << ": " << std::left << std::setw(12) << SPELL_STRING((kv.second * 100.) << '%');
    }
    return os;
}


bool operator == (const std::map<label_type, double>& ab1, const std::map<label_type, double>& ab2)
{
    bool ret = true;
    for (const auto& kv: ab1) {
        ret &= abs(kv.second - ab2.find(kv.first)->second) < 1.e-6;
    }
    return ret;
}


void save_matrix(const MatrixXd& m, const std::string& filename)
{
    ofile f(filename);
    int tmpi;
    tmpi = m.rows(); f.write((const char*)&tmpi, sizeof(tmpi));
    tmpi = m.cols(); f.write((const char*)&tmpi, sizeof(tmpi));
    f.write((const char*)m.data(), m.size() * sizeof(double));
}


MatrixXd load_matrix(const std::string& filename)
{
    MatrixXd ret;
    int rows, cols;
    ifile f(filename);
    f.read((char*)&rows, sizeof(rows));
    f.read((char*)&cols, sizeof(cols));
    ret.resize(rows, cols);
    f.read((char*)ret.data(), ret.size() * sizeof(double));
    return ret;
}


MatrixXd test_geno_matrix(const std::string& name, geno_matrix& GEN, const std::string& haplo)
{
    bool do_output = name != "";
    Eigen::IOFormat r_table(Eigen::StreamPrecision, Eigen::DontAlignCols, "\t", "\n", "", "", "", "");
    /*VectorXd A(4);*/
    /*VectorXd H(4);*/
    /*VectorXd B(4);*/
    /*A << 1, 0, 0, 0;*/
    /*H << 0, 1, 1, 0;*/
    /*B << 0, 0, 0, 1;*/
    ofile ofs(SPELL_STRING("values-" << haplo << '.' << name << ".txt"));
    MSG_DEBUG("TEST_GENO_MATRIX " << name);

    /*MatrixXd abundance = GEN.lim_inf().col(0);*/
    MatrixXd abundance = GEN.stat_dist;
    /*MSG_DEBUG("### " << name);*/
    /*MSG_DEBUG(GEN);*/
    /*MSG_DEBUG("" << abundances(GEN));*/

    Eigen::Map<Eigen::Array<label_type, Eigen::Dynamic, 1>> labels(GEN.labels.data(), GEN.labels.size(), 1);
    VectorXd A = (labels == label_type{'a', 'a'}).cast<double>();
    VectorXd B;
    if ((labels == label_type('b', 'b')).any()) {
        B = (labels == label_type{'b', 'b'}).cast<double>();
    } else {
        B = A;
    }
    VectorXd H = ((labels == label_type{'a', 'b'}) + (labels == label_type{'b', 'a'})).cast<double>();
    A = (A.array() * abundance.array()).matrix();
    H = (H.array() * abundance.array()).matrix();
    B = (B.array() * abundance.array()).matrix();
    A /= A.sum();
    B /= B.sum();
    H /= H.sum();
    /*MSG_DEBUG("A " << A.transpose());*/
    /*MSG_DEBUG("B " << B.transpose());*/
    /*MSG_DEBUG("H " << H.transpose());*/
    /*A << 1,1,1, 0,0, 0,0,0;*/
    /*H << 0,0,0, 1,1, 0,0,0;*/
    /*B << 0,0,0, 0,0, 1,1,1;*/
    /*VectorXd A(2);*/
    /*VectorXd H(2);*/
    /*A << 1, 0;*/
    /*H << 0, 1;*/
    VectorXd U = A + H + B;

    if (haplo.size() != 3) {
        MSG_ERROR("THERE HAS TO BE 3 MARKERS, NO MORE, NO LESS. 3 IS THE NUMBER YOU SHALL COUNT TO. YOU SHALL NOT COUNT TO 2, EXCEPT ON THE WAY TO 3.", "");
        return {};
    }

#define GET_MRK(_n) (haplo[_n] == 'A' ? A : haplo[_n] == 'B' ? B : haplo[_n] == 'H' ? H : U)

    VectorXd M1 = GET_MRK(0);
    double d1 = log(1. - .8) * -50.; /* #8 */
    VectorXd M2 = GET_MRK(1);
    double d2 = log(1. - .1) * -50.; /* #1 */
    MSG_DEBUG("d1=" << d1 << "cM");
    MSG_DEBUG("d2=" << d2 << "cM");
    VectorXd M3 = GET_MRK(2);
    double step = d1 / 120;

    MatrixXd result(GEN.rows(), 121);
    /*MatrixXd result(GEN.rows(), 3);*/
    int i = 0;
    {
        std::stringstream labelstr;
        labelstr << GEN.labels[0].first << GEN.labels[0].second;
        for (size_t l = 1; l < GEN.labels.size(); ++l) {
            labelstr << '\t' << GEN.labels[l].first << GEN.labels[l].second;
        }
        if (do_output) { ofs << labelstr.str() << std::endl; }
    }
    /* map: A - 30 - B - 5 - H */
    MatrixXd reverse = abundance.array().inverse().matrix().asDiagonal();
    VectorXd outer = (M2.array() * ((reverse * GEN.exp(d2 * .01)).transpose() * M3).array()).matrix();
    outer /= outer.sum();
    for (double d = 0; d <= d1; d += step) {
        MatrixXd prob = ((GEN.exp(d * .01) * M1).array() * ((reverse * GEN.exp((d1 - d) * .01)).transpose() * outer).array()).matrix();
        /*MatrixXd prob = GEN.exp(d * .01) * A;*/
        /*MSG_DEBUG("##DUMP TR(" << d << ")");*/
        /*MSG_DEBUG(GEN.exp(d * .01));*/
        result.col(i++) = prob / prob.sum();
    }

    /*MSG_DEBUG(result.transpose().format(r_table));*/
    if (do_output) { ofs << result.transpose().format(r_table); }
    if (A == B) {
        MatrixXd redux(2, A.size());
        redux.row(0) = (labels == label_type{'a', 'a'}).cast<double>().transpose();
        redux.row(1) = ((labels == label_type{'a', 'b'}) + (labels == label_type{'b', 'a'})).cast<double>().transpose();
        result = redux * result;
    } else {
        MatrixXd redux(3, A.size());
        redux.row(0) = (labels == label_type{'a', 'a'}).cast<double>().transpose();
        redux.row(1) = ((labels == label_type{'a', 'b'}) + (labels == label_type{'b', 'a'})).cast<double>().transpose();
        redux.row(2) = (labels == label_type{'b', 'b'}).cast<double>().transpose();
        result = redux * result;
    }
    return result;
}


struct component {
    bool l1;  /* label_in[0] == label_out[0] */
    bool l2;  /* label_in[1] == label_out[1] */
    double value;

    component(const label_type& L1, const label_type& L2, double v)
        : l1(L1.first == L2.first), l2(L1.second == L2.second), value(v)
    {}

    bool
        operator < (const component& other) const
        {
            return l1 < other.l1
                   || (l1 == other.l1 && (l2 < other.l2
                                          || (l2 == other.l2 && value < other.value)));
        }

    bool
        operator == (const component& other) const
        {
            return l1 == other.l1 && l2 == other.l2 && value == other.value;
        }
};


struct vector_descriptor {
    std::multiset<component> components;

    vector_descriptor(const geno_matrix& M, int i)
        : components()
    {
        const auto& label = M.labels[i];
        for (int j = 0; j < M.rows(); ++j) {
            components.emplace(label, M.labels[j], M.inf_mat(j, i));
        }
    }

    bool
        operator == (const vector_descriptor& vd) const
        {
            return components == vd.components;
        }
};


MatrixXb find_symmetries(const geno_matrix& M)
{
    MatrixXb ret(M.cols(), M.cols());
    std::vector<vector_descriptor> descriptors;
    descriptors.reserve(M.cols());

    for (int i = 0; i < M.cols(); ++i) {
        descriptors.emplace_back(M, i);
    }

    for (int i = 0; i < M.cols(); ++i) {
        for (int j = 0; j < M.cols(); ++j) {
            ret(i, j) = i == j || descriptors[i] == descriptors[j];
        }
    }

    return ret;
}


struct label_switch {
    char first;
    char second;

    label_switch(char a, char b)
        : first(a < b ? a : b), second(a < b ? b : a)
    {}

    bool operator < (const label_switch& other) const { return first < other.first || (first == other.first && second < other.second); }
    bool operator == (const label_switch& other) const { return first == other.first && second == other.second; }
    bool operator != (const label_switch& other) const { return first != other.first || second != other.second; }
    bool is_identity() const { return first == second; }
    bool incompatible_with(const label_switch& other) const
    {
        return ((is_identity() && other.is_identity()) && (*this) != other)
        /*return (is_identity() && (first == other.first || first == other.second))*/
            || (other.is_identity() && (first == other.first || second == other.second));
    }

    bool compatible (const label_switch& other) const
    {
        bool id1 = first == second;
        bool id2 = other.first == other.second;
        bool x1 = first == other.second;
        bool x2 = second == other.first;
        bool l1 = first == other.first;
        bool l2 = second == other.second;
        /*MSG_DEBUG("compatible(" << (*this) << ", " << other << ") " << a << b << c << d << " @" << std::hex << ofs);*/
        /*return table[ofs];*/
        return !(
                (id1 && id2)                   /* refuse trivial switch */
            || ((id1 || id2) && (l1 || l2))    /* refuse switch if one is identity and the other actually switches the same letter */
            || ((!id1 && !id2                  /* refuse switch if both are non-trivial, different, and involving at least one letter in common */
                && ((l1 && !l2)
                   || (l2 && !l1) || x1 || x2)))
            );
    }

    friend
        std::ostream& operator << (std::ostream& os, const label_switch& ls)
        {
            return os << '[' << ls.first << " <-> " << ls.second << ']';
        }
};


bool operator < (const std::pair<label_switch, label_switch>& p1, const std::pair<label_switch, label_switch>& p2)
{
    return p1.first < p2.first || (p1.first == p2.first && p1.second < p2.second);
}


void analyse_symmetries0(const geno_matrix& M)
{
    std::vector<std::multiset<double>> descriptors;
    descriptors.reserve(M.cols());
    std::set<std::pair<label_switch, label_switch>> uniq;

    for (int i = 0; i < M.cols(); ++i) {
        descriptors.emplace_back();
        for (int j = 0; j < M.rows(); ++j) {
            descriptors.back().insert(M.inf_mat(j, i));
        }
    }

    label_switch nil(0, 0);

    MatrixXb groups = MatrixXb::Zero(M.rows(), M.cols());

    for (size_t i = 0; i < descriptors.size(); ++i) {
        vector_descriptor vi(M, i);
        for (size_t j = i + 1; j < descriptors.size(); ++j) {
            if (descriptors[i] == descriptors[j]) {
                auto l1 = M.labels[i];
                auto l2 = M.labels[j];
                label_switch s1(l1.first, l2.first);
                label_switch s2(l1.second, l2.second);
                vector_descriptor vj(M, j);
                bool grouping = vi == vj;
                if (s1.compatible(s2) || s2.compatible(s1)) {
                    groups(i, j) = grouping;
                    groups(j, i) = grouping;
                    MSG_DEBUG(i << ',' << j << ": " << M.labels[i] << ' ' << M.labels[j] << ' ' << s1 << s2);
                    label_switch u1 = s1.is_identity() ? nil : s1;
                    label_switch u2 = (s1 == s2 || s2.is_identity()) ? nil : s2;
                    uniq.emplace(u1 < u2 ? u1 : u2, u1 < u2 ? u2 : u1);
                }
            }
        }
    }

    MSG_DEBUG("GROUPS");
    MSG_DEBUG(groups);

    MSG_DEBUG("UNIQ:");
    for (const auto& p: uniq) {
        if (p.first == p.second) {
            if (!p.first.is_identity()) {
                MSG_DEBUG("- " << p.first);
            }
        } else if (p.first.is_identity()) {
            if (!p.second.is_identity()) {
                MSG_DEBUG("- " << p.second);
            }
        } else if (p.second.is_identity()) {
            if (!p.first.is_identity()) {
                MSG_DEBUG("- " << p.first);
            }
        } else {
            MSG_DEBUG("- " << p.first << ' ' << p.second);
        }
    }
}


struct labelled_component {
    label_type label;
    double value;
    labelled_component(const label_type& l, double v) : label(l), value(v) {}
    bool operator < (const labelled_component& lc) const { return label < lc.label || (label == lc.label && value < lc.value); }
    bool operator == (const labelled_component& lc) const { return label == lc.label && value == lc.value; }
};


struct label_switch_pair {
    label_switch s1;
    label_switch s2;

    label_switch_pair(const label_switch& a, const label_switch& b)
        : s1(0, 0), s2(0, 0)
    {
        s1 = a;
        s2 = b;
        if (s1.is_identity() || s1 == s2) {
            s1 = s2;
            s2 = {0, 0};
        }
        if (s1.is_identity()) {
            throw 0;
        }
        if (s1.first == s2.second || s1.second == s2.first || s1.first == s2.first || s1.second == s2.second) {
            throw 1;
        }
    }

    char perform_switch(char c) const
    {
        return c == s1.first
               ? s1.second
               : c == s1.second
                 ? s1.first
                 : c == s2.first
                   ? s2.second
                   : c == s2.second
                     ? s2.first
                     : c;
    }

    label_type operator () (const label_type& l) const
    {
        return {perform_switch(l.first), perform_switch(l.second)};
    }

    bool operator < (const label_switch_pair& other) const
    {
        return s1 < other.s1 || (s1 == other.s1 && s2 < other.s2);
    }
};


struct vector_switch_descriptor {
    std::multiset<labelled_component> components;

    vector_switch_descriptor(const geno_matrix& M, int i)
        : components()
    {
        for (int j = 0; j < M.rows(); ++j) {
            components.emplace(M.labels[j], M.inf_mat(j, i));
        }
    }

    vector_switch_descriptor(const geno_matrix& M, int i, const label_switch_pair& lsp)
        : components()
    {
        for (int j = 0; j < M.rows(); ++j) {
            components.emplace(lsp(M.labels[j]), M.inf_mat(j, i));
        }
    }

    bool
        operator == (const vector_switch_descriptor& other) const
        {
            return components == other.components;
        }

    friend
        std::ostream& operator << (std::ostream& os, const vector_switch_descriptor& vsd)
        {
            os << '{';
            for (const auto& lc: vsd.components) {
                os << ' ' << lc.label << ':' << lc.value;
            }
            return os << " }";
        }
};


typedef std::map<int, int> symmetry_table_type;
typedef std::vector<symmetry_table_type> symmetry_list_type;

symmetry_list_type
analyse_symmetries(const geno_matrix& M)
{
    symmetry_list_type ret;
    label_switch nil(0, 0);
    std::set<std::pair<label_switch, label_switch>> uniq;

    MatrixXb groups = MatrixXb::Zero(M.rows(), M.cols());

    for (size_t i = 0; i < M.cols(); ++i) {
        vector_switch_descriptor vi(M, i);
        for (size_t j = i + 1; j < M.cols(); ++j) {
            auto l1 = M.labels[i];
            auto l2 = M.labels[j];
            label_switch s1(l1.first, l2.first);
            label_switch s2(l1.second, l2.second);

            if (s1.compatible(s2)) {
                label_switch_pair lsp(s1, s2);
                vector_switch_descriptor vj(M, j, lsp);
                MSG_DEBUG("" << vi << " vs " << vj);
                if (vi == vj) {
                    groups(i, j) = 1;
                    groups(j, i) = 1;
                    MSG_DEBUG(i << ',' << j << ": " << M.labels[i] << ' ' << M.labels[j] << ' ' << s1 << s2);
                    label_switch u1 = s1.is_identity() ? nil : s1;
                    label_switch u2 = (s1 == s2 || s2.is_identity()) ? nil : s2;
                    uniq.emplace(u1 < u2 ? u1 : u2, u1 < u2 ? u2 : u1);
                }
            }
        }
    }

    MSG_DEBUG("GROUPS");
    MSG_DEBUG(groups);

    MSG_DEBUG("UNIQ:");
    MatrixXb prev = MatrixXb::Identity(M.cols(), M.cols());
    for (const auto& p: uniq) {
        ret.emplace_back();
        if (p.first == p.second) {
            if (!p.first.is_identity()) {
                MSG_DEBUG("- " << p.first);
            }
        } else if (p.first.is_identity()) {
            if (!p.second.is_identity()) {
                MSG_DEBUG("- " << p.second);
            }
        } else if (p.second.is_identity()) {
            if (!p.first.is_identity()) {
                MSG_DEBUG("- " << p.first);
            }
        } else {
            MSG_DEBUG("- " << p.first << ' ' << p.second);
        }
        label_switch_pair lsp(p.first, p.second);
        MatrixXb group(M.cols(), M.cols());
        for (int i = 0; i < M.cols(); ++i) {
            vector_switch_descriptor vi(M, i);
            for (int j = 0; j < M.cols(); ++j) {
                if (i == j) {
                    group(j, i) = 0;
                } else {
                    vector_switch_descriptor vj(M, j, lsp);
                    group(j, i) = vi == vj;
                    if (vi == vj) {
                        ret.back()[i] = j;
                        ret.back()[j] = i;
                    }
                }
            }
        }
        MSG_DEBUG(group);
        MSG_DEBUG("---------------------------");
        MSG_DEBUG((group * group.transpose()));
        MSG_DEBUG("= = = = = = = = = = = = = =");
        MSG_DEBUG((group * prev.transpose()));
        prev = group;
    }
}



int main(int argc, char** argv)
{
    auto A = ancestor_matrix('a');
    auto B = ancestor_matrix('b');

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################                  F1                  ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F1 = lump(kronecker(kronecker(A, gamete), kronecker(B, gamete)));

    if (0)
    {
        auto BC = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
        auto BC2 = lump(kronecker(kronecker(BC, gamete), kronecker(A, gamete)));
        VectorXd ab = BC2.lim_inf().col(0);
        MSG_DEBUG("BC2 . ab = " << (BC2.inf_mat * ab).transpose());
        MSG_DEBUG("Ker(BC2) =" << std::endl << BC2.inf_mat.fullPivLu().kernel());
        VectorXd s = BC2.inf_mat.fullPivLu().kernel();

        test_geno_matrix("BC2", BC2, "AAH");
        test_geno_matrix("BC2", BC2, "AAA");

        return 0;
    }

    if (0)
    {
        MSG_DEBUG("* * * SELFING * * *");
        auto F2 = lump(kronecker(F1, selfing_gamete));
        auto F3 = lump(kronecker(F2, selfing_gamete));
        auto F4 = lump(kronecker(F3, selfing_gamete));
        auto F5 = lump(kronecker(F4, selfing_gamete));
        auto F6 = lump(kronecker(F5, selfing_gamete));

        MSG_DEBUG("* * * BACKCROSSING * * *");
        auto BC = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
        auto BC2 = lump(kronecker(kronecker(BC, gamete), kronecker(A, gamete)));
        auto BC3 = lump(kronecker(kronecker(BC2, gamete), kronecker(A, gamete)));
        auto BC4 = lump(kronecker(kronecker(BC3, gamete), kronecker(A, gamete)));
        auto BC5 = lump(kronecker(kronecker(BC4, gamete), kronecker(A, gamete)));
        auto BC6 = lump(kronecker(kronecker(BC5, gamete), kronecker(A, gamete)));

        MSG_DEBUG("* * * RANDOM CROSSING * * *");
        auto C2 = lump(kronecker(kronecker(F1, gamete), kronecker(F1, gamete)));
        auto C3 = lump(kronecker(kronecker(C2, gamete), kronecker(C2, gamete)));
        auto C4 = lump(kronecker(kronecker(C3, gamete), kronecker(C3, gamete)));
     }

    if (0)
    {
        auto F2 = lump(kronecker(F1, selfing_gamete));
        auto F3 = kronecker(F2, selfing_gamete);
        auto F3l = lump(F3);
        /*MSG_DEBUG("STUDY LUMPING F3");*/
        experimental_lumper el(F3);
        auto g8 = el.do_lump(8);
        MSG_DEBUG("=== === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === ===");
        experimental_lumper el2(F3l);
        auto g6 = el.do_lump(6);
        test_geno_matrix("g8_data.matrix", g8, "ABH");
        test_geno_matrix("g6_data.matrix", g6, "ABH");
    }

    if (0)
    {
        auto F2 = lump(kronecker(F1, selfing_gamete));
        auto F3 = lump(kronecker(F2, selfing_gamete));
        auto F4 = lump(kronecker(F3, selfing_gamete));
        auto F5 = lump(kronecker(F4, selfing_gamete));
        auto F6 = lump(kronecker(F5, selfing_gamete));
        MSG_DEBUG("STUDY LUMPING F6");
        experimental_lumper el(kronecker(F5, selfing_gamete));
        auto g6 = el.do_lump(32);
        /*auto g6 = el.do_lump(ORDER);*/
        test_geno_matrix("g64_data.matrix", F6, "ABH");
        test_geno_matrix("g32_data.matrix", g6, "ABH");
        /*for (int i = 0; i < 20; i += 2) {*/
            /*el.do_lump(i);*/
        /*}*/
        return 0;
    }

    if (0)
    {
        auto F = F1;
        auto Fl = F1;
        auto Fl2 = F1;
        /*for (int i = 2; i < 10; ++i) {*/
            /*F = lump(kronecker(F, selfing_gamete));*/
        /*}*/
        /*test_geno_matrix("pseudo-RIL", F, "ABB");*/
        /*test_geno_matrix("pseudo-RIL", F, "ABA");*/
#if 1
        double test_crit = std::numeric_limits<double>::infinity();
        int i = 2;
        for (; i < 40; ++i) {
            F = lump(kronecker(Fl, selfing_gamete));
            experimental_lumper el(F);
            auto inf_mat = Fl.inf_mat;
            auto bak = Fl;
            Fl = el.do_lump(64);
            /*Fl = el.do_lump(32);*/
            if (1 && inf_mat.rows() == Fl.inf_mat.rows()) {
                /*MSG_DEBUG("@@@ DELTA at #" << i);*/
                /*MSG_DEBUG((Fl.inf_mat - inf_mat));*/
                /*auto ang = min_column_angles(inf_mat, Fl.inf_mat);*/
                /*auto G1 = lump_using_partition_weighted(bak, experimental_lumper(bak).partition_on_labels());*/
                /*auto G2 = lump_using_partition_weighted(Fl, experimental_lumper(Fl).partition_on_labels());*/
                /*auto ang = min_column_angles(G1.inf_mat, G2.inf_mat);*/
                /*auto G1 = bak.inf_mat * bak.stat_dist.asDiagonal();*/
                /*auto G2 = Fl.inf_mat * Fl.stat_dist.asDiagonal();*/
                auto G1 = bak.inf_mat;
                auto G2 = Fl.inf_mat;
                auto ang = min_column_angles(G1, G2);

                double plogp = 0;
                for (int i = 0; i < Fl.stat_dist.size(); ++i) {
                    plogp -= Fl.stat_dist(i) * log(Fl.stat_dist(i));
                }
                /*MSG_DEBUG("sigma_p*log_p = " << plogp);*/

                /*ang.array() *= Fl.stat_dist.array();*/
                /*MSG_DEBUG("" << Fl.stat_dist.transpose());*/
                double a = ang.lpNorm<2>();
                /*MSG_DEBUG("ANGLES(G" << (i - 1) << ", G" << i << ") = " << a);*/

                int n_A = 0;
                for (const auto& l: Fl.labels) {
                    if (l == label_type{'a', 'a'}) {
                        ++n_A;
                    }
                }
                MSG_DEBUG("n_A=" << n_A);

                MatrixXd tmpA(Fl.inf_mat.rows(), n_A);
                int col = 0, i = 0;

                for (const auto& l: Fl.labels) {
                    if (l == label_type{'a', 'a'}) {
                        tmpA.col(col) = Fl.inf_mat.col(i) * Fl.stat_dist(i);
                        ++col;
                    }
                    ++i;
                }

                MatrixXd ata = tmpA.transpose() * tmpA;
                SelfAdjointEigenSolver<MatrixXd> saes(ata);

                /*MSG_DEBUG("SPECTRUM At.A");*/
                /*MSG_DEBUG(saes.eigenvalues().real().transpose());*/
                /*VectorXd ev = saes.eigenvalues().real().transpose();*/

                /*MSG_DEBUG("EV min=" << ev(0) << " max=" << ev(ev.size() - 1) << " cond=" << (ev(ev.size() - 1) / ev(0)));*/

                if (test_crit > a) {
                    test_crit = a;
                } else {
                    Fl = bak;
                    break;
                }
#if 0
                MSG_DEBUG("ANGLES(G" << (i - 1) << ", G" << i << ") = " << ang.transpose());
                auto lu = Fl.inf_mat.fullPivLu();
                MSG_DEBUG("RANK = " << lu.rank());
                lu.setThreshold(1.e-10);
                MSG_DEBUG("RANK(1.e-10) = " << lu.rank());
                lu.setThreshold(1.e-6);
                MSG_DEBUG("RANK(1.e-6) = " << lu.rank());
#endif
            }
            /*Fl2 = lump(kronecker(Fl2, selfing_gamete));*/
            /*experimental_lumper el2(Fl2);*/
            /*Fl2 = el2.do_lump(16);*/
            /*test_geno_matrix(SPELL_STRING("F" << i << "_normal"), F);*/
            /*test_geno_matrix(SPELL_STRING("F" << i << "_over"), Fl);*/
            /*test_geno_matrix(SPELL_STRING("F" << i << "_inherit"), Fl2);*/
        }
#else
        experimental_lumper el(F);
        Fl = el.do_lump(128);
#endif
        /*MSG_DEBUG(Fl);*/
        test_geno_matrix("RIL6", Fl, "ABB");
        test_geno_matrix("RIL6", Fl, "ABA");

        for (; i < 40; ++i) {
            F = lump(kronecker(Fl, selfing_gamete));
            experimental_lumper el(F);
            auto inf_mat = Fl.inf_mat;
            auto bak = Fl;
            Fl = el.do_lump(64);
            /*Fl = el.do_lump(32);*/
            if (1 && inf_mat.rows() == Fl.inf_mat.rows()) {
                /*MSG_DEBUG("@@@ DELTA at #" << i);*/
                /*MSG_DEBUG((Fl.inf_mat - inf_mat));*/
                /*auto ang = min_column_angles(inf_mat, Fl.inf_mat);*/
                /*auto G1 = lump_using_partition_weighted(bak, experimental_lumper(bak).partition_on_labels());*/
                /*auto G2 = lump_using_partition_weighted(Fl, experimental_lumper(Fl).partition_on_labels());*/
                /*auto ang = min_column_angles(G1.inf_mat, G2.inf_mat);*/
                /*auto G1 = bak.inf_mat * bak.stat_dist.asDiagonal();*/
                /*auto G2 = Fl.inf_mat * Fl.stat_dist.asDiagonal();*/
                auto G1 = bak.inf_mat;
                auto G2 = Fl.inf_mat;
                auto ang = min_column_angles(G1, G2);

                double plogp = 0;
                for (int i = 0; i < Fl.stat_dist.size(); ++i) {
                    plogp -= Fl.stat_dist(i) * log(Fl.stat_dist(i));
                }
                /*MSG_DEBUG("sigma_p*log_p = " << plogp);*/

                /*ang.array() *= Fl.stat_dist.array();*/
                /*MSG_DEBUG("" << Fl.stat_dist.transpose());*/
                double a = ang.lpNorm<2>();
                /*MSG_DEBUG("ANGLES(G" << (i - 1) << ", G" << i << ") = " << a);*/

                int n_A = 0;
                for (const auto& l: Fl.labels) {
                    if (l == label_type{'a', 'a'}) {
                        ++n_A;
                    }
                }
                MSG_DEBUG("n_A=" << n_A);

                MatrixXd tmpA(Fl.inf_mat.rows(), n_A);
                int col = 0, i = 0;

                for (const auto& l: Fl.labels) {
                    if (l == label_type{'a', 'a'}) {
                        tmpA.col(col) = Fl.inf_mat.col(i) * Fl.stat_dist(i);
                        ++col;
                    }
                    ++i;
                }

                MatrixXd ata = tmpA.transpose() * tmpA;
                SelfAdjointEigenSolver<MatrixXd> saes(ata);

                /*MSG_DEBUG("SPECTRUM At.A");*/
                /*MSG_DEBUG(saes.eigenvalues().real().transpose());*/
                /*VectorXd ev = saes.eigenvalues().real().transpose();*/
                /*MSG_DEBUG("EV min=" << ev(0) << " max=" << ev(ev.size() - 1) << " cond=" << (ev(ev.size() - 1) / ev(0)));*/

            }
        }
    }

    if (0)
    {
        auto F2 = lump(kronecker(F1, selfing_gamete));
        auto F3 = lump(kronecker(F2, selfing_gamete));
        auto F4 = lump(kronecker(F3, selfing_gamete));
        auto F5 = lump(kronecker(F4, selfing_gamete));
        auto F6 = lump(kronecker(F5, selfing_gamete));
        auto F7 = lump(kronecker(F6, selfing_gamete));
        auto F8 = lump(kronecker(F7, selfing_gamete));
        auto F9 = lump(kronecker(F8, selfing_gamete));
        {
            experimental_lumper el(F9);
            auto F3l = el.do_lump(16);
            test_geno_matrix("RIL6", F3l, "ABB");
            test_geno_matrix("RIL6", F3l, "ABA");
            return 0;
        }
        test_geno_matrix("RIL6", F9, "ABB");
        test_geno_matrix("RIL6", F9, "ABA");
        return 0;
        MSG_DEBUG("STUDY LUMPING F6");
        experimental_lumper el6(F6);
        auto F6l = el6.do_lump(16);
        experimental_lumper el7(lump(kronecker(F6l, selfing_gamete)));
        auto F7l = el7.do_lump(16);
        test_geno_matrix("g64_data.matrix", F7, "ABH");
        test_geno_matrix("g32_data.matrix", F7l, "ABH");
        /*for (int i = 0; i < 20; i += 2) {*/
            /*el.do_lump(i);*/
        /*}*/
        return 0;
    }

    if (1)
    {
        auto C = ancestor_matrix('c');
        auto D = ancestor_matrix('d');
        auto CD = lump(kronecker(kronecker(C, gamete), kronecker(D, gamete)));
        auto CP = lump(kronecker(kronecker(F1, gamete), kronecker(CD, gamete)));
        auto CPs = lump(kronecker(CP, selfing_gamete));
        MSG_DEBUG(CPs);
        MSG_DEBUG("==========");
        MSG_DEBUG(find_symmetries(CPs));
        analyse_symmetries(CPs);
        MSG_DEBUG("==========");
        MSG_DEBUG(CP);
        MSG_DEBUG("==========");
        MSG_DEBUG(find_symmetries(CP));
        analyse_symmetries(CP);
        auto F2 = lump(kronecker(F1, selfing_gamete));
        MSG_DEBUG("==========");
        MSG_DEBUG(F2);
        MSG_DEBUG("==========");
        MSG_DEBUG(find_symmetries(F2));
        analyse_symmetries(F2);
        auto F2f = kronecker(F1, selfing_gamete);
        MSG_DEBUG("==========");
        MSG_DEBUG(F2f);
        MSG_DEBUG("==========");
        MSG_DEBUG(find_symmetries(F2f));
        analyse_symmetries(F2f);
        auto F3 = lump(kronecker(F2, selfing_gamete));
        MSG_DEBUG("==========");
        MSG_DEBUG(F3);
        MSG_DEBUG("==========");
        MSG_DEBUG(find_symmetries(F3));
        analyse_symmetries(F3);
        auto F3f = kronecker(F2, selfing_gamete);
        MSG_DEBUG("==========");
        MSG_DEBUG(F3f);
        MSG_DEBUG("==========");
        MSG_DEBUG(find_symmetries(F3f));
        analyse_symmetries(F3f);
        auto BC = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
        MSG_DEBUG("==========");
        MSG_DEBUG(BC);
        MSG_DEBUG("==========");
        MSG_DEBUG(find_symmetries(BC));
        analyse_symmetries(BC);
        auto BC2 = lump(kronecker(kronecker(BC, gamete), kronecker(A, gamete)));
        MSG_DEBUG("==========");
        MSG_DEBUG(BC2);
        MSG_DEBUG("==========");
        MSG_DEBUG(find_symmetries(BC2));
        analyse_symmetries(BC2);
        return 0;
    }

    if (0)
    {
        auto F2 = lump(kronecker(F1, selfing_gamete));
        /*auto F3 = kronecker(F2, selfing_gamete);*/
        auto F3 = lump(kronecker(F2, selfing_gamete));
        MSG_DEBUG(F3);
        return 0;
        auto F3l = lump(F3);
        MSG_DEBUG(F3l);
        MSG_DEBUG("STUDY LUMPING F3");
        experimental_lumper el(F3);
        MSG_DEBUG(el.do_lump(4));
        return 0;
        el.do_lump(8);
        MSG_DEBUG("STUDY LUMPING F3-lumped");
        experimental_lumper el2(F3l);
        el2.do_lump(8);
        auto F4 = lump(kronecker(F3, selfing_gamete));
        std::map<double, size_t> hist;
        for (int i = 0; i < 1000; ++i) {
            double delta;
            /*auto kmF4 = kmeans_lump(kronecker(F3, selfing_gamete), 22, delta);*/
            /*auto kmF3 = kmeans_lump(kronecker(F2, selfing_gamete), 16, delta);*/
            /*auto kmF5 = kmeans_lump(kronecker(F4, selfing_gamete), 18, delta);*/
            hist[delta]++;
        }
        /*MSG_DEBUG(F3);*/
        for (const auto& kv: hist) {
            MSG_DEBUG(std::setw(16) << kv.first << "\t" << kv.second);
        }
        auto F5 = lump(kronecker(F4, selfing_gamete));
        auto F6 = kronecker(F5, selfing_gamete);
        auto F6l = lump(F6);
        MSG_DEBUG("" << F6);
        experimental_lumper el3(F6);
        el3.do_lump(32);
        return 0;
    }

    if (0)
    {
        auto F2 = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
        auto F3 = lump(kronecker(kronecker(F2, gamete), kronecker(A, gamete)));
        auto F4 = lump(kronecker(kronecker(F3, gamete), kronecker(A, gamete)));
        auto F5 = lump(kronecker(kronecker(F4, gamete), kronecker(A, gamete)));
        auto F6 = lump(kronecker(kronecker(F5, gamete), kronecker(A, gamete)));
        auto F7 = lump(kronecker(kronecker(F6, gamete), kronecker(A, gamete)));
        auto F8 = lump(kronecker(kronecker(F7, gamete), kronecker(A, gamete)));
        auto F9 = lump(kronecker(kronecker(F8, gamete), kronecker(A, gamete)));
        auto F10 = lump(kronecker(kronecker(F9, gamete), kronecker(A, gamete)));
        auto F11 = lump(kronecker(kronecker(F10, gamete), kronecker(A, gamete)));
        MSG_DEBUG("STUDY LUMPING BC10");
        experimental_lumper el(F11);
        auto g6 = el.do_lump(5);
        test_geno_matrix("g11.matrix", F11, "ABH");
        test_geno_matrix("g5_data.matrix", g6, "ABH");
    }

    return 0;
    (void) argc; (void) argv;
}

