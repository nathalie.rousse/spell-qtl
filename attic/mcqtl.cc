/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "input.h"
#include <iostream>
#include "file.h"
#include "settings.h"
#include "probabilities.h"
#include "struct_predicates.h"
#include <algorithm>
#include <x2c/x2c.h>
#include "probapop_dtd.h"

using namespace x2c;

#if 0
template <typename T>
std::ostream& output_vec(std::ostream& os, const std::vector<T>& v)
{
    os << '[';
    auto b = v.begin();
    auto e = v.end();
    if (b != e) {
        os << (*b++);
        for (; b != e; ++b) {
            os << ' ' << (*b);
        }
    }
    return os << ']';
}

std::ostream& operator << (std::ostream& os, const std::vector<double>& v) { return output_vec(os, v); }
std::ostream& operator << (std::ostream& os, const std::vector<size_t>& v) { return output_vec(os, v); }
std::ostream& operator << (std::ostream& os, const std::vector<int>& v) { return output_vec(os, v); }
std::ostream& operator << (std::ostream& os, const std::vector<long int>& v) { return output_vec(os, v); }



std::vector<double> compute_steps(const std::vector<double>& loci,
                                  double step)
{
    std::vector<double> ret;
    auto next_locus = loci.begin();
    auto done = loci.end();
    double cursor = *next_locus;
    while (next_locus != done) {
        if (cursor >= *next_locus) {
            cursor = *next_locus;
            ++next_locus;
        }
        ret.push_back(cursor);
        cursor += step;
    }
    return ret;
}

GenoProbMatrix compute_prob(const Generation& transition_matrix_generator,
                            const std::vector<double>& loci,
                            const std::vector<size_t>& windows,
                            const std::vector<char>& obs,
                            std::map<char, VectorXd>& mo,
                            double step, double coef=1.)
{
    size_t l = 0;
    /*size_t s = 0;*/
    /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/
    std::vector<double> steps = compute_steps(loci, step);
    /*std::cout << "loci " << loci << std::endl;*/
    /*std::cout << "windows " << windows << std::endl;*/
    /*std::cout << "steps " << steps << std::endl;*/
    /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/
    GenoProbMatrix ret(transition_matrix_generator.row_labels, steps);
    ret.data = Eigen::MatrixXd::Zero(ret.innerSize(), ret.outerSize());
    /*std::cout << ret << std::endl;*/
    /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/
    auto loc = steps.begin();
    size_t block_start = 0;
    for (size_t i = 1; i < windows.size(); ++i) {
        geno_prob_computer gpc;
        std::vector<double> steps_in_window;
        for (; *loc < loci[windows[i]]; ++loc) {
            steps_in_window.push_back(*loc);
        }
        if (i == windows.size() - 1) {
            steps_in_window.push_back(*loc);
        }
        /*std::cout << "steps_in_window " << steps_in_window << std::endl;*/
        /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/
        gpc.LV = MatrixXd(transition_matrix_generator.innerSize(), windows[i] - windows[i - 1] + 1);
        for (; l < windows[i]; ++l) {
            gpc.locus.push_back(loci[l]);
            gpc.LV.col(l - windows[i - 1]) = mo[obs[l]];
        }
        gpc.locus.push_back(loci[l]);
        gpc.LV.col(l - windows[i - 1]) = mo[obs[l]];
        gpc.init(transition_matrix_generator, coef);
        /*std::cout << "locus " << gpc.locus << std::endl;*/
        /*std::cout << "LV" << std::endl << gpc.LV << std::endl;*/
        /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/
        ret.data.block(0, block_start, ret.innerSize(), steps_in_window.size())
            = gpc.compute_over_segment(steps_in_window);
        /*std::cout << "block_start=" << block_start << " ret.innerSize()=" << ret.innerSize() << " steps_in_window.size()=" << steps_in_window.size() << std::endl;*/
        /*std::cout << ret.data.block(0, block_start, ret.innerSize(), steps_in_window.size()) << std::eendl;*/
        block_start += steps_in_window.size();
        /*std::cout << ret << std::endl;*/
        /*std::cout << __FILE__ << ':' << __LINE__ << std::endl;*/
    }
    return ret;
}
#endif


Eigen::MatrixXd make_reductor(const std::vector<haplo_type> all_lines)
{
    std::vector<haplo_type> phaselocked;
    for (auto h: all_lines) { phaselocked.push_back(phaselock(h)); }
    std::vector<int> cliques;
    cliques.resize(phaselocked.size(), -1);
    int n_cliques = 0;
    for (size_t i = 0; i < phaselocked.size(); ++i) {
        if (cliques[i] == -1) {
            cliques[i] = n_cliques;
            ++n_cliques;
        }
        for (size_t j = i + 1; j < phaselocked.size(); ++j) {
            if (phaselocked[i] == phaselocked[j]) {
                cliques[j] = cliques[i];
            }
        }
    }
    Eigen::MatrixXd ret = Eigen::MatrixXd::Zero(n_cliques, cliques.size());
    for (size_t i = 0; i < cliques.size(); ++i) {
        ret(cliques[i], i) = 1;
    }
    return ret;
}


void output_old_style_probapop_xml(
        settings* conf,
        const std::vector<std::vector<GenoProbMatrix>>& chromosome_probabilities,
        std::ostream& os)
{
    os << R"(<?xml version="1.0" encoding="ISO-8859-1" ?>)" << std::endl;
    os << "<!-- ProbaPop version 42 (MCQTL v6 plus epsilon <DATE> -->" << std::endl;
    os << R"(<POPULATION name="TODO" type="TODO">)" << std::endl;
    auto chrom_map = conf->map.begin();
    Eigen::MatrixXd redux = make_reductor(chromosome_probabilities[0][0].row_labels);
    /*std::cout << "redux" << std::endl << redux << std::endl;*/
    /*std::cout << "effect:" << std::endl;*/
    VectorXd line(redux.outerSize());
    line(0) = 1;
    for (int i = 1; i < line.innerSize(); ++i) {
        line(i) = 10*line(i - 1);
    }
    /*std::cout << (redux * line) << std::endl;*/
    os.setf(std::ios::fixed);

    for (auto chromprob: chromosome_probabilities) {
        os << "    <CHROMOSOME name=\"" << chrom_map->name << "\" number=\"" << (1 + (chrom_map - conf->map.begin())) << "\">" << std::endl;
        auto positions = chromprob[0].column_labels;
        size_t loc_num = 0;
        for (size_t pos = 0; pos < positions.size(); ++pos) {
            std::string mark_name = "";
            std::string on_mark = "no";
            if (chrom_map->marker_locus[loc_num] == positions[pos]) {
                on_mark = "yes";
                mark_name = chrom_map->marker_name[loc_num];
                ++loc_num;
            }
            os.precision(3);
            os << "        <POSITION value=\"" << positions[pos] << "\" marker=\"" << on_mark << "\" name=\"" << mark_name << "\" weight=\"TODO\">" << std::endl;
            os << "            <MATRIX_P number_rows=\"" << chromprob.size() << "\" number_columns=\"" << redux.innerSize() << "\">" << std::endl;
            size_t l = 1;
            os.precision(6);
            for (size_t cp = 0; cp < chromprob.size(); ++cp) {
                os << "                <LINE number=\"" << (l++) << "\">" << std::endl;
                line = redux * chromprob[cp].data.col(pos);
                for (int i = 0; i < line.innerSize(); ++i) {
                    os << "                    <PROBABILITY value=\"";
                    if (line(i) < 5.e-7) {
                        os << 0;
                    } else if (line(i) > .9999995) {
                        os << 1;
                    } else {
                        os << line(i);
                    }
                    os << "\" />" << std::endl;
                }
                os << "                </LINE>" << std::endl;
            }
            os << "            </MATRIX_P>" << std::endl;
            os << "        </POSITION>" << std::endl;
        }
        os << "    </CHROMOSOME>" << std::endl;
    }
    os << "</POPULATION>" << std::endl;
}





int main(int argc, char** argv)
{
    if (argc < 3
        ||
        std::any_of(argv+1, argv+argc,
                    [] (std::string s)
                    {
                        return s == "-h" || s == "--help";
                    })) {
        std::cout << "McQTL v6  © INRA 2013" << std::endl;
        std::cout << "Usage: " << argv[0] << " [-h|--help] SETTINGS_FILE.XML <operation_mode>..." << std::endl;
        std::cout << "Where operation_mode is one of 'probmap', 'detect', 'estimate'." << std::endl;
        return 0;
    }

    ifile is(argv[1]);
    settings* conf = NULL;
    try {
        conf = read_data::read_settings(is);
    } catch(x2c::xml_exception xe) {
        std::cerr << "Could not read settings file \"" << argv[1] << '"' << std::endl;
        std::cerr << xe.what() << std::endl;
        return -1;
    } catch(input_exception ie) {
        std::cerr << "Error: " << ie.what() << std::endl;
        return -2;
    } catch(bad_settings_exception bse) {
        std::cerr << "There are inconsistencies in the setup described in " << argv[1] << std::endl;
        std::cerr << bse.what() << std::endl;
        std::cerr << "Please fix that in order to continue." << std::endl;
        return -3;
    }

    /*std::cout << (*conf) << std::endl;*/

    GenoProbMatrix mine;
    GenoProbMatrix probapop;

    {
        ifile is(argv[2]);
        probapop_output* po = read_probapop(is);
        probapop.data = *po;
        delete po;
    }

    std::vector<std::vector<GenoProbMatrix>> all_chrom_genoprob;
    for (auto og: conf->observed) {
        std::cout << "Observed generation " << og.name << std::endl;
        all_chrom_genoprob.clear();
        for (auto chrom: conf->map) {
            std::vector<GenoProbMatrix> chrom_genoprob;
            std::cout << "* Chromosome " << chrom.name << std::endl;
            for (size_t i = 0; i < og.observed_mark.n_obs; ++i) {
                std::cout << '#' << i << std::endl;
                std::vector<char> marks = og.observed_mark.get_obs(chrom.marker_name.begin(), chrom.marker_name.end(), i);
                std::vector<size_t> windows = og.get_windows(marks);
#if 0
                {
                    for (size_t k = 0; k < marks.size(); ++ k) {
                        if (og.find_obs(marks[k]).is_ambiguous()) {
                            std::cout << '(' << marks[k] << ')';
                        } else {
                            std::cout << ' ' << marks[k] << ' ';
                        }
                        std::cout << ' ';
                    }
                    std::cout << std::endl;
                    auto w = windows.begin();
                    std::cout << " |[ ";
                    ++w;
                    for (size_t k = 1; k < marks.size() - 1; ++ k) {
                        if (k == *w) {
                            std::cout << "]|[";
                            ++w;
                        } else {
                            std::cout << "   ";
                        }
                        std::cout << ' ';
                    }
                    std::cout <<  "]|" << std::endl;
                }
#endif
                auto mo = conf->genotype_vectors(og.name);
#ifdef BIDOUILLE
                std::pair<double, double> opt = optimize(.1, 10., [&] (double c) {
                            return (probapop.data - compute_prob(conf->design->generation[og.name].matrix,
                                                                 chrom.marker_locus,
                                                                 windows,
                                                                 marks,
                                                                 mo,
                                                                 conf->step, c).data).norm();
                        }, 0);
                std::cout << "best coef found: " << opt.first << " norm=" << opt.second << std::endl;
#endif
                chrom_genoprob.push_back(
                        mine =
                        compute_prob(conf->design->generation[og.name].matrix,
                                     chrom.marker_locus,
                                     windows,
                                     marks,
                                     mo,
                                     conf->step));
                                     /*conf->step, opt.first));*/
                /*std::cout << chrom_genoprob.back() << std::endl;*/
            }
            all_chrom_genoprob.push_back(chrom_genoprob);
        }
        std::string ofn(og.name);
        ofn += ".xml";
        ofile os(ofn);
        output_old_style_probapop_xml(conf, all_chrom_genoprob, os);
    }


    /*probapop.row_labels = all_chrom_genoprob.front().front().row_labels;*/
    /*probapop.column_labels = all_chrom_genoprob.front().front().column_labels;*/

    if (0) {
        ifile is(argv[3]);
        probapop_output* po = read_probapop(is);
        mine = *po;
        mine.row_labels = all_chrom_genoprob.front().front().row_labels;
        mine.column_labels = all_chrom_genoprob.front().front().column_labels;
        delete po;
    }

    GenoProbMatrix tmp;

    probapop.row_labels = mine.row_labels;
    probapop.column_labels = mine.column_labels;

    ofile of(std::string(argv[3]) + "outputs.txt");
    of << probapop << std::endl << mine << std::endl;
    
    std::cout << "probapop" << std::endl << probapop << std::endl;
    std::cout << "mine" << std::endl << mine << std::endl;
    tmp = probapop;
    std::cout << "colsums" << std::endl << mine.data.colwise().sum() << std::endl;
    tmp.data -= mine.data;
    std::cout << "diff" << std::endl << tmp << std::endl;
    tmp = probapop;
    tmp.data = (tmp.data.array() / mine.data.array()).matrix();
    std::cout << "ratio" << std::endl << tmp << std::endl;
    tmp = probapop;
    tmp.data = ((tmp.data - mine.data).array() / (tmp.data + mine.data).array()).matrix();
    std::cout << "error" << std::endl << tmp << std::endl;
    VectorXd v = tmp.data.colwise().norm();
    std::cout << "error norm" << std::endl << v << std::endl;
    std::cout << "norm = " << (probapop.data - mine.data).norm() << std::endl;

    return 0;
}



