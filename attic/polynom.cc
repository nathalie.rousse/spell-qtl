/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*#include "polynom.h"*/
/*#include <Eigen/Core>*/
/*#include <sstream>*/
/*#include "file.h"*/
/*#include "fmin.h"*/

/*#include <unsupported/Eigen/KroneckerProduct>*/

/*#include "labelled_matrix.h"*/
/*#include "design.h"*/

#include "all.h"


#ifdef TEST_POLY

int fact(size_t n)
{
    if (n < 3) {
        return n;
    }
    size_t accum = 2;
    for (size_t i = 3; i <= n; ++i) {
        accum *= i;
    }
    return accum;
}


int comb(int n, int p)
{
    if (p == 0 || p == n) {
        return 1;
    }
    return fact(n) / (fact(n - p) * fact(p));
}


polynom test_poly(int n)
{
    if (n == 0) {
        return {1};
    }
    polynom P = {1, 1};
    P *= test_poly(n - 1);
    std::cerr << P << std::endl;
    for (size_t i = 0; i < P.size(); ++i) {
        if (P[i] != comb(P.size() - 1, i)) {
            std::cerr << "mismatch (" << n << ") at #" << i << ": " << P[i] << " vs " << comb(P.size(), 1 + i) << std::endl;
        }
    }
    return P;
}


void test_der()
{
    polynom x2 = {0, 0, 1};
    polynom x = {0, 1};
    polynom x0 = {1};
    polynom nxn = {0, 1, 2, 3};
    polynom dx2 = {0, 2};
    polynom dx = {1};
    polynom d0 = {};
    std::cout << x0 << " | " << x0.derive() << " | " << d0 << std::endl;
    std::cout << x << " | " << x.derive() << " | " << dx << std::endl;
    std::cout << x2 << " | " << x2.derive() << " | " << dx2 << std::endl;
    std::cout << nxn << " | " << nxn.derive() << " | " << std::endl;
    std::cout << ((polynom){1, -1}).derive() << std::endl;
    std::cout << ((polynom){-1}) << std::endl;
}


int main(int argc, char** argv)
{
    test_poly(10);
    test_der();
#if 0
    output_exponent(std::cout, 1234567890) << std::endl;
    polynom a = { 1, -2, 1 };
    polynom b = { 1, 2, 1 };
    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << "a + b = " << (a + b) << std::endl;
    std::cout << "a - b = " << (a - b) << std::endl;
    std::cout << "a * b = " << (a * b) << std::endl;
    polynom c = { 1, 1 };
    polynom d = { -1, 1 };
    std::cout << "c = " << c << std::endl;
    std::cout << "d = " << d << std::endl;
    std::cout << "c * d = " << (c * d) << std::endl;
    std::cout << "d * d = " << (d * d) << std::endl;
    std::cout << "c * c = " << (c * c) << std::endl;
    std::cout << "c * c * c = " << (c * c * c) << std::endl;

    PolynomMatrix m(2, 2);
    m << polynom({0, 1}), polynom({1}), polynom({1}), polynom({0, 1});
    std::cout << "m =" << std::endl;
    std::cout << m << std::endl;
    std::cout << "m² =" << std::endl;
    std::cout << (m * m) << std::endl;

    std::cout << "m²(0) =" << std::endl;
    std::cout << eval_poly(m * m, 0) << std::endl;
    std::cout << "m²(1) =" << std::endl;
    std::cout << eval_poly(m * m, 1) << std::endl;
    std::cout << "m²(.5) =" << std::endl;
    std::cout << eval_poly(m * m, .5) << std::endl;

    PolynomMatrix kp;

    PolynomMatrix n(2, 2);
    n << polynom({0, 1}), polynom({1, -1}), polynom({1, -1}), polynom({0, 1});
    kp = kroneckerProduct(n, n);
    std::cout << "source matrix :" << std::endl << n << std::endl;
    std::cout << "kroneckered matrix :" << std::endl << kp << std::endl;

    labelled_matrix<PolynomMatrix, haplo_type> pouet({{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}});
    pouet = kp;
    std::cout << "labels ?" << std::endl << pouet << std::endl;
#endif

    Generation F2, F3c, F3s, BC, BC2;

    /*ifile ifs("../data/toto.xml");*/
    /*design_type* des = read_design(ifs);*/

    Generation P1 = Homozygous('a');
    Generation P2 = Homozygous('b');
    Generation F1 = Cross(P1, P2);

    std::cout << "P1" << std::endl << P1 << std::endl;
    std::cout << "P2" << std::endl << P2 << std::endl;
    std::cout << "F1" << std::endl << F1 << std::endl;

    F2 = Cross(F1, F1);
    F3c = Cross(F2, F2);
    BC = Cross(F2, P1);
    BC2 = Cross(BC, P1);

    std::cout << "F2" << std::endl << F2 << std::endl;
    std::cout << "F2 (self)" << std::endl << Self(F1) << std::endl;
    std::cout << "F2 - F2 (self)" << std::endl << (F2.data - Self(F1).data) << std::endl;
    std::cout << "colsums(F2)" << std::endl << F2.data.colwise().sum() << std::endl;
    std::cout << "rowsums(F2)" << std::endl << F2.data.rowwise().sum() << std::endl;
    std::cout << "sum(F2)" << std::endl << F2.data.sum() << std::endl;

    std::cout << "F3" << std::endl << F3c << std::endl;
    std::cout << "colsums(F3c)" << std::endl << F3c.data.colwise().sum() << std::endl;
    std::cout << "rowsums(F3c)" << std::endl << F3c.data.rowwise().sum() << std::endl;
    std::cout << "sum(F3c)" << std::endl << F3c.data.rowwise().sum() << std::endl;
#if 0
    std::cout << "BC" << std::endl << F3c << std::endl;
    std::cout << "colsums(BC)" << std::endl << BC.data.colwise().sum() << std::endl;
    std::cout << "rowsums(BC)" << std::endl << BC.data.rowwise().sum() << std::endl;
    std::cout << "sum(BC)" << std::endl << BC.data.sum() << std::endl;
#endif
    F3s = Self(F2);
    std::cout << "F3 (self)" << std::endl << F3s << std::endl;
    std::cout << "colsums(F3s)" << std::endl << F3s.data.colwise().sum() << std::endl;
    std::cout << "rowsums(F3s)" << std::endl << F3s.data.rowwise().sum() << std::endl;
    std::cout << "sum(F3s)" << std::endl << F3s.data.sum() << std::endl;

    std::cout << "F3c" << std::endl;
    std::cout << "X=0" << std::endl << eval_poly(F3c, 0) << std::endl;
    std::cout << "X=.5" << std::endl << eval_poly(F3c, .5) << std::endl;

    std::cout << "BC" << std::endl;
    std::cout << "X=0" << std::endl << eval_poly(BC, 0) << std::endl;
    std::cout << "X=.5" << std::endl << eval_poly(BC, .5) << std::endl;

    std::cout << "BC2" << std::endl;
    std::cout << "X=0" << std::endl << eval_poly(BC2, 0) << std::endl;
    std::cout << "X=.5" << std::endl << eval_poly(BC2, .5) << std::endl;

    std::cout << "F3s" << std::endl;
    std::cout << "X=0" << std::endl << eval_poly(F3s, 0) << std::endl;
    std::cout << "X=.5" << std::endl << eval_poly(F3s, .5) << std::endl;

    /*delete des;*/

    Generation F = F2;
    for (size_t g = 3; g < 20; ++g) {
        F = Self(F);
        std::cout << std::endl << "---------------------------------------------" << std::endl;
        std::cout << "F" << g << std::endl;
        /*std::cout << F << std::endl;*/
        std::cout << "colsums" << std::endl << F.data.colwise().sum().transpose() << std::endl;
        std::cout << "rowsums" << std::endl << F.data.rowwise().sum() << std::endl;
        std::cout << "sum" << std::endl << F.data.sum() << std::endl;
        std::cout << "X=0" << std::endl << eval_poly(F, 0) << std::endl;
        std::cout << "X=.5" << std::endl << eval_poly(F, .5) << std::endl;
        Eigen::VectorXd v1 = eval_poly(F.data, .5).col(0);
        PolynomMatrix pm(v1.innerSize(), 1);
        for (int k = 0; k < v1.innerSize(); ++k) {
            pm(k, 0) = { v1(k) };
        }
        std::cout << "Check eigenvector" << std::endl;
        PolynomMatrix delta = F.data * pm - pm;
        std::pair<double, double> maximum = optimize(0, .5, [&] (double x) { return -eval_poly(delta, x).norm(); }, 0);
        std::cout << (F.data * pm - pm) << std::endl;
        std::cout << " max=(" << maximum.first << ',' << (-maximum.second) << ") " << (maximum.second > -1.e-10 ? "epsilon-ish" : "BIG") << std::endl;
        std::cout << std::endl << "---------------------------------------------" << std::endl;
    }

    return 0;
}
#endif

