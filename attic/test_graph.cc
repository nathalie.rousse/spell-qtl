/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "bayes/graphnode.h"
#include <sstream>
#include <fstream>
#include <set>


graph_type
pedigree_to_graph(const std::vector<pedigree_item>& ped, size_t n_al)
{
    graph_type g;
    g.aggregate_cycles = false;
    g.n_alleles = n_al;
    system("rm -vf test_*.png");
    for (const auto& p: ped) {
        /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
        /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
        /*MSG_DEBUG("-------------------------------------------------------------------------------");*/
        if (p.is_ancestor()) {
            g.add_interface(node_vec{}, p.id);
            g.add_ancestor(p.id);
        } else if (p.is_cross()) {
            g.add_cross(p.p1, p.p2, p.id);
        } else if (p.is_dh()) {
            g.add_dh(p.p1, p.id);
        } else if (p.is_self()) {
            g.add_selfing(p.p1, p.id);
        }
        {
            auto tmp = g;
            tmp.finalize();
            tmp.to_image(SPELL_STRING(std::setw(4) << std::setfill('0') << p.id << "_fg"), "png");
        }
    }
    g.finalize();
    return g;
}


typedef std::map<colour_proxy, char> colour_name;


var_vec
parse_query(char** argv, int i)
{
    std::stringstream ss(argv[i]);
    var_vec ret;
    while (!ss.eof()) {
        int i;
        ss >> i;
        /*if (ss.good()) {*/
            ret.push_back(i);
        /*}*/
    }
    return ret;
}




int main(int argc, char** argv)
{
    (void)msg_handler_t::instance();

    if (argc < 3) {
        MSG_DEBUG("Usage: " << argv[0] << " ped_file n_alleles [obs_var genotypes]...");

        genotype_comb_type f;
        f.m_combination.emplace_back(genotype_comb_type::element_type{{{1, {'a', 'a', 0, 0}}}, .25});
        f.m_combination.emplace_back(genotype_comb_type::element_type{{{1, {'b', 'b', 0, 0}}}, .75});
        auto g = kronecker(f, f);
        MSG_DEBUG("f = " << f);
        MSG_DEBUG("g = " << g);
        return 0;
    }
    if (0) {
        read_pedigree(argv[1]);
        return 0;
    }
    auto ped = read_csv(argv[1], ';');
    int n_al = atoi(argv[2]);
    auto g = pedigree_to_graph(ped, n_al);
    std::ofstream ofs("test.dot");
    MSG_DEBUG(std::endl);
    /*for (int i = 2; i < argc; ++i) {*/
        /*auto q = parse_query(argv, i);*/
        /*sort_and_unique(q);*/
        /*auto qo = g.build_query_operation(q);*/
        /*MSG_DEBUG("query " << q << " tree " << qo);*/
    /*}*/
    std::vector<std::shared_ptr<message_type>> inputs;
    for (int i = 3; i < argc; i += 2) {
        int var = atoi(argv[i]);
        char f, s, fa, sa;
        size_t len = strlen(argv[i + 1]);
        std::map<bn_label_type, double> obs;
        for (size_t z = 0; z < len; z += 5) {
            f = argv[i + 1][z];
            fa = argv[i + 1][z + 1] - '0';
            s = argv[i + 1][z + 2];
            sa = argv[i + 1][z + 3] - '0';
            obs.emplace(bn_label_type{f, s, fa, sa}, 1);
        }
        inputs.push_back(std::make_shared<message_type>(message_type{g.build_evidence(var, obs)}));
        MSG_DEBUG("obs " << var << "  " << obs);
    }
    /*inputs.push_back(std::make_shared<message_type>(message_type{g.build_evidence(1, {{{'a', 'a', 0, 0}, 1}})}));*/
    /*inputs.push_back(std::make_shared<message_type>(message_type{g.build_evidence(2, {{{'b', 'b', 1, 1}, 1}})}));*/
    /*inputs.push_back(std::make_shared<message_type>(message_type{g.build_evidence(41, {{{'a', 'a', 0, 0}, 1}})}));*/
    /*inputs.push_back(std::make_shared<message_type>(message_type{g.build_evidence(42, {{{'a', 'a', 0, 0}, 1}})}));*/
    /*inputs.push_back(std::make_shared<message_type>(message_type{g.build_evidence(43, {{{'a', 'a', 0, 0}, 1}})}));*/
    /*inputs.push_back(std::make_shared<message_type>(message_type{g.build_evidence(45, {{{'b', 'b', 0, 0}, 1}})}));*/
    if (argc > 3) {
        std::map<edge_type, std::shared_ptr<message_type>> messages;
        std::vector<graph_type::query_operation_type> marginals;
        for (const auto& i: ped) {
            auto q = g.build_query_operation({i.id});
            marginals.insert(marginals.end(), q.begin(), q.end());
        }
        for (node_index_type n: g.active_nodes()) {
            if (g.is_interface(n) && g.is_aggregate(n)) {
                auto q = g.build_query_operation(g.variables_of(n));
                marginals.insert(marginals.end(), q.begin(), q.end());
            }
        }
        g.compute_messages(inputs.begin(), inputs.end(), messages);
        g.compute_state(marginals, messages);
        auto state = g.extract(marginals);

        std::map<variable_index_type, genotype_comb_type> output;
        g.compute_full_factor_state(messages, output);

        MSG_DEBUG("MARGINALS");

        for (size_t i = 0; i < state.size(); ++i) {
            var_vec v = marginals[i].back().variables;
            MSG_DEBUG_INDENT_EXPR("" << v << ' ');
            MSG_DEBUG("" << marginals[i]);
            MSG_DEBUG("" << g.domains[v]);
            MSG_DEBUG("" << state[i]);
            MSG_DEBUG_DEDENT;
            MSG_DEBUG("");
        }

        MSG_DEBUG("FACTOR STATE");
        for (const auto& kv: output) {
            MSG_DEBUG("Factor producing " << kv.first);
            MSG_DEBUG("" << kv.second);
            MSG_DEBUG("");
        }
    }

    return 0;
}

