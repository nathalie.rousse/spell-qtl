/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "outbred.h"

#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}



template <typename RHS_TYPE>
static inline bool
if_startswith_then_extract_to(
        const std::string& s,
        const std::string& what,
        RHS_TYPE& target)
{
    bool match = s.substr(0, what.size()) == what;
    if (match) {
        std::stringstream ss(s.substr(what.size(), s.size() - what.size()));
        ss >> target;
    }
    return match;
}


inline std::istream& operator >> (std::istream& is, const char* cstr)
{
    const char* ptr = cstr;
    while (*ptr) {
        if (is.get() != *ptr++) {
            std::cerr << "Expected '" << cstr << "'." << std::endl;
            throw 0;
        }
    }
    return is;
}



inline std::istream& operator >> (std::istream& is, segregation& seg)
{
    char mf, ms, ff, fs;
    is >> "<" >> mf >> ms >> "x" >> ff >> fs >> ">";
    seg = segregation(mf, ms, ff, fs);
    return is;
}


inline std::istream& operator >> (std::istream& is, phase& p)
{
    return is >> "{" >> p.mother >> p.father >> "}";
}


static inline std::string readline(std::istream& is, int& lineno)
{
    std::string line;
    while (is.good() && !is.eof() && (line.size() == 0 || line[0] == ';')) {
        getline(is, line);
        ++lineno;
        line = trim(line);
    }
    /*std::cout << ">> " << line << " <<" << std::endl;*/
    return line;
}


namespace read_data {
marker_data read_outbred(std::istream& datafile)
{
    int lineno;
    std::string line;
    marker_data md;
    std::string name;
    static char hex_obs[16] = {'0', '1', '2', '3',
                               '4', '5', '6', '7',
                               '8', '9', 'a', 'b',
                               'c', 'd', 'e', 'f'};

    datafile >> std::skipws;

    md.data_type = "outbred";

    while (true) {
        line = readline(datafile, lineno);
        if (datafile.bad() || datafile.eof() || line.size() == 0) {
            break;
        }
        if (if_startswith_then_extract_to(line, "name = ", name)
                || if_startswith_then_extract_to(line, "popt = ", md.data_type)
                || if_startswith_then_extract_to(line, "nind = ", md.n_obs)
                || if_startswith_then_extract_to(line, "nloc = ", md.n_mark)) {
            continue;
        }

        /* Data format : */
        std::stringstream ss(line);
        /*std::cout << "NAME SEG PHASE " << line << std::endl;*/
        /* marker name */
        ss >> name;
        ss >> std::ws;
        /* segregation type */
        segregation seg;
        ss >> seg;
        ss >> std::ws;
        /* phase */
        phase p;
        ss >> p;
        seg *= p;
        /* EOL */
        ss >> std::ws;
        if (ss.bad() || !ss.eof()) {
            std::cerr << "Expected EOL" << std::endl;
            throw 0;
        }
        line = readline(datafile, lineno);
        /*std::cout << "OBS " << line << std::endl;*/
        ss.str(line);
        ss.seekg(0, std::ios_base::beg);
        /* md.n_obs observations */
        allele_pair obs = {{0}, {0}};
        for (size_t i = 0; i < md.n_obs; ++i) {
            ss >> std::ws;
            ss >> obs.first.ancestor >> obs.second.ancestor;
            /*std::cout << "got obs " << obs << std::endl;*/
            md.data[name].push_back(hex_obs[obs % seg]);
        }
    }
    /* data type is lowercase */
    std::transform(md.data_type.begin(), md.data_type.end(),
                   md.data_type.begin(), ::tolower);
    return md;
}
}



#ifdef TEST_OUTBRED
int main(int argc, char** argv)
{
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " outbred_data_filename" << std::endl;
        return 1;
    }
    ifile ifs(argv[1]);
    marker_data md = read_data::read_outbred(ifs);
    std::cout << md << std::endl;
    return 0;
}
#endif

