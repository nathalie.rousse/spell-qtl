/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "eigen.h"
#include "error.h"
#include "labelled_matrix.h"
#include "lumping2.h"
#include <limits>
#include <cmath>

#define FLOAT_TOL 1.e-6

/*#define eq_dbl(_x_, _y_) (fabs((_x_) - (_y_)) < FLOAT_TOL)*/
/*#define neq_dbl(_x_, _y_) (fabs((_x_) - (_y_)) >= FLOAT_TOL)*/

inline
bool eq_dbl(double a, double b) { return fabs(a - b) < FLOAT_TOL; }

inline
bool eq_dbl(double a, double b, double tol_) { return fabs(a - b) < tol_; }

inline
bool neq_dbl(double a, double b) { return fabs(a - b) >= FLOAT_TOL; }

using namespace Eigen;

typedef Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> MatrixXb;
typedef Eigen::Matrix<bool, Eigen::Dynamic, 1> VectorXb;


typedef std::map<int, int> symmetry_table_type;
typedef std::vector<symmetry_table_type> symmetry_list_type;

template <typename SCALAR_>
Eigen::Matrix<SCALAR_, Eigen::Dynamic, Eigen::Dynamic>
make_symmetry_matrix(const symmetry_table_type& S)
{
    Eigen::Matrix<SCALAR_, Eigen::Dynamic, Eigen::Dynamic> tmp = Eigen::Matrix<SCALAR_, Eigen::Dynamic, Eigen::Dynamic>::Zero(S.size(), S.size());
    /*MSG_DEBUG(MATRIX_SIZE(tmp));*/
    /*MSG_QUEUE_FLUSH();*/
    for (const auto& kv: S) {
        if (kv.second != -1) {
            /*tmp(kv.first, kv.second) = tmp(kv.second, kv.first) = 1;*/
            tmp(kv.second, kv.first) = 1;
        } else {
            MSG_ERROR("INCOMPLETE SYMMETRY! (missing index " << kv.first << ')', "");
        }
    }
    return tmp;
}


template <typename SCALAR_>
symmetry_table_type
make_symmetry(const Eigen::Matrix<SCALAR_, Eigen::Dynamic, Eigen::Dynamic>& M)
{
    symmetry_table_type ret;
    /*MSG_DEBUG("Make symmetry from matrix");*/
    /*MSG_DEBUG(MATRIX_SIZE(M));*/
    /*MSG_DEBUG(M);*/
    for (int j = 0; j < M.cols(); ++j) {
        for (int i = 0; i < M.rows(); ++i) {
            if (M(i, j) != 0) {
                /*ret[i] = j;*/
                ret[j] = i;
            }
        }
    }
    /*MSG_DEBUG("Symmetry size: " << ret.size());*/
    return ret;
}


std::ostream& operator << (std::ostream& os, const symmetry_table_type& S)
{
    return os << ('.' + ('@' - '.') * make_symmetry_matrix<char>(S).array());
}


std::ostream& operator << (std::ostream& os, const symmetry_list_type& SYM)
{
    for (const auto& S: SYM) {
        os << "SYMMETRY:" << std::endl << S << std::endl;
    }
    return os;
}


MatrixXi classify_values(const MatrixXd& mat, double tol_)
{
    MatrixXi ret(mat.rows(), mat.cols());
    std::set<double> all_values;
    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            all_values.insert(mat(i, j));
        }
    }
    std::map<double, unsigned long> indices;
    double prev = std::numeric_limits<double>::infinity();
    for (double d: all_values) {
        if (eq_dbl(prev, d)) {
            indices[d] = indices[prev];
        } else {
            unsigned long s = indices.size();
            indices[d] = s;
        }
    }
    const double* dbl_data = mat.data();
    int* i_data = ret.data();
    for (int i = 0; i < mat.size(); ++i) {
        i_data[i] = indices[dbl_data[i]];
    }
    return ret;
}


MatrixXd round_values(const MatrixXd& mat, const MatrixXi& cls)
{
    std::vector<double> accum;
    std::vector<int> weight;
    int m = cls.maxCoeff();
    accum.resize(m + 1);
    weight.resize(m + 1);
    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            int k = cls(i, j);
            accum[k] += mat(i, j);
            ++weight[k];
        }
    }
    for (size_t i = 0; i < accum.size(); ++i) {
        accum[i] /= weight[i];
    }
    MatrixXd ret(mat.rows(), mat.cols());
    for (int i = 0; i < mat.rows(); ++i) {
        for (int j = 0; j < mat.cols(); ++j) {
            ret(i, j) = accum[cls(i, j)];
        }
    }
    return ret;
}


typedef std::pair<char, char> label_type;
enum geno_matrix_variant_type { Gamete, DoublingGamete, SelfingGamete, Haplo, Geno };


bool operator == (const label_type& l1, const label_type& l2)
{
    return (l1.first == l2.first && l1.second == l2.second)
        || ((l1.first == 0 || l2.first == 0) && (l1.second == 0 || l2.second == 0));
}


struct geno_matrix {
    geno_matrix_variant_type variant;
    std::vector<label_type> labels;
    MatrixXd inf_mat;
    MatrixXd p, p_inv, diag;
    VectorXd stat_dist;
    /*double norm_factor;*/

    size_t rows() const { return inf_mat.rows(); }
    size_t cols() const { return inf_mat.cols(); }
    size_t size() const { return rows(); }

    geno_matrix& operator = (const geno_matrix& gm)
    {
        variant = gm.variant;
        labels.assign(gm.labels.begin(), gm.labels.end());
        inf_mat = gm.inf_mat;
        p = gm.p;
        p_inv = gm.p_inv;
        diag = gm.diag;
        stat_dist = gm.stat_dist;
        return *this;
    }

    MatrixXd exp(double d) const
    {
        MatrixXd ret = p * ((d * diag).array().exp().matrix().asDiagonal()) * p_inv;
        /*MatrixXd check = (d * inf_mat).exp();*/
        /*if (!ret.isApprox(check)) {*/
            /*MSG_ERROR("BAD EXP" << std::endl << "with diag:" << std::endl << check << std::endl << "with exp:" << std::endl << ret, "");*/
        /*} else {*/
            /*MSG_INFO("GOOD EXP");*/
        /*}*/
        return ret;
    }

    void cleanup(MatrixXd& m)
    {
        m = (m.array().abs() <= FLOAT_TOL).select(MatrixXd::Zero(m.rows(), m.cols()), m);
    }

    void cleanup(VectorXd& v)
    {
        v = (v.array().abs() <= FLOAT_TOL).select(VectorXd::Zero(v.size()), v);
    }

    geno_matrix& cleanup()
    {
        cleanup(inf_mat);
        cleanup(diag);
        cleanup(p);
        cleanup(p_inv);
        return *this;
    }

    std::vector<double> unique_eigenvalues() const
    {
        std::set<double> tmp;
        for (int i = 0; i < diag.size(); ++i) {
            tmp.insert(diag(i));
        }
        return {tmp.begin(), tmp.end()};
    }

#if 0
    std::vector<MatrixXd> eigen_components() const
    {
        auto uev = unique_eigenvalues();
        std::vector<MatrixXd> ret;
        ret.reserve(uev.size());
        MSG_DEBUG_INDENT_EXPR("[EIGEN COMPONENTS] ");
        for (double ev: uev) {
            /*VectorXd tmp_diag = (diag.array() == ev).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();*/
            VectorXd tmp_diag = VectorXd::Zero(diag.size());
            for (int i = 0; i < tmp_diag.size(); ++i) {
                if (fabs(diag(i) - ev) < FLOAT_TOL) {
                    tmp_diag(i) = 1.;
                }
            }
            ret.emplace_back(p * tmp_diag.asDiagonal() * p_inv);
            MSG_DEBUG("eigenvalue " << ev << " at " << tmp_diag.transpose());
            MSG_DEBUG(ret.back());
            std::set<double> all_values;
            for (int i = 0; i < ret.back().rows(); ++i) {
                for (int j = 0; j < ret.back().cols(); ++j) {
                    all_values.insert(ret.back()(i, j));
                }
            }
            std::map<double, unsigned long> indices;
            double prev = std::numeric_limits<double>::infinity();
            for (double d: all_values) {
                if (eq_dbl(prev, d)) {
                    indices[d] = indices[prev];
                } else {
                    unsigned long s = indices.size();
                    indices[d] = s;
                }
            }
            MatrixXi grouped(ret.back().rows(), ret.back().cols());
            const double* dbl_data = ret.back().data();
            int* i_data = grouped.data();
            for (int i = 0; i < ret.back().size(); ++i) {
                i_data[i] = indices[dbl_data[i]];
            }
            MSG_DEBUG("--");
            MSG_DEBUG(('a' + grouped.array()).cast<char>());
        }
        MSG_DEBUG_DEDENT;
        return ret;
    }
#else
    std::vector<MatrixXi> eigen_components() const
    {
        auto uev = unique_eigenvalues();
        std::vector<MatrixXi> ret;
        ret.reserve(uev.size());
        /*MSG_DEBUG_INDENT_EXPR("[EIGEN COMPONENTS] ");*/
        for (double ev: uev) {
            /*VectorXd tmp_diag = (diag.array() == ev).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();*/
            VectorXd tmp_diag = VectorXd::Zero(diag.size());
            for (int i = 0; i < tmp_diag.size(); ++i) {
                if (fabs(diag(i) - ev) < FLOAT_TOL) {
                    tmp_diag(i) = 1.;
                }
            }
            /*MSG_DEBUG("eigenvalue " << ev << " at " << tmp_diag.transpose());*/
            MatrixXd tmp = p * tmp_diag.asDiagonal() * p_inv;
            /*MSG_DEBUG(tmp);*/
            std::set<double> all_values;
            for (int i = 0; i < tmp.rows(); ++i) {
                for (int j = 0; j < tmp.cols(); ++j) {
                    all_values.insert(tmp(i, j));
                }
            }
            std::map<double, unsigned long> indices;
            double prev = std::numeric_limits<double>::infinity();
            for (double d: all_values) {
                if (eq_dbl(prev, d)) {
                    indices[d] = indices[prev];
                } else {
                    unsigned long s = indices.size();
                    indices[d] = s;
                }
            }
            ret.emplace_back(tmp.rows(), tmp.cols());
            const double* dbl_data = tmp.data();
            int* i_data = ret.back().data();
            for (int i = 0; i < tmp.size(); ++i) {
                i_data[i] = indices[dbl_data[i]];
            }
            /*MSG_DEBUG("--");*/
            /*MSG_DEBUG(('A' + ret.back().array()).cast<char>());*/
        }
        /*MSG_DEBUG_DEDENT;*/
        return ret;
    }
#endif

    MatrixXd lim_inf() const
    {
        /*MSG_DEBUG("LIM_INF ####### LIM_INF");*/
        /*MSG_DEBUG((*this));*/
        MatrixXd diag_inf = (diag.array() == 0).select(VectorXd::Ones(diag.size()), VectorXd::Zero(diag.size())).matrix();
        return p * diag_inf.asDiagonal() * p_inv;
    }

    bool check_not_nan() const
    {
        if (!(inf_mat == inf_mat)) {
            MSG_DEBUG("NAN INF_MAT");
            return false;
        }
        if (!(p == p)) {
            MSG_DEBUG("NAN P");
            return false;
        }
        if (!(p_inv == p_inv)) {
            MSG_DEBUG("NAN P_INV");
            return false;
        }
        if (!(diag == diag)) {
            MSG_DEBUG("NAN DIAG");
            return false;
        }
        if (!(stat_dist == stat_dist)) {
            MSG_DEBUG("NAN STAT_DIST");
            return false;
        }
        return true;
    }
};

inline
std::ostream& operator << (std::ostream& os, const label_type& l)
{
    return os << (l.first ? l.first : '*') << (l.second ? l.second : '*');
}

inline
std::ostream& operator << (std::ostream& os, const std::vector<label_type>& vl)
{
    for (const auto& l: vl) { os << ' ' << l; }
    return os;
}


std::ostream& operator << (std::ostream& os, const geno_matrix& gm)
{
    Eigen::Map<Eigen::Array<label_type, Eigen::Dynamic, 1>> labels(const_cast<label_type*>(gm.labels.data()), gm.labels.size(), 1);
    std::set<label_type> uniq_labels(gm.labels.begin(), gm.labels.end());
    for (const auto& l: uniq_labels) {
        MSG_DEBUG(((labels == l).cast<int>().sum()) << ' ' << l << " states");
    }
    Eigen::Matrix<std::string, Eigen::Dynamic, Eigen::Dynamic> tmp(gm.rows() + 1, gm.cols() + 1);
    for (size_t i = 0; i < gm.labels.size(); ++i) {
        tmp(0, 1 + i) = "  ";
        tmp(0, 1 + i)[0] = (gm.labels[i].first < 32 ? '0' : '\0') + gm.labels[i].first;
        tmp(0, 1 + i)[1] = (gm.labels[i].second < 32 ? '0' : '\0') + gm.labels[i].second;
        tmp(1 + i, 0) = tmp(0, 1 + i);
    }
    for (size_t i = 0; i < gm.rows(); ++i) {
        for (size_t j = 0; j < gm.rows(); ++j) {
            std::stringstream ss; ss << gm.inf_mat(i, j);
            tmp(i + 1, j + 1) = ss.str();
        }
    }
    os << tmp;
    os << std::endl;
    os << "P" << std::endl << gm.p << std::endl;
    os << "Pinv" << std::endl << gm.p_inv << std::endl;
    MatrixXd diag = gm.diag.asDiagonal();
    os << "D" << std::endl << diag << std::endl;
    os << "STATIONARY DISTRIBUTION " << gm.stat_dist.transpose() << std::endl;
    return os;
}


#if 1
std::pair<MatrixXd, std::vector<label_type>> make_redux_matrix(const geno_matrix& GEN)
{
    Eigen::Map<Eigen::Array<label_type, Eigen::Dynamic, 1>> labels(const_cast<label_type*>(GEN.labels.data()), GEN.labels.size(), 1);
    std::set<label_type> uniq_labels(GEN.labels.begin(), GEN.labels.end());
    MatrixXd mret(uniq_labels.size(), GEN.cols());
    std::vector<label_type> vret(uniq_labels.begin(), uniq_labels.end());
    int i = 0;
    for (const auto& l: uniq_labels) {
        mret.row(i) = (labels == l).cast<double>();
        ++i;
    }
    return {mret, vret};
}
#endif


#define P_NORM_FACTOR (.707106781186547524400844362104849039284835937688474036588339868995366239231053519425193767163820786367506923115456148512462418027925)


label_type empty = {0, 0};


geno_matrix
    gamete = {
        Gamete,
        {{0, 0}, {1, 0}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        /* BEWARE these matrices (p and p_inv) SHOULD be * 1/sqrt(2) */
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << 0, -2).finished(),
        (VectorXd(2) << .5, .5).finished()
        /*.5*/
    },
    doubling_gamete = {
        DoublingGamete,
        {{0, 0}, {1, 1}},
        (MatrixXd(2, 2) <<
         -1,  1,
          1, -1).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 2) <<
         P_NORM_FACTOR,  P_NORM_FACTOR,
         P_NORM_FACTOR, -P_NORM_FACTOR).finished(),
        (MatrixXd(2, 1) << 0, -2).finished(),
        (VectorXd(2) << .5, .5).finished()
        /*.5*/
    };

#define SELECT(__p, __b) ((__b) ? (__p).second : (__p).first)


geno_matrix kronecker(const geno_matrix& m1, const geno_matrix& m2)
{
    m1.check_not_nan();
    m2.check_not_nan();
    geno_matrix ret;
    ret.inf_mat.resize(m1.rows() * m2.rows(), m1.cols() * m2.cols());
    ret.labels.reserve(m1.labels.size() * m2.labels.size());
    switch (m1.variant) {
        case Gamete:
            if (m2.variant == Gamete) {
                for (const auto& l1: m1.labels) {
                    for (const auto& l2: m2.labels) {
                        ret.labels.emplace_back(l1.first, l2.first);
                    }
                }
                ret.variant = SelfingGamete;
                break;
            }
        case SelfingGamete:
        case DoublingGamete:
            MSG_ERROR("Gamete matrices can only be the right operand in a kronecker product", "");
            MSG_QUEUE_FLUSH();
            throw 0;
        case Haplo:
            if (m2.variant != Haplo) {
                MSG_ERROR("Only Haplo (x) Haplo is defined", "");
                MSG_QUEUE_FLUSH();
                throw 0;
            }
            ret.variant = Geno;
            for (const auto& l1: m1.labels) {
                for (const auto& l2: m2.labels) {
                    ret.labels.emplace_back(l1.first, l2.first);
                }
            }
            break;
        case Geno:
            switch (m2.variant) {
                case Gamete:
                    ret.variant = Haplo;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), 0);
                        }
                    }
                    break;
                case DoublingGamete:
                    ret.variant = Geno;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), SELECT(l1, (int)l2.first));
                        }
                    }
                    break;
                case SelfingGamete:
                    ret.variant = Geno;
                    for (const auto& l1: m1.labels) {
                        for (const auto& l2: m2.labels) {
                            ret.labels.emplace_back(SELECT(l1, (int)l2.first), SELECT(l1, (int)l2.second));
                        }
                    }
                    break;
                default:
                    MSG_ERROR("Only Geno (x) {any gamete} is defined", "");
                    MSG_QUEUE_FLUSH();
                    throw 0;
            };
            break;
    };
    ret.inf_mat = kroneckerProduct(m1.inf_mat, MatrixXd::Identity(m2.rows(), m2.cols()))
                + kroneckerProduct(MatrixXd::Identity(m1.rows(), m1.cols()), m2.inf_mat);
    ret.p = kroneckerProduct(m1.p, m2.p);
    ret.p_inv = kroneckerProduct(m1.p_inv, m2.p_inv);
    ret.diag = (kroneckerProduct(m1.diag, VectorXd::Ones(m2.cols()))
             + kroneckerProduct(VectorXd::Ones(m1.cols()), m2.diag));
    /*ret.norm_factor = m1.norm_factor * m2.norm_factor;*/

    /* Check! */
    /*MSG_DEBUG("CHECK");*/
    /*MSG_DEBUG((ret.p * ret.diag.asDiagonal() * ret.p_inv * ret.norm_factor) - ret.inf_mat);*/
    /*MSG_DEBUG("END CHECK");*/
    ret.stat_dist = kroneckerProduct(m1.stat_dist, m2.stat_dist);
    return ret.cleanup();
}


geno_matrix
    selfing_gamete = kronecker(gamete, gamete);



geno_matrix ancestor_matrix(char a)
{
    std::vector<label_type> l;
    l.emplace_back(a, a);
    return {Geno, l, (MatrixXd(1, 1) << 0).finished(), (MatrixXd(1, 1) << 1).finished(), (MatrixXd(1, 1) << 1).finished(), (MatrixXd(1, 1) << 0).finished()/*, 1.*/, (VectorXd(1) << 1.).finished()};
}


symmetry_list_type analyse_symmetries(const geno_matrix& M);



struct label_switch {
    char first;
    char second;

    label_switch(char a, char b)
        : first(a < b ? a : b), second(a < b ? b : a)
    {}

    label_switch(char a, char b, int /*unsorted*/)
        : first(a), second(b)
    {}

    bool operator < (const label_switch& other) const { return first < other.first || (first == other.first && second < other.second); }
    bool operator == (const label_switch& other) const { return first == other.first && second == other.second; }
    bool operator != (const label_switch& other) const { return first != other.first || second != other.second; }
    bool is_identity() const { return first == second; }
    bool incompatible_with(const label_switch& other) const
    {
        return ((is_identity() && other.is_identity()) && (*this) != other)
        /*return (is_identity() && (first == other.first || first == other.second))*/
            || (other.is_identity() && (first == other.first || second == other.second));
    }

    bool compatible (const label_switch& other) const
    {
        bool id1 = first == second;
        bool id2 = other.first == other.second;
        bool x1 = first == other.second;
        bool x2 = second == other.first;
        bool l1 = first == other.first;
        bool l2 = second == other.second;
        /*MSG_DEBUG("compatible(" << (*this) << ", " << other << ") " << a << b << c << d << " @" << std::hex << ofs);*/
        /*return table[ofs];*/
        return !(
                (id1 && id2)                   /* refuse trivial switch */
            || ((id1 || id2) && (l1 || l2))    /* refuse switch if one is identity and the other actually switches the same letter */
            || ((!id1 && !id2                  /* refuse switch if both are non-trivial, different, and involving at least one letter in common */
                && ((l1 && !l2)
                   || (l2 && !l1) || x1 || x2)))
            );
    }

    friend
        std::ostream& operator << (std::ostream& os, const label_switch& ls)
        {
            return os << '[' << ls.first << " <-> " << ls.second << ']';
        }
};



struct label_switches {
    std::set<label_switch> elems;

    label_switches(const geno_matrix& M, const symmetry_table_type& S)
        : elems()
    {
        for (const auto& kv: S) {
            char l11 = M.labels[kv.first].first;
            char l12 = M.labels[kv.first].second;
            char l21 = M.labels[kv.second].first;
            char l22 = M.labels[kv.second].second;
            if (l11 != l21) {
                elems.insert({l11, l21, 0});
            }
            if (l12 != l22) {
                elems.insert({l12, l22, 0});
            }
        }
    }

    bool coherent() const
    {
        std::map<char, size_t> counts;
        for (const auto& ls: elems) {
            if (counts[ls.first]) {
                return false;
            }
            if (counts[ls.second]) {
                return false;
            }
            ++counts[ls.first];
            ++counts[ls.second];
        }
        return true;
    }

    struct ls_mat {
        MatrixXb mat;
        std::map<char, int> letters;

        ls_mat() : mat(), letters() {}

        ls_mat(const geno_matrix& M, const label_switches& ls)
            : mat()
            , letters()
        {
            for (const auto& e: M.labels) {
                if (e.first) {
                    letters[e.first] = 0;
                }
                if (e.second) {
                    letters[e.second] = 0;
                }
            }
            int count = -1;
            for (auto& kv: letters) {
                kv.second = ++count;
            }
            mat = MatrixXb::Identity(letters.size(), letters.size());
            for (const auto& e: ls.elems) {
                if (e.first && e.second) {
                    mat(letters[e.second], letters[e.first]) = true;
                    mat(letters[e.second], letters[e.second]) = false;
                }
                /*mat(letters[e.first], letters[e.second]) = true;*/
                /*mat(letters[e.first], letters[e.first]) = false;*/
            }
        }

        ls_mat operator * (const ls_mat& other) const
        {
            if (letters != other.letters) {
                MSG_QUEUE_FLUSH();
                throw 23;
            }
            ls_mat ret;
            ret.mat = mat * other.mat;
            ret.letters = letters;
            return ret;
        }

        friend
            std::ostream& operator << (std::ostream& os, const ls_mat& lsm)
            {
                Eigen::Matrix<char, Eigen::Dynamic, Eigen::Dynamic> temp = 
                    Eigen::Matrix<char, Eigen::Dynamic, Eigen::Dynamic>::Constant(lsm.letters.size() + 3, lsm.letters.size() + 3, ' ');
                temp.block(2, 2, lsm.letters.size(), lsm.letters.size()) = ('.' + ('@' - '.') * lsm.mat.cast<char>().array()).matrix();
                int i = 1;
                for (const auto& kv: lsm.letters) {
                    temp(1, i + 1) = temp(i + 1, 1) = kv.first;
                    temp(i + 1, 0) = temp(i + 1, 2 + lsm.letters.size()) = '|';
                    temp(0, 1 + i) = temp(2 + lsm.letters.size(), i + 1) = '-';
                    ++i;
                }
                temp(1, 0) = temp(1, 2 + lsm.letters.size()) = '|';
                temp(0, 1) = temp(2 + lsm.letters.size(), 1) = '-';
                return os << temp;
            }
    };

    ls_mat to_matrix(const geno_matrix& M) const { return ls_mat(M, *this); }

    friend
        std::ostream& operator << (std::ostream& os, const label_switches& sw)
        {
            auto i = sw.elems.begin();
            auto j = sw.elems.end();
            if (i != j) {
                os << (*i);
                for (++i; i != j; ++i) {
                    os << ' ' << (*i);
                }
            }
            return os;
        }
};




bool operator < (const std::pair<label_switch, label_switch>& p1, const std::pair<label_switch, label_switch>& p2)
{
    return p1.first < p2.first || (p1.first == p2.first && p1.second < p2.second);
}


bool operator == (const std::vector<double>& v1, const std::vector<double>& v2)
{
    if (v1.size() != v2.size()) {
        return false;
    }
    auto i1 = v1.begin();
    auto i2 = v2.begin();
    auto j1 = v1.end();
    while (i1 != j1 && eq_dbl(*i1, *i2)) { ++i1; ++i2; }
    return i1 == j1;
}




namespace std {
    template <>
    struct hash<label_type> {
        size_t operator () (const label_type& l) const
        {
            return hash<unsigned short>()(*(unsigned short*)&l);
        }
    };
}

/*#define DEBUG_LUMPING*/
#ifdef DEBUG_LUMPING
#define LUMP_DEBUG(_expr_) MSG_DEBUG(_expr_)
#define LUMP_QUEUE_FLUSH() MSG_QUEUE_FLUSH()
#else
#define LUMP_DEBUG(_expr_)
#define LUMP_QUEUE_FLUSH()
#endif

double cosV(const VectorXd& v1, const VectorXd& v2)
{
    double n1 = v1.lpNorm<2>();
    double n2 = v2.lpNorm<2>();

    return n1 ? n2 ? abs(v1.transpose() * v2) / (n1 * n2)
                   : 0
              : 1;
}

double cosM(const VectorXd& v1, const MatrixXd& mat)
{
    MatrixXd tmp = v1.transpose() * mat;
    double ret = cosV(v1, mat * tmp.transpose());
    /*MSG_DEBUG("CosM");*/
    /*MSG_DEBUG(v1.transpose());*/
    /*MSG_DEBUG("-------------");*/
    /*MSG_DEBUG(mat);*/
    /*MSG_DEBUG("-------------");*/
    /*MSG_DEBUG(ret);*/
    return ret;
}



void compute_LR(const geno_matrix& m, const std::set<subset>& lumping_partition, MatrixXd& L1, MatrixXd& L2)
{
    /* Creation of L1 */
    L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    for (const auto& D: lumping_partition) {
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /* Creation of L2 */
    L2 = m.lim_inf().col(0).asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();
}



void subset_difference(const subset& s1, const subset& s2, subset& output)
{
    output.resize(s1.size() + s2.size());
    auto it = std::set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(), output.begin());
    output.resize(it - output.begin());
}



geno_matrix lump_using_partition_weighted(const geno_matrix& m, const std::set<subset>& lumping_partition);


VectorXd column_angles(const MatrixXd& g1, const MatrixXd& g2)
{
    /*MSG_DEBUG(MATRIX_SIZE(g1));*/
    /*MSG_DEBUG(g1);*/
    /*MSG_DEBUG(MATRIX_SIZE(g2));*/
    /*MSG_DEBUG(g2);*/
    VectorXd n1 = (g1.array() * g1.array()).colwise().sum().array().sqrt().matrix();
    /*MSG_DEBUG(MATRIX_SIZE(n1) << std::endl << n1.transpose());*/
    VectorXd n2 = (g2.array() * g2.array()).colwise().sum().array().sqrt().matrix();
    /*MSG_DEBUG(MATRIX_SIZE(n2) << std::endl << n2.transpose());*/
    VectorXd angles(n1.size());
    for (int i = 0; i < angles.size(); ++i) {
        double n = n1(i) * n2(i);
        if (n != 0.) {
            double dot = (g1.col(i).array() * g2.col(i).array()).sum();
            /*MSG_DEBUG("(" << g1.col(i).transpose() << ") . (" << g2.col(i).transpose() << ") = " << dot << ", n = " << n);*/
            if (dot < -n) {
                dot = -n;
            } else if (dot > n) {
                dot = n;
            }
            angles(i) = acos(dot / n);
        } else {
            angles(i) = 0.;
        }
    }
    /*MSG_DEBUG("ANGLES " << angles.transpose());*/
    return angles;
}



MatrixXd greedy_permute(const MatrixXd& m)
{
    VectorXd ret = VectorXd::Zero(m.cols());
    /* todo */
    MatrixXd tmp = m;
    /*tmp.diagonal().setConstant(std::numeric_limits<double>::infinity());*/

    int i;
    for (int n = 0; n < m.cols(); ++n) {
        /*MSG_DEBUG("angles(" << n << ") = " << tmp.col(n).transpose());*/
        double a = tmp.col(n).minCoeff(&i);
        /*MSG_DEBUG("   minimum at " << i << ": " << a);*/
        ret(n) = a;
        tmp.row(i).setConstant(std::numeric_limits<double>::infinity());
    }
    return ret;
}




VectorXd min_column_angles(const MatrixXd& g1, const MatrixXd& g2)
{
    VectorXd n1 = (g1.array() * g1.array()).colwise().sum().array().sqrt().matrix();
    VectorXd n2 = (g2.array() * g2.array()).colwise().sum().array().sqrt().matrix();
    MatrixXd angles(n1.size(), n2.size());
    for (int i1 = 0; i1 < n1.size(); ++i1) {
        for (int i2 = 0; i2 < n2.size(); ++i2) {
            double n = n1(i1) * n2(i2);
#if 1
            if (n != 0.) {
                double dot = (g1.col(i1).array() * g2.col(i2).array()).sum();
                if (dot < -n) {
                    dot = -n;
                } else if (dot > n) {
                    dot = n;
                }
                angles(i1, i2) = acos(dot / n);
            } else {
                angles(i1, i2) = 0.;
            }
#else
            double dot = (g1.col(i1).array() * g2.col(i2).array()).sum();
            angles(i1, i2) = n - dot;
#endif
        }
    }



    return greedy_permute(angles);
}


#include <forward_list>

struct experimental_lumper {
    geno_matrix M;
    symmetry_list_type SYM;
    symmetry_list_type LUMPED_SYM;

    experimental_lumper(const geno_matrix& m)
        : M(m), SYM()
    {
        /*M.inf_mat = round_values(M.inf_mat, classify_values(M.inf_mat, FLOAT_TOL));*/
        SYM = analyse_symmetries(M);
        complete_symmetries();
        for (const auto& tab: SYM) {
            MSG_DEBUG("SYMMETRY " << std::endl << label_switches(M, tab).to_matrix(M));
            MSG_DEBUG("" << tab);
            /*for (const auto& kv: tab) {*/
                /*MSG_DEBUG(kv.first << " -> " << kv.second);*/
            /*}*/
        }
        if (SYM.size()) {
            MatrixXd accum = MatrixXd::Zero(M.rows(), M.cols());
            for (const auto& tab: SYM) {
                MatrixXd S = make_symmetry_matrix<double>(tab);
                accum += S.transpose() * M.inf_mat * S;
            }
            accum.array() /= SYM.size();
            MSG_DEBUG_INDENT_EXPR("[DIFF inf_mat] ");
            MSG_DEBUG((M.inf_mat - accum));
            MSG_DEBUG("--");
            MSG_DEBUG(accum.colwise().sum());
            MSG_DEBUG_DEDENT;
            /*M.inf_mat = accum;*/
        }
    }

    experimental_lumper(const geno_matrix& m, const symmetry_list_type& sym_table)
        : M(m), SYM()
    {
        /*M.inf_mat = round_values(M.inf_mat, classify_values(M.inf_mat, FLOAT_TOL));*/
        std::set<symmetry_table_type> temp;
        for (const auto& st: sym_table) {
            temp.insert(st);
        }
        SYM.assign(temp.begin(), temp.end());
        MSG_DEBUG_INDENT_EXPR("[init with sym] ");
        complete_symmetries();
        check_partition_against_symmetries(partition_on_labels());
        for (const auto& tab: SYM) {
            MSG_DEBUG("SYMMETRY " << std::endl << label_switches(M, tab).to_matrix(M));
            MSG_DEBUG("" << tab);
        }
        MSG_DEBUG_DEDENT;
        if (SYM.size()) {
            MatrixXd accum = MatrixXd::Zero(M.rows(), M.cols());
            for (const auto& tab: SYM) {
                MatrixXd S = make_symmetry_matrix<double>(tab);
                accum += S.transpose() * M.inf_mat * S;
            }
            accum.array() /= SYM.size();
            MSG_DEBUG_INDENT_EXPR("[DIFF inf_mat] ");
            MSG_DEBUG((M.inf_mat - accum));
            MSG_DEBUG("--");
            MSG_DEBUG(accum.colwise().sum());
            MSG_DEBUG_DEDENT;
            /*M.inf_mat = accum;*/
        }
    }

    double
        compute_sum(size_t s, const subset& B)
        {
            double accum = 0;
            for (int j: B) {
                accum += M.inf_mat(j, s);
            }
            return accum;
        }

    std::map<double, subset>
        compute_keys(const subset& C, const subset& B)
        {
            std::map<double, subset> ret;
            for (int i: C) {
                double sum = compute_sum(i, B);
                ret[sum].push_back(i);
            }
            return ret;
        }

    std::set<subset>
        partition_on_labels() const
        {
            std::set<subset> ret;
            std::map<label_type, subset> tmp;
            for (size_t i = 0; i < M.labels.size(); ++i) {
                tmp[M.labels[i]].push_back(i);
            }
            for (const auto& kv: tmp) {
                ret.insert(kv.second);
            }
            return ret;
        }

    experimental_lumper
        derisavi()
        {
            auto P = partition_on_labels();
            std::forward_list<const subset*> stack;
            for (const auto& x: P) { stack.push_front(&x); }
            size_t count = 0;
            MSG_DEBUG_INDENT_EXPR("[derisavi] ");
            while (!stack.empty()) {
                ++count;
                const subset& C = *stack.front();
                stack.pop_front();
                MSG_DEBUG("Studying " << C);
                MSG_QUEUE_FLUSH();
                if (C.size() <= 1) {
                    continue;
                }
                for (auto& B: P) {
                    std::map<double, subset> subsets = compute_keys(C, B);
                    if (subsets.size() > 1) {
                        MSG_DEBUG("CONFLICT! " << B << ' ' << subsets);
                        MSG_QUEUE_FLUSH();
                        std::set<subset> sym_C = apply_symmetries(C);
                        MSG_DEBUG("REMOVING");
                        MSG_DEBUG("" << sym_C);
                        MSG_QUEUE_FLUSH();
                        stack.remove_if([&](const subset*& s) { return sym_C.find(*s) != sym_C.end(); });
                        for (const auto& c: sym_C) {
                            P.erase(c);
                        }
                        for (auto& s: subsets) {
                            for (const auto& sym_s: apply_symmetries(s.second)) {
                                MSG_DEBUG("ADDING " << sym_s);
                                MSG_QUEUE_FLUSH();
                                auto io = P.insert(sym_s);
                                if (!io.second) {
                                    /*MSG_DEBUG("PROUT SUBSET ALREADY IN P " << (*io.first));*/
                                }
                                if (io.first->size() > 1) {
                                    stack.push_front(&*io.first);
                                }
                            }
                        }
                        break;
                    }
                }
            }

            MSG_DEBUG("FINAL PARTITION after " << count << " iterations");
            /*MSG_DEBUG("final base cost = " << compute_cost(P));*/
            MSG_DEBUG("" << P);

            check_partition_against_symmetries(P);

            LUMPED_SYM.clear();
            LUMPED_SYM.reserve(SYM.size());

            std::map<subset, int> tmp;
            int i = 0;
            for (const subset& B: P) {
                tmp[B] = i++;
            }
            for (const auto& S: SYM) {
                LUMPED_SYM.emplace_back();
                for (const auto& B: P) {
                    LUMPED_SYM.back()[tmp[B]] = tmp[make_symmetric_subset(S, B)];
                }
            }

            MSG_DEBUG_DEDENT;

            return {lump_using_partition_weighted(M, P), LUMPED_SYM};
        }

    struct conflict_type {
        subset C;
        subset B;
        std::map<double, subset> keys;
        double cost_;

        conflict_type(const experimental_lumper& el, const std::set<subset>& P0,
                      const subset& c, const subset& b, const std::map<double, subset>& k)
            : C(c), B(b), keys(k)
            /*, cost_(opportunity_cost(el, P0))*/
            , cost_(k.rbegin()->first - k.begin()->first)
        {}

        /*conflict_type(const subset& c, const subset& b, const std::map<double, subset>& k)*/
            /*: C(c), B(b), keys(k), cost_(0)*/
        /*{}*/

        friend
            std::ostream& operator << (std::ostream& os, const conflict_type ct)
            {
                os << "< " << ct.C << " vs " << ct.B;
                for (const auto& kv: ct.keys) {
                    os << ' ' << kv.first << ':' << kv.second;
                }
                return os << " >";
            }

        double
            magnitude() const
            {
                return keys.rbegin()->first - keys.begin()->first;
            }

        bool
            operator < (const conflict_type& other) const
            {
                return C.size() > other.C.size() || (C.size() == other.C.size() && cost_ < other.cost_);
                /*return C.size() > other.C.size() || (C.size() == other.C.size() && cost_ > other.cost_);*/
                /*return cost_ < other.cost_;*/
                /*double m1 = magnitude();*/
                /*double m2 = other.magnitude();*/
                /*return m1 < m2*/
                    /*|| (m1 == m2*/
                        /*&& (C < other.C*/
                            /*|| (C == other.C && B < other.B)))*/
                    /*;*/
            }

        double
            opportunity_cost0(const experimental_lumper& el, const std::set<subset>& P0) const
            {
                std::set<subset> P(P0);
                P.erase(C);
                for (const auto& kv: keys) { P.insert(kv.second); }
                double this_cost = el.compute_cost(P);
                /*double base_cost = el.compute_cost(P0);*/
                /*return this_cost - base_cost;*/
                return this_cost;
            }

        double
            opportunity_cost(const experimental_lumper& el, const std::set<subset>& P0) const
            {
                double accum = 0;
                size_t total_size = 0;
                for (const auto& kv: keys) {
                    accum += abs(kv.first) * kv.second.size();
                    total_size += kv.second.size();
                }
                double avg = accum / total_size;
                accum = 0;
                for (const auto& kv: keys) {
                    accum += abs(avg - abs(kv.first)) / avg;
                }
                return accum;
                (void) el; (void) P0;
            }

        std::multimap<double, subset>
            opportunity_cost_per_part(const experimental_lumper& el, const std::set<subset>& P0) const
            {
#if 0
                std::multimap<double, subset> ret;
                /*double base_cost = el.compute_cost(P0);*/
                subset C1;
                for (const auto& kv: keys) {
                    std::set<subset> P(P0);
                    subset_difference(C, kv.second, C1);
                    P.erase(C);
                    P.insert(kv.second);
                    P.insert(C1);
                    double this_cost = el.compute_cost(P);
                    /*ret.insert({this_cost - base_cost, kv.second});*/
                    ret.insert({this_cost, kv.second});
                }
#else
                std::multimap<double, subset> ret;
                subset C1;
                double accum = 0;
                size_t total_size = 0;
/*#define OP(_x_) ((_x_) * (_x_))*/
/*#define OP(_x_) ((_x_) * (_x_))*/
#define OP(_x_) (_x_)
                for (const auto& kv: keys) {
                    accum += OP(kv.first) * kv.second.size();
                    /*accum += OP(kv.first);*/
                    total_size += kv.second.size();
                }

                /*double avg = accum / keys.size();*/
                double avg = accum / total_size;
                double div;

                if (avg == 0.) {
                    div = 1.;
                } else {
                    div = 1. / avg;
                }

                for (const auto& kv: keys) {
                    /*double this_cost = (accum - OP(kv.first) * kv.second.size());*/
                    /*double this_cost = (accum - OP(kv.first));*/
                    double this_cost = abs(avg - OP(kv.first)) * div;
                    ret.insert({this_cost, kv.second});
                }
#endif
                return ret;
            }
    };

    double
        compute_cost0(const std::set<subset>& P) const 
        {
            MatrixXd PI, PI_inv;
            compute_LR(M, P, PI, PI_inv);
            MatrixXd Ptilde = PI * M.inf_mat * PI_inv;
            VectorXd stat_dist = PI * M.stat_dist;

#if 1
            VectorXd s = stat_dist.array().sqrt().inverse().matrix();
            MatrixXd U = s.array().matrix().transpose();
            MatrixXd Uinv = M.stat_dist.array().sqrt().matrix().asDiagonal();
#else
            auto Pl = partition_on_labels();
            MatrixXd U, discard;
            compute_LR(M, Pl, U, discard);
            U = (PI * U.transpose()).transpose();
            /*MatrixXd Uinv = M.stat_dist.array().sqrt().matrix();*/
            MatrixXd Uinv = M.stat_dist.asDiagonal();
#endif
            /*MatrixXd U = s.array().matrix().asDiagonal();*/

            /*MSG_DEBUG(MATRIX_SIZE(U));*/
            /*MSG_DEBUG(MATRIX_SIZE(Uinv));*/
            /*MSG_DEBUG(MATRIX_SIZE(PI));*/
            /*MSG_DEBUG(MATRIX_SIZE(M.inf_mat));*/
            /*MSG_DEBUG(MATRIX_SIZE(Ptilde));*/
            /*MSG_QUEUE_FLUSH();*/
            /*return (U * (PI * M.inf_mat - Ptilde * PI) * Uinv).lpNorm<Eigen::Infinity>();*/

            MatrixXd mat = (U * (PI * M.inf_mat - Ptilde * PI) * Uinv);
            double accum = 0;
            for (int i = 0; i < mat.cols(); ++i) {
                accum += mat.col(i).lpNorm<Eigen::Infinity>();
            }
            /*for (int i = 0; i < mat.cols(); ++i) {*/
                /*double tmp = mat.col(i).array().abs().sum();*/
                /*if (tmp > accum) {*/
                    /*accum = tmp;*/
                /*}*/
            /*}*/
            return accum;
            /*return mat.array().abs().colwise().max().sum();*/


            /*return (U * (PI * M.inf_mat - Ptilde * PI) * Uinv)(0, 0);*/
            /*return (PI * M.inf_mat - Ptilde * PI).lpNorm<2>();*/
        }

    double
        compute_cost(const std::set<subset>& P) const 
        {
            MatrixXd PI, PI_inv;
            compute_LR(M, P, PI, PI_inv);
            MatrixXd Gtilde1 = PI * M.inf_mat * PI_inv;
            VectorXd stat_dist = PI * M.stat_dist;

            auto Pl = partition_on_labels();
            MatrixXd U, Uinv;
            compute_LR(M, Pl, U, Uinv);

            U = (PI * U.transpose()).transpose();
            /*MatrixXd Uinv = M.stat_dist.asDiagonal();*/

            MatrixXd G = M.inf_mat;
            MatrixXd Gtilde = Gtilde1;
#if 0
            double accum = 0;
            int n = 1;
            double weight = .15;
            for (int i = 0; i < 10; ++i) {
                MatrixXd mat = (U * (PI * G - Gtilde * PI) * Uinv);
                accum += weight * mat.lpNorm<Eigen::Infinity>() / n;
                n *= (i + 2);
                G *= M.inf_mat;
                Gtilde *= Gtilde1;
                weight *= .15;
            }

            /*MSG_DEBUG(MATRIX_SIZE(U));*/
            /*MSG_DEBUG(MATRIX_SIZE(Uinv));*/
            /*MSG_DEBUG(MATRIX_SIZE(PI));*/
            /*MSG_DEBUG(MATRIX_SIZE(M.inf_mat));*/
            /*MSG_DEBUG(MATRIX_SIZE(Gtilde));*/
            /*MSG_QUEUE_FLUSH();*/
            /*return (U * (PI * M.inf_mat - Gtilde * PI) * Uinv).lpNorm<Eigen::Infinity>();*/

            return accum;
#elif 0
#define DIST_0 .3
            MatrixXd mat = (U * (PI * G - Gtilde * PI) * Uinv);
            double weight = DIST_0 / 2;
            MatrixXd accum = weight * mat;

            for (int n = 1; n < ORDER; ++n) {
                G *= M.inf_mat;
                Gtilde *= Gtilde1;
                weight *= DIST_0 / (n + 1);
                /*weight *= DIST_0 / (n + 2);*/
                /*weight *= DIST_0;*/
                mat = (U * (PI * G - Gtilde * PI) * Uinv);
                accum += weight * mat;
            }

            /*return accum.lpNorm<Eigen::Infinity>();*/
            return accum.lpNorm<1>();
#else
            /*return (U * (PI * G - Gtilde * PI) * Uinv).lpNorm<1>();*/
            /*MatrixXd g1 = U * PI * G * Uinv;*/
            /*MatrixXd g2 = U * Gtilde * PI * Uinv;*/
            MatrixXd g1 = PI * G;
            MatrixXd g2 = Gtilde * PI;
            return column_angles(g1, g2).lpNorm<2>();
#endif
        }

    std::vector<conflict_type>
        check_conflicts(std::set<subset>& P)
        {
            std::vector<conflict_type> ret;
            for (const auto& C: P) {
                for (const auto& B: P) {
                    auto keys = compute_keys(C, B);
                    if (keys.size() > 1) {
                        ret.emplace_back(*this, P, C, B, keys);
                    }
                }
            }
            return ret;
        }

    void
        dump_conflicts(const std::set<subset>& P, const std::multiset<conflict_type>& ordered)
        {
            MSG_DEBUG("===============================================================================================================================");
            MSG_DEBUG("base cost = " << compute_cost(P));
            MSG_DEBUG("conflicts");
            for (const auto& c: ordered) {
                MSG_DEBUG(c);
                /*MSG_DEBUG("opportunity_cost = " << c.opportunity_cost(*this, P));*/
                MSG_DEBUG("opportunity_cost = " << c.cost_);
                auto ocpp = c.opportunity_cost_per_part(*this, P);
                for (const auto& kv: ocpp) {
                    MSG_DEBUG("| cost for " << kv.second << " only: " << kv.first);
                }
            }
        }


    /*void*/
        /*pick_conflict(const std::vector<conflict>& conflicts, const std::map<label_type, size_t>& state_count)*/
        /*{*/
            /* TODO */
        /*}*/

    label_type state_label(const subset& s) { return M.labels[s.front()]; }

    inline
        void _safe_insert(subset& s, int i, std::vector<bool>& visited)
        {
            if (!visited[i]) {
                s.push_back(i);
                visited[i] = true;
            }
        }

    inline
        void _safe_commit(subset& s, std::set<subset>& P)
        {
            if (!s.size()) {
                return;
            }
            std::sort(s.begin(), s.end());
            P.insert(s);
        }

    inline
        std::set<subset>
        apply_symmetries(const subset& B)
        {
            std::set<subset> ret;
            ret.insert(B);
            for (const auto& S: SYM) {
                subset tmp;
                for (int i: B) {
                    tmp.push_back(S.find(i)->second);
                }
                std::sort(tmp.begin(), tmp.end());
                ret.insert(tmp);
            }
            return ret;
        }

    MatrixXb make_adjacency_matrix(const std::set<subset>& P0)
    {
        MatrixXb adj = MatrixXb::Zero(M.cols(), M.cols());
        for (const auto& s: P0) {
            for (int i: s) {
                for (int j: s) {
                    adj(i, j) = 1;
                }
            }
        }
        return adj;
    }

    std::set<subset> split_and_apply_symmetries(const std::set<subset>& P0, MatrixXb& adj, const subset& splitted, const subset& split)
    {
        MSG_DEBUG("NEW SPLIT IMPLEMENTATION");
        MSG_DEBUG("SPLITTING " << split << " FROM PARTITION");
        MSG_DEBUG("" << P0);

        MSG_DEBUG_INDENT_EXPR("[adj BEFORE] ");
        MSG_DEBUG(adj);
        MSG_DEBUG_DEDENT;

        for (int s1: splitted) {
            for (int s2: split) {
                if (s1 != s2) {
                    adj(s1, s2) = adj(s2, s1) = 0;
                }
            }
        }

        for (const auto& S: SYM) {
            subset sym_split;
            sym_split.reserve(split.size());
            for (int i: split) {
                sym_split.push_back(S.find(i)->second);
            }
            for (int s1: splitted) {
                int i = S.find(s1)->second;
                for (int j: sym_split) {
                    if (i != j) {
                        adj(i, j) = adj(j, i) = 0;
                    }
                }
            }
        }

        MSG_DEBUG_INDENT_EXPR("[adj AFTER] ");
        MSG_DEBUG(adj);
        MSG_DEBUG_DEDENT;
        MSG_QUEUE_FLUSH();

        std::vector<bool> visited(M.cols(), false);
        std::set<subset> P;

        for (int j = 0; j < adj.cols(); ++j) {
            subset s;
            if (visited[j]) {
                continue;
            }
            for (int i = 0; i < adj.rows(); ++i) {
                if (adj(i, j)) {
                    s.push_back(i);
                    visited[i] = true;
                }
            }
            P.insert(s);
        }

        check_partition_against_symmetries(P);

        return P;

#if 0
        MSG_DEBUG("SPLITTING " << split << " FROM PARTITION");
        MSG_DEBUG("" << P0);
        std::vector<bool> visited(M.cols(), false);
        std::set<subset> P;

        /*auto safe_insert = [&] (subset& s, int i) { if (!visited[i]) { s.push_back(i); visited[i] = true; } };*/

        /*auto safe_commit = [&] (subset& s) { if (!s.size()) { return; } std::sort(s.begin(), s.end()); P.insert(s); };*/

        /* first, insert split and sym(split) */
        for (int i: split) { visited[i] = true; }
        MSG_DEBUG("inserting split " << split);
        P.insert(split);

        for (const auto& S: SYM) {
            subset s;
            subset debug;
            for (int i: split) { _safe_insert(s, S.find(i)->second, visited); debug.push_back(S.find(i)->second); }
            std::sort(debug.begin(), debug.end());
            MSG_DEBUG("inserting symmetric split --" << debug << "-> " << s);
            _safe_commit(s, P);
        }

        /* then, insert splitted and sym(splitted) */
        /*for (int i: splitted) { visited[i] = true; }*/
        subset s;
        for (int i: splitted) { _safe_insert(s, i, visited); }
        MSG_DEBUG("inserting splitted --" << splitted << "-> " << s);
        _safe_commit(s, P);

        for (const auto& S: SYM) {
            subset s;
            subset debug;
            for (int i: splitted) { _safe_insert(s, S.find(i)->second, visited); debug.push_back(S.find(i)->second); }
            std::sort(debug.begin(), debug.end());
            MSG_DEBUG("inserting symmetric splitted --" << debug << "-> " << s);
            _safe_commit(s, P);
        }

        for (const auto& B: P0) {
            subset ns;
            for (int i: B) { _safe_insert(ns, i, visited); }
            MSG_DEBUG("inserting " << B << " -> " << ns);
            _safe_commit(ns, P);
            if (ns.size()) {
                for (const auto& S: SYM) {
                    subset s;
                    subset debug;
                    for (int i: ns) { debug.push_back(S.find(i)->second); _safe_insert(s, S.find(i)->second, visited); }
                    std::sort(debug.begin(), debug.end());
                    MSG_DEBUG("inserting symmetric " << ns << " --" << debug << "-> " << s);
                    _safe_commit(s, P);
                }
            }
        }

        check_partition_against_symmetries(P);

        return P;
#endif
    }

    subset make_symmetric_subset(const symmetry_table_type& S, const subset& B)
    {
        subset s;
        s.reserve(B.size());
        for (int i: B) {
            s.push_back(S.find(i)->second);
        }
        std::sort(s.begin(), s.end());
        return s;
    }

    void check_partition_against_symmetries(const std::set<subset>& P0)
    {
        for (const auto& S: SYM) {
            std::set<subset> P;
            for (const auto& B: P0) {
                P.insert(make_symmetric_subset(S, B));
            }
            if (P != P0) {
                MSG_DEBUG("PARTITION IS NOT SYMMETRIC!");
                MSG_DEBUG("SYMMETRY:");
                MSG_DEBUG("" << S);
                MSG_DEBUG("P0:");
                MSG_DEBUG("" << P0);
                MSG_DEBUG("P:");
                MSG_DEBUG("" << P);
                MSG_QUEUE_FLUSH();
                /* whine */
                throw 0;
            }
        }
    }

    int rec_complete_symmetries(std::set<symmetry_table_type>& uniq_sym)
    {
        int count = 0;
        for (size_t S1 = 0; S1 < SYM.size(); ++S1) {
        /*for (const auto& S1: SYM) {*/
            /*MatrixXb s1 = make_symmetry_matrix<bool>(S1);*/
            MatrixXb s1 = make_symmetry_matrix<bool>(SYM[S1]);
            /*for (const auto& S2: SYM) {*/
            for (size_t S2 = 0; S2 <= S1; ++S2) {
                MatrixXb s2 = make_symmetry_matrix<bool>(SYM[S2]);
                symmetry_table_type comp = make_symmetry<bool>(s1 * s2);

                auto lsm1 = label_switches(M, SYM[S1]).to_matrix(M);
                auto lsm2 = label_switches(M, SYM[S2]).to_matrix(M);
                auto lsm_comp = label_switches(M, comp).to_matrix(M);
                auto lsm_12 = lsm1 * lsm2;

                if (lsm_12.mat != lsm_comp.mat) {
                    MSG_DEBUG_INDENT_EXPR("[FOIRURE] ");
                    MSG_DEBUG("lsm1");
                    MSG_DEBUG("" << lsm1);
                    MSG_DEBUG("s1");
                    MSG_DEBUG(s1);
                    MSG_DEBUG("lsm2");
                    MSG_DEBUG("" << lsm2);
                    MSG_DEBUG("s2");
                    MSG_DEBUG(s2);
                    MSG_DEBUG("lsm_comp");
                    MSG_DEBUG("" << lsm_comp);
                    MSG_DEBUG("lsm_12");
                    MSG_DEBUG("" << lsm_12);
                    MSG_DEBUG("comp");
                    MSG_DEBUG(make_symmetry_matrix<bool>(comp));
                    auto cm = make_symmetry_matrix<double>(comp);
                    MSG_DEBUG("compT.M.comp - M = 0 ? " << (cm.transpose() * M.inf_mat * cm - M.inf_mat).isZero(FLOAT_TOL));
                    MSG_DEBUG_DEDENT;
                }

                /*symmetry_table_type comp;*/
                /*for (const auto& kv1: SYM[S1]) {*/
                    /*comp[kv1.first] = SYM[S2].find(kv1.second)->second;*/
                /*}*/

                /*symmetry_table_type comp;*/
                /*for (const auto& kv: S1) {*/
                /*comp[kv.first] = S2.find(kv.second)->second;*/
                /*}*/
                /*auto temp = make_symmetry_matrix<bool>(comp);*/
                /*MSG_DEBUG(MATRIX_SIZE(temp));*/
                /*MSG_QUEUE_FLUSH();*/
                if (uniq_sym.insert(comp).second) {
                    SYM.push_back(comp);
                    MSG_DEBUG("NEW SYM");
                    MSG_DEBUG(label_switches(M, SYM[S1]) << " x " << label_switches(M, SYM[S2]) << " => " << label_switches(M, SYM.back()));
                    /*MSG_DEBUG("" << comp);*/
                    /*MSG_QUEUE_FLUSH();*/
                    ++count;
                    /*rec_complete_symmetries(uniq_sym);*/
                    /*return;*/
                }
            }
        }
        return count;
    }

     void quotient_set0()
    {
        std::vector<bool> visited(M.cols(), false);
        std::set<subset> P;
        for (int i = 0; i < M.cols(); ++i) {
            if (visited[i]) { continue; }
            std::vector<bool> local_visited(M.cols(), false);
            visited[i] = true;
            local_visited[i] = true;
            subset s;
            s.push_back(i);
            subset stack;
            stack.push_back(i);
            while (stack.size()) {
                int state = stack.back();
                stack.pop_back();
                for (const auto& S: SYM) {
                    int ss = S.find(i)->second;
                    if (!local_visited[ss]) {
                        local_visited[ss] = true;
                        stack.push_back(ss);
                        s.push_back(ss);
                    }
                }
            }
            std::sort(s.begin(), s.end());
            P.insert(s);
        }
        MSG_DEBUG("QUOTIENT SET");
        MSG_DEBUG("" << P);
    }

   void quotient_set()
    {
        MatrixXb accum = MatrixXb::Identity(M.rows(), M.cols());
        while (true) {
            MatrixXb tmp = accum;
            for (const auto& S: SYM) {
                MatrixXb sm = make_symmetry_matrix<bool>(S);
                accum += tmp * sm;
            }
            if (accum == tmp) {
                break;
            }
        }
        MSG_DEBUG("QUOTIENT SET");
        MSG_DEBUG(('.' + ('@' - '.') * accum.cast<char>().array()));
    }

    void complete_symmetries()
    {
        std::set<symmetry_table_type> uniq_sym(SYM.begin(), SYM.end());
        MSG_DEBUG_INDENT_EXPR("[complete sym] ");
        int count = rec_complete_symmetries(uniq_sym);
        MSG_DEBUG("Found " << count << " new symmetries by composition for a total of " << SYM.size() << " symmetries.");
        count = rec_complete_symmetries(uniq_sym);
        MSG_DEBUG("(2) Found " << count << " new symmetries by composition for a total of " << SYM.size() << " symmetries.");
        MSG_DEBUG_DEDENT;
        quotient_set();
    }

    geno_matrix
        do_lump(size_t max_size)
        {
            size_t count = 0;
            /*MSG_DEBUG(M);*/
            /*MSG_QUEUE_FLUSH();*/
            auto P = partition_on_labels();
            auto adj = make_adjacency_matrix(P);
            MSG_DEBUG("INITIAL PARTITION SIZE " << P.size());
            check_partition_against_symmetries(P);
            /*auto conflicts = check_conflicts(P);*/
            /*std::multiset<conflict_type> ordered(conflicts.begin(), conflicts.end());*/
            /*dump_conflicts(P, ordered);*/
            /*double max = ordered.rbegin()->opportunity_cost(*this, P);*/
            std::map<subset, std::pair<double, subset>> split_picker;
            while (P.size() < max_size && compute_cost(P) > 0.) {
                auto conflicts = check_conflicts(P);
                if (0) {
                    std::multiset<conflict_type> ordered(conflicts.begin(), conflicts.end());
                    dump_conflicts(P, ordered);
                }

                if (!conflicts.size()) {
                    break;
                }
#if 0
                auto best = std::min_element(conflicts.begin(), conflicts.end());

                auto ocpp = best->opportunity_cost_per_part(*this, P);
                /*auto bestppi = ocpp.rbegin();*/
                auto bestppi = ocpp.begin();

                P.erase(best->C);
                subset C1;
                subset_difference(best->C, bestppi->second, C1);
                P.insert(bestppi->second);
                P.insert(C1);
                ++count;
#else
#if 1
                double min_cost = std::numeric_limits<double>::infinity();
                /*const subset* split = NULL;*/
                subset split;
                subset splitted;
                for (const auto& c: conflicts) {
                    auto ocpp = c.opportunity_cost_per_part(*this, P);
                    auto bestppi = ocpp.begin();
                    if (bestppi->first < min_cost) {
                        split = bestppi->second;
                        splitted = c.C;
                        min_cost = bestppi->first;
                    }
                }

                if (split.size()) {
                    auto tmp = split_and_apply_symmetries(P, adj, splitted, split);
                    P = tmp;
                    MSG_DEBUG("NEW PARTITION SIZE " << P.size());
                }
#else
                split_picker.clear();
                double min_cost = std::numeric_limits<double>::infinity();
                for (const auto& c: conflicts) {
                    auto& pick = split_picker[c.C];
                    pick.first = std::numeric_limits<double>::infinity();
                }
                for (const auto& c: conflicts) {
                    auto& pick = split_picker[c.C];
                    auto ocpp = c.opportunity_cost_per_part(*this, P);
                    auto bestppi = ocpp.begin();
                    if (bestppi->first < pick.first) {
                        pick.first = bestppi->first;
                        pick.second = bestppi->second;
                        if (min_cost > bestppi->first) {
                            min_cost = bestppi->first;
                        }
                    }
                }

                for (const auto& kv: split_picker) {
                    if (P.size() >= max_size) {
                        break;
                    }
                    if (kv.second.first - min_cost < FLOAT_TOL) {
                        P.erase(kv.first);
                        subset C1;
                        subset_difference(kv.first, kv.second.second, C1);
                        P.insert(C1);
                        P.insert(kv.second.second);
                    }
                }
#endif
                ++count;
#endif
            }
            MSG_DEBUG("FINAL PARTITION after " << count << " iterations");
            /*MSG_DEBUG("final base cost = " << compute_cost(P));*/
            MSG_DEBUG("" << P);

            check_partition_against_symmetries(P);

            LUMPED_SYM.clear();
            LUMPED_SYM.reserve(SYM.size());

            std::map<subset, int> tmp;
            int i = 0;
            for (const subset& B: P) {
                tmp[B] = i++;
            }
            for (const auto& S: SYM) {
                LUMPED_SYM.emplace_back();
                for (const auto& B: P) {
                    LUMPED_SYM.back()[tmp[B]] = tmp[make_symmetric_subset(S, B)];
                }
            }

            return lump_using_partition_weighted(M, P);
        }
};








/*
 * De lumpibus

(%i1) kill(all);
load(diag);
(%o0) done
(%o1) "/usr/share/maxima/5.32.1/share/contrib/diag.mac"
-->  / * création manuelle du BC2 * /
     bc2: matrix([-2, 2, 2],
                 [1, -2, 0],
                 [1, 0, -2]);
(%o2) matrix([-2,2,2],[1,-2,0],[1,0,-2])
(%i3) jbc2: jordan(bc2);
(%o3) [[-4,1],[-2,1],[0,1]]
(%i4) p: ModeMatrix(bc2, jbc2);
      p^^-1;
(%o4) matrix([1,0,1],[-1/2,1,1/2],[-1/2,-1,1/2])
(%o5) matrix([1/2,-1/2,-1/2],[0,1/2,-1/2],[1/2,1/2,1/2])
(%i6) pt: args(transpose(p));
(%o6) [[1,-1/2,-1/2],[0,1,-1],[1,1/2,1/2]]
(%i7) Ptmp:transpose(apply(matrix, map(uvect, pt)));
(%o7) matrix([sqrt(2)/sqrt(3),0,sqrt(2)/sqrt(3)],[-1/(sqrt(2)*sqrt(3)),1/sqrt(2),1/(sqrt(2)*sqrt(3))],[-1/(sqrt(2)*sqrt(3)),-1/sqrt(2),1/(sqrt(2)*sqrt(3))])
(%i8) float(Ptmp);
(%o8) matrix([0.81649658092772,0.0,0.81649658092772],[-0.40824829046386,0.70710678118654,0.40824829046386],[-0.40824829046386,-0.70710678118654,0.40824829046386])
-->  / * permutation pour être cohérent avec l'implémentation C++ de matrixexp (au 30/01/2015) * /
     zzz: matrix([1, 0, 0], [0, 0, 1], [0, 1, 0]);
     zzz2: matrix([-1, 0, 0], [0, 1, 0], [0, 0, 1]);
(%o9) matrix([1,0,0],[0,0,1],[0,1,0])
(%o10) matrix([-1,0,0],[0,1,0],[0,0,1])
(%i11) p: zzz . Ptmp . zzz . zzz2;
(%o11) matrix([-sqrt(2)/sqrt(3),sqrt(2)/sqrt(3),0],[1/(sqrt(2)*sqrt(3)),1/(sqrt(2)*sqrt(3)),-1/sqrt(2)],[1/(sqrt(2)*sqrt(3)),1/(sqrt(2)*sqrt(3)),1/sqrt(2)])
(%i12) float(p);
(%o12) matrix([-0.81649658092772,0.81649658092772,0.0],[0.40824829046386,0.40824829046386,-0.70710678118654],[0.40824829046386,0.40824829046386,0.70710678118654])
(%i13) pinv: p^^-1;
(%o13) matrix([-sqrt(3)/2^(3/2),sqrt(3)/2^(3/2),sqrt(3)/2^(3/2)],[sqrt(3)/2^(3/2),sqrt(3)/2^(3/2),sqrt(3)/2^(3/2)],[0,-1/sqrt(2),1/sqrt(2)])
(%i14) p . pinv;
(%o14) matrix([1,0,0],[0,1,0],[0,0,1])
-->  d:pinv.bc2.p;
     / * On est contents c'est bien la bonne diag, les bonnes p et pinv, tout va bien * /
(%o15) matrix([-4,0,0],[0,0,0],[0,0,-2])
(%i16) dinf: diag([0, 1, 0]);
(%o16) matrix([0,0,0],[0,1,0],[0,0,0])
(%i17) p.dinf.pinv;
(%o17) matrix([1/2,1/2,1/2],[1/4,1/4,1/4],[1/4,1/4,1/4])
(%i18) limit(matrixexp(bc2, ddd), ddd, inf);
(%o18) matrix([1/2,1/2,1/2],[1/4,1/4,1/4],[1/4,1/4,1/4])
-->  / * Note à benêts :
        l1 est la matrice de lumping.
        l2 est sa transposée pondérée par les abondances normalisées PAR SOUS-ENSEMBLE DE LA PARTITION.
        Il semble (sic) que cette magouille (sic) est nécessaire pour que
        l'abondance de la somme soit la même que la somme des abondances.
      * /
     l1: matrix([1, 1, 0], [0, 0, 1]);
     l2: matrix([2/3, 0], [1/3, 0], [0, 1]);
(%o19) matrix([1,1,0],[0,0,1])
(%o20) matrix([2/3,0],[1/3,0],[0,1])
(%i30) ab: [1/2, 1/4, 1/4];
(%o30) [1/2,1/4,1/4]
(%i31) l1 . ab;
(%o31) matrix([3/4],[1/4])
(%i21) l1 . l2;
(%o21) matrix([1,0],[0,1])
(%i22) bc2l: l1 . bc2 . l2;
(%o22) matrix([-2/3,2],[2/3,-2])
(%i23) eigenvectors(bc2l);
(%o23) [[[-8/3,0],[1,1]],[[[1,-1]],[[1,1/3]]]]
(%i24) pl: ModeMatrix(bc2l);
(%o24) matrix([1,1],[-1,1/3])
(%i25) plinv: pl^^-1;
(%o25) matrix([1/4,-3/4],[3/4,3/4])
(%i26) dl: plinv . bc2l . pl;
(%o26) matrix([-8/3,0],[0,0])
(%i27) limit(matrixexp(bc2l, dd), dd, inf);
(%o27) matrix([3/4,3/4],[1/4,1/4])
-->  / * Hou qu'il est beau le Youki * /
     wxplot2d(flatten(args(l1 . float(matrixexp(bc2, dd)) . l2 - float(matrixexp(bc2l, dd)))), [dd, 0, 5]);
(%t29)  << Graphics >> 
(%o29) 

 */


geno_matrix lump_using_partition_weighted(const geno_matrix& m, const std::set<subset>& lumping_partition)
{
    m.check_not_nan();
    /*MSG_DEBUG("|| LUMPING");*/
    /*MSG_DEBUG("" << m);*/
    /*MSG_DEBUG("|| WITH PARTITION");*/
    /*MSG_DEBUG("" << lumping_partition);*/

    geno_matrix ret_lump;

    /* Creation of L1 */
    MatrixXd L1 = MatrixXd::Zero(lumping_partition.size(), m.cols());
    int i = 0;
    ret_lump.labels.clear();
    ret_lump.labels.reserve(lumping_partition.size());
    for (const auto& D: lumping_partition) {
        ret_lump.labels.push_back(m.labels[D.front()]);
        for (int s: D) {
            L1(i, s) = 1.;
        }
        ++i;
    }
    /*MSG_DEBUG("L1");*/
    /*MSG_DEBUG(L1);*/
    /* Creation of L2 */
    MatrixXd L2 = m.stat_dist.asDiagonal() * L1.transpose();
    L2.array().rowwise() /= L2.array().colwise().sum();

    /* Let's be blunt. */
    ret_lump.inf_mat =
        /* <em>And bold.</em> */
        L1 * m.inf_mat * L2;

    /*MSG_DEBUG("## m.inf_mat" << std::endl << m.inf_mat);*/
    /*MSG_DEBUG("## L1" << std::endl << L1);*/
    /*MSG_DEBUG("## L2" << std::endl << L2);*/
    /*MSG_DEBUG("## ret_lump.inf_mat" << std::endl << ret_lump.inf_mat);*/

    /*MSG_DEBUG(MATRIX_SIZE(L1));*/
    /*MSG_DEBUG(L1);*/
    /*MSG_DEBUG(MATRIX_SIZE(m.stat_dist));*/
    /*MSG_DEBUG(m.stat_dist.transpose());*/
    /*MSG_QUEUE_FLUSH();*/

    ret_lump.stat_dist = L1 * m.stat_dist;

    VectorXd s = ret_lump.stat_dist.array().sqrt().matrix();
    /*MSG_DEBUG("## s = " << s.transpose());*/
    MatrixXd Uinv = s.asDiagonal();
    /*MSG_DEBUG("## Uinv" << std::endl << Uinv);*/
    MatrixXd U = s.array().inverse().matrix().asDiagonal();
    /*MSG_DEBUG("## U" << std::endl << U);*/
    MatrixXd tmp = U * ret_lump.inf_mat * Uinv;
    /*MSG_DEBUG("## tmp" << std::endl << tmp);*/

    if (!(tmp - tmp.transpose()).isZero()) {
        MSG_DEBUG("NOT SYMMETRIC!!!");
        MSG_DEBUG(tmp);
    }

    SelfAdjointEigenSolver<MatrixXd> saes(tmp);

    ret_lump.diag = saes.eigenvalues().real();
    MatrixXd p = saes.eigenvectors().real();
    MatrixXd p_inv = p.transpose();
    ret_lump.cleanup(p);
    ret_lump.cleanup(p_inv);
    /*MSG_DEBUG("================= p");*/
    /*MSG_DEBUG(p);*/
    /*MSG_DEBUG("================= p^-1");*/
    /*MSG_DEBUG(p_inv);*/
    /*MSG_DEBUG("================= diag");*/
    /*MSG_DEBUG(ret_lump.diag);*/
    /*MSG_DEBUG("================= p.p^-1");*/
    /*MSG_DEBUG((p * p_inv));*/

    ret_lump.p = Uinv * p;
    ret_lump.p_inv = p_inv * U;

    ret_lump.variant = m.variant;

    ret_lump.cleanup();

#if 0
    bool fail = false;

    if (!(p * p_inv - MatrixXd::Identity(ret_lump.p.rows(), ret_lump.p.cols())).isZero(FLOAT_TOL)) {
        MSG_DEBUG("@@ PROUT p.p^-1");
        MSG_DEBUG((p * p_inv - MatrixXd::Identity(p.rows(), p.cols())));
        fail = true;
    }

    if (!(p * ret_lump.diag.asDiagonal() * p_inv - tmp).isZero(FLOAT_TOL)) {
        MSG_DEBUG("@@ PROUT p.D.p^-1 == M");
        MSG_DEBUG((p * ret_lump.diag.asDiagonal() * p_inv));
        MSG_DEBUG("---");
        MSG_DEBUG(tmp);
        MSG_DEBUG("---");
        MSG_DEBUG((p * ret_lump.diag.asDiagonal() * p_inv - tmp));
        MSG_DEBUG("p^-1.M.p");
        MatrixXd prout = p_inv * tmp * p;
        ret_lump.cleanup(prout);
        MSG_DEBUG(prout);
        fail = true;
    }

    /*if (!(p_inv * ret_lump.diag.asDiagonal() * p - tmp).isZero(FLOAT_TOL)) {*/
        /*MSG_DEBUG("@@ PROUT P^-1.D.P == M");*/
        /*MSG_DEBUG((p_inv * ret_lump.diag.asDiagonal() * p - tmp));*/
        /*fail = true;*/
    /*}*/

    if (!(ret_lump.p * ret_lump.p_inv - MatrixXd::Identity(ret_lump.p.rows(), ret_lump.p.cols())).isZero(FLOAT_TOL)) {
        MSG_DEBUG("@@ PROUT P.P^-1");
        MSG_DEBUG((ret_lump.p * ret_lump.p_inv - MatrixXd::Identity(ret_lump.p.rows(), ret_lump.p.cols())));
        fail = true;
    }

    if (!(ret_lump.p * ret_lump.diag.asDiagonal() * ret_lump.p_inv - ret_lump.inf_mat).isZero(FLOAT_TOL)) {
        MSG_DEBUG("@@ LOSER (BAD DIAG)");
        MSG_DEBUG((ret_lump.p * ret_lump.diag.asDiagonal() * ret_lump.p_inv - ret_lump.inf_mat));
        fail = true;
    }

    MatrixXd eigtest = ret_lump.inf_mat * ret_lump.stat_dist;
    /*if (!(eigtest == MatrixXd::Zero(eigtest.rows(), eigtest.cols()) || eigtest.isZero(FLOAT_TOL))) {*/
    if (!eigtest.isZero(FLOAT_TOL)) {
        MSG_DEBUG("@@ LOSER AGAIN");
        MSG_DEBUG(eigtest);
        MSG_DEBUG("---");
        MSG_DEBUG(eigtest.isZero());
        MSG_DEBUG(eigtest.isZero(FLOAT_TOL));
        MSG_DEBUG(eigtest.sum());
        MSG_DEBUG(eigtest.lpNorm<1>());
        fail = true;
    }   

    if (0 && fail) {
        MSG_DEBUG("MEMENTO");
        MSG_DEBUG("U" << std::endl << U);
        MSG_DEBUG("U^-1" << std::endl << Uinv);
        MSG_DEBUG("tmp" << std::endl << tmp);
        MSG_DEBUG("p" << std::endl << p);
        MSG_DEBUG("p^-1" << std::endl << p_inv);
        MSG_DEBUG("- - - - - - - - - - - - - - - - - - - - - - - - - - -");
        MSG_DEBUG(ret_lump);
        exit(-1);
    }
#endif

    return ret_lump;
}


MatrixXd compute_lumping_costs(const geno_matrix& m, const std::set<subset>& P0);
std::set<subset> init_partition(const geno_matrix& m);

geno_matrix lump(const geno_matrix& m, bool harder=false)
{
    /*MSG_DEBUG("LUMPING=====================================================");*/
    /*MSG_DEBUG(MATRIX_SIZE(m.inf_mat));*/
    /*MSG_DEBUG(m);*/
    /*MSG_DEBUG("------------------------------------------------------------");*/
    ::lumper<MatrixXd, label_type> l(m.inf_mat, m.labels);
    auto lumping_partition = l.refine_all();
    /*if (harder) {*/
        /*l.try_harder(lumping_partition);*/
    /*}*/
    /*MSG_DEBUG("" << lumping_partition);*/

    /* TODO tester que selon la formule de Sylvain cette partition donne un joli 0 */

    /*MSG_DEBUG("PARTITION");*/
    /*MSG_DEBUG("" << lumping_partition);*/
#if 0
    if (0)
    {
        auto P = init_partition(m);
        MSG_DEBUG("LUMPING COSTS");
        MSG_DEBUG("" << compute_lumping_costs(m, P));
        MSG_DEBUG("" << lumping_partition);

        double delta = compute_delta(m, lumping_partition);
        MSG_DEBUG("LUMP_DELTA=" << delta);
        if (delta > FLOAT_TOL || delta != delta) {
            /*MSG_DEBUG(m.inf_mat);*/
            MSG_DEBUG("" << lumping_partition);
        }
    }
#endif
    return lump_using_partition_weighted(m, lumping_partition);
    (void) harder;
}


geno_matrix split_matrix(const geno_matrix& m)
{
    std::set<label_type> labels(m.labels.begin(), m.labels.end());
    int row, col;
    std::set<subset> partition;
    /*std::map<label_type, std::map<double, std::vector<int>>> lump_states;*/
    for (const auto& l: labels) {
        int rows = 0;
        for (const auto& ml: m.labels) {
            if (ml == l) { ++rows; }
        }
        MatrixXd splitter = MatrixXd::Zero(rows, m.cols());
        col = 0;
        row = 0;
        std::vector<int> state_index;
        for (const auto& ml: m.labels) {
            if (ml == l) {
                splitter(row, col) = 1;
                ++row;
                state_index.push_back(col);
            }
            ++col;
        }
        /*MSG_DEBUG("SPLITTER FOR " << l);*/
        /*MSG_DEBUG(splitter);*/
        /*MSG_DEBUG("SUB-MATRIX FOR " << l);*/
        MatrixXd mat = splitter * m.inf_mat * splitter.transpose();
        /*MSG_DEBUG(mat);*/
        VectorXd sums = mat.colwise().sum();
        /*MSG_DEBUG("SUMS " << sums);*/
        std::map<double, std::vector<int>> state_groups;
        for (int i = 0; i < sums.size(); ++i) {
            state_groups[sums[i]].push_back(state_index[i]);
            /*state_groups[0].push_back(state_index[i]);*/
        }
        for (const auto& kv: state_groups) {
            partition.insert(kv.second);
        }
    }
    /*MSG_DEBUG("OVER-LUMP");*/
    /*MSG_DEBUG("" << partition);*/
    /*for (const auto& C: partition) {*/
        /*std::stringstream msg;*/
        /*for (int s: C) {*/
            /*msg << ' ' << m.labels[s] << '[' << s << ']';*/
        /*}*/
        /*MSG_DEBUG(msg.str());*/
    /*}*/
    /*MSG_DEBUG_INDENT;*/

    geno_matrix ret = lump_using_partition_weighted(m, partition);

    /*MSG_DEBUG(ret);*/
    /*MSG_DEBUG_DEDENT;*/

    return ret;
}


void output_sur_son_balcon(const MatrixXd& sums, const std::set<subset>& P)
    /* manifestement parce que derisavi par procuration. */
{
    VectorXd part(sums.cols());
    int col = 0;
    for (const auto& C: P) {
        for (int s: C) {
            part(s) = col;
        }
        ++col;
    }

    MatrixXd transp = MatrixXd::Zero(sums.cols(), sums.cols());
    int row = 0;
    for (const auto& C: P) {
        for (int s: C) {
            transp(s, row) = 1;
            ++row;
        }
    }

    /*part = transp * part;*/

    /*MSG_DEBUG("=========" << std::endl << transp << std::endl << "=========");*/

    MatrixXd output(sums.rows() + 2, sums.cols());
    for (int i = 0; i < output.cols(); ++i) {
        output(0, i) = i;
    }
    output.row(1) = part.transpose();
    output.bottomRows(sums.rows()) = sums;
    output *= transp;
    /*MSG_DEBUG(output);*/
}


/*bool operator < (const VectorXd& v1, const VectorXd& v2)*/
namespace std {
    template <int L, typename _Scalar>
        struct less<Eigen::Matrix<_Scalar, L, 1>> {
            bool operator () (const Eigen::Matrix<_Scalar, L, 1>& v1, const Eigen::Matrix<_Scalar, L, 1>& v2) const
            {
                /*if ((v1 - v2).isZero()) {*/
                    /*return false;*/
                /*}*/
                for (int i = 0; i < v1.size(); ++i) {
                    if (v1(i) < v2(i)) return true;
                    if (v2(i) < v1(i)) return false;
                }
                return false;
            }
        };
}


std::set<subset> partition_on_labels(const std::vector<label_type>& labels)
{
    std::set<subset> P;
    lumper_map<label_type, subset> tmp;
    for (size_t i = 0; i < labels.size(); ++i) {
        auto& l = labels[i];
        /*if (tmp[l].size() == 0) {*/
            /*tmp[l].resize(G.column_labels.size());*/
        /*}*/
        /*tmp[l].set(i);*/
        tmp[l]./*insert*/push_back(i);
    }
    for (auto& kv: tmp) {
        /*std::cout << kv.first << "\t" << kv.second << std::endl;*/
        P.insert(kv.second);
    }

    return P;
}


void compute_U(const MatrixXd inf_mat, VectorXd& U, VectorXd& Uinv)
{
}


void subset_union(const subset& s1, const subset& s2, subset& output)
{
    output.resize(s1.size() + s2.size());
    auto it = std::set_union(s1.begin(), s1.end(), s2.begin(), s2.end(), output.begin());
    output.resize(it - output.begin());
}


std::set<subset> init_partition(const geno_matrix& m)
{
    std::set<subset> ret;
    for (int i = 0; i < m.inf_mat.cols(); ++i) {
        ret.insert({i});
    }
    return ret;
}

std::set<subset>::const_iterator merge(const subset& S1, const subset& S2, std::set<subset>& P)
{
    P.erase(S1);
    P.erase(S2);
    subset Stot;
    subset_union(S1, S2, Stot);
    return P.insert(Stot).first;
}

void unmerge(const subset& S1, const subset& S2, std::set<subset>::const_iterator Stot, std::set<subset>& P)
{
    P.erase(Stot);
    P.insert(S1);
    P.insert(S2);
}




struct ClassLabels {
    std::vector<label_type> labels;
    const std::vector<label_type>* all_labels;

    ClassLabels(const geno_matrix& m)
        : labels(), all_labels(&m.labels)
    {}

    void recompute(const MatrixXd& C)
    {
        labels.clear();
        labels.resize(C.rows(), {0, 0});
        C.visit(*this);
        /*MSG_DEBUG("class labels: " << labels);*/
    }

    // called for the first coefficient
    void init(const double& value, int i, int j)
    {
        if (value != 0.0) {
            labels[i] = (*all_labels)[j];
        }
    }
    // called for all other coefficients
    void operator() (const double& value, int i, int j)
    {
        init(value, i, j);
    }
};


void removeZeroRows0(Eigen::MatrixXd& mat)
{
    Eigen::Matrix<bool, Eigen::Dynamic, 1> empty = (mat.array() == 0).rowwise().all();

    size_t last = mat.rows() - 1;
    while (empty(last)) { --last; }
    for (size_t i = 0; i < last + 1;)
    {
        if (empty(i))
        {
            mat.row(i).swap(mat.row(last));
            empty.segment<1>(i).swap(empty.segment<1>(last));
            while (empty(last)) { --last; }
        }
        else
            ++i;
    }
    mat.conservativeResize(last + 1, mat.cols());
}


std::set<subset> matrix2partition(const MatrixXd& C)
{
    std::set<subset> P0;
    for (int i = 0; i < C.rows(); ++i) {
        subset s;
        for (int j = 0; j < C.cols(); ++j) {
            if (C(i, j) != 0.) {
                s.push_back(j);
            }
        }
        P0.insert(s);
    }
    return P0;
}


std::map<label_type, double> abundances(geno_matrix& m)
{
    Eigen::Map<Eigen::Array<label_type, Eigen::Dynamic, 1>> labels(m.labels.data(), m.labels.size(), 1);
    MatrixXd abund = m.lim_inf().col(0);
    std::set<label_type> unique(m.labels.begin(), m.labels.end());
    std::map<label_type, double> ret;
    for (const auto& l: unique) {
        ret[l] = ((labels == l).cast<double>().array() * abund.array()).sum();
    }
    return ret;
}


std::ostream& operator << (std::ostream& os, const std::map<label_type, double>& ab)
{
    os << "abundances:";
    for (const auto& kv: ab) {
        os << ' ' << kv.first << ": " << std::left << std::setw(12) << SPELL_STRING((kv.second * 100.) << '%');
    }
    return os;
}


bool operator == (const std::map<label_type, double>& ab1, const std::map<label_type, double>& ab2)
{
    bool ret = true;
    for (const auto& kv: ab1) {
        ret &= abs(kv.second - ab2.find(kv.first)->second) < FLOAT_TOL;
    }
    return ret;
}


void save_matrix(const MatrixXd& m, const std::string& filename)
{
    ofile f(filename);
    int tmpi;
    tmpi = m.rows(); f.write((const char*)&tmpi, sizeof(tmpi));
    tmpi = m.cols(); f.write((const char*)&tmpi, sizeof(tmpi));
    f.write((const char*)m.data(), m.size() * sizeof(double));
}


MatrixXd load_matrix(const std::string& filename)
{
    MatrixXd ret;
    int rows, cols;
    ifile f(filename);
    f.read((char*)&rows, sizeof(rows));
    f.read((char*)&cols, sizeof(cols));
    ret.resize(rows, cols);
    f.read((char*)ret.data(), ret.size() * sizeof(double));
    return ret;
}


MatrixXd test_geno_matrix(const std::string& name, geno_matrix& GEN, const std::string& haplo)
{
    bool do_output = name != "";
    Eigen::IOFormat r_table(Eigen::StreamPrecision, Eigen::DontAlignCols, "\t", "\n", "", "", "", "");
    /*VectorXd A(4);*/
    /*VectorXd H(4);*/
    /*VectorXd B(4);*/
    /*A << 1, 0, 0, 0;*/
    /*H << 0, 1, 1, 0;*/
    /*B << 0, 0, 0, 1;*/
    ofile ofs(SPELL_STRING("values-" << haplo << '.' << name << ".txt"));
    MSG_DEBUG("TEST_GENO_MATRIX " << name);

    /*MatrixXd abundance = GEN.lim_inf().col(0);*/
    MatrixXd abundance = GEN.stat_dist;
    /*MSG_DEBUG("### " << name);*/
    /*MSG_DEBUG(GEN);*/
    /*MSG_DEBUG("" << abundances(GEN));*/

    Eigen::Map<Eigen::Array<label_type, Eigen::Dynamic, 1>> labels(GEN.labels.data(), GEN.labels.size(), 1);
    VectorXd A = (labels == label_type{'a', 'a'}).cast<double>();
    VectorXd B;
    VectorXd H = ((labels == label_type{'a', 'b'}) + (labels == label_type{'b', 'a'})).cast<double>();
    if ((labels == label_type('b', 'b')).any()) {
        B = (labels == label_type{'b', 'b'}).cast<double>();
    } else {
        B = A;
    }
    VectorXd C;
    if ((labels == label_type('c', 'c')).any()) {
        C = (labels == label_type{'c', 'c'}).cast<double>();
    } else {
        C = A + H;
    }
    VectorXd D;
    if ((labels == label_type('d', 'd')).any()) {
        D = (labels == label_type{'d', 'd'}).cast<double>();
    } else {
        D = B + H;
    }
    A = (A.array() * abundance.array()).matrix();
    H = (H.array() * abundance.array()).matrix();
    B = (B.array() * abundance.array()).matrix();
    C = (C.array() * abundance.array()).matrix();
    D = (D.array() * abundance.array()).matrix();
    A /= A.sum();
    B /= B.sum();
    C /= C.sum();
    D /= D.sum();
    H /= H.sum();
    /*MSG_DEBUG("A " << A.transpose());*/
    /*MSG_DEBUG("B " << B.transpose());*/
    /*MSG_DEBUG("H " << H.transpose());*/
    /*A << 1,1,1, 0,0, 0,0,0;*/
    /*H << 0,0,0, 1,1, 0,0,0;*/
    /*B << 0,0,0, 0,0, 1,1,1;*/
    /*VectorXd A(2);*/
    /*VectorXd H(2);*/
    /*A << 1, 0;*/
    /*H << 0, 1;*/
    VectorXd U = abundance;

    if (haplo.size() != 3) {
        MSG_ERROR("THERE HAS TO BE 3 MARKERS, NO MORE, NO LESS. 3 IS THE NUMBER YOU SHALL COUNT TO. YOU SHALL NOT COUNT TO 2, EXCEPT ON THE WAY TO 3.", "");
        return {};
    }

    std::map<char, VectorXd> state_vectors = {
        {'A', A},
        {'B', B},
        {'H', H},
        {'C', C},
        {'D', D},
        {'U', U}
    };

/*#define GET_MRK(_n) (haplo[_n] == 'A' ? A : haplo[_n] == 'B' ? B : haplo[_n] == 'H' ? H : U)*/
#define GET_MRK(_n) state_vectors[haplo[_n]]
#define N_STEPS 25

    VectorXd M1 = GET_MRK(0);
    double d1 = log(1. - .8) * -50.; /* #8 */
    VectorXd M2 = GET_MRK(1);
    double d2 = log(1. - .1) * -50.; /* #1 */
    MSG_DEBUG("d1=" << d1 << "cM");
    MSG_DEBUG("d2=" << d2 << "cM");
    VectorXd M3 = GET_MRK(2);
    double step = d1 / N_STEPS;
    MSG_DEBUG("M1 " << M1.transpose());
    MSG_DEBUG("M2 " << M2.transpose());
    MSG_DEBUG("M3 " << M3.transpose());

    MatrixXd result(GEN.rows(), N_STEPS + 1);
    /*MatrixXd result(GEN.rows(), 3);*/
    int i = 0;
    {
        std::stringstream labelstr;
        labelstr << GEN.labels[0].first << GEN.labels[0].second;
        for (size_t l = 1; l < GEN.labels.size(); ++l) {
            labelstr << '\t' << GEN.labels[l].first << GEN.labels[l].second;
        }
        if (do_output) { ofs << labelstr.str() << std::endl; }
    }
    /* map: A - 30 - B - 5 - H */
    MatrixXd reverse = abundance.array().inverse().matrix().asDiagonal();
    VectorXd outer = (M2.array() * ((reverse * GEN.exp(d2 * .01)).transpose() * M3).array()).matrix();
    outer /= outer.sum();
    for (double d = 0; d <= d1; d += step) {
        MatrixXd prob = ((GEN.exp(d * .01) * M1).array() * ((reverse * GEN.exp((d1 - d) * .01)).transpose() * outer).array()).matrix();
        /*MatrixXd prob = GEN.exp(d * .01) * A;*/
        /*MSG_DEBUG("##DUMP TR(" << d << ")");*/
        /*MSG_DEBUG(GEN.exp(d * .01));*/
        result.col(i++) = prob / prob.sum();
    }

    /*MSG_DEBUG(result.transpose().format(r_table));*/
    if (do_output) { ofs << result.transpose().format(r_table); }
    if (A == B) {
        MatrixXd redux(2, A.size());
        redux.row(0) = (labels == label_type{'a', 'a'}).cast<double>().transpose();
        redux.row(1) = ((labels == label_type{'a', 'b'}) + (labels == label_type{'b', 'a'})).cast<double>().transpose();
        result = redux * result;
    } else {
        MatrixXd redux(3, A.size());
        redux.row(0) = (labels == label_type{'a', 'a'}).cast<double>().transpose();
        redux.row(1) = ((labels == label_type{'a', 'b'}) + (labels == label_type{'b', 'a'})).cast<double>().transpose();
        redux.row(2) = (labels == label_type{'b', 'b'}).cast<double>().transpose();
        result = redux * result;
    }
    return result;
}


MatrixXd read_table(const std::string& filename)
{
    ifile ifs(filename);
    auto get_row = [&] () {
        std::string line;
        std::getline(ifs, line);
        std::stringstream ss(line);
        std::vector<double> ret;
        std::string rowname;
        ss >> rowname;
        while (!ss.eof()) {
            ret.emplace_back();
            ss >> ret.back();
        }
        return ret;
    };

    std::string header;
    std::getline(ifs, header);

    std::vector<std::vector<double>> tmp;

    while (!ifs.eof()) {
        tmp.emplace_back(get_row());
    }

    MatrixXd ret(tmp.size(), tmp[0].size());
    for (size_t i = 0; i < tmp.size(); ++i) {
        for (size_t j = 0; j < tmp[i].size(); ++j) {
            ret(i, j) = tmp[i][j];
        }
    }
    ret /= ret.row(0).sum();
    return ret;
}



bool operator < (const std::vector<double>& v1, const std::vector<double>& v2)
{
    if (v1.size() != v2.size()) {
        return v1.size() < v2.size();
    }
    auto i1 = v1.begin();
    auto i2 = v2.begin();
    auto j1 = v1.end();
    while (i1 != j1 && eq_dbl(*i1, *i2)) { ++i1; ++i2; }
    return i1 != j1 && *i1 < *i2;
}



struct labelled_component {
    label_type label;
    std::vector<int> value;
    labelled_component(const label_type& l, const std::vector<int>& v) : label(l), value(v) {}
    bool operator < (const labelled_component& lc) const { return label < lc.label || (label == lc.label && value < lc.value); }
    bool operator == (const labelled_component& lc) const { return label == lc.label && value == lc.value; }
};


struct label_switch_pair {
    label_switch s1;
    label_switch s2;

    label_switch_pair(const label_switch& a, const label_switch& b)
        : s1(0, 0), s2(0, 0)
    {
        s1 = a;
        s2 = b;
        if (s1.is_identity() || s1 == s2) {
            s1 = s2;
            s2 = {0, 0};
        }
        /*if (s1.is_identity()) {*/
            /*throw 0;*/
        /*}*/
        /*if (s1.first == s2.second || s1.second == s2.first || s1.first == s2.first || s1.second == s2.second) {*/
        if (s1.first == s2.first && (s1.second != s2.second)) {
            throw 1;
        }
    }

    char perform_switch(char c) const
    {
        return c == s1.first
               ? s1.second
               : c == s1.second
                 ? s1.first
                 : c == s2.first
                   ? s2.second
                   : c == s2.second
                     ? s2.first
                     : c;
    }

    label_type operator () (const label_type& l) const
    {
        return {perform_switch(l.first), perform_switch(l.second)};
    }

    bool operator < (const label_switch_pair& other) const
    {
        return s1 < other.s1 || (s1 == other.s1 && s2 < other.s2);
    }
};


struct vector_switch_descriptor {
    std::multiset<labelled_component> components;
    std::map<labelled_component, std::vector<int>> state_map;

    vector_switch_descriptor(const std::vector<label_type>& labels, const std::vector<MatrixXi>& eigen_components, int i, bool by_row=false)
        : components()
    {
        if (by_row) {
            int size = eigen_components.front().rows();
            for (int j = 0; j < size; ++j) {
                std::vector<int> cv;
                cv.reserve(eigen_components.size());
                for (const auto& c: eigen_components) { cv.emplace_back(c(i, j)); }
                auto it = components.emplace(labels[j], cv);
                state_map[*it].push_back(j);
            }
        } else {
            int size = eigen_components.front().cols();
            for (int j = 0; j < size; ++j) {
                std::vector<int> cv;
                cv.reserve(eigen_components.size());
                for (const auto& c: eigen_components) { cv.emplace_back(c(j, i)); }
                auto it = components.emplace(labels[j], cv);
                state_map[*it].push_back(j);
            }
        }
    }

    vector_switch_descriptor(const std::vector<label_type>& labels, const std::vector<MatrixXi>& eigen_components, int i, const label_switch_pair& lsp, bool by_row=false)
        : components()
    {
        if (by_row) {
            int size = eigen_components.front().rows();
            for (int j = 0; j < size; ++j) {
                std::vector<int> cv;
                cv.reserve(eigen_components.size());
                for (const auto& c: eigen_components) { cv.emplace_back(c(i, j)); }
                auto it = components.emplace(lsp(labels[j]), cv);
                state_map[*it].push_back(j);
            }
        } else {
            int size = eigen_components.front().cols();
            for (int j = 0; j < size; ++j) {
                std::vector<int> cv;
                cv.reserve(eigen_components.size());
                for (const auto& c: eigen_components) { cv.emplace_back(c(j, i)); }
                auto it = components.emplace(lsp(labels[j]), cv);
                state_map[*it].push_back(j);
            }
        }
    }

    double
        operator - (const vector_switch_descriptor& other) const
        {
            /*return components == other.components;*/
            auto i = components.begin();
            auto oi = other.components.begin();
            auto j = components.end();
            double accum = 0;
            /*auto oj = other.components.end();*/
            for (; i != j; ++i, ++oi) {
                if (i->label == oi->label) {
                    for (size_t k = 0; k < i->value.size(); ++k) {
                        accum += fabs(i->value[k] - oi->value[k]);
                    }
                } else {
                    accum = std::numeric_limits<double>::infinity();
                }
            }
            return accum;
        }

    bool
        operator == (const vector_switch_descriptor& other) const
        {
            /*return (*this - other) < FLOAT_TOL;*/
            return components == other.components;
#if 0
            auto i = components.begin();
            auto oi = other.components.begin();
            auto j = components.end();
            /*auto oj = other.components.end();*/
            for (; i != j; ++i, ++oi) {
                if (i->label != oi->label || i->value.size() != oi->value.size()) {
                    return false;
                }
                for (size_t n = 0; n < i->value.size(); ++n) {
                    if (neq_dbl(i->value[n], oi->value[n])) {
                        return false;
                    }
                }
            }
            return true;
#endif
        }

    friend
        std::ostream& operator << (std::ostream& os, const vector_switch_descriptor& vsd)
        {
            os << '{';
            for (const auto& lc: vsd.components) {
                os << ' ' << lc.label << ':' << lc.value;
            }
            return os << " }";
        }
};


std::ostream& operator << (std::ostream& os, const labelled_component& lc)
{
    return os << lc.label;
}

MatrixXb combine(const vector_switch_descriptor& v1, const vector_switch_descriptor& v2)
{
    MatrixXb group = MatrixXb::Zero(v1.components.size(), v1.components.size());
    auto i1 = v1.state_map.begin();
    auto i2 = v2.state_map.begin();
    auto j1 = v1.state_map.end();
    auto j2 = v2.state_map.end();
    MSG_DEBUG_INDENT_EXPR("[combine] ");
    for (; i1 != j1 && i2 != j2; ++i1, ++i2) {
        /*MSG_DEBUG("i1: " << i1->first.label << ":[" << i1->first.value << "] / " << i1->second);*/
        /*MSG_DEBUG("i2: " << i2->first.label << ":[" << i2->first.value << "] / " << i2->second);*/
        /*MSG_QUEUE_FLUSH();*/
        /*if (i1->second.size() > 1) {*/
            /*MSG_DEBUG("skip ambiguity");*/
            /*continue;*/
        /*}*/
        if (!(i1->first == i2->first)) {
            MSG_DEBUG("v1.state_map.size=" << v1.state_map.size() << " v2.state_map.size=" << v2.state_map.size());
            MSG_DEBUG("DISCREPANCY");
            std::stringstream s1, s2;
            s1 << i1->first.label << ":[";
            s2 << i2->first.label << ":[";
            int skip = 0, skip_n = 0;
            const auto& v1 = i1->first.value;
            const auto& v2 = i2->first.value;
            for (size_t n = 0; n < i1->first.value.size(); ++n) {
                if (fabs(v1[n] - v2[n]) > FLOAT_TOL) {
                    if (skip) {
                        if (skip_n) {
                            s1 << ' ';
                            s2 << ' ';
                        }
                        s1 << ".." << skip << "..";
                        s2 << ".." << skip << "..";
                        skip = 0;
                    }
                    if (n) {
                        s1 << ' ';
                        s2 << ' ';
                    }
                    std::stringstream sv1, sv2;
                    sv1 << v1[n];
                    sv2 << v2[n];
                    size_t w = sv1.str().size() > sv2.str().size() ? sv1.str().size() : sv2.str().size();
                    s1 << std::setw(w) << sv1.str();
                    s2 << std::setw(w) << sv2.str();
                } else {
                    if (!skip) {
                        skip_n = n;
                    }
                    ++skip;
                }
            }
            if (skip) {
                if (skip_n) {
                    s1 << ' ';
                    s2 << ' ';
                }
                s1 << ".." << skip << "..";
                s2 << ".." << skip << "..";
                skip = 0;
            }
            MSG_DEBUG(s1.str() << ']');
            MSG_DEBUG(s2.str() << ']');
            MSG_QUEUE_FLUSH();
            /*throw 0;*/
        }
        for (int s1: i1->second) {
            for (int s2: i2->second) {
                group(s2, s1) = true;
                group(s1, s2) = true;
            }
        }
    }
    if (i1 != j1 || i2 != j2) {
        MSG_DEBUG("V" << (i1 != j1 ? '2' : '1') << " too short!");
        std::vector<std::string> s1, s2;
        std::vector<size_t> sizes;
        for (const auto& kv: v1.state_map) {
            std::stringstream ss;
            ss << kv.first << ':' << kv.second;
            s1.emplace_back(ss.str());
        }
        for (const auto& kv: v2.state_map) {
            std::stringstream ss;
            ss << kv.first << ':' << kv.second;
            s2.emplace_back(ss.str());
        }
        while (s2.size() < s1.size()) { s2.emplace_back(); }
        while (s1.size() < s2.size()) { s1.emplace_back(); }
        for (size_t n = 0; n < s1.size(); ++n) {
            sizes.push_back(1 + (s1[n].size() > s2[n].size() ? s1[n].size() : s2[n].size()));
        }

        std::stringstream ss1, ss2;
        for (size_t n = 0; n < s1.size(); ++n) {
            ss1 << std::setw(sizes[n]) << s1[n];
            ss2 << std::setw(sizes[n]) << s2[n];
        }

        MSG_DEBUG("V1:" << ss1.str());
        MSG_DEBUG("V2:" << ss2.str());
        MSG_QUEUE_FLUSH();
        throw 0;
    }
    /*MSG_DEBUG(group);*/
    /*MSG_DEBUG("--");*/
    MSG_DEBUG_DEDENT;
    return group;
}


bool compute_next_permutation(std::map<subset, subset>& A)
{
    auto i = A.begin();
    auto j = A.end();
    while (i != j) {
        if (std::next_permutation(i->second.begin(), i->second.end())) {
            return true;
        }
        ++i;
    }
    return false;
}


MatrixXd make_permutation_matrix(int size, const std::map<subset, subset>& A)
{
    MatrixXd ret = MatrixXd::Zero(size, size);
    for (const auto& kv: A) {
        for (size_t i = 0; i < kv.first.size(); ++i) {
            ret(kv.first[i], kv.second[i]) = 1.;
            ret(kv.second[i], kv.first[i]) = 1.;
        }
    }
    return ret;
}



double
vector_angle(const VectorXd& v1, const VectorXd& v2)
{
    double angle;
    double n = v1.lpNorm<2>() * v2.lpNorm<2>();
    if (n != 0.) {
        double dot = (v1.array() * v2.array()).sum();
        /*MSG_DEBUG("(" << g1.col(i).transpose() << ") . (" << g2.col(i).transpose() << ") = " << dot << ", n = " << n);*/
        if (dot < -n) {
            dot = -n;
        } else if (dot > n) {
            dot = n;
        }
        angle = acos(dot / n) / M_PI;
        /*angle = -dot;*/
    } else {
        angle = 0.;
    }
    return angle;
}


MatrixXd compute_self_angles(const geno_matrix& M)
{
    MatrixXd sa(M.inf_mat.rows(), M.inf_mat.cols());
    for (int r = 0; r < M.inf_mat.rows(); ++r) {
        for (int c = 0; c < M.inf_mat.cols(); ++c) {
            sa(r, c) = vector_angle(M.inf_mat.col(r), M.inf_mat.col(c));
        }
    }
    return sa;
}


MatrixXd build_partial_permutation_matrix(int size, const subset& first, const subset& second)
{
    MatrixXd perm = MatrixXd::Identity(size, size);
    for (int n = 0; n < first.size(); ++n) {
        perm(first[n], first[n]) = 0;
        perm(second[n], second[n]) = 0;
        perm(first[n], second[n]) = 1;
        perm(second[n], first[n]) = 1;
    }
    return perm;
}


struct perm_iter {
    subset::const_iterator b, e, i;
    perm_iter(const subset& s) : b(s.begin()), e(s.end()), i(s.begin()) {}
    size_t size() const { return e - b; }
    bool next() { ++i; if (i == e) { i = b; return true; } return false; }
    int operator * () const { return *i; }
    void reset() { i = b; }
};


struct perm_vec {
    std::vector<perm_iter> elems;
    subset result;
    subset backup;
    subset visited;
    bool good;
    bool at_end;

    perm_vec(size_t N, const std::map<int, subset>& map)
        : elems(), result(map.size()), backup(map.size()), visited(N, 0), good(false), at_end(false)
    {
        elems.reserve(map.size());
        for (const auto& kv: map) {
            elems.emplace_back(kv.second);
        }
        fast_make_next_result();
    }

    perm_vec& operator ++ () { next(); fast_make_next_result(); return *this; }
    const subset& operator * () const { return result; }
    void reset()
    {
        /*MSG_DEBUG("reset");*/
        at_end = false;
        for (auto& e: elems) { e.reset(); }
        fast_make_next_result();
    }

    bool next()
    {
        /*MSG_DEBUG("next at_end=" << at_end << " good=" << good);*/
        if (at_end) {
            return false;
        }
        auto i = elems.rbegin();
        auto j = elems.rend();
        while (i != j && i->next()) { /*MSG_DEBUG("carry");*/ ++i; }
        at_end = i == j;
        /*MSG_DEBUG("at_end=" << at_end);*/
        return !at_end;
    }

    private:

    void commit(size_t& e)
    {
        result[e] = *elems[e];
        visited[result[e]] = true;
        ++e;
    }

    bool find_first_valid(size_t& e)
    {
#if 0
        int size = (int) elems[e].size();
        for (; size > 0; --size) {
            if (!visited[*elems[e]]) {
                return true;
            }
            elems[e].next();
        }
#else
        bool carry = false;
        for (; visited[*elems[e]] && !(carry = elems[e].next()););
        /*MSG_DEBUG("first valid as #" << e << " is " << (*elems[e]));*/
        return !carry;
#endif
    }

    bool rollback(size_t& e)
    {
        --e;
        visited[result[e]] = false;
        return elems[e].next();
    }

    bool fast_make_next_result()
    {
        if (at_end) {
            return good = false;
        }
        std::fill(visited.begin(), visited.end(), 0);
        /*MSG_DEBUG("result before " << result);*/
        size_t e = 0;
        while (true) {
            if (e == elems.size()) {
                /*MSG_DEBUG("result after  " << result);*/
                MSG_QUEUE_FLUSH();
                return good = true;
            }
            if (find_first_valid(e)) {
                commit(e);
            } else {
                bool carry = true;
                while (e > 0 && (carry = rollback(e)));
                if (carry) {
                    /*MSG_DEBUG("no result");*/
                    /*MSG_QUEUE_FLUSH();*/
                    return good = false;
                }
            }
        }
    }
};


void test_perm()
{
    subset s = {1, 2, 3, 4};
    std::map<int, subset> m = {{1, s}, {2, s}, {3, s}, {4, s}};
    perm_vec v(5, m);
    MSG_DEBUG("v.good=" << v.good);
    while (v.good) {
        MSG_DEBUG("" << (*v));
        ++v;
    }
}


struct by_angle_compare {
    bool operator () (double a, double b) const
    {
        if (b == 0) {
            return a < -FLOAT_TOL;
        }
        return a < b && fabs((b - a) / b) > FLOAT_TOL;
    }
};


typedef std::map<double, std::map<int, subset>, by_angle_compare> by_angle_type;

by_angle_type compute_by_angle(const geno_matrix& M, const subset& first, const subset& second)
{
    MSG_DEBUG("COMPUTE BY ANGLE " << first << " vs " << second);
    by_angle_type by_angle;
    for (int i: first) {
        for (int j: second) {
            double va = vector_angle(M.inf_mat.col(i), M.inf_mat.col(j));
            by_angle[va][i].push_back(j);
        }
    }
    auto oldprec = std::cout.precision();
    by_angle_type ret;
    for (const auto& am: by_angle) {
        MSG_DEBUG_INDENT_EXPR("[BY_ANGLE " << std::setprecision(17) << am.first << std::setprecision(oldprec) << "] ");
        for (const auto& kv: am.second) {
            MSG_DEBUG(kv.first << ": " << kv.second);
        }

        std::map<int, bool> visited_first, visited_second;
        for (int f: first) { visited_first[f] = false; }
        for (int f: second) { visited_second[f] = false; }
        for (const auto& kv: am.second) {
            visited_first[kv.first] = true;
            for (const auto& s: kv.second) {
                visited_second[s] = true;
            }
        }
        bool complete = true;
        for (const auto& kv: visited_first) { if (!kv.second) { complete = false; break; } }
        for (const auto& kv: visited_second) { if (!kv.second) { complete = false; break; } }
        if (!complete) {
            MSG_DEBUG("INCOMPLETE!");
        } else {
            ret.insert(am);
        }

        MSG_DEBUG_DEDENT;
        MSG_DEBUG("--");
        MSG_QUEUE_FLUSH();
    }
    return ret;
}


std::map<subset, subset> compute_ambi_map(const std::map<int, subset>& ambiguities)
{
    std::map<subset, subset> ambi_map, ambi_map_tmp;
    size_t count = 0;
    std::set<subset> uniq;
    for (const auto& kv: ambiguities) {
        ambi_map_tmp[kv.second].push_back(kv.first);
    }
    for (auto& kv: ambi_map_tmp) {
        subset tmp = kv.first;
        std::sort(tmp.begin(), tmp.end());
        std::sort(kv.second.begin(), kv.second.end());
        if (uniq.insert(tmp).second) {
            ambi_map[tmp] = kv.second;
            uniq.insert(kv.second);
        }
    }
    return ambi_map;
}

typedef std::pair<unsigned long, double> by_angle_sorted_key_type;

inline
bool operator < (const by_angle_sorted_key_type& k1, const by_angle_sorted_key_type& k2)
{
    return k1.first < k2.first || (k1.first == k2.first && k1.second < k2.second);
}


struct by_angle_sorted_type {
    typedef std::map<std::pair<unsigned long, double>, std::map<int, subset>> data_type;
    typedef data_type::iterator iterator;
    typedef data_type::const_iterator const_iterator;
    typedef data_type::value_type value_type;

    data_type data;

    by_angle_sorted_type(const by_angle_type& ba)
        : data()
    {
        std::map<double, unsigned long> sizes;
        unsigned long min_accum = (unsigned long) -1;
        for (const auto& kv: ba) {
            unsigned long accum = 1;
            for (const auto& is: kv.second) {
                accum *= is.second.size();
            }
            sizes[kv.first] = accum;
            if (accum < min_accum) {
                min_accum = accum;
            }
        }
        for (const auto& kv: ba) {
            unsigned long accum = sizes[kv.first];
            if (accum == min_accum) {
                MSG_DEBUG("keeping angle=" << kv.first << " with under " << accum << " permutations");
                data[{accum, -fabs(.5 - kv.first)}] = kv.second;
            }
        }
    }

    iterator begin() { return data.begin(); }
    iterator end() { return data.end(); }
    const_iterator begin() const { return data.begin(); }
    const_iterator end() const { return data.end(); }
};


struct by_angle_table_type {
    std::map<subset, subset> ambi_map;
    std::vector<by_angle_sorted_type> pieces;
    std::vector<perm_vec> permutable_pieces;
    size_t N;

    struct by_angle_iter {
        by_angle_sorted_type::const_iterator b, e, i;
        by_angle_iter(const by_angle_sorted_type& ba) : b(ba.begin()), e(ba.end()), i(ba.begin()) {}
        bool next() { ++i; if (i == e) { i = b; return true; } return false; }
        const by_angle_sorted_type::value_type& operator * () const { return *i; }
    };

    std::vector<by_angle_iter> iter;

    by_angle_table_type(const geno_matrix& M, const std::map<int, subset>& ambiguities)
        : ambi_map(compute_ambi_map(ambiguities))
        , pieces()
        , permutable_pieces()
        , N(M.cols())
        , iter()
    {
        pieces.reserve(ambi_map.size());
        iter.reserve(ambi_map.size());
        for (const auto& kv: ambi_map) {
            pieces.emplace_back(compute_by_angle(M, kv.first, kv.second));
            iter.emplace_back(pieces.back());
        }
        init_perm_vec();
    }

    void init_perm_vec()
    {
        permutable_pieces.clear();
        permutable_pieces.reserve(N);
        for (const auto& i: iter) {
            permutable_pieces.emplace_back(N, (*i).second);
        }
    }

    bool next_piece()
    {
        auto i = iter.rbegin();
        auto j = iter.rend();
        while (i != j && i->next()) { ++i; }
        return i == j;
    }

    bool next_permut_in_current_pieces()
    {
        auto i = permutable_pieces.rbegin();
        auto j = permutable_pieces.rend();
        int count;
#if 0
        while (i != j && !(++*i).good) { i->reset(); ++i; }
#else
        while (i != j) {
            /*MSG_DEBUG("  next(" << (count++) << ')');*/
            ++*i;
            if (!i->good) {
                i->reset();
                ++i;
            } else {
                break;
            }
        }
#endif
        return i == j;
    }

    bool next()
    {
        /*MSG_DEBUG("next_permut_in_current_pieces");*/
        if (next_permut_in_current_pieces()) {
            /*MSG_DEBUG("next_piece");*/
            if (next_piece()) {
                /*MSG_DEBUG("carry");*/
                return true;
            } else {
                /*MSG_DEBUG("reinit perm vec");*/
                init_perm_vec();
                return false;
            }
        }
        return false;
    }

    MatrixXd operator * () const
    {
        auto i = permutable_pieces.begin();
        MatrixXd ret = MatrixXd::Zero(N, N);
        for (const auto& kv: ambi_map) {
            const subset& first = kv.first;
            const subset& second = **i;
            /*MSG_DEBUG("building block " << first << " <-> " << second);*/
            for (size_t n = 0; n < first.size(); ++n) {
                ret(first[n], second[n]) = ret(second[n], first[n]) = 1.;
            }
            ++i;
        }
        return ret;
    }

    void setup(symmetry_table_type& ret)
    {
        auto i = permutable_pieces.begin();
        for (const auto& kv: ambi_map) {
            const subset& first = kv.first;
            const subset& second = **i;
            for (size_t n = 0; n < first.size(); ++n) {
                ret[first[n]] = second[n];
                ret[second[n]] = first[n];
            }
            ++i;
        }
    }
};


size_t find_good_permutation_for_block(const geno_matrix& M, const subset& first, subset& second, symmetry_table_type& ret)
{
    size_t count = 0;
#if 0
    do {
        ++count;
        MatrixXd perm = build_partial_permutation_matrix(M.cols(), first, second);
        if ((perm * M.inf_mat * perm - M.inf_mat).isZero(FLOAT_TOL)) {
            for (int n = 0; n < first.size(); ++n) {
                ret[first[n]] = second[n];
                ret[second[n]] = first[n];
            }
        }
        return count;
    } while (std::next_permutation(second.begin(), second.end()));
#else
    by_angle_type by_angle = compute_by_angle(M, first, second);

    for (const auto& am: by_angle) {
        ++count;
        double angle = am.first;
        /*MSG_DEBUG("PERMUTATION FOR GROUP (angle=" << angle <<") size=" << am.second.begin()->second.size());*/
        perm_vec v(M.cols(), am.second);
        /*MSG_DEBUG_INDENT_EXPR("[PERMUTATIONS angle=" << angle << "] ");*/
        while (v.good) {
            const subset& s = *v;
            MatrixXd perm = build_partial_permutation_matrix(M.cols(), first, s);
            /*MSG_DEBUG(perm);*/
            /*MSG_DEBUG("RESULT:");*/
            /*MSG_DEBUG((perm * M.inf_mat * perm - M.inf_mat));*/
            /*MSG_DEBUG("=================================================================");*/
            if ((perm * M.inf_mat * perm - M.inf_mat).isZero(FLOAT_TOL)) {
                /*MSG_DEBUG("Valid block permutation found! count=" << count);*/
                for (int n = 0; n < first.size(); ++n) {
                    ret[first[n]] = s[n];
                    ret[s[n]] = first[n];
                }
                return count;
            }
            ++v;
        }
        /*MSG_DEBUG_DEDENT;*/
    }
#endif
    return count;
}


symmetry_table_type find_good_permutation(const geno_matrix& M, const std::map<int, subset>& ambiguities)
{
#if 0
    symmetry_table_type ret;
    std::map<subset, subset> ambi_map, ambi_map_tmp;
    size_t count = 0;
    for (const auto& kv: ambiguities) {
        ambi_map_tmp[kv.second].push_back(kv.first);
    }
    for (auto& kv: ambi_map_tmp) {
        subset tmp = kv.first;
        std::sort(tmp.begin(), tmp.end());
        std::sort(kv.second.begin(), kv.second.end());
        ambi_map[tmp] = kv.second;
    }
    auto i = ambi_map.begin();
    auto j = ambi_map.end();
    while (i != j) {
        /*if (std::next_permutation(i->second.begin(), i->second.end())) {*/
        /*return true;*/
        /*}*/
        count += find_good_permutation_for_block(M, i->first, i->second, ret);
        ++i;
    }
    MatrixXd perm = make_permutation_matrix(M.cols(), ambi_map);
    if (!(perm * M.inf_mat * perm - M.inf_mat).isZero(FLOAT_TOL)) {
        MSG_QUEUE_FLUSH();
        MSG_ERROR("BAD PERMUTATION!" << std::endl << perm, "");
        MSG_QUEUE_FLUSH();
    }
#endif
    by_angle_table_type bat(M, ambiguities);
    symmetry_table_type ret;
    size_t count = 0;
    do {
        ++count;
        MatrixXd perm = *bat;
        /*MSG_DEBUG("TESTING PERMUTATION");*/
        /*MSG_DEBUG(perm);*/
        /*MSG_QUEUE_FLUSH();*/
        if ((perm * M.inf_mat * perm - M.inf_mat).isZero(FLOAT_TOL)) {
            MSG_DEBUG_INDENT_EXPR("[DUMP PERM] ");
            MSG_DEBUG("Valid permutation found! count=" << count);
            MSG_DEBUG(perm);
            MSG_DEBUG_DEDENT;
            bat.setup(ret);
            break;
        }
    } while (!bat.next());

    MSG_DEBUG("Valid permutation found after " << count << " steps.");
    return ret;
}


symmetry_list_type
analyse_symmetries(const geno_matrix& M)
{
    MSG_DEBUG("=====================================================================================");
    MSG_DEBUG("ANALYSING SYMMETRIES FOR GENERATION:");
    MSG_DEBUG(M);
    MSG_DEBUG("-------------------------------------------------------------------------------------");
    symmetry_list_type ret;
    label_switch nil(0, 0);
    std::set<std::pair<label_switch, label_switch>> uniq;

    MatrixXb groups = MatrixXb::Zero(M.rows(), M.cols());

    MatrixXd inf_mat = classify_values(M.inf_mat, FLOAT_TOL).cast<double>();

    uniq.emplace(nil, nil);

    auto components = M.eigen_components();
    MSG_DEBUG("WE HAVE " << components.size() << " EIGEN COMPONENTS");
    /*std::vector<MatrixXi> components;*/
    /*components.push_back(classify_values(M.inf_mat, FLOAT_TOL));*/

    for (size_t i = 0; i < M.cols(); ++i) {
        vector_switch_descriptor vi(M.labels, components, i);
        /*vector_switch_descriptor vir(M, i, true);*/
        for (size_t j = i + 1; j < M.cols(); ++j) {
            auto l1 = M.labels[i];
            auto l2 = M.labels[j];
            label_switch s1(l1.first, l2.first);
            label_switch s2(l1.second, l2.second);

            if (s1.compatible(s2)) {
                label_switch_pair lsp(s1, s2);
                vector_switch_descriptor vj(M.labels, components, j, lsp);
                /*vector_switch_descriptor vjr(M, j, lsp, true);*/
                /*MSG_DEBUG("" << s1 << s2);*/
                /*MSG_DEBUG(i << ' ' << vi << std::endl << " vs" << std::endl << j << ' ' << vj << std::endl << " : " << (vi - vj));*/
                /*if (vi == vj && vir == vjr) {*/
                if (vi == vj) {
                    /*MSG_DEBUG(i << ',' << j << ": " << M.labels[i] << ' ' << M.labels[j] << ' ' << s1 << s2);*/
                    /*MSG_DEBUG("EQUAL! " << i << ',' << j << ": " << M.labels[i] << ' ' << M.labels[j] << ' ' << s1 << s2);*/
                    label_switch u1 = s1.is_identity() ? nil : s1;
                    label_switch u2 = (s1 == s2 || s2.is_identity()) ? nil : s2;
                    uniq.emplace(u1 < u2 ? u1 : u2, u1 < u2 ? u2 : u1);
                }
            } else {
                /*MSG_DEBUG("<<skipping incompatible vector pair (" << M.labels[i] << ", " << M.labels[j] << ")");*/
            }
        }
    }

    /*MSG_DEBUG("UNIQ:");*/
    for (const auto& p: uniq) {
        if (p.first == p.second) {
            if (!p.first.is_identity()) {
                MSG_DEBUG("- " << p.first);
            }
        } else if (p.first.is_identity()) {
            if (!p.second.is_identity()) {
                MSG_DEBUG("- " << p.second);
            }
        } else if (p.second.is_identity()) {
            if (!p.first.is_identity()) {
                MSG_DEBUG("- " << p.first);
            }
        } else {
            MSG_DEBUG("- " << p.first << ' ' << p.second);
        }
        label_switch_pair lsp(p.first, p.second);
        MatrixXb group = MatrixXb::Zero(M.cols(), M.cols());
        std::map<int, subset> ambiguities;
        bool have_ambiguities = false;
        std::vector<MatrixXb> total;
        for (int i = 0; i < M.cols(); ++i) {
            vector_switch_descriptor vi(M.labels, components, i);
            for (int j = i; j < M.cols(); ++j) {
                vector_switch_descriptor vj(M.labels, components, j, lsp);
                group(j, i) = vi == vj;
                group(i, j) = group(j, i);
                if (vi == vj) {
                    MatrixXb comb = combine(vi, vj);
                    bool accepted = false;
                    auto ti = total.begin();
                    auto tj = total.end();
                    for (; ti != tj; ++ti) {
                        MatrixXb temp = (ti->array() * comb.array()).matrix();
                        if (temp.colwise().sum().all() && temp.rowwise().sum().all()) {
                            *ti = temp;
                            accepted = true;
                            /*break;*/
                        }
                    }
                    /*if (ti == tj) {*/
                    if (!accepted) {
                        total.emplace_back(comb);
                    }
                    /*MSG_DEBUG_INDENT_EXPR("[TOTAL] ");*/
                    /*for (const auto& t: total) {*/
                        /*MSG_DEBUG(t);*/
                        /*MSG_DEBUG("--");*/
                    /*}*/
                    /*MSG_DEBUG_DEDENT;*/
                    /*if (ret.back()[i] == -1 && ret.back()[j] == -1) {*/
                        /*ret.back()[i] = j;*/
                        /*ret.back()[j] = i;*/
                    /*}*/
                    /*ambiguities[i].push_back(j);*/
                }
            }
        }
        for (const auto& t: total) {
            if ((t * t) == MatrixXb::Identity(M.cols(), M.cols())) {
                MatrixXd temp = t.cast<double>();
                if ((temp * inf_mat * temp - inf_mat).isZero(FLOAT_TOL)) {
                    group = t;
                    break;
                }
            }
        }
        if (group * MatrixXb::Ones(group.rows(), 1) != MatrixXb::Ones(group.rows(), 1)) {
            MSG_DEBUG("Incomplete permutation matrix!");
            continue;
        }
        if (MatrixXb::Ones(1, group.cols()) * group != MatrixXb::Ones(1, group.cols())) {
            MSG_DEBUG("Incomplete permutation matrix!");
            continue;
        }
        /*MSG_DEBUG(group);*/
        /*MSG_DEBUG("---------------------------");*/
        /*MSG_DEBUG((group * group.transpose()));*/
        std::vector<bool> visited(group.cols(), false);
        ret.emplace_back();
        for (int i = 0; i < M.cols(); ++i) {
            ret.back()[i] = -1;
        }
        for (int i = 0; i < group.cols(); ++i) {
            if (visited[i]) {
                continue;
            }
            if (group.col(i).cast<int>().sum() > 1) {
                have_ambiguities = true;
            }
            for (int j = 0; j < group.rows(); ++j) {
                if (group(j, i)) {
                    /*if (ret.back()[i] == -1) {*/
                        ret.back()[i] = j;
                    /*}*/
                    ambiguities[i].push_back(j);
                    /*MSG_DEBUG("ANGLE(" << i << ", " << j << ") = " << vector_angle(M.inf_mat.col(i), M.inf_mat.col(j)));*/
                }
            }
        }
        if (have_ambiguities) {
            for (const auto& kv: find_good_permutation(M, ambiguities)) {
                ret.back()[kv.first] = kv.second;
            }
        }
        MSG_QUEUE_FLUSH();
    }
    return ret;
}



geno_matrix overlump(const geno_matrix& M, size_t n_states)
{
    experimental_lumper l(M);
    return l.do_lump(n_states);
}



int main(int argc, char** argv)
{
    /*test_perm();*/
    /*return 0;*/
    auto A = ancestor_matrix('a');
    auto B = ancestor_matrix('b');

    /*MSG_DEBUG("#############################################################################################");*/
    /*MSG_DEBUG("#######################                  F1                  ################################");*/
    /*MSG_DEBUG("#############################################################################################");*/
    auto F1 = lump(kronecker(kronecker(A, gamete), kronecker(B, gamete)));

    if (0)
    {
        auto BC = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
        auto BC2 = lump(kronecker(kronecker(BC, gamete), kronecker(A, gamete)));
        VectorXd ab = BC2.lim_inf().col(0);
        MSG_DEBUG("BC2 . ab = " << (BC2.inf_mat * ab).transpose());
        MSG_DEBUG("Ker(BC2) =" << std::endl << BC2.inf_mat.fullPivLu().kernel());
        VectorXd s = BC2.inf_mat.fullPivLu().kernel();

        test_geno_matrix("BC2", BC2, "AAH");
        test_geno_matrix("BC2", BC2, "AAA");

        return 0;
    }

    if (0)
    {
        MSG_DEBUG("* * * SELFING * * *");
        auto F2 = lump(kronecker(F1, selfing_gamete));
        auto F3 = lump(kronecker(F2, selfing_gamete));
        auto F4 = lump(kronecker(F3, selfing_gamete));
        auto F5 = lump(kronecker(F4, selfing_gamete));
        auto F6 = lump(kronecker(F5, selfing_gamete));

        MSG_DEBUG("* * * BACKCROSSING * * *");
        auto BC = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
        auto BC2 = lump(kronecker(kronecker(BC, gamete), kronecker(A, gamete)));
        auto BC3 = lump(kronecker(kronecker(BC2, gamete), kronecker(A, gamete)));
        auto BC4 = lump(kronecker(kronecker(BC3, gamete), kronecker(A, gamete)));
        auto BC5 = lump(kronecker(kronecker(BC4, gamete), kronecker(A, gamete)));
        auto BC6 = lump(kronecker(kronecker(BC5, gamete), kronecker(A, gamete)));

        MSG_DEBUG("* * * RANDOM CROSSING * * *");
        auto C2 = lump(kronecker(kronecker(F1, gamete), kronecker(F1, gamete)));
        auto C3 = lump(kronecker(kronecker(C2, gamete), kronecker(C2, gamete)));
        auto C4 = lump(kronecker(kronecker(C3, gamete), kronecker(C3, gamete)));
     }

    if (0)
    {
        auto F2 = lump(kronecker(F1, selfing_gamete));
        auto F3 = kronecker(F2, selfing_gamete);
        auto F3l = lump(F3);
        /*MSG_DEBUG("STUDY LUMPING F3");*/
        experimental_lumper el(F3);
        auto g8 = el.do_lump(8);
        MSG_DEBUG("=== === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === === ===");
        experimental_lumper el2(F3l);
        auto g6 = el.do_lump(6);
        test_geno_matrix("g8_data.matrix", g8, "ABH");
        test_geno_matrix("g6_data.matrix", g6, "ABH");
    }

    if (0)
    {
        auto F2 = lump(kronecker(F1, selfing_gamete));
        auto F3 = lump(kronecker(F2, selfing_gamete));
        auto F4 = lump(kronecker(F3, selfing_gamete));
        auto F5 = lump(kronecker(F4, selfing_gamete));
        auto F6 = lump(kronecker(F5, selfing_gamete));
        MSG_DEBUG("STUDY LUMPING F6");
        experimental_lumper el(kronecker(F5, selfing_gamete));
        auto g6 = el.do_lump(32);
        /*auto g6 = el.do_lump(ORDER);*/
        test_geno_matrix("g64_data.matrix", F6, "ABH");
        test_geno_matrix("g32_data.matrix", g6, "ABH");
        /*for (int i = 0; i < 20; i += 2) {*/
            /*el.do_lump(i);*/
        /*}*/
        return 0;
    }

    if (1)
    {
        auto C = ancestor_matrix('c');
        auto D = ancestor_matrix('d');
        auto CD = lump(kronecker(kronecker(C, gamete), kronecker(D, gamete)));
        auto CP = lump(kronecker(kronecker(F1, gamete), kronecker(CD, gamete)));

        (void) experimental_lumper{A};
        (void) experimental_lumper{B};

        (void) experimental_lumper{kronecker(A, gamete)};
        (void) experimental_lumper{kronecker(B, gamete)};

        auto gF1 = kronecker(F1, gamete);
        (void) experimental_lumper{gF1};

        auto gCD = kronecker(CD, gamete);
        (void) experimental_lumper{gCD};

        auto CPnl = kronecker(gF1, gCD);
        (void) experimental_lumper{CPnl};


        return 0;
    }

    /* MAGIC 4 */
    if (0)
    {
        std::vector<size_t> nstates = {12, 18, 24, 30, 36, 42, 48, 54, 60};
        /*std::vector<size_t> nstates = {64, 96, 128};*/
        auto C = ancestor_matrix('c');
        auto D = ancestor_matrix('d');
        auto CD = lump(kronecker(kronecker(C, gamete), kronecker(D, gamete)));
        auto CP = lump(kronecker(kronecker(F1, gamete), kronecker(CD, gamete)));
        for (size_t NSTATES: nstates) {
            ofile min_angle_file(SPELL_STRING("min_angles_MAGIC4_" << NSTATES << ".txt"));
            min_angle_file << "gen\tsym\tmin\tmax\tavg" << std::endl;
            MSG_DEBUG("NSTATES=" << NSTATES);

            auto ref = CP;
            auto F = CP;

            for (int i = 2; i < 25; ++i) {
                MSG_DEBUG_INDENT_EXPR("[NSTATES=" << NSTATES << " GEN#" << i << "] ");

                experimental_lumper el(kronecker(F, selfing_gamete));
                ref = F;

                MSG_DEBUG_INDENT_EXPR("[INF_MAT CLASSES] ");
                MatrixXd prout = classify_values(el.M.inf_mat, FLOAT_TOL).cast<double>();
                prout.array() += prout.minCoeff();
                prout.array() /= prout.maxCoeff();
                MSG_DEBUG((' ' + ((126 - ' ') * prout.array()).cast<char>()));
                MSG_DEBUG_DEDENT;

                F = el.derisavi().do_lump(NSTATES);
                auto sym_maps = el.LUMPED_SYM;

                test_geno_matrix(SPELL_STRING("MAGIC4-" << NSTATES << "-" << i), F, "ABA");
                test_geno_matrix(SPELL_STRING("MAGIC4-" << NSTATES << "-" << i), F, "ABB");
                test_geno_matrix(SPELL_STRING("MAGIC4-" << NSTATES << "-" << i), F, "ABC");

                /*MSG_DEBUG("LUMPED SYMMETRIES");*/
                /*MSG_DEBUG("" << sym_maps);*/

                int s_num = 0;
                MatrixXd tmp = F.exp(.3);
                for (const auto& S: sym_maps) {
                    VectorXd angles(S.size());
                    int a = 0;
                    for (const auto& kv: S) {
                        angles(a) = vector_angle(tmp.col(kv.first), tmp.col(kv.second));
                        ++a;
                    }
                    MSG_DEBUG(std::setw(3) << i << " SYMMETRY ANGLES " << angles.transpose());
                    MSG_DEBUG("sum = " << angles.lpNorm<1>());
                    MSG_DEBUG("norm = " << angles.lpNorm<2>());
                    /*MSG_DEBUG("CRITERION" << i << "\t" << angles.minCoeff());*/
                    min_angle_file << i << '\t' << (++s_num) << '\t' << angles.minCoeff() << '\t' << angles.maxCoeff() << '\t' << (angles.sum() / angles.size()) << std::endl;
                }
                if (!sym_maps.size()) {
                    min_angle_file << i << "\tNA\tNA\tNA\tNA" << std::endl;
                }
                /*MSG_DEBUG_INDENT_EXPR("[FLAIR] ");*/
                /*MSG_DEBUG((F.p * F.diag.asDiagonal()));*/
                /*MSG_DEBUG_DEDENT;*/
#if 0
                MSG_DEBUG_INDENT_EXPR("[SELF ANGLES] ");
                for (const auto& S: sym_maps) {
                    std::stringstream sass;
                    int sym_num = 0;
                        sass.clear();
                        sass.str("");
                        MSG_DEBUG("=== SYM #" << (++sym_num));
                        for (int r = 0; r < F.inf_mat.rows(); ++r) {
                            for (int c = 0; c < F.inf_mat.cols(); ++c) {
                                bool in_sym = false;
                                if (S.find(r)->second == c) {
                                    in_sym = true;
                                }
                                if (in_sym) {
                                    sass << _GREEN;
                                }
                                sass << std::setw(11) << vector_angle(F.inf_mat.col(r), F.inf_mat.col(c));
                                if (in_sym) {
                                    sass << _NORMAL;
                                }
                                sass << (c < F.inf_mat.cols() - 1 ? "  " : "\n");
                            }
                        }
                        MSG_DEBUG(sass.str());
                        
                    MatrixXd tmp = F.exp(.1);
                    /*std::stringstream sass;*/
                        sass.clear();
                        sass.str("");
                        MSG_DEBUG("=== SYM #" << sym_num);
                        for (int r = 0; r < F.inf_mat.rows(); ++r) {
                            for (int c = 0; c < F.inf_mat.cols(); ++c) {
                                bool in_sym = false;
                                if (S.find(r)->second == c) {
                                    in_sym = true;
                                }
                                if (in_sym) {
                                    sass << _GREEN;
                                }
                                sass << std::setw(11) << vector_angle(tmp.col(r), tmp.col(c));
                                if (in_sym) {
                                    sass << _NORMAL;
                                }
                                sass << (c < F.inf_mat.cols() - 1 ? "  " : "\n");
                            }
                        }
                        MSG_DEBUG(sass.str());

                    tmp = F.exp(.3);
                    /*std::stringstream sass;*/
                        sass.clear();
                        sass.str("");
                        MSG_DEBUG("=== SYM #" << sym_num);
                        for (int r = 0; r < F.inf_mat.rows(); ++r) {
                            for (int c = 0; c < F.inf_mat.cols(); ++c) {
                                bool in_sym = false;
                                if (S.find(r)->second == c) {
                                    in_sym = true;
                                }
                                if (in_sym) {
                                    sass << _GREEN;
                                }
                                sass << std::setw(11) << vector_angle(tmp.col(r), tmp.col(c));
                                if (in_sym) {
                                    sass << _NORMAL;
                                }
                                sass << (c < F.inf_mat.cols() - 1 ? "  " : "\n");
                            }
                        }
                        MSG_DEBUG(sass.str());

                    tmp = F.exp(.5);
                    /*std::stringstream sass;*/
                        sass.clear();
                        sass.str("");
                        MSG_DEBUG("=== SYM #" << sym_num);
                        for (int r = 0; r < F.inf_mat.rows(); ++r) {
                            for (int c = 0; c < F.inf_mat.cols(); ++c) {
                                bool in_sym = false;
                                if (S.find(r)->second == c) {
                                    in_sym = true;
                                }
                                if (in_sym) {
                                    sass << _GREEN;
                                }
                                sass << std::setw(11) << vector_angle(tmp.col(r), tmp.col(c));
                                if (in_sym) {
                                    sass << _NORMAL;
                                }
                                sass << (c < F.inf_mat.cols() - 1 ? "  " : "\n");
                            }
                        }
                        MSG_DEBUG(sass.str());
                }
                if (0)
                {
                    std::stringstream sass;
                    for (int r = 0; r < F.inf_mat.rows(); ++r) {
                        for (int c = 0; c < F.inf_mat.cols(); ++c) {
                            bool in_sym = false;
                            for (const auto& S: sym_maps) {
                                if (S.find(r)->second == c) {
                                    in_sym = true;
                                }
                            }
                            if (in_sym) {
                                sass << _GREEN;
                            }
                            sass << std::setw(11) << vector_angle(F.inf_mat.col(r), F.inf_mat.col(c));
                            if (in_sym) {
                                sass << _NORMAL;
                            }
                            sass << (c < F.inf_mat.cols() - 1 ? "  " : "\n");
                        }
                    }
                    MSG_DEBUG(sass.str());
                }
                MSG_DEBUG_DEDENT;
#endif
                MSG_DEBUG_DEDENT;
            }
        }
    }

    if (0)
    {
        //std::vector<size_t> nstates = {/*4, 8, 12, 16, 24, 26, 28, 30, 32,*/ 36};
        //for (size_t NSTATES: nstates) {
        for (int s = 32; s <= 32; ++s) {
            size_t NSTATES = s << 1;
            ofile min_angle_file(SPELL_STRING("min_angles_" << NSTATES << ".txt"));
            min_angle_file << "gen\tmin\tmax\tavg" << std::endl;
            MSG_DEBUG("NSTATES=" << NSTATES);

            auto ref = F1;
            auto F = F1;
            MatrixXd prev = MatrixXd::Ones(4, 4);
            /*auto sym_maps = analyse_symmetries(F);*/

            MatrixXb selfing_symmetry = MatrixXb::Identity(4, 4);
            /*selfing_symmetry << 0,0,0,1,*/
                                /*0,0,1,0,*/
                                /*0,1,0,0,*/
                                /*1,0,0,0;*/

            for (int i = 2; i < 40; ++i) {
                /*ref = overlump(lump(kronecker(ref, selfing_gamete)), 4);*/
                /*F = overlump(lump(kronecker(F, selfing_gamete)), NSTATES);*/
                /*experimental_lumper el(lump(kronecker(F, selfing_gamete)));*/
                MSG_DEBUG_INDENT_EXPR("[NSTATES=" << NSTATES << " GEN#" << i << "] ");
                /*symmetry_list_type new_sym_maps;*/
                /*symmetry_list_type lumped_sym;*/
                /*for (const auto& S: sym_maps) {*/
                    /*new_sym_maps.emplace_back(make_symmetry<bool>(kroneckerProduct(make_symmetry_matrix<bool>(S), selfing_symmetry)));*/
                /*}*/
#if 1
                /*experimental_lumper el(lump(kronecker(F, selfing_gamete)));*/
                experimental_lumper el(kronecker(F, selfing_gamete));
                ref = F;
                F = el.do_lump(NSTATES);
                auto sym_maps = el.LUMPED_SYM;
                /*MSG_DEBUG_INDENT_EXPR("ANGLE WITH PREVIOUS GEN");*/
                /*auto ang = column_angles(F.inf_mat, ref.inf_mat);*/
                /*double a = ang.lpNorm<2>();*/
                /*MSG_DEBUG("ANGLES(G" << (i - 1) << ", G" << i << ") = " << a);*/
                /*MSG_DEBUG_DEDENT;*/
#else
                if (new_sym_maps.size()) {
                    /*experimental_lumper el(kronecker(F, selfing_gamete), new_sym_maps);*/
                    experimental_lumper el(kronecker(F, selfing_gamete));
                    F = el.do_lump(NSTATES);
                    sym_maps = el.LUMPED_SYM;
                } else {
                    experimental_lumper el(kronecker(F, selfing_gamete));
                    F = el.do_lump(NSTATES);
                    sym_maps = el.LUMPED_SYM;
                }
#endif
                /*auto oF = overlump(F, 4);*/

                /*auto Fl = overlump(F, 4);*/

                auto aba = test_geno_matrix(SPELL_STRING("RIL" << NSTATES << "-" << i), F, "ABB");
                auto abb = test_geno_matrix(SPELL_STRING("RIL" << NSTATES << "-" << i), F, "ABA");

                /*auto refa = test_geno_matrix(SPELL_STRING("ref-" << i), ref, "ABB");*/
                /*auto refb = test_geno_matrix(SPELL_STRING("ref-" << i), ref, "ABB");*/
                /*double d = (aba - refa).lpNorm<1>() + (abb - refb).lpNorm<1>();*/
                /*MSG_DEBUG("DIST TO REF = " << d);*/
                /*auto redux = make_redux_matrix(F).first;*/
                /*auto redux_inv = redux.transpose();*/
                /*for (int i = 0; i < redux_inv.cols(); ++i) {*/
                    /*redux_inv.col(i) /= redux_inv.col(i).sum();*/
                    /*redux_inv.col(i) /= F.stat_dist(i);*/
                /*}*/
                /*auto ang = column_angles(F.inf_mat * redux.transpose(), redux.transpose() * ref.inf_mat);*/
                /*auto ang = column_angles(F.inf_mat, redux.transpose() * ref.inf_mat * redux_inv.transpose());*/
                /*double a = ang.lpNorm<2>();*/
                /*MSG_DEBUG("ANGLES(G" << (i - 1) << ", G" << i << ") = " << a);*/
                /*double a = (F.inf_mat * redux.transpose() - redux.transpose() * ref.inf_mat).lpNorm<1>();*/

                /*double a = (ref.inf_mat - oF.inf_mat).lpNorm<1>();*/
                /*MSG_DEBUG("DELTA(G" << (i - 1) << ", G" << i << ") = " << a);*/

#if 1
                MatrixXd tmp = F.exp(.3);

                MatrixXd sa(tmp.rows(), tmp.cols());
                for (int r = 0; r < tmp.rows(); ++r) {
                    for (int c = 0; c < tmp.cols(); ++c) {
                        sa(r, c) = vector_angle(tmp.col(r), tmp.col(c));
                    }
                }
                /*MSG_DEBUG_INDENT_EXPR("[analyse_symmetries] ");*/
                /*auto sym_maps2 = analyse_symmetries(F);*/
                /*MSG_DEBUG_DEDENT;*/
                /*if (sym_maps != sym_maps2) {*/
                    /*MSG_DEBUG("/!\\ BUG COMPUTED SYMMETRIES");*/
                    /*MSG_DEBUG("" << sym_maps2);*/
                /*}*/
                MSG_DEBUG("LUMPED SYMMETRIES");
                MSG_DEBUG("" << sym_maps);
                /*if (!sym_maps.size()) {*/
                    /*sym_maps = sym_maps2;*/
                /*}*/
                for (const auto& S: sym_maps) {
                    VectorXd angles(S.size());
                    int a = 0;
                    for (const auto& kv: S) {
                        angles(a) = vector_angle(tmp.col(kv.first), tmp.col(kv.second));
                        ++a;
                    }
                    MSG_DEBUG(std::setw(3) << i << " SYMMETRY ANGLES " << angles.transpose());
                    MSG_DEBUG("sum = " << angles.lpNorm<1>());
                    MSG_DEBUG("norm = " << angles.lpNorm<2>());
                    /*MSG_DEBUG("CRITERION" << i << "\t" << angles.minCoeff());*/
                    min_angle_file << i << '\t' << angles.minCoeff() << '\t' << angles.maxCoeff() << '\t' << (angles.sum() / angles.size()) << std::endl;
                }
                if (!sym_maps.size()) {
                    min_angle_file << i << "\tNA\tNA\tNA" << std::endl;
                }
                /*MSG_DEBUG(sa);*/
                MSG_DEBUG_INDENT_EXPR("[SELF ANGLES] ");
                std::stringstream sass;
                for (int r = 0; r < sa.rows(); ++r) {
                    for (int c = 0; c < sa.cols(); ++c) {
                        bool in_sym = false;
                        for (const auto& S: sym_maps) {
                            if (S.find(r)->second == c) {
                                in_sym = true;
                            }
                        }
                        if (in_sym) {
                            sass << _GREEN;
                        }
                        sass << std::setw(11) << sa(r, c);
                        if (in_sym) {
                            sass << _NORMAL;
                        }
                        sass << (c < sa.cols() - 1 ? "  " : "\n");
                    }
                }
                MSG_DEBUG(sass.str());
                MSG_DEBUG_DEDENT;
#endif
                MSG_DEBUG_DEDENT;
            }
        }
    }

    if (0)
    {
        auto F = F1;
        auto Fl = F1;
        auto Fl2 = F1;
        /*for (int i = 2; i < 10; ++i) {*/
            /*F = lump(kronecker(F, selfing_gamete));*/
        /*}*/
        /*test_geno_matrix("pseudo-RIL", F, "ABB");*/
        /*test_geno_matrix("pseudo-RIL", F, "ABA");*/
#define NSTATES 4
        MatrixXd red = MatrixXd::Zero(NSTATES, 3);
        for (int i = 0; i < (NSTATES >> 1) -1; ++i) {
            red(i, 0) = red(NSTATES - 1 - i, 2) = 1.;
        }
        red(NSTATES >> 1, 1) = red((NSTATES >> 1) - 1, 1) = 1.;

#if 1
        double test_crit = std::numeric_limits<double>::infinity();
        int i = 2;
        for (; i < 40; ++i) {
            F = lump(kronecker(Fl, selfing_gamete));
            experimental_lumper el(F);
            auto inf_mat = Fl.inf_mat;
            auto bak = Fl;
            Fl = el.do_lump(NSTATES);
            /*Fl = el.do_lump(32);*/
            if (1 && inf_mat.rows() == Fl.inf_mat.rows()) {
                /*MSG_DEBUG("@@@ DELTA at #" << i);*/
                /*MSG_DEBUG((Fl.inf_mat - inf_mat));*/
                /*auto ang = min_column_angles(inf_mat, Fl.inf_mat);*/
                /*auto G1 = lump_using_partition_weighted(bak, experimental_lumper(bak).partition_on_labels());*/
                /*auto G2 = lump_using_partition_weighted(Fl, experimental_lumper(Fl).partition_on_labels());*/
                /*auto ang = min_column_angles(G1.inf_mat, G2.inf_mat);*/
                /*auto G1 = bak.inf_mat * bak.stat_dist.asDiagonal();*/
                /*auto G2 = Fl.inf_mat * Fl.stat_dist.asDiagonal();*/
                auto G1 = bak.inf_mat;
                auto G2 = Fl.inf_mat;
                auto ang = min_column_angles(G1, G2);

                double plogp = 0;
                for (int i = 0; i < Fl.stat_dist.size(); ++i) {
                    plogp -= Fl.stat_dist(i) * log(Fl.stat_dist(i));
                }
                /*MSG_DEBUG("sigma_p*log_p = " << plogp);*/

                /*ang.array() *= Fl.stat_dist.array();*/
                /*MSG_DEBUG("" << Fl.stat_dist.transpose());*/
                double a = ang.lpNorm<2>();
                MSG_DEBUG("ANGLES(G" << (i - 1) << ", G" << i << ") = " << a);

                int n_A = 0;
                for (const auto& l: Fl.labels) {
                    if (l == label_type{'a', 'a'}) {
                        ++n_A;
                    }
                }
                MSG_DEBUG("n_A=" << n_A);

                MatrixXd tmpA(Fl.inf_mat.rows(), n_A);
                int col = 0, i = 0;

                for (const auto& l: Fl.labels) {
                    if (l == label_type{'a', 'a'}) {
                        tmpA.col(col) = Fl.inf_mat.col(i) * Fl.stat_dist(i);
                        ++col;
                    }
                    ++i;
                }

                MatrixXd ata = tmpA.transpose() * tmpA;
                SelfAdjointEigenSolver<MatrixXd> saes(ata);

                /*MSG_DEBUG("SPECTRUM At.A");*/
                /*MSG_DEBUG(saes.eigenvalues().real().transpose());*/
                /*VectorXd ev = saes.eigenvalues().real().transpose();*/

                /*MSG_DEBUG("EV min=" << ev(0) << " max=" << ev(ev.size() - 1) << " cond=" << (ev(ev.size() - 1) / ev(0)));*/

                /*if (test_crit > a) {*/
                    /*test_crit = a;*/
                /*} else {*/
                    /*Fl = bak;*/
                    /*break;*/
                /*}*/
#if 0
                MSG_DEBUG("ANGLES(G" << (i - 1) << ", G" << i << ") = " << ang.transpose());
                auto lu = Fl.inf_mat.fullPivLu();
                MSG_DEBUG("RANK = " << lu.rank());
                lu.setThreshold(1.e- 10);
                MSG_DEBUG("RANK(1.e- 10) = " << lu.rank());
                lu.setThreshold(1.e- 6);
                MSG_DEBUG("RANK(1.e- 6) = " << lu.rank());
#endif
            }
            {
                auto aba = test_geno_matrix(SPELL_STRING("RIL6-" << i), Fl, "ABB");
                /*auto sim_aba = read_table("simulation/gen_RIL.old/counts_10/8/1/ABA.txt");*/
                /*MSG_DEBUG(MATRIX_SIZE(aba));*/
                /*MSG_DEBUG(MATRIX_SIZE(sim_aba));*/
                /*MSG_DEBUG("TOTAL DIST ABA = " << (aba.transpose() - sim_aba.topRows(27)).lpNorm<1>());*/
                auto abb = test_geno_matrix(SPELL_STRING("RIL6-" << i), Fl, "ABA");
                /*auto sim_abb = read_table("simulation/gen_RIL.old/counts_10/8/1/ABA.txt");*/
                /*MSG_DEBUG(MATRIX_SIZE(abb));*/
                /*MSG_DEBUG(MATRIX_SIZE(sim_abb));*/
                /*MSG_DEBUG("TOTAL DIST ABB = " << (abb.transpose() - sim_abb.topRows(27)).lpNorm<1>());*/
                /*MSG_DEBUG("STAT DIST RATIO = " << (Fl.stat_dist.maxCoeff() / Fl.stat_dist.minCoeff()));*/
                double plogp = 0;
                for (int i = 0; i < Fl.stat_dist.size(); ++i) {
                    plogp -= Fl.stat_dist(i) * log(Fl.stat_dist(i));
                }
                MSG_DEBUG(std::setw(3) << i << " sigma_p*log_p = " << plogp);
            }
            /*Fl2 = lump(kronecker(Fl2, selfing_gamete));*/
            /*experimental_lumper el2(Fl2);*/
            /*Fl2 = el2.do_lump(16);*/
            /*test_geno_matrix(SPELL_STRING("F" << i << "_normal"), F);*/
            /*test_geno_matrix(SPELL_STRING("F" << i << "_over"), Fl);*/
            /*test_geno_matrix(SPELL_STRING("F" << i << "_inherit"), Fl2);*/
        }
        return 0;
#else
        experimental_lumper el(F);
        Fl = el.do_lump(128);
#endif
        /*MSG_DEBUG(Fl);*/
        auto aba = test_geno_matrix("RIL6", Fl, "ABB");
        auto sim_aba = read_table("simulation/gen_RIL.old/counts_10/8/1/ABA.txt");
        MSG_DEBUG(MATRIX_SIZE(aba));
        MSG_DEBUG(MATRIX_SIZE(sim_aba));
        MSG_DEBUG("TOTAL DIST ABA = " << (aba.transpose() - sim_aba.topRows(26)).lpNorm<1>());
        auto abb = test_geno_matrix("RIL6", Fl, "ABA");
        auto sim_abb = read_table("simulation/gen_RIL.old/counts_10/8/1/ABA.txt");
        MSG_DEBUG(MATRIX_SIZE(abb));
        MSG_DEBUG(MATRIX_SIZE(sim_abb));
        MSG_DEBUG("TOTAL DIST ABB = " << (abb.transpose() - sim_abb.topRows(26)).lpNorm<1>());

        for (; i < 40; ++i) {
            F = lump(kronecker(Fl, selfing_gamete));
            experimental_lumper el(F);
            auto inf_mat = Fl.inf_mat;
            auto bak = Fl;
            Fl = el.do_lump(NSTATES);
            /*Fl = el.do_lump(32);*/
            if (1 && inf_mat.rows() == Fl.inf_mat.rows()) {
                /*MSG_DEBUG("@@@ DELTA at #" << i);*/
                /*MSG_DEBUG((Fl.inf_mat - inf_mat));*/
                /*auto ang = min_column_angles(inf_mat, Fl.inf_mat);*/
                /*auto G1 = lump_using_partition_weighted(bak, experimental_lumper(bak).partition_on_labels());*/
                /*auto G2 = lump_using_partition_weighted(Fl, experimental_lumper(Fl).partition_on_labels());*/
                /*auto ang = min_column_angles(G1.inf_mat, G2.inf_mat);*/
                /*auto G1 = bak.inf_mat * bak.stat_dist.asDiagonal();*/
                /*auto G2 = Fl.inf_mat * Fl.stat_dist.asDiagonal();*/
                auto G1 = bak.inf_mat;
                auto G2 = Fl.inf_mat;
                auto ang = min_column_angles(G1, G2);

                double plogp = 0;
                for (int i = 0; i < Fl.stat_dist.size(); ++i) {
                    plogp -= Fl.stat_dist(i) * log(Fl.stat_dist(i));
                }
                /*MSG_DEBUG("sigma_p*log_p = " << plogp);*/

                /*ang.array() *= Fl.stat_dist.array();*/
                /*MSG_DEBUG("" << Fl.stat_dist.transpose());*/
                double a = ang.lpNorm<2>();
                MSG_DEBUG("ANGLES(G" << (i - 1) << ", G" << i << ") = " << a);

                int n_A = 0;
                for (const auto& l: Fl.labels) {
                    if (l == label_type{'a', 'a'}) {
                        ++n_A;
                    }
                }
                MSG_DEBUG("n_A=" << n_A);

                MatrixXd tmpA(Fl.inf_mat.rows(), n_A);
                int col = 0, i = 0;

                for (const auto& l: Fl.labels) {
                    if (l == label_type{'a', 'a'}) {
                        tmpA.col(col) = Fl.inf_mat.col(i) * Fl.stat_dist(i);
                        ++col;
                    }
                    ++i;
                }

                MatrixXd ata = tmpA.transpose() * tmpA;
                SelfAdjointEigenSolver<MatrixXd> saes(ata);

                /*MSG_DEBUG("SPECTRUM At.A");*/
                /*MSG_DEBUG(saes.eigenvalues().real().transpose());*/
                /*VectorXd ev = saes.eigenvalues().real().transpose();*/
                /*MSG_DEBUG("EV min=" << ev(0) << " max=" << ev(ev.size() - 1) << " cond=" << (ev(ev.size() - 1) / ev(0)));*/

            }
        }
    }

    if (0)
    {
        auto F2 = lump(kronecker(F1, selfing_gamete));
        auto F3 = lump(kronecker(F2, selfing_gamete));
        auto F4 = lump(kronecker(F3, selfing_gamete));
        auto F5 = lump(kronecker(F4, selfing_gamete));
        auto F6 = lump(kronecker(F5, selfing_gamete));
        auto F7 = lump(kronecker(F6, selfing_gamete));
        auto F8 = lump(kronecker(F7, selfing_gamete));
        auto F9 = lump(kronecker(F8, selfing_gamete));
        {
            experimental_lumper el(F9);
            auto F3l = el.do_lump(16);
            test_geno_matrix("RIL6", F3l, "ABB");
            test_geno_matrix("RIL6", F3l, "ABA");
            return 0;
        }
        test_geno_matrix("RIL6", F9, "ABB");
        test_geno_matrix("RIL6", F9, "ABA");
        return 0;
        MSG_DEBUG("STUDY LUMPING F6");
        experimental_lumper el6(F6);
        auto F6l = el6.do_lump(16);
        experimental_lumper el7(lump(kronecker(F6l, selfing_gamete)));
        auto F7l = el7.do_lump(16);
        test_geno_matrix("g64_data.matrix", F7, "ABH");
        test_geno_matrix("g32_data.matrix", F7l, "ABH");
        /*for (int i = 0; i < 20; i += 2) {*/
            /*el.do_lump(i);*/
        /*}*/
        return 0;
    }

    if (0)
    {
        auto C = ancestor_matrix('c');
        auto D = ancestor_matrix('d');
        auto CD = lump(kronecker(kronecker(C, gamete), kronecker(D, gamete)));
        auto CP = lump(kronecker(kronecker(F1, gamete), kronecker(CD, gamete)));
        auto CPs = lump(kronecker(CP, selfing_gamete));
        auto _3V = lump(kronecker(kronecker(F1, gamete), kronecker(C, gamete)));
        auto _3Vs = lump(kronecker(_3V, gamete));

        MSG_DEBUG_INDENT_EXPR("[3V]\t");
        MSG_DEBUG("==========");
        MSG_DEBUG(_3V);
        MSG_DEBUG("==========");
        analyse_symmetries(_3V);
        MSG_DEBUG_DEDENT;

#if 1
        MSG_DEBUG_INDENT_EXPR("[S(3V)]\t");
        MSG_DEBUG("==========");
        MSG_DEBUG(_3Vs);
        MSG_DEBUG("==========");
        analyse_symmetries(_3Vs);
        MSG_DEBUG_DEDENT;

        MSG_DEBUG_INDENT_EXPR("[S(CP)]\t");
        MSG_DEBUG("==========");
        MSG_DEBUG(CPs);
        MSG_DEBUG("==========");
        analyse_symmetries(CPs);
        MSG_DEBUG_DEDENT;
#endif

        MSG_DEBUG_INDENT_EXPR("[CP]\t");
        MSG_DEBUG("==========");
        MSG_DEBUG(CP);
        MSG_DEBUG("==========");
        analyse_symmetries(CP);
        MSG_DEBUG_DEDENT;
        MSG_DEBUG_INDENT_EXPR("[F2]\t");
        auto F2 = lump(kronecker(F1, selfing_gamete));
        MSG_DEBUG("==========");
        MSG_DEBUG(F2);
        MSG_DEBUG("==========");
        analyse_symmetries(F2);
        MSG_DEBUG_DEDENT;
        MSG_DEBUG_INDENT_EXPR("[F2f]\t");
        auto F2f = kronecker(F1, selfing_gamete);
        MSG_DEBUG("==========");
        MSG_DEBUG(F2f);
        MSG_DEBUG("==========");
        analyse_symmetries(F2f);
        MSG_DEBUG_DEDENT;
        MSG_DEBUG_INDENT_EXPR("[F3]\t");
        auto F3 = lump(kronecker(F2, selfing_gamete));
        MSG_DEBUG("==========");
        MSG_DEBUG(F3);
        MSG_DEBUG("==========");
        analyse_symmetries(F3);
        MSG_DEBUG_DEDENT;
        MSG_DEBUG_INDENT_EXPR("[F3f]\t");
        auto F3f = kronecker(F2, selfing_gamete);
        MSG_DEBUG("==========");
        MSG_DEBUG(F3f);
        MSG_DEBUG("==========");
        analyse_symmetries(F3f);
        MSG_DEBUG_DEDENT;
        MSG_DEBUG_INDENT_EXPR("[BC]\t");
        auto BC = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
        MSG_DEBUG("==========");
        MSG_DEBUG(BC);
        MSG_DEBUG("==========");
        analyse_symmetries(BC);
        MSG_DEBUG_DEDENT;
        MSG_DEBUG_INDENT_EXPR("[BC2]\t");
        auto BC2 = lump(kronecker(kronecker(BC, gamete), kronecker(A, gamete)));
        MSG_DEBUG("==========");
        MSG_DEBUG(BC2);
        MSG_DEBUG("==========");
        analyse_symmetries(BC2);
        MSG_DEBUG_DEDENT;
        MSG_DEBUG_INDENT_EXPR("[S(BC)]\t");
        auto BCs = lump(kronecker(BC, selfing_gamete));
        MSG_DEBUG("==========");
        MSG_DEBUG(BCs);
        MSG_DEBUG("==========");
        analyse_symmetries(BCs);
        MSG_DEBUG_DEDENT;
        MSG_DEBUG_INDENT_EXPR("[S(BCB)]\t");
        auto BCBs = lump(kronecker(kronecker(kronecker(BC, gamete), kronecker(B, gamete)), selfing_gamete));
        MSG_DEBUG("==========");
        MSG_DEBUG(BCBs);
        MSG_DEBUG("==========");
        analyse_symmetries(BCBs);
        MSG_DEBUG_DEDENT;
        return 0;
    }

    if (0)
    {
        auto F2 = lump(kronecker(F1, selfing_gamete));
        /*auto F3 = kronecker(F2, selfing_gamete);*/
        auto F3 = lump(kronecker(F2, selfing_gamete));
        MSG_DEBUG(F3);
        return 0;
        auto F3l = lump(F3);
        MSG_DEBUG(F3l);
        MSG_DEBUG("STUDY LUMPING F3");
        experimental_lumper el(F3);
        MSG_DEBUG(el.do_lump(4));
        return 0;
        el.do_lump(8);
        MSG_DEBUG("STUDY LUMPING F3-lumped");
        experimental_lumper el2(F3l);
        el2.do_lump(8);
        auto F4 = lump(kronecker(F3, selfing_gamete));
        std::map<double, size_t> hist;
        for (int i = 0; i < 1000; ++i) {
            double delta;
            /*auto kmF4 = kmeans_lump(kronecker(F3, selfing_gamete), 22, delta);*/
            /*auto kmF3 = kmeans_lump(kronecker(F2, selfing_gamete), 16, delta);*/
            /*auto kmF5 = kmeans_lump(kronecker(F4, selfing_gamete), 18, delta);*/
            hist[delta]++;
        }
        /*MSG_DEBUG(F3);*/
        for (const auto& kv: hist) {
            MSG_DEBUG(std::setw(16) << kv.first << "\t" << kv.second);
        }
        auto F5 = lump(kronecker(F4, selfing_gamete));
        auto F6 = kronecker(F5, selfing_gamete);
        auto F6l = lump(F6);
        MSG_DEBUG("" << F6);
        experimental_lumper el3(F6);
        el3.do_lump(32);
        return 0;
    }

    if (0)
    {
        auto F2 = lump(kronecker(kronecker(F1, gamete), kronecker(A, gamete)));
        auto F3 = lump(kronecker(kronecker(F2, gamete), kronecker(A, gamete)));
        auto F4 = lump(kronecker(kronecker(F3, gamete), kronecker(A, gamete)));
        auto F5 = lump(kronecker(kronecker(F4, gamete), kronecker(A, gamete)));
        auto F6 = lump(kronecker(kronecker(F5, gamete), kronecker(A, gamete)));
        auto F7 = lump(kronecker(kronecker(F6, gamete), kronecker(A, gamete)));
        auto F8 = lump(kronecker(kronecker(F7, gamete), kronecker(A, gamete)));
        auto F9 = lump(kronecker(kronecker(F8, gamete), kronecker(A, gamete)));
        auto F10 = lump(kronecker(kronecker(F9, gamete), kronecker(A, gamete)));
        auto F11 = lump(kronecker(kronecker(F10, gamete), kronecker(A, gamete)));
        MSG_DEBUG("STUDY LUMPING BC10");
        experimental_lumper el(F11);
        auto g6 = el.do_lump(5);
        test_geno_matrix("g11.matrix", F11, "ABH");
        test_geno_matrix("g5_data.matrix", g6, "ABH");
    }

    return 0;
    (void) argc; (void) argv;
}

