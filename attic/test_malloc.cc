/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include "../src/malloc.h"
#include <vector>
#include <malloc.h>
#include <unistd.h>
#include <random>

#define N_ALLOC 200

int main(int argc, const char** argv)
{
    std::random_device rd;

    std::uniform_int_distribution<size_t> uniform_dist(10, 10000);
 
    std::mt19937 e2(rd());

    std::vector<size_t> sizes;
    for (int i = 0; i < N_ALLOC; ++i) {
        sizes.push_back(uniform_dist(rd));
    }

    /*std::vector<std::size_t> z = {1, 2, 4, 8, 16, 23, 32, 37, 41, 64, 100, 128, 202, 256, 500, 512, 1000, 1024};*/
#pragma omp parallel for
    for (size_t bsz = 1; bsz < 1000000; ++bsz) {
        /*std::cout << bsz << " => " << FIND_BIN(bsz) << " => " << BIN_TO_SIZE(FIND_BIN(bsz)) << std::endl;*/
        char* allocs[N_ALLOC];
        for (int i = 0; i < N_ALLOC; ++i) {
            allocs[i] = (char*) malloc(sizes[i]);
        }
        /*for (int i = 0; i < N_ALLOC; ++i) {*/
            /*for (size_t x = 0; x < sizes[i]; ++x) {*/
                /**(allocs[i] + x) = (x & 0x7f);*/
            /*}*/
        /*}*/
        usleep(5);
        for (int i = 0; i < N_ALLOC; ++i) {
            free(allocs[i]);
        }
    }
    /*atexit(dump_bins);*/
    return 0;
}

