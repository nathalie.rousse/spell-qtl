#if defined(_SPEL_FAST_POLY_H_) && !defined(_SPEL_FAST_POLY_OUTPUT_H_)
#define _SPEL_FAST_POLY_OUTPUT_H_


inline void output_monomial(std::ostream& os, bool& is_first, bool is_product, const char* variable, coef_t coef, int exponent)
{
    if (coef == 0) {
        return;
    }
    bool need_plus = (coef > 0) && (!is_first) && (!is_product);
    bool need_star = is_product && !is_first;
    if (need_plus) {
        os << '+';
    } else if (need_star) {
        os << '*';
    }
    if (exponent > 0) {
        if (coef == -1.) {
            os << '-';
        } else if (coef != 1.) {
            os << coef << '*';
        }
        os << variable;
        if (exponent > 1) {
            os << '^' << exponent;
        }
    } else {
        if (is_first || coef != 0) {
            os << coef;
        }
    }
    is_first = false;
}


inline void output_polynomial(std::ostream& os, const std::vector<coef_t>& coefs)
{
    int exponent = coefs.size() - 1;
    bool is_first = true;
    if (exponent == -1) {
        os << "0";
    }
    auto c = coefs.rbegin();
    auto e = coefs.rend();
    for (; c != e; ++c) {
        output_monomial(os, is_first, false, "X", *c, exponent);
        --exponent;
    }
}



inline std::ostream& operator << (std::ostream& os, const impl::d_polynom& p)
{
    bool is_first = true;
    if (p.valuation > 0) {
        output_monomial(os, is_first, false, "X", 1., p.valuation);
        if (p.size() > 1) {
            os << "*(";
            output_polynomial(os, p);
            os << ')';
        } else if (p.size() != 1 || p[0] != 1.) {
            os << '*';
            output_polynomial(os, p);
        }
    } else {
        output_polynomial(os, p);
    }
    return os;
}

inline std::ostream& operator << (std::ostream& os, const impl::f_polynom& p)
{
    bool is_first = true;
    if (p.r_exp != 0) {
        output_monomial(os, is_first, true, "X", 1., p.r_exp);
    }
    if (p.s_exp != 0) {
        output_monomial(os, is_first, true, "(1-X)", 1., p.s_exp);
    }
    if (is_first) {
        output_polynomial(os, p.P);
    } else if (p.P.size() == 1 && p.P[0] != 1.) {
        os << '*';
        output_polynomial(os, p.P);
    } else if (!(p.P.size() == 1 && p.P[0] == 1.)) {
        os << "*(";
        output_polynomial(os, p.P);
        os << ')';
    }
    return os;
}


inline std::ostream& operator << (std::ostream& os, const fast_polynom& f)
{
    return os << ((const impl::f_polynom&)f);
    /*return os << ((const impl::d_polynom&)f);*/
}

inline std::ostream& operator << (std::ostream& os, const impl::polynom& p)
{
    /*return os << "{f=" << p.to_f() << " d=" << p.to_d() << '}';*/
    os << '{';
    if (p.f_init) {
        os << "f=" << p.f;
        if (p.d_init) {
            os << ' ';
        }
    }
    if (p.d_init) {
        os << "d=" << p.d;
    }
    return os << '}';
}

inline std::ostream& operator << (std::ostream& os, const impl::operation_table_t& ot)
{
    for (auto& kv: ot) {
        os << " * " << kv.first.first << ',' << kv.first.second << " => " << kv.second << std::endl;
    }
    return os;
}

#endif

