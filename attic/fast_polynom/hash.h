#if defined(_SPEL_FAST_POLY_H_) && !defined(_SPEL_FAST_POLY_HASH_H_)
#define _SPEL_FAST_POLY_HASH_H_

#ifndef ROTATE_DEFINED
#define ROTATE_DEFINED
namespace impl {
    template <size_t N_BITS>
        static inline size_t ROTATE(size_t value)
        {
            return (value >> N_BITS) | (value << (sizeof(size_t) * 8 - N_BITS));
        }
}
#endif

namespace std {
    template <>
        struct hash<std::vector<double>> {
            size_t operator () (const std::vector<double>& vec) const
            {
                static hash<double> h;
                size_t accum = 0;
                for (auto d: vec) {
                    accum = impl::ROTATE<7>(accum ^ h(d));
                }
                return accum;
            }
        };

    template <>
        struct hash<std::vector<long double>> {
            size_t operator () (const std::vector<long double>& vec) const
            {
                static hash<long double> h;
                size_t accum = 0;
                for (auto d: vec) {
                    accum = impl::ROTATE<15>(accum ^ h(d));
                }
                return accum;
            }
        };

#ifdef POLYNOM_COEF_INT
    template <>
        struct hash<std::vector<coef_t>> {
            size_t operator () (const std::vector<coef_t>& vec) const
            {
                static hash<coef_t> h;
                size_t accum = 0;
                for (auto d: vec) {
                    accum = impl::ROTATE<7>(accum ^ h(d));
                }
                return accum;
            }
        };
#endif

    template <>
        struct hash<std::vector<int>> {
            size_t operator () (const std::vector<int>& vec) const
            {
                static hash<int> h;
                size_t accum = 0;
                for (auto d: vec) {
                    accum = impl::ROTATE<7>(accum ^ h(d));
                }
                return accum;
            }
        };

    template <>
        struct hash<impl::f_polynom> {
            size_t operator () (const impl::f_polynom& f) const
            {
                static hash<int> hi;
                static hash<std::vector<coef_t>> hv;
                size_t ret = impl::ROTATE<7>(impl::ROTATE<7>(hi(f.r_exp)) ^ hi(f.s_exp)) ^ hv(f.P);
#if 0
                {
                    std::stringstream debug;
                    debug << "f_poly::hash(" << f.r_exp << ", " << f.s_exp << ',';
                    for (auto c: f.P) { debug << ' ' << c; }
                    MSG_DEBUG(debug.str() << ")=" << ret);
                }
#endif
                return ret;
            }
        };

    template <>
        struct hash<impl::polynom> {
            size_t operator () (const impl::polynom& p) const
            {
                static hash<impl::f_polynom> h;
                return h(p.to_f());
            }
        };

    template <>
        struct hash<impl::operands_t> {
            size_t operator () (const impl::operands_t& o) const
            {
                static hash<int> hi;
                /*size_t ret = impl::ROTATE<7>(hi(o.first)) ^ hi(o.second);*/
                size_t ret = (hi(o.first) * 0xdeadb3ef) ^ hi(o.second);
                /*MSG_DEBUG("operands::hash(" << o.first << ',' << o.second << ")=" << ret);*/
                return ret;
            }
        };
}

#endif

