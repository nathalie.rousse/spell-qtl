#if defined(_SPEL_FAST_POLY_H_) && !defined(_SPEL_FAST_POLY_POLY_H_)
#define _SPEL_FAST_POLY_POLY_H_

namespace impl {
    inline polynom::polynom() : f_init(false), d_init(false), f(0, 0, 0), d() {DEBUG_SCOPE;}

    inline polynom::polynom(d_polynom&& dp, f_polynom&& fp)
        : f_init(true), d_init(true), f(fp), d(dp)
    {DEBUG_SCOPE;}

    inline polynom::polynom(const polynom& p)
        : f_init(p.f_init), d_init(p.d_init), f(p.f), d(p.d)
    {DEBUG_SCOPE;}

    inline polynom::polynom(polynom&& p)
        : f_init(p.f_init), d_init(p.d_init), f(std::move(p.f)), d(std::move(p.d))
    {DEBUG_SCOPE;}

    inline polynom::polynom(const f_polynom& fp)
        : f_init(true), d_init(false), f(fp), d()
    {DEBUG_SCOPE;}

    inline polynom::polynom(const d_polynom& dp)
        : f_init(false), d_init(true), f(0, 0, 0), d(dp)
    {DEBUG_SCOPE;}

    inline polynom::polynom(f_polynom&& fp)
        : f_init(true), d_init(false), f(std::move(fp)), d()
    {DEBUG_SCOPE;}

    inline polynom::polynom(d_polynom&& dp)
        : f_init(false), d_init(true), f(0, 0, 0), d(std::move(dp))
    {DEBUG_SCOPE;}

    inline bool polynom::operator == (const polynom& p) const
    {DEBUG_SCOPE;
        return to_f() == p.to_f();
    }

    inline const f_polynom& polynom::to_f() const
    {
        /*if (d_init) {*/
        /*std::cout << "(to_f) d_poly=" << d << std::endl;*/
        /*}*/
        if (!f_init) {DEBUG_SCOPE;
            polynom* p = const_cast<polynom*>(this);
            p->f = p->d;
            p->f_init = true;
        }
        /*std::cout << "(to_f) f_poly=" << f << std::endl;*/
        return f;
    }

    inline const d_polynom& polynom::to_d() const
    {
        /*if (f_init) {*/
        /*std::cout << "(to_d) f_poly=" << f << std::endl;*/
        /*}*/
        if (!d_init) {DEBUG_SCOPE;
            polynom* p = const_cast<polynom*>(this);
            p->d = p->f;
            p->d_init = true;
        }
        /*std::cout << "(to_d) d_poly=" << d << std::endl;*/
        return d;
    }

    inline polynom polynom::mul(const polynom& p) const
    {DEBUG_SCOPE;
        return to_f().mul(p.to_f());
    }

    inline polynom polynom::div(coef_t scalar) const
    {DEBUG_SCOPE;
        return to_f().div(scalar);
    }

    inline polynom polynom::add(const polynom& p) const
    {DEBUG_SCOPE;
        /*polynom p1 = to_d().add(p.to_d());*/
        /*polynom p2 = to_f().add(p.to_f());*/
        /*if (!(p1 == p2)) {*/
            /*MSG_DEBUG(p1);*/
            /*MSG_DEBUG(p2);*/
            /*throw 0;*/
        /*}*/
        /*
        return to_f().add(p.to_f());
        /*/
        return to_d().add(p.to_d());
        //*/
    }

    inline int polynom::degree() const
    {
        return d_init ? d.degree : to_f().degree();
    }

    inline int polynom::valuation() const
    {
        return d_init ? d.valuation : to_f().r_exp;
    }

    inline bool polynom::inf(const polynom& p) const
    {
        return to_f().inf(p.to_f());
    }
}

#endif

