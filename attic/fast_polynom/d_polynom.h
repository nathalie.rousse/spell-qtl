#if defined(_SPEL_FAST_POLY_H_) && !defined(_SPEL_FAST_D_POLY_H_)
#define _SPEL_FAST_D_POLY_H_

namespace impl {
    inline d_polynom::d_polynom()
        : std::vector<coef_t>(), valuation(0), degree(0)
    {DEBUG_SCOPE;}

    inline d_polynom::d_polynom(const d_polynom& d)
        : std::vector<coef_t>(d), valuation(d.valuation), degree(d.degree)
    {DEBUG_SCOPE;}

    inline d_polynom::d_polynom(d_polynom&& d)
        : std::vector<coef_t>(std::move(d)), valuation(d.valuation), degree(d.degree)
    {DEBUG_SCOPE;}

    inline d_polynom::d_polynom(size_t n, int v)
        : std::vector<coef_t>(), valuation(v), degree(v + n - 1)
    {DEBUG_SCOPE; resize(n); }

    inline d_polynom::d_polynom(size_t n)
        : std::vector<coef_t>(), valuation(0), degree(n - 1)
    {DEBUG_SCOPE; resize(n); }

    inline d_polynom::d_polynom(std::initializer_list<coef_t> x)
        : std::vector<coef_t>(x), valuation(0)
    {DEBUG_SCOPE; cleanup(); }

    inline d_polynom::d_polynom(const std::vector<coef_t>& x)
        : std::vector<coef_t>(x), valuation(0)
    {DEBUG_SCOPE; cleanup(); }

    inline bool d_polynom::operator == (const d_polynom& d) const
    {DEBUG_SCOPE;
        //MSG_DEBUG((*this));// << " == " << d << " ?");
        bool ok = valuation == d.valuation && degree == d.degree;
        auto ai = begin(), aj = end(), bi = d.begin();
        while (ok && ai != aj) {DEBUG_SCOPE;
            ok = *ai++ == *bi++;
        }
        return ok;
    }

    inline void d_polynom::cleanup()
    {DEBUG_SCOPE;
        auto v = std::find_if(begin(), end(), [](coef_t k) { return k != 0; });
        valuation += v - begin();
        if (begin() != v) {
            erase(begin(), v);
        }
        while(size() && back() == 0) { pop_back(); }
        degree = valuation + size() - 1;
    }

    inline d_polynom d_polynom::add(const d_polynom& d) const
    {DEBUG_SCOPE;
        /*std::cout << "add " << (*this) << " and " << d << std::endl;*/
        size_t result_valuation = std::min(valuation, d.valuation);
        d_polynom ret(std::max(degree, d.degree) - result_valuation + 1, result_valuation);
        size_t vi = valuation - result_valuation;
        size_t vj = d.valuation - result_valuation;
        /*std::cout << "val=" << valuation << " d.val=" << d.valuation << " result_val=" << result_valuation << std::endl;*/

        /*std::cout << "this size=" << size() << " contents=";*/
        /*for (auto k: *this) { std::cout << ' ' << k; }*/
        /*std::cout << std::endl;*/

        /*std::cout << "d size=" << d.size() << " contents=";*/
        /*for (auto k: d) { std::cout << ' ' << k; }*/
        /*std::cout << std::endl;*/

        for (auto k: *this) {
            ret[vi++] += k;
        }

        for (auto k: d) {
            ret[vj++] += k;
        }

        /*std::cout << "size=" << ret.size() << " contents=";*/
        /*for (auto k: ret) { std::cout << ' ' << k; }*/
        /*std::cout << std::endl;*/
        ret.cleanup();

        /*std::cout << "result is " << ret << std::endl;*/

        return ret;
    }

    inline d_polynom d_polynom::sub(const d_polynom& d) const
    {DEBUG_SCOPE;
        /*std::cout << "add " << (*this) << " and " << d << std::endl;*/
        size_t result_valuation = std::min(valuation, d.valuation);
        d_polynom ret(std::max(degree, d.degree) - result_valuation + 1, result_valuation);
        size_t vi = valuation - result_valuation;
        size_t vj = d.valuation - result_valuation;
        /*std::cout << "val=" << valuation << " d.val=" << d.valuation << " result_val=" << result_valuation << std::endl;*/

        /*std::cout << "this size=" << size() << " contents=";*/
        /*for (auto k: *this) { std::cout << ' ' << k; }*/
        /*std::cout << std::endl;*/

        /*std::cout << "d size=" << d.size() << " contents=";*/
        /*for (auto k: d) { std::cout << ' ' << k; }*/
        /*std::cout << std::endl;*/

        for (auto k: *this) {
            ret[vi++] += k;
        }

        for (auto k: d) {
            ret[vj++] -= k;
        }

        /*std::cout << "size=" << ret.size() << " contents=";*/
        /*for (auto k: ret) { std::cout << ' ' << k; }*/
        /*std::cout << std::endl;*/
        ret.cleanup();

        /*std::cout << "result is " << ret << std::endl;*/

        return ret;
    }

    inline d_polynom d_polynom::mul(coef_t k) const
    {DEBUG_SCOPE;
        if (k == 0) {
            return d_polynom(0);
        }
        d_polynom ret(*this);
        for (auto& coef: ret) {
            coef *= k;
        }
        return ret;
    }

    inline d_polynom d_polynom::mul(const d_polynom& d) const
    {DEBUG_SCOPE;
        d_polynom ret;
        ret.valuation = valuation + d.valuation;
        ret.resize(degree + d.degree + 1);
        for (size_t i = 0; i < size(); ++i) {
            for (size_t j = 0; j < d.size(); ++j) {
                ret[i + j] += (*this)[i] * d[j];
            }
        }
        ret.cleanup();
        return ret;
    }


    inline d_polynom& d_polynom::operator = (d_polynom&& d)
    {DEBUG_SCOPE;
        valuation = d.valuation;
        degree = d.degree;
        swap(d);
        return *this;
    }


    inline d_polynom d_polynom::o(const fast_polynom& fd) const
    {DEBUG_SCOPE;
        /*std::cout << "[" << (*this) << "] o [" << fd << ']' << std::endl;*/
        fast_polynom fd_pow = fd.pow(valuation);
        /*std::cout << fd << ' ' << valuation << ' ' << fd.pow(valuation) << std::endl;*/
        /*std::cout << "valuation=" << valuation << "   initial fd_pow = " << fd_pow << std::endl;*/
        d_polynom ret((1 + ((const polynom&)fd).degree()) * (1 + degree) - 1);

        for (auto coef: *this) {
            const d_polynom& o_tmp = fd_pow;
            /*std::cout << "fd_pow = " << fd_pow << std::endl;*/
            /*std::cout << "o_tmp = " << o_tmp << std::endl;*/
            /*ret = ret.add(o_tmp.mul(coef));*/
            size_t v = o_tmp.valuation;
            for (auto k: o_tmp) {
                ret[v++] += coef * k;
            }
            fd_pow = fd_pow * fd;
        }
        ret.cleanup();
        /*std::cout << "[" << (*this) << "] o [" << fd << ']' << " = " << ret << std::endl;*/

        return ret;
    }


    inline d_polynom s_poly_from_exp(int n)
    {DEBUG_SCOPE;
        if (n == 0) {
            return fast_polynom::one;
        } else if (n == 1) {
            return fast_polynom::s;
        }
        polynom& p = *const_cast<polynom*>(&(const polynom&) (fast_polynom::s.pow(n)));
        if (!p.d_init) {
            d_polynom prev = s_poly_from_exp(n - 1);
            p.d_init = true;
            p.d = prev.mul((const d_polynom&) fast_polynom::s);
        }
        return p.d;
    }


    inline d_polynom::operator f_polynom () const
    {DEBUG_SCOPE;
        /*std::cout << "converting " << (*this) << " to f" << std::endl;*/
        if (size() == 0) {
            /*std::cout << "zero!" << std::endl;*/
            return fast_polynom::zero;
        }
        if (size() > 1) {
            /*std::cout << "compute s_exp and remainder" << std::endl;*/
            const d_polynom& sval = o(fast_polynom::s);
            int s_exp = sval.valuation;
            d_polynom R((const std::vector<coef_t>&)sval);
            return f_polynom(valuation, s_exp, R.o(fast_polynom::s));
        }
        /*std::cout << "s_exp is 0 and remainder is " << at(0) << std::endl;*/
        return f_polynom(valuation, 0, *this);
    }

    inline double d_polynom::norm() const
    {DEBUG_SCOPE;
        double x = pow(.5, valuation);
        double accum = 0;
        for (auto coef: *this) {
            accum += fabs(coef) * x;
            x *= .5;
        }
        return accum;
    }

    inline bool d_polynom::inf(const d_polynom& d) const
    {
        if (degree < d.degree
                || (degree == d.degree
                    && (valuation < d.valuation
                        || (valuation == d.valuation
                            && size() < d.size())))) {
            return true;
        }
        if (size() == d.size()) {
            auto mismatch = std::mismatch(begin(), end(), d.begin());
            return mismatch.first != end()
                && mismatch.second != d.end()
                && (*mismatch.first < *mismatch.second);
        }
        return false;
    }
}

#endif

