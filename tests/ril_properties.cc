#include "catch.hpp"

#include "io/error.h"
#include "input.h"
#include "generation_rs.h"


bool compare_processes(const generation_rs* g1, const generation_rs* g2)
{
    const auto& p1 = g1->P;
    const auto& p2 = g2->P;
    /*MSG_DEBUG("g1: " << (*g1));*/
    /*MSG_DEBUG("g2: " << (*g2));*/
    REQUIRE(p1.size() == p2.size());
    for (size_t i = 0; i < p1.size(); ++i) {
        REQUIRE(p1[i].weight == p2[i].weight);
        REQUIRE(p1[i].G == p2[i].G);
    }
    return true;
}


generation_rs*
rilify(const generation_rs* g, const char* n1, const char* n2)
{
    return g->to_ril(n2);
    (void)n1;
    /*return g->to_doubled_haploid(n1)->to_ril(n2);*/
    /*return g->to_ril(n2)->to_doubled_haploid(n1);*/
}

/* Line */

TEST_CASE("RILoA", "RIL(A) = A")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* RIL = rilify(A, "_", "RIL");
    compare_processes(A, RIL);
}


TEST_CASE("SoA", "S(A) = A")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* S = A->selfing("S");
    compare_processes(A, S);
}


TEST_CASE("DHoA", "DH(A) = A")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* DH = A->to_doubled_haploid("DH");
    compare_processes(A, DH);
}

/* DH */

TEST_CASE("RILoDH", "RIL(DH) = DH")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* B = generation_rs::ancestor("B", 'b', {{0, 0}});
    generation_rs* F1 = A->crossing("F1", B);
    generation_rs* DH = F1->to_doubled_haploid("DH");
    generation_rs* RIL = rilify(DH, "_", "RIL");
    compare_processes(DH, RIL);
}

TEST_CASE("SoDH", "S(DH) = DH")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* B = generation_rs::ancestor("B", 'b', {{0, 0}});
    generation_rs* F1 = A->crossing("F1", B);
    generation_rs* DH = F1->to_doubled_haploid("DH");
    generation_rs* S = DH->selfing("S");
    compare_processes(DH, S);
}

TEST_CASE("DHoDH", "DH(DH) = DH")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* B = generation_rs::ancestor("B", 'b', {{0, 0}});
    generation_rs* F1 = A->crossing("F1", B);
    generation_rs* DH = F1->to_doubled_haploid("DH");
    generation_rs* DH2 = DH->to_doubled_haploid("DH2");
    compare_processes(DH, DH2);
}

/* RIL */

TEST_CASE("DHoRIL", "DH(RIL) = RIL")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* B = generation_rs::ancestor("B", 'b', {{0, 0}});
    generation_rs* F1 = A->crossing("F1", B);
    generation_rs* RIL = rilify(F1, "_", "RIL");
    generation_rs* DH = RIL->to_doubled_haploid("DH");
    compare_processes(DH, RIL);
}


TEST_CASE("SoRIL", "S(RIL) = RIL")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* B = generation_rs::ancestor("B", 'b', {{0, 0}});
    generation_rs* F1 = A->crossing("F1", B);
    generation_rs* RIL = rilify(F1, "_", "RIL");
    generation_rs* S = RIL->selfing("S");
    compare_processes(S, RIL);
}


TEST_CASE("RILoRIL", "RIL(RIL) = RIL")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* B = generation_rs::ancestor("B", 'b', {{0, 0}});
    generation_rs* F1 = A->crossing("F1", B);
    generation_rs* RIL = rilify(F1, "_", "RIL");
    generation_rs* RIL2 = rilify(RIL, "_2", "RIL2");
    compare_processes(RIL, RIL2);
}


TEST_CASE("RILoRILoRIL", "RIL(RIL(RIL)) = RIL")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* B = generation_rs::ancestor("B", 'b', {{0, 0}});
    generation_rs* F1 = A->crossing("F1", B);
    generation_rs* RIL = rilify(F1, "_", "RIL");
    generation_rs* RIL2 = rilify(RIL, "_2", "RIL2");
    generation_rs* RIL3 = rilify(RIL2, "_3", "RIL3");
    compare_processes(RIL, RIL3);
}


#if 1
TEST_CASE("RILoF1vsRILoF2", "RIL(F1) = RIL(F2)")
{
    generation_rs::clear_dict();
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* B = generation_rs::ancestor("B", 'b', {{0, 0}});
    generation_rs* F1 = A->crossing("F1", B);
    generation_rs* F2 = F1->selfing("F2");
    generation_rs* RIL = rilify(F1, "_", "RIL");
    generation_rs* RIL2 = rilify(F2, "_2", "RIL2");
    compare_processes(RIL, RIL2);
}
#endif


