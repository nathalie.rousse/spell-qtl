#include "catch.hpp"

#include "io/error.h"
#include "settings.h"
#include "math_objects/labelled_matrix.h"
#include "cache2.h"
#include "computations/base.h"
#include "computations/basic_data.h"
#include "computations/probabilities.h"

#include "settings_fixture.h"

const char* bc_conf[] = {
    "",
    "-n", "test",
    /*"-P", "2",*/
    "-wd", "tmp",
    "--clean",
    "-bds", "../data/design-BC.xml",
    "-gm", "dataset/debug.map",
    "-mos", "../data/format-ABHCD.xml",
    "-p", "toto", "BC", "dataset/debug_BC.phen",
    "-m", "BC:ABHCD", "dataset/debug_BC.gen",
    "step", "1",
    "-na",
NULL};


template <typename X>
size_t md_value(const X& x)
{
    md5_digest md;
    md << x;
    std::string s = md;
    /*std::cout << "hash(" << x << ") = " << s << std::endl;*/
    return md.context;
}


#if 0
static inline
std::ostream& operator << (std::ostream& os, const multi_generation_observations& mgo)
{
    return os << "MGO[" << mgo.n_mark << ']' << std::endl << mgo.LV;
}
#endif


int mult(int a, int b) { MSG_DEBUG(a << " * " << b << " = " << (a * b)); return a * b; }

#if 0
int mmult(int a, int b, const multi_generation_observations&) { MSG_DEBUG(a << " * " << b << " = " << (a * b)); return a * b; }
#endif

TEST_CASE("hash.1", "check hash of some single values")
{
    settings_fixture _(bc_conf);
    /*multi_generation_observations mgo1, mgo2;*/
    /*mgo1.n_mark = 3;*/
    /*mgo2.n_mark = 3;*/
    /*mgo1.pop = &active_settings->populations[0];*/
    /*mgo2.pop = &active_settings->populations[0];*/

    CHECK(md_value(0) != md_value(1));
    CHECK(md_value(std::string("toto")) != md_value(std::string("titi")));
    MatrixXb m1(3, 3);
    m1 << 1, 0, 0,
          0, 1, 0,
          1, 1, 1;
    MatrixXb m2(3, 3);
    m2 << 1, 0, 1,
          1, 1, 0,
          1, 1, 1;
    CHECK(md_value(m1) != md_value(m2));
    labelled_matrix<MatrixXb, char, char> lm1, lm2;
    lm1.data = m1;
    lm2.data = m2;
    lm1.column_labels = {'a', 'b', 'c'};
    lm2.column_labels = {'a', 'b', 'c'};
    lm1.row_labels = {'a', 'b', 'c'};
    lm2.row_labels = {'a', 'b', 'c'};
    CHECK(md_value(lm1) != md_value(lm2));
    /*mgo1.LV = m1; mgo2.LV = m2;*/
    /*CHECK(md_value(mgo1) != md_value(mgo2));*/
    lm2.data = m1;
    CHECK(md_value(lm1) == md_value(lm2));
    /*mgo1.LV = m1; mgo2.LV = m1;*/
    /*CHECK(md_value(mgo1) != md_value(mgo2));*/
    lm2.column_labels[1] = 'z';
    CHECK(md_value(lm1) != md_value(lm2));
}


TEST_CASE("hash.2", "check hash of other single values")
{
    settings_fixture _(bc_conf);
    qtl_chromosome qc1(&active_settings->map[0]);
    qtl_chromosome qc2(&active_settings->map[0]);
    CHECK(md_value(qc1) == md_value(qc2));
    qc2.add_qtl({0});
    CHECK(md_value(qc1) != md_value(qc2));
    qc1.add_qtl({0});
    CHECK(md_value(qc1) == md_value(qc2));
    qc1.remove_qtl({0});
    qc1.add_qtl({2});
    /*CHECK(md_value(qc1) != md_value(qc2));*/
    CHECK(md_value(qc1) == md_value(qc2)); /* BY DESIGN! only the number of selected QTLs matter, not their positions! */
}


#if 0
TEST_CASE("hash.3", "check hash of multiple arguments as in parental_origin2")
{
    settings_fixture _(bc_conf);

    qtl_chromosome qc(&active_settings->map[0]);
    population* pop = &active_settings->populations.begin()->second;
    generation_rs* gen = active_settings->design->generation["BC"];
    labelled_matrix<Eigen::MatrixXd, std::vector<char>, allele_pair>& stf = gen->state_to_parental_origin[0];
    multi_generation_observations mgo1(pop, qc.chr, gen, 0);
    multi_generation_observations mgo2(pop, qc.chr, gen, 1);
    std::vector<double> sqoc;
    std::vector<double> loci = {0, 1};

    CHECK(md_value(mgo1) != md_value(mgo2));
    CHECK(new_redux::md5(mgo1) != new_redux::md5(mgo2));
    CHECK(new_redux::md5(&qc, pop, gen, stf, mgo1, sqoc, loci) == new_redux::md5(qc, pop, gen, stf, mgo1, sqoc, loci));
    CHECK(new_redux::md5(&qc, pop, gen, stf, mgo1, sqoc, loci) != new_redux::md5(&qc, pop, gen, stf, mgo2, sqoc, loci));
}



parental_origin_type
parental_origin2(qtl_chromosome_value qtl_chr,
			 	 population_value pop,
			 	 generation_value gen,
				 const state_to_parental_origin_matrix_type& stfopom,
		 		 const multi_generation_observations& mgo,
		 		 const selected_qtls_on_chromosome_type& sqoc,
                 const std::vector<double>& loci)
{
    return {};
}

using cc = cached_computation<decltype(parental_origin2)>;
using ccv = cached_computed_value<decltype(parental_origin2)>;

TEST_CASE("hash.4", "check behaviour of cached_computation<parental_origin2>")
{
    settings_fixture _(bc_conf);
    qtl_chromosome qc_(&active_settings->map[0]);

    value<qtl_chromosome_value> qc = &qc_;
    value<population_value> pop {&active_settings->populations.begin()->second};
    value<generation_value> gen {active_settings->design->generation["BC"]};
    value<labelled_matrix<Eigen::MatrixXd, std::vector<char>, allele_pair>> stf = (*gen)->state_to_parental_origin[0];
    value<multi_generation_observations> mgo1 = multi_generation_observations(*pop, (*qc)->chr, *gen, 0);
    value<multi_generation_observations> mgo2 = multi_generation_observations(*pop, (*qc)->chr, *gen, 1);
    value<selected_qtls_on_chromosome_type> sqoc = selected_qtls_on_chromosome_type{};
    value<std::vector<double>> loci = std::vector<double>{0, 1};

    CHECK(md_value(mgo1) != md_value(mgo2));
    CHECK(new_redux::md5(mgo1) != new_redux::md5(mgo2));
    CHECK(new_redux::md5(qc, pop, gen, stf, mgo1, sqoc, loci) != new_redux::md5(qc, pop, gen, stf, mgo2, sqoc, loci));

    /*value<parental_origin_type> pot1 = make_value(parental_origin2, qc, pop, gen, stf, mgo1, sqoc, loci);*/
    /*value<parental_origin_type> pot2 = make_value(parental_origin2, qc, pop, gen, stf, mgo2, sqoc, loci);*/
    cc po1("parental_origin2", parental_origin2, qc, pop, gen, stf, mgo1, sqoc, loci);
    cc po2("parental_origin2", parental_origin2, qc, pop, gen, stf, mgo2, sqoc, loci);
    CHECK(po1.m_md5_hash.accum != po2.m_md5_hash.accum);
    CHECK(po1.m_md5_hash.append != po2.m_md5_hash.append);
    CHECK(po1.get_path() != po2.get_path());
    MSG_DEBUG("po1 path: " << po1.get_path());
    MSG_DEBUG("po2 path: " << po2.get_path());
    ccv pov1(CachingPolicy::Disk, parental_origin2, qc, pop, gen, stf, mgo1, sqoc, loci);
    ccv pov2(CachingPolicy::Disk, parental_origin2, qc, pop, gen, stf, mgo2, sqoc, loci);
    CHECK(pov1.m_comp.m_md5_hash.accum != pov2.m_comp.m_md5_hash.accum);
    CHECK(pov1.m_comp.m_md5_hash.append != pov2.m_comp.m_md5_hash.append);
    CHECK(pov1.m_comp.get_path() != pov2.m_comp.get_path());
}


TEST_CASE("hash.5", "check hashing of computed values")
{
    settings_fixture _(bc_conf);
    value<population_value> pop {&active_settings->populations.begin()->second};
    value<const pop_mgo_data*> pmd = new pop_mgo_data(*pop);
    value<chromosome_value> chr = &active_settings->map[0];
    context_key ck(new context_key_struc(*pop, *chr, std::vector<double>()));
    value<multi_generation_observations>
        mv1 = make_value<Sync|Disk>(population_marker_obs,
                                    as_value(ck), value<int>{0});
    value<multi_generation_observations>
        mv2 = make_value<Sync|Disk>(population_marker_obs,
                                    as_value(ck), value<int>{1});
    auto tmp = mv1->LV;
    tmp = mv2->LV;
    /*std::cout << "mv1" << std::endl << (*mv1) << std::endl;*/
    /*std::cout << "mv2" << std::endl << (*mv2) << std::endl;*/
    CHECK(mv1 != mv2);
    CHECK(mv1.hash() != mv2.hash());
    CHECK(new_redux::md5(mv1) != new_redux::md5(mv2));
    value<multi_generation_observations>
        mv1bis = make_value<Disk>(population_marker_obs,
                                  as_value(ck), value<int>{0});
    CHECK(mv1.hash() == mv1bis.hash());
    CHECK(new_redux::md5(mv1, mv2) != new_redux::md5(mv1, mv1));
}
#endif


void check_eq(const locus_key& k1, const locus_key& k2) { CHECK(k1 == k2); }
void check_neq(const locus_key& k1, const locus_key& k2) { CHECK(k1 != k2); }
void check_inf(const locus_key& k1, const locus_key& k2) { CHECK(k1 < k2); }
void check_sup(const locus_key& k1, const locus_key& k2) { CHECK(k1 > k2); }
void check_not_sup(const locus_key& k1, const locus_key& k2) { CHECK_FALSE(k1 > k2); }


TEST_CASE("hash.lk", "Check hashing of locus_key as well as some locus_key operators")
{
    std::hash<locus_key> h;
    CHECK(h(locus_key() + 23) != h(locus_key() + 42));
    CHECK(h(locus_key()) != h(locus_key() + 42));
    check_inf((locus_key() + 23), (locus_key() + 42));
    check_not_sup((locus_key() + 23), (locus_key() + 42));
    check_inf((locus_key() + 23 + 42.), (locus_key() + 42));
    check_not_sup((locus_key() + 23 + 42.), (locus_key() + 42));
    check_eq((locus_key() + 23 + 42), (locus_key() + 23 + 42));
    check_neq((locus_key() + 23 + 43), (locus_key() + 23 + 42));
    check_neq((locus_key() + 22 + 42), (locus_key() + 23 + 42));
    check_eq((locus_key() + 42. + 23), (locus_key() + 23 + 42));
    check_inf((locus_key()), (locus_key() + 42));
    check_not_sup((locus_key()), (locus_key() + 42));
    check_not_sup((locus_key()), (locus_key() + 23 + 42.));
    check_eq((locus_key()), (locus_key()));
    check_neq((locus_key() + 43), (locus_key()));
    check_neq((locus_key() + 22 + 42), (locus_key()));
}



int fibo(int n) { return n <= 1 ? 1 : fibo(n - 1) + fibo(n - 2); }


int n_fibo = 0;

int compute_fibo(int n)
{
    ++n_fibo;
    return fibo(n);
}


TEST_CASE("concurrent_disk", "check that concurrent disk tasks don't interfere")
{
    settings_fixture _(bc_conf);
    int n_max = 35;
    std::vector<int> all_same;
    for (int i = 0; i < 20; ++i) {
        all_same.push_back(n_max);
    }

    auto coll = make_collection<Disk>(compute_fibo, values_of(all_same));
    REQUIRE(coll.size() == 20);
    for (auto& x: coll) { (void) *x; }
    CHECK(n_fibo == 1);
    auto val = make_value<Disk>(compute_fibo, value<int>{n_max});
    for (auto& x: coll) {
        CHECK(x == val);
    }
}


double ambiguous(double x) { return x; }
double ambiguous(double x, double y) { return x * y; }

TEST_CASE("overloaded task", "check that the cache systems handles overloaded functions properly")
{
    settings_fixture _(bc_conf);
    auto val = make_value<CachingPolicy::Sync, double, double>(ambiguous, value<double>{23.});
    CHECK(*val == 23.);
    val = make_value<CachingPolicy::Sync, double, double, double>(ambiguous, value<double>{23.}, value<double>{42.});
    CHECK(*val == 966.);
}

