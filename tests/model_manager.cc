#include "catch.hpp"

#include "io/error.h"
#include "math_objects/labelled_matrix.h"
#include "cache2.h"
#include "settings.h"
#include "computations/base.h"
#include "computations/basic_data.h"
#include "computations/probabilities.h"
#include "computations/model.h"
#include "../attic/frontends2.h"

#include "settings_fixture.h"

TEST_CASE("model.manager.1", "Check consistency of product/joint probability mode switch")
{
    settings_fixture _(bc_conf);
    model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                     1.e-22, ComputationType::FTest, SolverType::QR);
    mm.init_loci_by_marker_list({{"ch1", {"M_1_1", "M_1_5", "M_1_11"}}});
    mm.select_chromosome("ch1");
    mm.add_all_loci();
    mm.cofactors_to_QTLs();
    model M1 = mm.Mcurrent;
    mm.QTLs_to_cofactors();
    mm.cofactors_to_QTLs();
    model M2 = mm.Mcurrent;
    CHECK(M2 == M1);
    REQUIRE(M2.m_blocks.size() == M1.m_blocks.size());
    auto i1 = M1.m_blocks.begin(), j1 = M1.m_blocks.end();
    auto i2 = M2.m_blocks.begin();
    while (i1 != j1) {
        CHECK(i1->second == i2->second);
        ++i1; ++i2;
    }
}


void check_models_equal(const std::string& m1_name, const model& M1, const std::string& m2_name, const model& M2)
{
    CHECK(M2 == M1);
    CHECK(M2.m_blocks.size() == 2);
    REQUIRE(M2.m_blocks.size() == M1.m_blocks.size());
    auto i1 = M1.m_blocks.begin(), j1 = M1.m_blocks.end();
    auto i2 = M2.m_blocks.begin();
    bool ok = true;
    while (i1 != j1) {
        REQUIRE(i1->second->column_labels.size() == i2->second->column_labels.size());
        auto l1 = i1->second->column_labels.begin();
        auto l2 = i2->second->column_labels.begin();
        auto e1 = i1->second->column_labels.end();
        while (l1 != e1) {
            std::string L1(l1->begin(), l1->end());
            std::string L2(l2->begin(), l2->end());
            CHECK(L1 == L2);
            ++l1; ++l2;
        }
        CHECK(i1->second->data.isApprox(i2->second->data));
        if (!i1->second->data.isApprox(i2->second->data)) {
            /*CHECK(i1->second->data == i2->second->data);*/
            /*MSG_DEBUG(i1->second->data - i2->second->data);*/
            MSG_DEBUG(m1_name << std::endl << M1);
            MSG_DEBUG(m2_name << std::endl << M2);
            ok = false;
        }
        ++i1; ++i2;
    }
    if (ok) {
        MSG_INFO("Models " << m1_name << " and " << m2_name << " are equal");
    }
}


void run_test_remove(const locus_key& lk_base, double loc_remove)
{
    settings_fixture _(bc_conf);
    model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                     1.e-22, ComputationType::FTest, SolverType::QR);
    mm.select_chromosome("ch1");
    /*auto chr = mm.chrom_under_study;*/
    mm.pmode = Joint;
    locus_key lk;
    lk = lk_base;
    auto lk2 = lk - loc_remove;
    mm.set_selection(lk2);
    model M2 = mm.Mcurrent;
    mm.set_selection(lk);
    auto mbk = mm.remove(loc_remove);
    model M1 = mm.Mcurrent;

    check_models_equal("M1 (remove)", M1, "M2 (construct)", M2);
}


TEST_CASE("mash.probabilities.0", "Track a weird bug where some probabilities may be overwritten or something")
{
    settings_fixture _(bc_conf);
    model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                     1.e-22, ComputationType::FTest, SolverType::QR);
    mm.select_chromosome("ch1");
    /*auto chr = mm.chrom_under_study;*/
    mm.pmode = Joint;

    mm.set_selection(locus_key() + 42);
    model M2 = mm.Mcurrent;
    M2.X();
    mm.set_selection(locus_key() + 23);
    model M1 = mm.Mcurrent;
    M1.X();
    CHECK(M1.X() != M2.X());

    CHECK_FALSE(M2 == M1);
    CHECK(M2.m_blocks.size() == 2);
    REQUIRE(M2.m_blocks.size() == M1.m_blocks.size());
    auto i1 = M1.m_blocks.begin();
    auto i2 = M2.m_blocks.begin();
    ++i1; ++i2;
    REQUIRE(i1->second->column_labels.size() == i2->second->column_labels.size());
    auto l1 = i1->second->column_labels.begin();
    auto l2 = i2->second->column_labels.begin();
    auto e1 = i1->second->column_labels.end();
    while (l1 != e1) {
        std::string L1(l1->begin(), l1->end());
        std::string L2(l2->begin(), l2->end());
        CHECK(L1 == L2);
        ++l1; ++l2;
    }
    CHECK(!i1->second->data.isApprox(i2->second->data, 1.e-5));
    if (i1->second->data.isApprox(i2->second->data, 1.e-5)) {
        /*CHECK(i1->second->data == i2->second->data);*/
        MSG_DEBUG(i1->second->data - i2->second->data);
        MSG_DEBUG("M1 (remove)" << std::endl << M1);
        MSG_DEBUG("M2 (construct)" << std::endl << M2);
    }
}


TEST_CASE("mash.probabilities.1", "Track a weird bug where some probabilities may be overwritten or something")
{
    settings_fixture _(bc_conf);
    model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                     1.e-22, ComputationType::FTest, SolverType::QR);
    mm.select_chromosome("ch1");
    /*auto chr = mm.chrom_under_study;*/
    mm.pmode = Joint;

    mm.set_selection(locus_key() + 23);
    model M1 = mm.Mcurrent;
    mm.set_selection(locus_key() + 42);
    model M2 = mm.Mcurrent;

    CHECK_FALSE(M2 == M1);
    CHECK(M2.m_blocks.size() == 2);
    REQUIRE(M2.m_blocks.size() == M1.m_blocks.size());
    auto i1 = M1.m_blocks.begin();
    auto i2 = M2.m_blocks.begin();
    ++i1; ++i2;
    REQUIRE(i1->second->column_labels.size() == i2->second->column_labels.size());
    auto l1 = i1->second->column_labels.begin();
    auto l2 = i2->second->column_labels.begin();
    auto e1 = i1->second->column_labels.end();
    while (l1 != e1) {
        std::string L1(l1->begin(), l1->end());
        std::string L2(l2->begin(), l2->end());
        CHECK(L1 == L2);
        ++l1; ++l2;
    }
    CHECK(!i1->second->data.isApprox(i2->second->data, 1.e-5));
    if (i1->second->data.isApprox(i2->second->data, 1.e-5)) {
        /*CHECK(i1->second->data == i2->second->data);*/
        MSG_DEBUG(i1->second->data - i2->second->data);
        MSG_DEBUG("M1 (remove)" << std::endl << M1);
        MSG_DEBUG("M2 (construct)" << std::endl << M2);
    }
}


TEST_CASE("mash.probabilities.2", "Track a weird bug where some probabilities may be overwritten or something")
{
    settings_fixture _(bc_conf);
    model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                     1.e-22, ComputationType::FTest, SolverType::QR);
    model_manager mm2(all_populations(), trait_matrix("trait1", all_populations()),
                      1.e-22, ComputationType::FTest, SolverType::QR);
    mm.select_chromosome("ch1");
    mm2.select_chromosome("ch1");
    /*auto chr = mm.chrom_under_study;*/
    mm.pmode = Joint;
    mm2.pmode = Joint;

    mm.set_selection(locus_key() + 23);
    model M1 = mm.Mcurrent;
    mm2.set_selection(locus_key() + 42);
    model M2 = mm2.Mcurrent;

    CHECK_FALSE(M2 == M1);
    CHECK(M2.m_blocks.size() == 2);
    REQUIRE(M2.m_blocks.size() == M1.m_blocks.size());
    auto i1 = M1.m_blocks.begin();
    auto i2 = M2.m_blocks.begin();
    ++i1; ++i2;
    REQUIRE(i1->second->column_labels.size() == i2->second->column_labels.size());
    auto l1 = i1->second->column_labels.begin();
    auto l2 = i2->second->column_labels.begin();
    auto e1 = i1->second->column_labels.end();
    while (l1 != e1) {
        std::string L1(l1->begin(), l1->end());
        std::string L2(l2->begin(), l2->end());
        CHECK(L1 == L2);
        ++l1; ++l2;
    }
    CHECK(!i1->second->data.isApprox(i2->second->data, 1.e-5));
    if (i1->second->data.isApprox(i2->second->data, 1.e-5)) {
        /*CHECK(i1->second->data == i2->second->data);*/
        MSG_DEBUG(i1->second->data - i2->second->data);
        MSG_DEBUG("M1 (remove)" << std::endl << M1);
        MSG_DEBUG("M2 (construct)" << std::endl << M2);
    }
}


TEST_CASE("mash.probabilities.3", "Track a weird bug where some probabilities may be overwritten or something")
{
    model M1, M2;
    settings_fixture _(bc_conf);
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.set_selection(locus_key() + 18);
        M1 = mm.Mcurrent;
    }
    {
        model_manager mm2(all_populations(), trait_matrix("trait1", all_populations()),
                          1.e-22, ComputationType::FTest, SolverType::QR);
        mm2.select_chromosome("ch1");
        mm2.pmode = Joint;
        mm2.set_selection(locus_key() + 75);
        M2 = mm2.Mcurrent;
    }


    CHECK_FALSE(M2 == M1);
    CHECK(M2.m_blocks.size() == 2);
    REQUIRE(M2.m_blocks.size() == M1.m_blocks.size());
    auto i1 = M1.m_blocks.begin();
    auto i2 = M2.m_blocks.begin();
    ++i1; ++i2;
    REQUIRE(i1->second->column_labels.size() == i2->second->column_labels.size());
    auto l1 = i1->second->column_labels.begin();
    auto l2 = i2->second->column_labels.begin();
    auto e1 = i1->second->column_labels.end();
    while (l1 != e1) {
        std::string L1(l1->begin(), l1->end());
        std::string L2(l2->begin(), l2->end());
        CHECK(L1 == L2);
        ++l1; ++l2;
    }
    CHECK(!i1->second->data.isApprox(i2->second->data, 1.e-5));
    if (i1->second->data.isApprox(i2->second->data, 1.e-5)) {
        /*CHECK(i1->second->data == i2->second->data);*/
        MSG_DEBUG(i1->second->data - i2->second->data);
        MSG_DEBUG("M1 (remove)" << std::endl << M1);
        MSG_DEBUG("M2 (construct)" << std::endl << M2);
    }
}


TEST_CASE("model.manager.2L.1", "Check consistency of remove(loc) with 2->1 loci")
{
    run_test_remove(locus_key() + 23 + 42, 23);
}

TEST_CASE("model.manager.2L.2", "Check consistency of remove(loc) with 2->1 loci")
{
    run_test_remove(locus_key() + 22 + 23, 22);
}

TEST_CASE("model.manager.2R.1", "Check consistency of remove(loc) with 2->1 loci")
{
    run_test_remove(locus_key() + 23 + 42, 42);
}

TEST_CASE("model.manager.2R.2", "Check consistency of remove(loc) with 2->1 loci")
{
    run_test_remove(locus_key() + 2 + 42, 42);
}

TEST_CASE("model.manager.3L", "Check consistency of remove(loc) with 3->2 loci")
{
    run_test_remove(locus_key() + 25 + 45 + 85, 25);
}

TEST_CASE("model.manager.3C", "Check consistency of remove(loc) with 3->2 loci")
{
    run_test_remove(locus_key() + 24 + 44 + 84, 44);
}

TEST_CASE("model.manager.3R", "Check consistency of remove(loc) with 3->2 loci")
{
    run_test_remove(locus_key() + 26 + 46 + 86, 86);
}

TEST_CASE("probability.order.1", "Check the order of the addition of loci has no effect")
{
    settings_fixture _(bc_conf);
    model M1, M2;
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, 20.);
        mm.add(mm.chrom_under_study, 40.);
        M1 = mm.Mcurrent;
        M1.compute();
    }
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, 40.);
        mm.add(mm.chrom_under_study, 20.);
        M2 = mm.Mcurrent;
        M2.compute();
    }

    check_models_equal("M1 20-40", M1, "M2 40-20", M2);
}

TEST_CASE("probability.order.2", "Check the order of the addition of loci has no effect")
{
    settings_fixture _(bc_conf);
    model M1, M2, M3;
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, 20.);
        mm.add(mm.chrom_under_study, 40.);
        mm.add(mm.chrom_under_study, 60.);
        M1 = mm.Mcurrent;
        M1.compute();
    }
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, 40.);
        mm.add(mm.chrom_under_study, 60.);
        mm.add(mm.chrom_under_study, 20.);
        M2 = mm.Mcurrent;
        M2.compute();
    }
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, 60.);
        mm.add(mm.chrom_under_study, 40.);
        mm.add(mm.chrom_under_study, 20.);
        M3 = mm.Mcurrent;
        M3.compute();
    }

    std::string m1 = "M1 20-40-60";
    std::string m2 = "M2 40-60-20";
    std::string m3 = "M3 60-40-20";

    check_models_equal(m1, M1, m2, M2);
    check_models_equal(m1, M1, m3, M3);
    check_models_equal(m3, M3, m2, M2);
}

TEST_CASE("probability.order.3", "Check the order of the addition of loci has no effect")
{
    settings_fixture _(bc_conf);
    double l1 = active_settings->map[0].raw.locus_by_name("M_1_2");
    double l2 = active_settings->map[0].raw.locus_by_name("M_1_4");
    double l3 = active_settings->map[0].raw.locus_by_name("M_1_6");
    model M123, M132, M213, M231, M312, M321;
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, l1);
        mm.add(mm.chrom_under_study, l2);
        mm.add(mm.chrom_under_study, l3);
        M123 = mm.Mcurrent;
        M123.compute();
    }
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, l1);
        mm.add(mm.chrom_under_study, l3);
        mm.add(mm.chrom_under_study, l2);
        M132 = mm.Mcurrent;
        M132.compute();
    }
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, l2);
        mm.add(mm.chrom_under_study, l1);
        mm.add(mm.chrom_under_study, l3);
        M213 = mm.Mcurrent;
        M213.compute();
    }
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, l2);
        mm.add(mm.chrom_under_study, l3);
        mm.add(mm.chrom_under_study, l1);
        M231 = mm.Mcurrent;
        M231.compute();
    }
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, l3);
        mm.add(mm.chrom_under_study, l1);
        mm.add(mm.chrom_under_study, l2);
        M312 = mm.Mcurrent;
        M312.compute();
    }
    {
        model_manager mm(all_populations(), trait_matrix("trait1", all_populations()),
                         1.e-22, ComputationType::FTest, SolverType::QR);
        mm.select_chromosome("ch1");
        mm.pmode = Joint;
        mm.add(mm.chrom_under_study, l3);
        mm.add(mm.chrom_under_study, l2);
        mm.add(mm.chrom_under_study, l1);
        M321 = mm.Mcurrent;
        M321.compute();
    }

    std::string m123 = "M1 l1-l2-l3";
    std::string m132 = "M1 l1-l3-l2";
    std::string m213 = "M2 l2-l1-l3";
    std::string m231 = "M2 l2-l3-l1";
    std::string m312 = "M3 l3-l1-l2";
    std::string m321 = "M3 l3-l2-l1";

#define CME(x, y) check_models_equal(m ## x, M ## x, m ## y, M ## y)
    CME(123, 132);
    CME(123, 213);
    CME(123, 231);
    CME(123, 312);
    CME(123, 321);

    CME(132, 213);
    CME(132, 231);
    CME(132, 312);
    CME(132, 321);

    CME(213, 231);
    CME(231, 312);
    CME(231, 321);

    CME(312, 321);
}

TEST_CASE("locus_key.locus_order", "Check validity of locus_key->locus_order(locus)")
{
    CHECK((locus_key() + 20 + 40)->locus_order(40) == 0);
    CHECK((locus_key() + 20 + 40)->locus_order(20) == 1);
    CHECK((locus_key() + 20 + 40)->locus_order(30) == 1);
    CHECK((locus_key() + 20 + 40)->locus_order(10) == 2);
    CHECK((locus_key() + 20 + 40)->locus_order(60) == 0);
}

