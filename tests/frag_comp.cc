#define THIS_IS_NOT_A_TEST
#ifndef THIS_IS_NOT_A_TEST

#include "catch.hpp"
#define SPELL_UNSAFE_OUTPUT
#include "markov_population.h"
#include "input.h"
#include "input/invoke_probapop.h"
#include "probapop_dtd.h"
#include <exception>

#include "generation_rs.h"
#include "outbred.h"
#include "banner.h"
#include "chrono.h"
#include "data/genoprob_computer.h"

const char* pop_name = "f7";

template <typename V1, typename V2>
double delta(const V1& v1, const V2& v2)
{
    /*if (v1.innerSize() == 2) {*/
        /*Eigen::VectorXd tmp(4);*/
        /*tmp(0) = v1(0);*/
        /*tmp(3) = v1(1);*/
        /*tmp(1) = tmp(2) = 0;*/
        /*return acos(tmp.dot(v2) / (tmp.norm() * v2.norm()));*/
    /*}*/
    REQUIRE(v1.innerSize() == v2.innerSize());
#if 0
    double ret = acos(v1.dot(v2) / (v1.norm() * v2.norm()));
    return std::isnan(ret) ? 0 : ret;
#else
    MatrixXd diff = v1 - v2;
    return diff.lpNorm<Eigen::Infinity>();
#endif
}


void test_segment(const std::vector<const char*>& args);
std::pair<chromosome, marker_data> from_argv(int argc, const char* const* argv, double& step);
generation_rs* select_pop(const std::string& pop);
void check_delta(const std::string& pop_type, double locus, double error, double threshold, int argc, const char* const* argv);


std::pair<size_t, double> test_segment_frag(int argc, const char* const* argv)
{
    std::vector<std::pair<char, std::vector<allele_pair>>> obs_spec = {
        {'A', {{'a', 'a'}}},
        {'B', {{'b', 'b'}}},
        {'H', {{'a', 'b'}, {'b', 'a'}}},
        {'-', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}}},
        {'C', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}}},
        {'D', {{'b', 'b'}, {'a', 'b'}, {'b', 'a'}}}
    };

    double step;
    chrono::start("read args");
    std::pair<chromosome, marker_data> data = from_argv(argc, argv, step);
    chrono::stop("read args");
    chromosome& chr1 = data.first;

    chrono::start("new[select_pop]");
    generation_rs& gen = *select_pop(data.second.data_type);
    chrono::stop("new[select_pop]");

    population pop;

    pop.name = "toto";
    pop.observed_mark[gen.name].filename = "<nada>";
    pop.observed_mark[gen.name].observations = data.second;
    pop.observed_mark[gen.name].compute_observation_vectors(&gen, obs_spec);

    chrono::start("test_probapop");
    labelled_matrix<MatrixXd, double, double> pp = test_probapop(chr1, data.second, step);
    chrono::stop("test_probapop");

    chrono::start("new[compute_over_segment]");
    auto LV = multi_generation_observations(&pop, &chr1, &gen, 0).LV;
    qtl_chromosome qc(&chr1);
    generation_rs::segment_computer_t sc = gen.segment_computer(&qc, step, 0);

    auto output = sc.compute(LV).transpose();
    if (argv[2][0] == 'f' && argv[2][1] != '2') {
        output.data *= (MatrixXd(4, 4) << 1, 0, 0, 0,
                                          0, 0, 0, 1,
                                          0, 0, 1, 0,
                                          0, 1, 0, 0).finished();
    }
    /*output = sc.compute(obs, obs_spec).transpose();*/
    chrono::stop("new[compute_over_segment]");

    double delta_max = 0;
    size_t i_max = 0;

    for (size_t i = 0; i < pp.outerSize(); ++i) {
        double d = delta(output.data.row(i), pp.data.col(i));
        if (d > delta_max) {
            delta_max = d;
            i_max = i;
        }
    }

    check_delta(data.second.data_type, output.row_labels[i_max], delta_max, 1.e-5, argc - 3, argv + 3);

    return {i_max, delta_max};
}


std::pair<std::pair<size_t, double>, std::vector<const char*>> test_segment_frag(const std::vector<const char*>& args)
{
    return {test_segment_frag(args.size(), &args[0]), args};
}



TEST_CASE("frag_suce_des_ours.1", "Et PLOS-ONE aussi...")
{
    test_segment_frag({"", "4.2", pop_name, "M0", "A", "21.1", "M1", "A", "52.3", "M2", "A"});
}

TEST_CASE("frag_suce_des_ours.2", "Et PLOS-ONE aussi...")
{
    test_segment_frag({"", "4.2", pop_name, "M0", "A", "21.1", "M1", "A", "52.3", "M2", "B"});
}

TEST_CASE("frag_suce_des_ours.3", "Et PLOS-ONE aussi...")
{
    test_segment_frag({"", "4.2", pop_name, "M0", "H", "21.1", "M1", "A", "52.3", "M2", "B"});
}

TEST_CASE("frag_suce_des_ours.4", "Et PLOS-ONE aussi...")
{
    test_segment_frag({"", "4.2", pop_name, "M0", "A", "20", "M1", "A", "50", "M2", "A", "20", "M3", "A"});
}

TEST_CASE("frag_suce_des_ours.5", "Et PLOS-ONE aussi...")
{
    test_segment_frag({"", "4.2", pop_name, "M0", "A", "20", "M1", "A", "50", "M2", "A", "20", "M3", "B"});
}

TEST_CASE("frag_suce_des_ours.6", "Et PLOS-ONE aussi...")
{
    test_segment_frag({"", "1", pop_name, "M0", "H", "20", "M1", "A", "50", "M2", "H"});
}

TEST_CASE("frag_suce_des_ours.7", "Et PLOS-ONE aussi...")
{
    test_segment_frag({"", "1", pop_name, "M0", "H", "20", "M1", "A", "20", "M11", "B", "20", "M3", "H"});
}


TEST_CASE("max_err_frag", "Tralala ploum ploum tsoin tsoin")
{
    std::vector<const char*> m_obs = {"A", "H", "B"};
    std::vector<const char*> dist = {"20.", "30.", "50."};

    std::pair<std::pair<size_t, double>, std::vector<const char*>> best = {{0, 0.}, {}};

    for (auto o1: m_obs) {
        for (auto d1: dist) {
            for (auto o2: m_obs) {
                for (auto d2: dist) {
                    for (auto o3: m_obs) {
                        auto ret = test_segment_frag({"", "1", pop_name, "M1", o1, d1, "M2", o2, d2, "M3", o3});
                        if (ret.first.second > best.first.second) {
                            best = ret;
                        }
                    }
                }
            }
        }
    }

    test_segment_frag(best.second);
}


TEST_CASE("surfrag", "Tralala ploum ploum tsoin tsoin")
{
    std::vector<const char*> m_obs = {"A", "H", "B"};
    std::vector<std::string> m_dist;

    int n_steps = 50;
    double step = .5 / n_steps;

    MatrixXd mat(n_steps, n_steps);

    for (int i = 1; i < n_steps; ++i) {
        std::stringstream d1;
        d1 << inverse_prob_recomb(i * step);
        m_dist.push_back(d1.str());
    }
    {
        std::stringstream d1;
        d1 << inverse_prob_recomb(.49);
        m_dist.push_back(d1.str());
    }
    std::cout << "distances[" << m_dist.size() << ']';
    for (const auto& d: m_dist) {
        std::cout << " " << d;
    }
    std::cout << std::endl;

    for (int i = 0; i < n_steps; ++i) {
        const char* d1 = m_dist[i].c_str();
        for (int j = 0; j < n_steps; ++j) {
            const char* d2 = m_dist[j].c_str();
            std::pair<std::pair<size_t, double>, std::vector<const char*>> best = {{0, 0.}, {}};
            for (auto o1: m_obs) {
                for (auto o2: m_obs) {
                    for (auto o3: m_obs) {
                        auto ret = test_segment_frag({"", "1", pop_name, "M1", o1, d1, "M2", o2, d2, "M3", o3});
                        if (ret.first.second > best.first.second) {
                            best = ret;
                        }
                    }
                }
            }
            mat(i, j) = best.first.second;
        }
    }

    std::cout << mat << std::endl;
}

#endif

