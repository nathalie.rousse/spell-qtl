#include "catch.hpp"
#include "markov_population.h"
#include "input.h"
#include "../attic/invoke_probapop.h"
#include "../attic/probapop_dtd.h"
#include <exception>

#include "generation_rs.h"
#include "outbred.h"
#include "io/banner.h"
#include "util/chrono.h"
#include "data/genoprob_computer.h"

/*#include "invoke_probapop.h"*/

#if 0

Eigen::IOFormat genomatrix_fmt(/*precision*/StreamPrecision, /*flags*/DontAlignCols,
                               /*coef_sep*/" ", /*row_sep*/"\n",
                               /*row_pfx*/"", /*row_sfx*/"",
                               /*mat_pfx*/"", /*mat_sfx*/"");


void convert_ril_distances(std::vector<double>& loci, std::vector<double>& steps)
{
    std::vector<double> new_steps, new_loci;
    double base = 0;
    double last = 0;
    for (auto l: loci) {
        base = base + dist_RIL_to_additive(l - last);
        last = l;
        new_loci.push_back(base);
    }
    size_t l = 0;
    size_t s;
    for (s = 0; s < (steps.size() - 1); ++s) {
        if (steps[s] > loci[l + 1]) {
            ++l;
        }
        new_steps.push_back(new_loci[l] + dist_RIL_to_additive(steps[s] - loci[l]));
    }
    new_steps.push_back(new_loci.back());
    loci.swap(new_loci);
    steps.swap(new_steps);
}


template <typename V1, typename V2>
double delta(const V1& v1, const V2& v2)
{
    /*if (v1.innerSize() == 2) {*/
        /*Eigen::VectorXd tmp(4);*/
        /*tmp(0) = v1(0);*/
        /*tmp(3) = v1(1);*/
        /*tmp(1) = tmp(2) = 0;*/
        /*return acos(tmp.dot(v2) / (tmp.norm() * v2.norm()));*/
    /*}*/
    REQUIRE(v1.innerSize() == v2.innerSize());
#if 0
    double ret = acos(v1.dot(v2) / (v1.norm() * v2.norm()));
    return std::isnan(ret) ? 0 : ret;
#else
    MatrixXd diff = v1 - v2;
    return diff.lpNorm<Eigen::Infinity>();
#endif
}


template <typename OutputType>
OutputType to(const char* x)
{
    std::stringstream s(x);
    OutputType ret;
    s >> ret;
    return ret;
}

std::pair<chromosome, marker_data> from_argv(int argc, const char* const* argv, double& step)
{
    chromosome chr;
    int i = 1;
    step = to<double>(argv[i++]);
    chr.name = "chr1";
    marker_data md;
    md.data_type =argv[i++];
    md.n_obs = 1;
    chr.raw.marker_name.push_back(argv[i++]);
    chr.raw.marker_locus.push_back(0);
    md.data[chr.raw.marker_name.back()] = argv[i++];
    /*if (md.data_type == "ril") {*/
        /*step = dist_RIL_to_additive(step);*/
    /*}*/
    for (; i < argc;) {
        double d = to<double>(argv[i++]);
        chr.raw.marker_name.push_back(argv[i++]);
        md.data[chr.raw.marker_name.back()] = argv[i++];
        /*if (md.data_type == "ril") {*/
            /*d = dist_RIL_to_additive(d);*/
        /*}*/
        chr.raw.marker_locus.push_back(chr.raw.marker_locus.back() + d);
    }
    /*for (auto& l: chr.marker_locus) {*/
        /*l = dist_RIL_to_additive(l);*/
    /*}*/
    md.n_mark = md.data.size();
    chr.compute_haplo_sizes();
    return {chr, md};
}


std::pair<chromosome, marker_data> from_argv_outbred(int argc, const char* const* argv, double& step)
{
    int i = 1;
    step = to<double>(argv[i++]);
    ++i;
    std::ifstream chrif(argv[i++]);
    chromosome chr = read_data::read_map(chrif).front();
    std::ifstream markif(argv[i++]);
    marker_data md = read_data::read_outbred(markif);
    chr.name = argv[i++]; /* ATTENTION C'EST LE CFG ! */
    chr.compute_haplo_sizes();
    return {chr, md};
}

#if 1
#endif




generation_rs* select_pop(const std::string& pop)
{
    generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});
    generation_rs* B = generation_rs::ancestor("B", 'b', {{0, 0}});
    /*std::cout << "Pop type: " << pop << std::endl;*/
    if (pop == "cp") {
        generation_rs* C = generation_rs::ancestor("C", 'c', {{0, 0}});
        generation_rs* D = generation_rs::ancestor("D", 'd', {{0, 0}});
        return A->crossing("AB", B)->crossing("CP", C->crossing("CD", D));
    }
    generation_rs* F1 = A->crossing("F1", B);
    if (pop == "bc") {
        return F1->crossing("BC", A);
    }
    if (pop == "bc2") {
        return F1->crossing("BC", A)->crossing("BC2", A)->selfing("BC2s1");
    }
    if (pop == "dh") {
        return F1->to_doubled_haploid("DH");
    }
    if (pop == "ril") {
        return F1->to_ril("RIL");
    }
    if (pop[0] == 'f') {
        std::stringstream s(pop);
        char _;
        int i;
        s >> _ >> i;
        generation_rs* ret = F1;
        for (int n = 2; n <= i; ++n) {
            std::stringstream popname;
            popname << "F" << n;
            ret = ret->selfing(popname.str());
        }
        return ret;
    }
    throw "unknown pop";
}

#if 0
bool compare(const std::string& aname, const std::vector<double>& a,
             const std::string& bname, const std::vector<double>& b)
{
    bool ok = true;
    /*std::cout << "###############################################" << std::endl*/
              /*<< "Comparing " << aname << " and " << bname << std::endl;*/
    if (a.size() < b.size()) {
        std::cout << aname << " is shorter (" << a.size() << ") than " << bname << " (" << b.size() << ')' << std::endl;
        ok = false;
    } else if (a.size() > b.size()) {
        std::cout << bname << " is shorter (" << b.size() << ") than " << aname << " (" << a.size() << ')' << std::endl;
        ok = false;
    }
    auto ai = a.begin();
    auto aj = a.end();
    auto bi = b.begin();
    auto bj = b.end();
    bool nl = false;
    while (ai != aj && bi != bj) {
        if (abs(*ai - *bi) < LOCUS_EPSILON) {
            /*std::cout << (*ai) << ' ';*/
            ++ai;
            ++bi;
            nl = true;
        } else if (*ai < *bi) {
            if (nl) {
                nl = false;
                /*std::cout << std::endl;*/
            }
            std::cout << "only in " << aname << ": " << (*ai) << " at #" << (ai - a.begin())
                      << " vs " << (*bi) << " in " << bname << " at #" << (bi - b.begin())
                      << std::endl;
            ++ai;
            ok = false;
        } else if (*ai > *bi) {
            if (nl) {
                nl = false;
                /*std::cout << std::endl;*/
            }
            std::cout << "only in " << bname << ": " << (*bi) << " at #" << (bi - b.begin())
                      << " vs " << (*ai) << " in " << aname << " at #" << (ai - a.begin())
                      << std::endl;
            ++bi;
            ok = false;
        }
    }
    if (nl) {
        nl = false;
        std::cout << std::endl;
    }
    if (ai != aj) {
        std::cout << (aj - ai) << " elements left in " << aname << std::endl;
    } else if (bi != bj) {
        std::cout << (bj - bi) << " elements left in " << bname << std::endl;
    }
    return ok;
}
#endif

std::string argv2str(int argc, const char* const* argv)
{
    std::stringstream t;
    t << (*argv);
    for (int i = 1; i < argc; ++i) { t << ' ' << argv[i]; }
    return t.str();
}


void check_delta(const std::string& pop_type, double locus, double error, double threshold, int argc, const char* const* argv)
{
    if (error >= threshold) {
        std::stringstream s;
        s << '[' << pop_type << "] at " << locus << " excessive error " << error << " >= " << threshold;
        std::string msg(s.str());
        std::string testcase = argv2str(argc, argv);
        CHECK(msg == testcase);
    } else {
        CHECK(true);
    }
}



int test_segment(int argc, const char* const* argv)
{
    std::vector<std::pair<char, std::vector<allele_pair>>> obs_spec = {
        {'A', {{'a', 'a'}}},
        {'B', {{'b', 'b'}}},
        {'H', {{'a', 'b'}, {'b', 'a'}}},
        {'-', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}, {'b', 'b'}}},
        {'C', {{'a', 'a'}, {'a', 'b'}, {'b', 'a'}}},
        {'D', {{'b', 'b'}, {'a', 'b'}, {'b', 'a'}}}
    };

    double step;
    chrono::start("read args");
    std::pair<chromosome, marker_data> data = from_argv(argc, argv, step);
    chrono::stop("read args");
    chromosome& chr1 = data.first;

    chrono::start("new[select_pop]");
    generation_rs& gen = *select_pop(data.second.data_type);
    chrono::stop("new[select_pop]");

    population pop;

    pop.name = "toto";
    pop.observed_mark[gen.name].filename = "<nada>";
    pop.observed_mark[gen.name].observations = data.second;
    pop.observed_mark[gen.name].compute_observation_vectors(&gen, obs_spec);

    chrono::start("test_probapop");
    labelled_matrix<MatrixXd, double, double> pp = test_probapop(chr1, data.second, step);
    chrono::stop("test_probapop");

    chrono::start("new[compute_over_segment]");
    auto LV = multi_generation_observations(&pop, &chr1, &gen, 0).LV;
    qtl_chromosome qc(&chr1);
    generation_rs::segment_computer_t sc = gen.segment_computer(&qc, step, 0);

    auto output = sc.compute(LV).transpose();
    if (argv[2][0] == 'f' && argv[2][1] != '2') {
        output.data *= (MatrixXd(4, 4) << 1, 0, 0, 0,
                                          0, 0, 0, 1,
                                          0, 0, 1, 0,
                                          0, 1, 0, 0).finished();
    }
    /*output = sc.compute(obs, obs_spec).transpose();*/
    chrono::stop("new[compute_over_segment]");

    for (size_t i = 0; i < pp.outerSize(); ++i) {
        check_delta(data.second.data_type, output.row_labels[i], delta(output.data.row(i), pp.data.col(i)), 1.e-5, argc - 3, argv + 3);
    }

    return 0;
}


#if 1
int test_segment_outbred(int argc, const char* const* argv) {
    /*std::cout << "Testing pop_type=" << argv[2] << ": " << argv2str(argc - 3, argv + 3) << std::endl;*/
    allele_pair a0 = {'a', 'c'};
    allele_pair a1 = {'a', 'd'};
    allele_pair a2 = {'b', 'c'};
    allele_pair a3 = {'b', 'd'};
    std::vector<std::pair<char, std::vector<allele_pair>>> obs_spec = {
        {'0', {}},
        {'1', {a0}},
        {'2', {a1}},
        {'3', {a0, a1}},
        {'4', {a2}},
        {'5', {a2, a0}},
        {'6', {a2, a1}},
        {'7', {a2, a1, a0}},
        {'8', {a3}},
        {'9', {a3, a0}},
        {'a', {a3, a1}},
        {'b', {a3, a1, a0}},
        {'c', {a3, a2}},
        {'d', {a3, a2, a0}},
        {'e', {a3, a2, a1}},
        {'f', {a3, a2, a1, a0}}
    };

    double step;
    std::pair<chromosome, marker_data> data = from_argv_outbred(argc, argv, step);
    chromosome& chr1 = data.first;
    marker_data& md = data.second;
    generation_rs& gen = *select_pop(md.data_type);
    /*std::cout << pop << std::endl;*/

    population pop;

    pop.name = "toto";
    pop.observed_mark[gen.name].observations = md;
    pop.observed_mark[gen.name].compute_observation_vectors(&gen, obs_spec);

    std::vector<char> obs = md.get_obs(chr1.raw.marker_name.begin(), chr1.raw.marker_name.end(), 0);
    /*std::cout << "================================================================" << std::endl;*/
    /*std::cout << output << std::endl;*/
    /*std::cout << "################################################################" << std::endl;*/
    
    chrono::start("test_probapop");
    MatrixXd pp = test_probapop(chr1, md, step);
    chrono::stop("test_probapop");

    chrono::start("new[compute_over_segment]");
    auto LV = multi_generation_observations(&pop, &chr1, &gen, 0).LV;
    qtl_chromosome qc(&chr1);
    generation_rs::segment_computer_t sc = gen.segment_computer(&qc, step, 0);

    auto output = sc.compute(LV).transpose();
    /*std::cout << "cp LABELS " << output.column_labels << std::endl;*/
    /*std::cout << output.upperRows(10) << std::endl;*/
    output.data *= (MatrixXd(4, 4) << 1, 0, 0, 0,
                                      0, 0, 1, 0,
                                      0, 1, 0, 0,
                                      0, 0, 0, 1).finished();
    chrono::stop("new[compute_over_segment]");

    const char* args[] = {"see outbred file"};

    for (int i = 0; i < pp.outerSize(); ++i) {
        /*std::cout << output.data.row(i) << std::endl;*/
        /*std::cout << pp.col(i).transpose() << std::endl << std::endl;*/
        check_delta(md.data_type, output.row_labels[i], delta(output.data.row(i), pp.col(i)), 1.e-3, 1, args);
    }

    return 0;
}
#endif


void test_segment(const std::vector<const char*>& args)
{
    test_segment(args.size(), &args[0]);
}


void test_segment_outbred(const std::vector<const char*>& args)
{
    test_segment_outbred(args.size(), &args[0]);
}



#if 0
TEST_CASE("markov_3V", "Check equality with old Probapop with 3V")
{
    //        AA        AC        BB        BC        CC
    MatrixXd pp(65, 5);
    pp <<
        0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 
        0.001402, 0.000177, 0.883528, 0.111378, 0.003516, 
        0.002307, 0.000620, 0.774894, 0.208158, 0.014021, 
        0.002811, 0.001212, 0.673903, 0.290613, 0.031462, 
        0.002999, 0.001855, 0.580377, 0.358973, 0.055795, 
        0.002948, 0.002467, 0.494163, 0.413433, 0.086989, 
        0.002726, 0.002982, 0.415125, 0.454144, 0.125023, 
        0.002390, 0.003352, 0.343148, 0.481222, 0.169888, 
        0.001993, 0.003545, 0.278135, 0.494741, 0.221586, 
        0.001576, 0.003545, 0.220009, 0.494741, 0.280128, 
        0.001175, 0.003352, 0.168713, 0.481222, 0.345538, 
        0.000816, 0.002982, 0.124208, 0.454144, 0.417851, 
        0.000516, 0.002467, 0.086473, 0.413433, 0.497112, 
        0.000287, 0.001855, 0.055508, 0.358973, 0.583377, 
        0.000131, 0.001212, 0.031331, 0.290613, 0.676714, 
        0.000042, 0.000620, 0.013979, 0.208158, 0.777201, 
        0.000006, 0.000177, 0.003510, 0.111378, 0.884930, 
        0.000000, 0.000000, 0.000000, 0.000000, 1.000000, 
        0.000007, 0.005235, 0.000086, 0.067671, 0.927002, 
        0.000049, 0.020815, 0.000293, 0.124311, 0.854532, 
        0.000151, 0.046589, 0.000553, 0.170191, 0.782516, 
        0.000325, 0.082444, 0.000811, 0.205536, 0.710883, 
        0.000571, 0.128311, 0.001025, 0.230531, 0.639562, 
        0.000875, 0.184160, 0.001166, 0.245318, 0.568481, 
        0.001215, 0.250000, 0.001215, 0.250000, 0.497570, 
        0.001553, 0.325883, 0.001166, 0.244640, 0.426758, 
        0.001842, 0.411899, 0.001025, 0.229259, 0.355974, 
        0.002022, 0.508179, 0.000811, 0.203840, 0.285148, 
        0.002019, 0.614896, 0.000553, 0.168324, 0.214209, 
        0.001748, 0.732261, 0.000293, 0.122612, 0.143086, 
        0.001112, 0.860530, 0.000086, 0.066565, 0.071707, 
        0.000000, 1.000000, 0.000000, 0.000000, 0.000000, 
        0.003246, 0.940341, 0.000182, 0.052802, 0.003428, 
        0.005776, 0.882294, 0.000686, 0.104783, 0.006461, 
        0.007658, 0.825699, 0.001448, 0.156091, 0.009105, 
        0.008958, 0.770404, 0.002405, 0.206869, 0.011363, 
        0.009741, 0.716259, 0.003499, 0.257262, 0.013240, 
        0.010069, 0.663118, 0.004668, 0.307408, 0.014737, 
        0.010004, 0.610837, 0.005854, 0.357447, 0.015858, 
        0.009606, 0.559275, 0.006999, 0.407516, 0.016605, 
        0.008933, 0.508293, 0.008045, 0.457752, 0.016978, 
        0.008045, 0.457752, 0.008933, 0.508293, 0.016978, 
        0.006999, 0.407516, 0.009606, 0.559275, 0.016605, 
        0.005854, 0.357447, 0.010004, 0.610837, 0.015858, 
        0.004668, 0.307408, 0.010069, 0.663118, 0.014737, 
        0.003499, 0.257262, 0.009741, 0.716259, 0.013240, 
        0.002405, 0.206869, 0.008958, 0.770404, 0.011363, 
        0.001448, 0.156091, 0.007658, 0.825699, 0.009105, 
        0.000686, 0.104783, 0.005776, 0.882294, 0.006461, 
        0.000182, 0.052802, 0.003246, 0.940341, 0.003428, 
        0.000000, 0.000000, 0.000000, 1.000000, 0.000000, 
        0.000003, 0.001284, 0.002526, 0.993656, 0.002530, 
        0.000011, 0.002360, 0.004652, 0.988313, 0.004663, 
        0.000021, 0.003233, 0.006384, 0.983958, 0.006405, 
        0.000031, 0.003907, 0.007725, 0.980581, 0.007756, 
        0.000039, 0.004386, 0.008681, 0.978174, 0.008720, 
        0.000044, 0.004673, 0.009253, 0.976733, 0.009297, 
        0.000046, 0.004768, 0.009444, 0.976252, 0.009490, 
        0.000044, 0.004673, 0.009253, 0.976733, 0.009297, 
        0.000039, 0.004386, 0.008681, 0.978174, 0.008720, 
        0.000031, 0.003907, 0.007725, 0.980581, 0.007756, 
        0.000021, 0.003233, 0.006384, 0.983958, 0.006405, 
        0.000011, 0.002360, 0.004652, 0.988313, 0.004663, 
        0.000003, 0.001284, 0.002526, 0.993656, 0.002530, 
        0.000000, 0.000000, 0.000000, 1.000000, 0.000000; 

    std::vector<std::pair<char, std::vector<allele_pair>>> obs_spec = {
        {'1', {{'a', 'a'}}},
        {'2', {{'a', 'b'}}},
        /*{'3', {{'a', 'c'}}},*/
        {'3', {{'a', 'c'}, {'c', 'a'}}},
        {'4', {{'b', 'a'}}},
        {'5', {{'b', 'b'}}},
        /*{'6', {{'b', 'c'}}},*/
        {'6', {{'b', 'c'}, {'c', 'b'}}},
        {'7', {{'c', 'a'}}},
        {'8', {{'c', 'b'}}},
        {'9', {{'c', 'c'}}}
    };

    /*generation_rs* A = generation_rs::ancestor("A", 'a', {{0, 0}});*/
    /*generation_rs* B = generation_rs::ancestor("B", 'b', {{0, 0}});*/
    /*generation_rs* C = generation_rs::ancestor("C", 'c', {{0, 0}});*/
    /*generation_rs* F1 = A->crossing("AB_3V", B)->crossing("F1_3V", C);*/
    /*generation_rs* F2 = F1->selfing("F2_3V");*/
    /*std::cout << (*F1) << std::endl << (*F2) << std::endl;*/
    /*std::cout << "A " << A << std::endl;*/
    /*std::cout << "B " << B << std::endl;*/
    /*std::cout << "C " << C << std::endl;*/
    /*std::cout << "F1 " << F1 << std::endl;*/
    /*std::cout << "F2 " << F2 << std::endl;*/

#if 0
    multi_generation_observations mgo(5);
    chromosome chr1 = {
        "chr1",
        {"M1", "M2", "M3", "M4", "M5"},
        {0, 17, 17+14, 17+14+19, 17+14+19+14}
    };
    std::vector<char> obs_f1 = {'6', '6', '3', '6', '6'};
    std::vector<char> obs_f2 = {'5', '9', '3', '6', '6'};
    auto lvf1 = F1->observation_vectors(obs_spec);
    auto lvf2 = F2->observation_vectors(obs_spec);
    mgo[F1] = F1->raw_observations(lvf1, obs_f1);
    mgo[F2] = F2->raw_observations(lvf2, obs_f2);
    F2->update_locus_vectors(mgo);
    qtl_chromosome qc(&chr1);
    generation_rs::segment_computer_t sc = F2->segment_computer(&qc, 1., 0);
    std::cout << sc << std::endl;
    auto output = sc.compute(mgo[F2]).transpose();
    /*auto output = compute_over_segment(chr1.marker_locus, mgo[F2], 1., 0).transpose();*/
    std::cout << F2->name << " LABELS " << output.column_labels << std::endl;

    // aa ca ac cc bb cb bc

    CHECK(output.column_labels[0] == allele_pair({'a', 'a'}));
    CHECK(output.column_labels[1] == allele_pair({'c', 'a'}));
    CHECK(output.column_labels[2] == allele_pair({'a', 'c'}));
    CHECK(output.column_labels[3] == allele_pair({'c', 'c'}));
    CHECK(output.column_labels[4] == allele_pair({'b', 'b'}));
    CHECK(output.column_labels[5] == allele_pair({'c', 'b'}));
    CHECK(output.column_labels[6] == allele_pair({'b', 'c'}));

    MatrixXd out(65, 5);
    out.col(0) = output.data.col(0); /* aa */
    out.col(1) = output.data.col(1) + output.data.col(2); /* ac ca */
    out.col(2) = output.data.col(4); /* bb */
    out.col(3) = output.data.col(5) + output.data.col(6); /* bc cb */
    out.col(4) = output.data.col(3); /* cc */
    
    const char* args[] = {"hardcoded example; see code"};

    REQUIRE(out.innerSize() == pp.innerSize());
    REQUIRE(out.outerSize() == pp.outerSize());

    for (size_t i = 0; i < 65; ++i) {
        check_delta("3V", i, delta(out.row(i), pp.row(i)), 1.e-4, 1, args);
    }
#endif
}
#endif



// A B H A C D H D C B

TEST_CASE("markov_f2", "Check equality with old Probapop with F2")
{
    test_segment({"", ".1", "f2",
                "M1", "A",
        "23.4", "M2", "B",
        "12.3", "M3", "H",
        "13.2", "M4", "A",
        "12.5", "M5", "C",
        "1.1",  "M6", "-",
        "2.6",  "M7", "H",
        "3.8",  "M8", "D",
        "4.9",  "M9", "C",
        "9.8", "M10", "B"});
}

void test_fn(const char* fn)
{
    std::vector<const char*> obs = {"A", "H", "B"};
    for (auto o1: obs) {
        for (auto o2: obs) {
            test_segment({"", ".2", fn, "M1", o1, "23.4", "M2", o2});
        }
    }
}

TEST_CASE("markov_f3", "Check equality with old Probapop with F3") { test_fn("f3"); }
TEST_CASE("markov_f4", "Check equality with old Probapop with F4") { test_fn("f4"); }
TEST_CASE("markov_f5", "Check equality with old Probapop with F5") { test_fn("f5"); }
TEST_CASE("markov_f6", "Check equality with old Probapop with F6") { test_fn("f6"); }
TEST_CASE("markov_f7", "Check equality with old Probapop with F7") { test_fn("f7"); }

TEST_CASE("markov_bc", "Check equality with old Probapop with BC")
{
    test_segment({"", ".1", "bc",
                "M1", "A",
        "12.3", "M3", "H",
        "2.6",  "M7", "A",
        "9.8", "M10", "H"});
}

TEST_CASE("markov_dh", "Check equality with old Probapop with DH")
{
    test_segment({"", ".1", "dh",
                "M1", "A",
        "12.3", "M3", "B",
        "2.6",  "M7", "A",
        "9.8", "M10", "B"});
}

TEST_CASE("markov_ril", "Check equality with old Probapop with RIL")
{
    test_segment({"", "4.2", "ril", "M1", "A", "52.3", "M2", "-"});
    test_segment({"", "4.2", "ril", "M1", "B", "52.3", "M2", "-"});
    test_segment({"", "4.2", "ril", "M1", "-", "52.3", "M2", "-"});
}

TEST_CASE("markov_outbred", "Check equality with old Probapop with CP")
{
    test_segment_outbred({"", "10", "cp", "outbred_map.txt", "outbred.loc", "LM2TDA10D.cfg"});
}

void compare_vec(const std::vector<double>& loci, double step,
                 std::vector<std::pair<double, double>> excl,
                 const std::vector<double>& actual)
{
    auto computed = compute_steps(loci, step, excl);
    CHECK(computed.size() == actual.size());
    if (computed.size() != actual.size()) {
        std::cout << "loci " << loci << std::endl;
        std::cout << "step " << step << std::endl;
        std::cout << "excl " << excl << std::endl;
        std::cout << "computed " << computed << std::endl;
        std::cout << "actual " << actual << std::endl;
        return;
    }
    for (size_t i = 0; i < computed.size(); ++i) {
        if (computed[i] != actual[i]) {
            std::cout << "loci " << loci << std::endl;
            std::cout << "step " << step << std::endl;
            std::cout << "excl " << excl << std::endl;
            std::cout << "computed " << computed << std::endl;
            std::cout << "actual " << actual << std::endl;
        }
        CHECK(computed[i] == actual[i]);
    }
}


TEST_CASE("compute_steps", "Check the validity of compute_steps")
{
    std::vector<double> loci = {0., 5.5, 10};
    compare_vec(loci, 1., {}, {0., 1., 2., 3., 4., 5., 5.5, 6.5, 7.5, 8.5, 9.5, 10.});
    compare_vec(loci, 2.75, {},  {0., 2.75, 5.5, 8.25, 10.});
    compare_vec(loci, 1., {{2, 5}}, {0., 1., 5.5, 6.5, 7.5, 8.5, 9.5, 10.});
    compare_vec(loci, 1., {{2, 5}, {8., 20.}}, {0., 1., 5.5, 6.5, 7.5});
}
#endif

