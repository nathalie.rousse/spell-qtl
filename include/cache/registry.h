/* Spell-QTL  Software suite for the QTL analysis of modern datasets.
 * Copyright (C) 2016,2017  Damien Leroux <damien.leroux@inra.fr>, Sylvain Jasson <sylvain.jasson@inra.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef _SPEL_CACHE_REGISTRY_H_
#error "_SPEL_CACHE_REGISTRY_H_ already defined!"
#else
#define _SPEL_CACHE_REGISTRY_H_

#include <mutex>
#include <unordered_map>

template <typename ValueType, typename... AllArgs>
struct computation_registry {
    typedef ValueType value_type;

    template <typename... Args> struct registry_impl;

    template <typename Arg0>
        struct registry_impl<Arg0> {
            std::unordered_map<Arg0, value_type> m_registry;
            value_type& get_(const Arg0& arg)
            {
                /*MSG_DEBUG("size=" << size() << " finding " << arg << "... " << m_registry[arg]);*/
                return m_registry[arg];
            }

            bool remove_(const Arg0& car)
            {
                m_registry.erase(car);
                return m_registry.size() == 0;
            }

            value_type* find_(const Arg0& car)
            {
                auto it = m_registry.find(car);
                if (it == m_registry.end()) {
                    return NULL;
                }
                return &it->second;
            }

            size_t size() const { return m_registry.size(); }
        };

    template <typename Arg0, typename... Args>
        struct registry_impl<Arg0, Args...> {
            std::unordered_map<Arg0, registry_impl<Args...>> m_registry;
            value_type& get_(const Arg0& car, const Args&... cdr)
            {
                /*MSG_DEBUG("size=" << size() << " finding " << car << "...");*/
                return m_registry[car].get_(cdr...);
            }

            bool remove_(const Arg0& car, const Args&... cdr)
            {
                if (m_registry[car].remove_(cdr...)) {
                    m_registry.erase(car);
                }
                return m_registry.size() == 0;
            }

            value_type* find_(const Arg0& car, const Args&... cdr)
            {
                auto it = m_registry.find(car);
                if (it == m_registry.end()) {
                    return NULL;
                }
                return it->second.find_(cdr...);
            }

            size_t size() const
            {
                size_t accum = 0;
                for (auto& kv: m_registry) {
                    accum += kv.second.size();
                }
                return accum;
            }
        };

    registry_impl<AllArgs...> m_registry;

    template <typename... Args>
        value_type& get(const Args&... args)
        {
            return m_registry.get_(args...);
        }

    template <typename... Args>
        void remove(const Args&... args)
        {
            m_registry.remove_(args...);
        }

    template <typename... Args>
        value_type* find(const Args&... args)
        {
            return m_registry.find_(args...);
        }

    size_t size() const { return m_registry.size(); }
};



#endif

