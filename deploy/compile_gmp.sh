#!/bin/bash

PATH_TO_GLIBC_H=$1
OUTPUT_PATH=$2

cd gmp

# fetch GMP
cd gmp-6.1.2 || (curl https://gmplib.org/download/gmp/gmp-6.1.2.tar.xz -o - | xz -d | tar xf - && cd gmp-6.1.2)

# cleanup and configure
if [ -f Makefile ]; then
make clean
else
CFLAGS="-include $PATH_TO_GLIBC_H" ./configure --disable-shared --enable-fat
fi

# build
make

# deploy lib
cp .libs/libgmp.a $OUTPUT_PATH
